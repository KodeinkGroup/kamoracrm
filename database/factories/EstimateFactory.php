<?php

namespace Database\Factories;

use App\Models\Estimate;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class EstimateFactory extends Factory
{

    /**
     * @var string
     */
    protected $model = Estimate::class;


    /**
     * Define the model's default state.
     *
     * @return array
     */

    public function definition()
    {
        return [
            'reference_no' => $this->faker->numerify('####'),
            'prospect_id' => 1,
            'subject' => $this->faker->text(50),
            'terms' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla at sem ante.
            Suspendisse mi orci, vallis in tempus sit amet, lobortis a justo.',
            'discount' => $this->faker->numerify('####'),
            'notes' => $this->faker->paragraph(),
            'estimate_status_id' => 1,
            'created_by' => 1,
            'amount' => $this->faker->numerify('#####'),
            'expiry_date' => $this->faker->dateTimeThisMonth(),
            'created_at' => now()

        ];
    }
}
