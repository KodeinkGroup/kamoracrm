<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->id();
            $table->string('subject');
            $table->decimal('discount', 8, 2)->nullable();
            $table->decimal('amount', 8, 2);
            $table->date('due_date')->nullable();
            $table->text('notes')->nullable();
            $table->text('terms')->nullable();
            $table->unsignedBigInteger('invoice_status_id');
            $table->unsignedBigInteger('estimate_id')->nullable();
            $table->unsignedBigInteger('invoice_type');
            $table->unsignedBigInteger('prospect_id');
            $table->unsignedBigInteger('created_by');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('invoice_item', function (Blueprint $table) {

            $table->foreignId('invoice_id')->constrained()->onDelete('cascade');
            $table->foreignId('item_id')->constrained()->onDelete('cascade');
            $table->integer('quantity');
            $table->decimal('discount')->nullable();


            $table->timestamps();
            $table->softDeletes();

            $table->primary(['invoice_id', 'item_id']);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoice_item');
        Schema::dropIfExists('invoices');
    }
}
