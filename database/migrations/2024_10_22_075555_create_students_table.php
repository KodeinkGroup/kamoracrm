<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function (Blueprint $table) {
            $table->id();
            $table->string('first_name');
            $table->string('last_name');
            $table->date('date_of_birth');
            $table->unsignedBigInteger('classroom_id'); // Relationship to Classroom
            $table->unsignedBigInteger('parent_id')->nullable(); // Relationship to Parent
            $table->timestamps();

//            $table->foreign('classroom_id')->references('id')->on('classrooms')->onDelete('cascade');
//            $table->foreign('parent_id')->references('id')->on('parents')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students');
    }
}
