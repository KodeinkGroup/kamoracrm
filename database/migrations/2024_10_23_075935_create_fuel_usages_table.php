<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFuelUsagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fuel_usages', function (Blueprint $table) {
            $table->id();
            $table->foreignId('vehicle_id')->constrained()->onDelete('cascade'); // Link to vehicle
            $table->foreignId('trip_id')->nullable()->constrained()->onDelete('cascade'); // Optional link to a trip
            $table->float('liters'); // Amount of fuel used
            $table->decimal('cost', 10, 2); // Cost of fuel
            $table->date('date'); // Date of fuel usage
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fuel_usages');
    }
}
