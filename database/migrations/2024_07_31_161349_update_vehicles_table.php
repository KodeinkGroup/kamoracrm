<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('vehicles', function (Blueprint $table) {
            $table->string('status')->change();
            $table->unsignedBigInteger('department_id')->after('status')->nullable();
            $table->string('fuel_efficiency')->after('fuel_type')->nullable();
            $table->string('fuel_tank')->after('fuel_efficiency')->nullable();
            $table->date('next_service_date')->after('last_service_date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vehicles', function (Blueprint $table) {
            $table->dropColumn(['department_id', 'fuel_efficiency', 'fuel_tank', 'next_service_date']);
        });
    }
};
