<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('items', function (Blueprint $table) {
            $table->string('quantity')->after('rate')->nullable();
            $table->string('category')->after('quantity')->nullable();
            $table->string('serial_number')->after('category')->nullable();
            $table->string('brand')->after('serial_number')->nullable();
            $table->string('supplier')->after('brand')->nullable();
            $table->unsignedBigInteger('condition')->after('supplier')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('items', function (Blueprint $table) {
            $table->dropColumn(['quantity','category', 'serial_number', 'brand', 'condition']);
        });
    }
};
