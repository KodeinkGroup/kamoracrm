<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTripsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trips', function (Blueprint $table) {
            $table->id();
            $table->foreignId('vehicle_id')->constrained()->onDelete('cascade'); // Link to vehicle
            $table->foreignId('driver_id')->constrained('employees')->onDelete('cascade'); // Link to employee (as the driver)
            $table->string('start_location');
            $table->string('end_location');
            $table->string('start_odometer_reading');
            $table->string('end_odometer_reading');
            $table->dateTime('start_time');
            $table->dateTime('end_time');
            $table->float('distance')->nullable(); // Optional, distance covered
            $table->text('description')->nullable(); // Trip purpose or additional notes
            $table->foreignId('authorised_by')->nullable()->constrained('users')->onDelete('cascade'); // Link to employee (as the manager)

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trips');
    }
}
