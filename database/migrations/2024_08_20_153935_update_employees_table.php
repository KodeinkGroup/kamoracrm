<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('employees', function (Blueprint $table) {
            $table->string('employee_number')->after('company_id')->nullable();
            $table->string('initials')->after('employee_number')->nullable();
            $table->string('title')->after('initials')->nullable();
            $table->string('full_names')->after('title')->nullable();
            $table->string('surname')->after('full_names')->nullable();
            $table->string('id_type')->after('id_no')->nullable();
            $table->date('date_of_birth')->after('id_type')->nullable();
            $table->string('next_of_kin_name')->after('appointment_date')->nullable();
            $table->string('next_of_kin_mobile_number')->after('next_of_kin_name')->nullable();
            $table->string('next_of_kin_relationship')->after('next_of_kin_mobile_number')->nullable();
            $table->string('zip_code')->after('country')->nullable();
            $table->string('employment_type')->after('position')->nullable();
            $table->string('available_leave_days')->after('salary')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('employees', function (Blueprint $table) {
            //
        });
    }
};
