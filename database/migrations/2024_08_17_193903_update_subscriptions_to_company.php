<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('subscriptions', function (Blueprint $table) {
            $table->renameColumn('user_id', 'company_id');
            $table->smallInteger('trial_active')->after('subscription_status_id')->default(1);
            $table->dateTime('trial_start_date')->after('trial_active')->nullable();
            $table->dateTime('trial_end_date')->after('trial_start_date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('subscriptions', function (Blueprint $table) {
            $table->renameColumn('company_id', 'user_id');
            $table->dropColumn(['trial_active', 'trial_start_date', 'trial_end_date']);
        });
    }
};
