<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Database\Seeders\UsersTableSeeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
      $this->call(UsersTableSeeder::class);
      $this->call(LeadSourcesSeeder::class);
      $this->call(ProspectTypesSeeder::class);
      $this->call(CompanySeeder::class);
      $this->call(PermissionsSeeder::class);
//      $this->call(RolesSeeder::class);
      $this->call(EstimateStatusesSeeder::class);
//      $this->call(Estimate::class);
    }
}
