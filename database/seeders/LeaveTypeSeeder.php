<?php

namespace Database\Seeders;

use App\Models\LeaveType;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class LeaveTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('leave_types')->truncate();
        $leaveTypes = [
            ['name' => 'Annual Leave', 'description' => '', 'accrual_rate' => 1.25, 'days_per_year' => 21, 'carried_over' => true],
            ['name' => 'Sick Leave', 'description' => '', 'accrual_rate' => 0,  'days_per_year' => 10, 'carried_over' => false],
            ['name' => 'Maternity Leave', 'description' => '', 'accrual_rate' => 0,  'days_per_year' => 90, 'carried_over' => false],
            ['name' => 'Paternity Leave', 'description' => '', 'accrual_rate' => 0, 'days_per_year' => 10, 'carried_over' => false],
            ['name' => 'Unpaid Leave', 'description' => '', 'accrual_rate' => 0, 'days_per_year' => 0, 'carried_over' => false],
        ];

        foreach ($leaveTypes as $type) {
            LeaveType::create($type);
        }
    }
}
