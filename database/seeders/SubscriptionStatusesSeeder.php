<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SubscriptionStatusesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('subscription_statuses')->truncate();

        DB::table('subscription_statuses')->insert(['name' => 'Active',
            'description' => 'The subscription is currently active, and the user has access to the services or products.
The user is being billed according to the subscription plan.']);
        DB::table('subscription_statuses')->insert(['name' => 'Inactive',
            'description' => 'The subscription is not active, and the user does not have access to the services or products.
This status can occur if the user manually pauses the subscription or if there is an issue with payment.']);
        DB::table('subscription_statuses')->insert(['name' => 'Pending',
            'description' => 'The subscription has been created but is waiting for a specific action to be completed, such as payment confirmation or manual activation.
The user may not yet have access to the services.']);
        DB::table('subscription_statuses')->insert(['name' => 'Trial',
            'description' => 'The subscription is in a trial period where the user can access the services for free or at a reduced rate.
Once the trial period ends, the subscription will typically move to the active status, provided the user continues.']);

        DB::table('subscription_statuses')->insert(['name' => 'Cancelled',
            'description' => 'The subscription has been cancelled by the user or by the system due to non-payment, fraud, or other reasons.
The user no longer has access to the services, and no further billing will occur.']);
        DB::table('subscription_statuses')->insert(['name' => 'Expired',
            'description' => 'The subscription period has ended, and the user has not renewed it.
The user no longer has access to the services, but the subscription could potentially be renewed.']);
        DB::table('subscription_statuses')->insert(['name' => 'Suspended',
            'description' => 'The subscription is temporarily on hold, often due to issues like failed payment, fraud detection, or administrative action.
The user does not have access to services until the suspension is lifted']);
       DB::table('subscription_statuses')->insert(['name' => 'Renewal Due',
            'description' => 'The subscription is nearing the end of its current period, and action may be required to renew it.
The system may notify the user that their subscription is about to renew or expire.']);
        DB::table('subscription_statuses')->insert(['name' => 'Grace Period',
            'description' => 'After a failed payment or before an upcoming renewal, some systems offer a grace period where the subscription remains active,
             but the user is notified that they need to take action to avoid cancellation or suspension.']);
        DB::table('subscription_statuses')->insert(['name' => 'Paused',
            'description' => 'The subscription has been paused by the user, often for a temporary period.
Billing may be paused, and access to services is temporarily suspended until the user resumes the subscription']);
    }
}
