<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TaskStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('task_status')->truncate();
        DB::table('task_status')->insert(['name' => 'To Do', 'color' => 'default']);
        DB::table('task_status')->insert(['name' => 'In Progress', 'color' => 'primary']);
        DB::table('task_status')->insert(['name' => 'In Review', 'color' => 'warning']);
        DB::table('task_status')->insert(['name' => 'Done', 'color' => 'success']);
        DB::table('task_status')->insert(['name' => 'Overdue', 'color' => 'danger']);
    }
}
