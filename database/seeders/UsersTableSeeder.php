<?php

namespace Database\Seeders;

use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::updateOrCreate(['id' =>1], [
            'name' => 'Admin',
            'email' => 'joseleh@live.com',
            'role'  =>  'master',
            'password'  => Hash::make('Password'), // Password
        ]);

//        $user->company()->attach(1, ['company_id' => 1,'active' => 1]);



//        Role::create(['id' => 1], [
//            'name' => 'master',
//            'guard_name' => 'api'
//
//        ]);
//
//        $masterRole = Role::find(1);
//
//        if($masterRole)
//            $user->assignRole('master');


    }
}
