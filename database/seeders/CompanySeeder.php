<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Company;

class CompanySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Company::updateOrCreate(['id' => 1], [
            'name' => 'Kodeink Group',
            'size' => 'Startup',
            'user_id' => 1,
            'industry_id' => 1
        ]);
    }
}
