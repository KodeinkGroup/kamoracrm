<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class InvoiceStatusesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('invoice_status')->truncate();

        DB::table('invoice_status')->insert(['name' => 'Draft']);
        DB::table('invoice_status')->insert(['name' => 'New']);
        DB::table('invoice_status')->insert(['name' => 'Sent']);
        DB::table('invoice_status')->insert(['name' => 'Paid']);
        DB::table('invoice_status')->insert(['name' => 'Partly Paid']);
        DB::table('invoice_status')->insert(['name' => 'Overdue']);
        DB::table('invoice_status')->insert(['name' => 'Cancelled']);
        DB::table('invoice_status')->insert(['name' => 'Deleted']);

    }
}
