<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TaskPrioritiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('task_priorities')->truncate();
        DB::table('task_priorities')->insert(['name' => 'Low', 'color' => 'primary']);
        DB::table('task_priorities')->insert(['name' => 'Medium', 'color' => 'warning']);
        DB::table('task_priorities')->insert(['name' => 'High', 'color' => 'danger']);
    }
}
