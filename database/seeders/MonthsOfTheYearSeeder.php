<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MonthsOfTheYearSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('months_of_the_year')->truncate();
        DB::table('months_of_the_year')->insert(['name' => 'January']);
        DB::table('months_of_the_year')->insert(['name' => 'February']);
        DB::table('months_of_the_year')->insert(['name' => 'March']);
        DB::table('months_of_the_year')->insert(['name' => 'April']);
        DB::table('months_of_the_year')->insert(['name' => 'May']);
        DB::table('months_of_the_year')->insert(['name' => 'June']);
        DB::table('months_of_the_year')->insert(['name' => 'July']);
        DB::table('months_of_the_year')->insert(['name' => 'August']);
        DB::table('months_of_the_year')->insert(['name' => 'September']);
        DB::table('months_of_the_year')->insert(['name' => 'October']);
        DB::table('months_of_the_year')->insert(['name' => 'November']);
        DB::table('months_of_the_year')->insert(['name' => 'December']);
    }
}
