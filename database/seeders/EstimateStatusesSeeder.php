<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EstimateStatusesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('estimate_status')->truncate();

        DB::table('estimate_status')->insert(['name' => 'Draft', 'color' => 'default']);
        DB::table('estimate_status')->insert(['name' => 'New', 'color' => 'primary']);
        DB::table('estimate_status')->insert(['name' => 'Sent', 'color' => 'primary']);
        DB::table('estimate_status')->insert(['name' => 'Accepted', 'color' => 'success']);
        DB::table('estimate_status')->insert(['name' => 'Converted', 'color' => 'success']);
        DB::table('estimate_status')->insert(['name' => 'Rejected', 'color' => 'warning']);
        DB::table('estimate_status')->insert(['name' => 'Deleted', 'color' => 'danger']);
    }
}
