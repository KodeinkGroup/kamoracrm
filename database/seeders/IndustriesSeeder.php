<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class IndustriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('industries')->truncate();

        DB::table('industries')->insert(['name' => 'Software Development']);
        DB::table('industries')->insert(['name' => 'IT Services']);
        DB::table('industries')->insert(['name' => 'Digital Marketing']);
        DB::table('industries')->insert(['name' => 'Civil Engineering']);
        DB::table('industries')->insert(['name' => 'Sales and Marketing']);
        DB::table('industries')->insert(['name' => 'Automotive']);
        DB::table('industries')->insert(['name' => 'Bakery']);
        DB::table('industries')->insert(['name' => 'Food and Beverages']);
        DB::table('industries')->insert(['name' => 'Mining and Minerals']);
        DB::table('industries')->insert(['name' => 'Agriculture']);
        DB::table('industries')->insert(['name' => 'Manufacturing']);
        DB::table('industries')->insert(['name' => 'Cosmetics and Beauty']);
        DB::table('industries')->insert(['name' => 'Finance']);
        DB::table('industries')->insert(['name' => 'Retail and Wholesale']);
        DB::table('industries')->insert(['name' => 'Tourism and Hospitality']);
        DB::table('industries')->insert(['name' => 'Construction']);
        DB::table('industries')->insert(['name' => 'Real Estate']);
        DB::table('industries')->insert(['name' => 'Energy and Utilities']);
        DB::table('industries')->insert(['name' => 'Transport and Logistics']);
        DB::table('industries')->insert(['name' => 'Healthcare and Pharmaceuticals']);
        DB::table('industries')->insert(['name' => 'Education and Training']);
        DB::table('industries')->insert(['name' => 'Media and Entertainment']);
        DB::table('industries')->insert(['name' => 'Textiles and Apparel']);
        DB::table('industries')->insert(['name' => 'Chemical Industry']);
    }
}
