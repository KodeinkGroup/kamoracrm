<?php

namespace Database\Seeders;

use App\Models\Role;
use Illuminate\Database\Seeder;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = Role::updateOrCreate(['id' => 1], [
                'name' => 'master',
                'guard_name' => 'api'
            ]
        );

        $role->givePermission('estimates.admin');
    }
}
