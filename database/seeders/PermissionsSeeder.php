<?php

namespace Database\Seeders;

use App\Models\Permission;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Temporarily disable foreign key checks
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        // Truncate the table
        DB::table('permissions')->truncate();

        // Re-enable foreign key checks
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        // Insert new permissions
        $permissions = [
            ['name' => 'projects.create', 'guard_name' => 'web'],
            ['name' => 'projects.view', 'guard_name' => 'web'],
            ['name' => 'projects.update', 'guard_name' => 'web'],
            ['name' => 'projects.delete', 'guard_name' => 'web'],

            ['name' => 'tasks.create', 'guard_name' => 'web'],
            ['name' => 'tasks.view', 'guard_name' => 'web'],
            ['name' => 'tasks.update', 'guard_name' => 'web'],
            ['name' => 'tasks.delete', 'guard_name' => 'web'],
            ['name' => 'tasks.assign', 'guard_name' => 'web'],
            // Add more permissions here
        ];
        // Insert only if the permission doesn't already exist
        foreach ($permissions as $permission) {
            if (!DB::table('permissions')->where('name', $permission['name'])->exists()) {
                DB::table('permissions')->insert($permission);
            }
        }
    }
}
