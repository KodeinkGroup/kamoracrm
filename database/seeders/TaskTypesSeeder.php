<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TaskTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('task_types')->truncate();
        DB::table('task_types')->insert(['name' => 'Task', 'color' => 'primary']);
        DB::table('task_types')->insert(['name' => 'Bug', 'color' => 'danger']);
        DB::table('task_types')->insert(['name' => 'Incident', 'color' => 'warning']);
        DB::table('task_types')->insert(['name' => 'Story', 'color' => 'success']);
    }
}
