<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class LeadSourcesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('lead_sources')->delete();
        DB::table('lead_sources')->insert([ 'name' => 'Facebook']);
        DB::table('lead_sources')->insert([ 'name' => 'Instagram']);
        DB::table('lead_sources')->insert([ 'name' => 'Twitter']);
        DB::table('lead_sources')->insert([ 'name' => 'LinkedIn']);
        DB::table('lead_sources')->insert([ 'name' => 'WhatsApp']);
        DB::table('lead_sources')->insert([ 'name' => 'Website']);
        DB::table('lead_sources')->insert([ 'name' => 'Referral']);
        DB::table('lead_sources')->insert([ 'name' => 'Other']);
    }
}
