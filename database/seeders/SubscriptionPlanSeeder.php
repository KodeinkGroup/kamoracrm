<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SubscriptionPlanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('subscription_plans')->truncate();

        DB::table('subscription_plans')->insert(['name' => 'Basic', 'price' => '29', 'duration' => '365',
            'features' => 'Account, Lead, and Opportunity Management, Sales Pipeline, Estimates/Quotes Management, Tasks Management']);
        DB::table('subscription_plans')->insert(['name' => 'Standard', 'price' => '99', 'duration' => '365',
            'features' => 'Account, Lead, and Opportunity Management, Estimates/Quotes Management, Invoice Management, Tasks Management']);
        DB::table('subscription_plans')->insert(['name' => 'Premium', 'price' => '499', 'duration' => '365',
            'features' => 'Account, Lead, and Opportunity Management, Financial Management, Project Management, Reporting Management, Automated Email Logging']);
    }
}
