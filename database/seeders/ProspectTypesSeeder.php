<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProspectTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('prospect_types')->delete();
        DB::table('prospect_types')->insert(['name' => 'Lead']);
        DB::table('prospect_types')->insert(['name' => 'Prospect']);
        DB::table('prospect_types')->insert(['name' => 'Account']);
    }
}
