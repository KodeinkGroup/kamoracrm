<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Invoice;
use App\Jobs\ProcessInvoices;
use Illuminate\Support\Facades\Log;

class UpdateInvoicesStatus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'invoices:process';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        Log::channel('invoices')->info("Processing Invoices::start");

        $invoices = Invoice::whereNotIn('invoice_status_id', [1,4,6,7,8])
            ->get();
        if(!empty($invoices)){
            ProcessInvoices::dispatch($invoices);
        } else {
            Log::channel('invoices')->info("There are no Invoices to process");
        }

        Log::channel('invoices')->info("Processing Invoices::end");

    }
}
