<?php

namespace App\Console\Commands;

use App\Jobs\ProcessEmployeesBirthdays;
use App\Jobs\ProcessEmployeesLeaveDays;
use App\Models\Employee;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use function PHPUnit\Framework\isNull;

class ProcessEmployees extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'employees:process';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Process employees to update employees birthdays, leave days and other functions';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $today = Carbon::today();

        Log::channel('employees')->info(__METHOD__. 'Start');

        $employeesWithBirthdaysToday = Employee::whereDay('date_of_birth', $today->day)
            ->whereMonth('date_of_birth', $today->month)
            ->get();

        Log::channel('employees')->info($employeesWithBirthdaysToday);

        if($employeesWithBirthdaysToday->isNotEmpty()){
            Log::channel('employees')->info("Employees with Birthdays");
            Log::channel('employees')->info($employeesWithBirthdaysToday);

            dispatch(new ProcessEmployeesBirthdays($employeesWithBirthdaysToday));

        } else {
            Log::channel('employees')->info("There are no Employees with Birthdays today!");
        }

        Log::channel('employees')->info("Continuing with Leave now");

        $employeesAppointedToday = Employee::with('leaveType')
            ->whereDay('appointment_date', $today->day)
            ->whereMonth('appointment_date', $today->month)
            ->where('employment_type', 'permanent')
            ->get();

        if($employeesAppointedToday->isNotEmpty()){
            Log::channel('employees')->info("Processing Leave Days for Employees appointed on this {$today->day} day");
            dispatch(new ProcessEmployeesLeaveDays($employeesAppointedToday));
        } else {
            Log::channel('employees')->info("No one was appointed today");
        }

        Log::channel('employees')->info(__METHOD__. 'End');
    }
}
