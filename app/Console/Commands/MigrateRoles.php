<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Spatie\Permission\Models\Role;
use App\Models\User;


class MigrateRoles extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'roles:migrate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Migrating existing User Roles into a defined Roles table';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        // Step 1: Extract unique roles from users table
        $roles = User::distinct()->pluck('role');

        // Step 2: Create roles in Spatie's table
        foreach ($roles as $roleName) {
            Role::firstOrCreate(['name' => $roleName]);
        }

        // Step 3: Assign roles to users
        User::all()->each(function ($user) {
            if ($user->role) {
                $user->assignRole($user->role);
            }
        });

        Log::channel('users')->info('Roles migrated and assigned successfully!');
    }
}
