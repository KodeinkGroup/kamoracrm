<?php

namespace App\Console\Commands;

use App\Jobs\ProcessRecurringInvoices;
use App\Models\Invoice;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;

class ProcessInvoices extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'invoices:processRecurring';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        Log::channel('invoices')->info("Processing Recurring Invoices::start");
        $today = Carbon::today();

        // Get all recurring invoices
        $invoices = Invoice::where('invoice_type', 2)
//            ->whereNot('status', 1) // Not currently running or stuck on 1
            ->whereNotIn('invoice_status_id', [1, 7, 8]) // Draft, Cancelled, Deleted
            ->get();
        // check frequency type
            foreach($invoices as $invoice) {
                Log::channel('invoices')->info("Processing recurring Invoice #{$invoice->id}");
                // check run_from
                if($invoice->run_from !== null){
                    Log::channel('invoices')->info("Invoice #{$invoice->id} has run_from set to {$invoice->run_from}");

                    if($invoice->next_run !== null) {
                        Log::channel('invoices')->info("Invoice #{$invoice->id} has next_run set to {$invoice->next_run}");

                        if($invoice->next_run == $today){
                            Log::channel('invoices')->info("Invoice #{$invoice->id} is set to run today");
                           // if new run_from day is today send for processing
                                 ProcessRecurringInvoices::dispatch($invoice, true);
                        } else {
                            Log::channel('invoices')->info("Invoice #{$invoice->id} is not set to run today, next_run is on {$invoice->next_run}");
                            if($invoice->next_run > $today) {
                                Log::channel('invoices')->info("Invoice #{$invoice->id} next_run is in future");
                            } else {
                                Log::channel('invoices')->info("Invoice #{$invoice->id} next_run has passed");

                                ProcessRecurringInvoices::dispatch($invoice, false);
                            }
                        }
                    } else {
                        Log::channel('invoices')->info("Invoice #{$invoice->id} has no set next_run date");
                        // processing but not sending email
                        ProcessRecurringInvoices::dispatch($invoice, false);
                    }
                } else {
                    // if run_from available
                    Log::channel('invoices')->info("Invoice #{$invoice->id} has no run_from setting to created_at which is {$invoice->created_at}");
                    // use created_at to update run from
                    $run_from = $invoice->created_at;
                    $run_until = $run_from ->addMonths(11);

                    if($invoice->created_at->day === $today->day ){
                        Log::channel('invoices')->info("Invoice #{$invoice->id} had no run_from, now setting it to created_at {$invoice->created_at} which is today in days");
                        // if new run_from day is today send for processing
                         ProcessRecurringInvoices::dispatch($invoice, true);
                    }

                    $invoice->update(['run_from'=>$run_from, 'run_until' => $run_until]);
                }
            }
        Log::channel('invoices')->info("Retrieved Invoices {$invoices->count()} to process");

//        $invoices = Invoice::where('invoice_type', 2)
//            ->whereDay('next_run', $today->day)
//            ->whereMonth('next_run', $today->month)
//            ->whereYear('next_run', $today->year)
//            ->whereNotIn('status', [1]) // Not running
//            ->whereNotIn('invoice_status_id', [1, 7, 8]) //
//            ->get();
        Log::channel('invoices')->info("Processing Monthly Invoices::end");
    }
}
