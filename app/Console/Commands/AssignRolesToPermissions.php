<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class AssignRolesToPermissions extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'roles:assign-permissions';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Assign roles to permissions without duplication or override';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        // Define roles and their permissions
        $rolesPermissions = [
            'master' => ['projects.create', 'projects.view', 'projects.update', 'projects.delete', 'tasks.create', 'tasks.view',
                'tasks.update', 'tasks.assign', 'tasks.delete','estimates.create', 'estimates.view', 'estimates.update', 'estimates.delete'],
            'project_manager' => ['projects.view', 'tasks.create', 'tasks.view','tasks.update', 'tasks.assign', 'tasks.delete'],
            'technician' => ['projects.view', 'tasks.create', 'tasks.view','tasks.update'],
            'admin' => ['projects.create', 'projects.view', 'projects.update', 'projects.delete', 'tasks.create', 'tasks.view',
                'tasks.update', 'tasks.assign', 'tasks.delete'],
        ];

        foreach ($rolesPermissions as $roleName => $permissions) {
            // Get or create the role
            $role = Role::firstOrCreate(['name' => $roleName, 'guard_name' => 'web']);

            foreach ($permissions as $permissionName) {
                // Get or create the permission
                $permission = Permission::firstOrCreate(['name' => $permissionName, 'guard_name' => 'web']);

                // Assign permission to the role if not already assigned
                if (!$role->hasPermissionTo($permission)) {
                    $role->givePermissionTo($permission);
                    $this->info("Assigned permission '{$permissionName}' to role '{$roleName}'.");
                } else {
                    $this->line("Permission '{$permissionName}' already assigned to role '{$roleName}'. Skipping.");
                }
            }
        }

        Log::channel('users')->info('Roles and permissions assigned successfully!');
    }
}
