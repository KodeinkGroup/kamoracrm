<?php

namespace App\Console\Commands;

use App\Models\Company;
use App\Models\Employee;
use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class AssignEmployees extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'company:syncEmployees';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Assign Employees to their companies';

    /**
     * Execute the console command.
     *
     * @return int
     */

    public function handle()
    {
        $companies = Company::get();
        Log::channel('companies')->info("Assigning Employees to companies");

        foreach ($companies as $company) {
            Log::channel('companies')->info("Assigning Employees to company #{$company->id}");
            $users = User::where('company_id', $company->id)->get();

            foreach ($users as $user) {
                // Check if an Employee record already exists for this User
                $employeeExists = Employee::where('user_id', $user->id)->exists();

                if (!$employeeExists) {
                    $employee = new Employee;
                    $employee->user_id = $user->id;
                    $employee->full_names = $user->name;
                    $employee->company_id = $user->company_id;
                    $employee->position = $user->role;
                    $employee->id_no = $user->id;

                    $employee->save();

                    Log::channel('companies')->info("Employee created for user #{$user->id}");
                } else {
                    Log::channel('companies')->info("Employee already exists for user #{$user->id}");
                }
            }
        }
    }

}
