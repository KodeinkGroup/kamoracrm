<?php

namespace App\Console\Commands;

use App\Models\Company;
use App\Models\Subscription;
use App\Notifications\TrialActivated;
use Carbon\Carbon;
use http\Client\Curl\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class ProcessSubscriptions extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'subscriptions:process';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Subscriptions data';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        Log::channel('subscriptions')->info("ProcessSubscriptions::init");


        $companiesWithoutSubscriptions = Company::doesntHave('subscription')->get();

//        Log::channel('subscriptions')->info("Retrieving all companies without subscriptions");
//        Log::channel('subscriptions')->debug($companiesWithoutSubscriptions);

        foreach ($companiesWithoutSubscriptions as $c) {
            Log::channel('subscriptions')->info("Company #{$c->id} hasn't subscribed");

            $today = Carbon::now();

            Log::channel('subscriptions')->info("Intending to start Company #{$c->id}'s subscription from{$today}");

            $subscription = new Subscription;
            $subscription->trial_active = 1;
            $subscription->trial_start_date = $today;
            $subscription->trial_end_date = $today->copy()->addDays(30);
            $subscription->subscription_status_id = 4;
            $subscription->start_date = $today->copy()->addDays(45);
            $subscription->end_date = $today->copy()->addYear();
            $subscription->company_id = $c->id;
            $subscription->subscription_plan_id = 1;

            $subscription->save();

//            unset($subscription);
        }

//        die;
        // get all subscriptions
        $subscriptions = Subscription::get();
        $trial_subscriptions = [];

        Log::channel('subscriptions')->info($subscriptions);
        foreach( $subscriptions as $subscription) {
            // check subscription days
            // if subscription status equals 4
            if($subscription->subscription_status_id = 4 && is_null($subscription->trial_start_date)) {

                // first subscription is 30 days
                $trialEndDate = Carbon::parse($subscription->trial_start_date)->addDays(30);
                $daysLeft = Carbon::now()->diffInDays($trialEndDate, false);

                Log::channel('subscriptions')->info("Subscription #{$subscription->id} is on trial ending on {$trialEndDate} and left with {$daysLeft} days");
//                $subscription->user->notify(new TrialActivated($subscription));
                // 14 days before send notifications to purchase subscription
                if($daysLeft === 14) {
                    // Notifiy KAM admin
                    // Notify client
                    Log::channel('subscriptions')->info("Subscription #{$subscription->id} is on trial ending on {$trialEndDate} and has {$daysLeft} days");
                }
            }
        }
            // if user is not purchasing subscription switch off 15 days later
            // second subscription is 275 days
    }
}
