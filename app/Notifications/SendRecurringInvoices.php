<?php

namespace App\Notifications;

use App\Models\Invoice;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\HtmlString;

class SendRecurringInvoices extends Notification
{
    use Queueable;

    private Invoice $invoice;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Invoice $invoice)
    {
        $this->invoice = $invoice;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject("Monthly Invoice for {$this->invoice->suject}")
            ->greeting(new HtmlString("Hi <strong>{$this->invoice->prospect->name}</strong>"))
            ->line(new HtmlString( "<p>This is a notification that an invoice has been generated. </p>"))
            ->line("Invoice #{$this->invoice->id}")
            ->line("Subject #{$this->invoice->subject}")
            ->line("Amount #{$this->invoice->amount}")
            ->line("Due Date #{$this->invoice->due_date}")
            ->line("Please use your company name as Reference.")
//            ->line(new HtmlString("<p><br><strong>Invoice Items</strong></p>"))
            //->line(" #{$this->invoice->due_date}")

//                  ->action('View Invoice', url('http://localhost:8080/api/invoices/export/46'))
            ->line('Thank you for business!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
