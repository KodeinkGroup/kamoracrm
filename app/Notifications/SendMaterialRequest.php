<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class SendMaterialRequest extends Notification
{
    use Queueable;

    protected $data;
    protected $materialRequest;
    protected $material;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($materialRequest, $material)
    {
        $this->materialRequest = $materialRequest;
        $this->material = $material;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->subject("{$this->materialRequest->project->name} - New Material Request")
                    ->view(
                        'admin.emails.notifications.material-request',
                        ['data' => $this->materialRequest]
                    );
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
