<?php

namespace App\Notifications;

use App\Models\Invoice;
use App\Models\Prospect;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\HtmlString;

class SendOverdueInvoicesEmail extends Notification
{
    use Queueable;

    private Invoice $invoice;
    private Prospect $customer;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Invoice $invoice)
    {
        $this->invoice = $invoice;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->subject("Invoice #{$this->invoice->id} Overdue")
                    ->greeting(new HtmlString("Hi <strong>{$this->invoice->prospect->name}</strong>"))
                    ->line(new HtmlString( "<p>We hope you are good. This is just A friendly reminder that your invoice #{$this->invoice->id} is overdue and awaiting payment. </p>"))
//                  ->action('View Invoice', url('http://localhost:8080/api/invoices/export/46'))
                    ->line('Thank you for business!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
