<?php

namespace App\Services;

use Facebook\Facebook;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Http;


class FacebookService
{
    protected $facebook;

    public function __construct()
    {
        $this->facebook = new Facebook([
            'app_id' => env('FACEBOOK_APP_ID'),
            'app_secret' => env('FACEBOOK_APP_SECRET'),
            'default_graph_version' => 'v20.0',
        ]);

        Log::channel('dashboard')->info("Facebook Service");
//        Log::channel('dashboard')->info((array)$this->facebook);
    }

    public function getUser()
    {
        Log::channel('dashboard')->info("Facebook User");
        $response = Http::get("https://graph.facebook.com/v20.0/me?fields=id,name&access_token=EAAa3i5ZAfkZA8BOZCLsfp2bZBMRM4Wow6Jdr5gRYxUZBTvXfhKBG1qiGeXAbCtw3gTBHgw5SGCZBZCvZAIeflKMdlVQvH6WR0s0mbRwsYN4eToMHfbbZC4mhcSZAvJe8I37kd2Iq6LYZAvL31r0UmDUmmJsHI4NaB74Vq9cG2TDtXMrK8o2ZAK7mhSSlZCDEibtDi8Imo991YZA2QGaublhR2aSLwqj8tlQ71uPl9DqqZC0KA5p50pK2IDkJ4rRSQJHcvKPPwZDZD" // Replace with your actual token
        );

        Log::channel('dashboard')->info((array)$response);

        // Check if the request was successful
        if ($response->successful()) {
            Log::channel('dashboard')->info("Succeeded");

            // Get the response body as an array
            $data = $response->json();

            // Access the id and name
            $id = $data['id'];
            $name = $data['name'];

            // Do something with the data
            return response()->json(['id' => $id, 'name' => $name]);
        } else {
            // Handle the error
            Log::channel('dashboard')->info("Failed");
            return response()->json(['error' => 'Failed to fetch data from Facebook'], 500);
        }

        Log::channel('dashboard')->info("Facebook User");
//        try {
//            $response = $this->facebook->get("https://graph.facebook.com/v20.0/me?fields=id,name", env('FACEBOOK_ACCESS_TOKEN'));
//            Log::channel('dashboard')->info((array)$response);
//        }  catch (\Facebook\Exceptions\FacebookResponseException $e) {
//            // Handle the error
//            return 'Graph returned an error: ' . $e->getMessage();
//        } catch (\Facebook\Exceptions\FacebookSDKException $e) {
//            // Handle the error
//            return 'Facebook SDK returned an error: ' . $e->getMessage();
//        }

        Log::channel('dashboard')->info("Facebook User end");
//        return $response->getGraphUser();
    }

    public function getPageLikes($pageId)
    {
        try {
            $response = $this->facebook->get("/{$pageId}?fields=fan_count", env('FACEBOOK_ACCESS_TOKEN'));
            return $response->getGraphNode()['fan_count'];
        } catch (\Facebook\Exceptions\FacebookResponseException $e) {
            // Handle the error
            return 'Graph returned an error: ' . $e->getMessage();
        } catch (\Facebook\Exceptions\FacebookSDKException $e) {
            // Handle the error
            return 'Facebook SDK returned an error: ' . $e->getMessage();
        }
    }

    public function getPageContentInteractions($pageId)
    {
        try {
            $response = $this->facebook->get("/{$pageId}/insights/page_engaged_users", env('FACEBOOK_ACCESS_TOKEN'));
            return $response->getGraphEdge()->asArray();
        } catch (\Facebook\Exceptions\FacebookResponseException $e) {
            // Handle the error
            return 'Graph returned an error: ' . $e->getMessage();
        } catch (\Facebook\Exceptions\FacebookSDKException $e) {
            // Handle the error
            return 'Facebook SDK returned an error: ' . $e->getMessage();
        }
    }
}
