<?php

namespace App\Services;

use Abraham\TwitterOAuth\TwitterOAuth;
use Illuminate\Support\Facades\Log;

class TwitterService
{
    protected $connection;

    public function __construct()
    {
        Log::channel('dashboard')->info("Twitter Service::Init");
        $this->connection = new TwitterOAuth(
            env('TWITTER_API_KEY'),
            env('TWITTER_API_SECRET_KEY'),
            null,
            env('AAAAAAAAAAAAAAAAAAAAAFq0vgEAAAAA4TI%2FQyopNa53o2%2BbVWAjdTka7MI%3DAD0Ns4foWKdyB1mBFvgTqYJgMaNuFtrIWjQDy2beGgszEgBpjM
')
        );
        // Log::channel('dashboard')->info((array)$this->connection);

        Log::channel('dashboard')->info("Twitter Service::End");

    }

    public function getUserTimeline($username, $count = 5)
    {
        return $this->connection->get("statuses/user_timeline", [
            "screen_name" => $username,
            "count" => $count
        ]);
    }
}

