<?php

namespace App\Providers;

use App\Models\Alert;
use Illuminate\Routing\UrlGenerator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\ServiceProvider;
use ConsoleTVs\Charts\Registrar as Charts;
use Illuminate\Support\Facades\View;

class AppServiceProvider extends ServiceProvider
{
    public $alerts;
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot( UrlGenerator $url)
    {
        //compose all the views....
        view()->composer('*', function ($view)
        {
            if(Auth::user()) {
                $alerts = Alert::where('addressed_to', Auth::user()->id)
                    ->where('status_id', 1)
                    ->orderBy('created_at', 'desc')
                    ->get();

                Log::channel('alerts')->info("Global Alerts");
                Log::channel('alerts')->info($alerts);

                $view->with('alerts', $alerts );
            }
        });

        if(env('REDIRECT_HTTPS') || config('app.env') === 'production') {
            $url->forceScheme('https');
        }

    }
}
