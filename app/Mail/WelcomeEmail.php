<?php

namespace App\Mail;

use App\Models\Company;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class WelcomeEmail extends Mailable
{
    use Queueable, SerializesModels;

    protected $user;
    protected $template;


    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user)
    {
        $this->user = $user;

        if($user->role === 'admin' && $user->company_id === null){
            $this->template = 'admin.emails.user.welcome';
        } else if($user->role === 'admin' && $user->company_id !== null){
            $this->template = 'admin.emails.company.welcome';
        } else {
            $this->template = 'admin.emails.users.new-user';
        }

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('support@kodeinkgroup.co.za', 'Kamora Software')
            ->subject('Welcome to Kamora Software')
            ->view($this->template, ['data' => $this->user])
            ->with([
                'name' => $this->user->name,
                'email' => $this->user->email,
                'resetLink' => $this->user->role !== 'admin' && $this->user->company_id !== null ?
                    route('password.reset', ['token' => app('auth.password.broker')->createToken($this->user)]) : ''
            ]);
    }
}
