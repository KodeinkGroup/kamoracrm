<?php

namespace App\Jobs;

use App\Models\Prospect;
use App\Models\ProspectActivity;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;

class AddContactActivity implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private Prospect $prospect;
    private ProspectActivity $activity;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Prospect $prospect)
    {
        $this->prospect = $prospect;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        \Log::info('Add Activity Job called');
        ProspectActivity::create([
            'prospect_id' => $this->prospect->id,
            'communication_type' => 'automated',
            'communication_channel' => 'automation',
            'type' => 'added_to_system',
            'notes' => "Added {$this->prospect->name} as a Contact"
        ]);
    }
}
