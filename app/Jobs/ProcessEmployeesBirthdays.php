<?php

namespace App\Jobs;

use App\Models\Alert;
use App\Models\Employee;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class ProcessEmployeesBirthdays implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $employees;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($employees)
    {
        Log::channel('employees')->info("Called Processor::construct");
        $this->employees = $employees;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Log::channel('employees')->info("Called Processor::handle");
        foreach ($this->employees as $employee) {
            Log::channel('employees')->info("Processing Employee #{$employee->id}");

            $companyUsers = Employee::where('company_id', $employee->company_id)->get();
            Log::channel('employees')->info("Company Employee");
            Log::channel('employees')->info($companyUsers);

            foreach($companyUsers as $user) {
                Log::channel('employees')->info("Company Employee");

                Log::channel('employees')->info("Creating Birthday Alert");
                $alert = new Alert;

                $alert->addressed_to = $user->id;
                $alert->status_id = 1;
                $alert->created_by = $employee->id;

                $employee->alerts()->save($alert);
                Log::channel('employees')->info("Done creating Birthday Alert");
            }
        }
    }
}
