<?php

namespace App\Jobs;

use App\Models\Prospect;
use App\Notifications\SendRecurringInvoices;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class ProcessRecurringInvoices implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $invoice;
    private $sendNotification;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($invoice, $sendNotification)
    {
        $this->invoice = $invoice;
        $this->sendNotification = $sendNotification;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Log::channel('invoices')->info("Handling Recurring Invoices");

//        foreach($this->invoices as $invoice){
            $now = Carbon::today();
            // Format the Carbon instance to "YYYY-MM-DD" format
            $formattedDate = $now->format('Y-m-d');
            $last_run = Carbon::now();
//            $next_run = $formattedDate;

            Log::channel('invoices')->info("ProcessRecurringInvoices::Processing Recurring Invoice #{$this->invoice->id}");

            $this->invoice->update(['status' => 1, 'last_run' => $last_run]);
            Log::channel('invoices')->info("Updated Recurring Invoice #{$this->invoice->id} status: {$this->invoice->status} and last run updated to {$this->invoice->last_run}");

            // Get User
            $user = Prospect::where('id', $this->invoice->prospect_id)->first();

            Log::channel('invoices')->info("Retrieved user #{$user->email} to send notification to");

            try {
                Log::channel('invoices')->info("Sending Email for Invoice #{$this->invoice->id}" );

                // Send Notification to User
                $this->sendNotification === true ?? $user->notify(new SendRecurringInvoices($this->invoice));
                Log::channel('invoices')->info("Successfully Sent Email for Invoice #{$this->invoice->id}");

                // Update the next run depending on the invoice frequency

                // update next_run -> today plus 30 days if monthly or 365 if annual and so forth if weekly
                if($this->invoice->frequency === 'monthly') {
                    $next_time = Carbon::today()->addMonth();
                    Log::channel('invoices')->info("Invoice #{$this->invoice->id} is monthly, updating next_run to{$next_time}");
                } else if ($this->invoice->frequency === 'annually') {
                    $next_time = Carbon::today()->addYear();
                    Log::channel('invoices')->info("Invoice #{$this->invoice->id} is annually, updating next_run to{$next_time}");
                } else if($this->invoice->frequency === 'weekly') {
                    $next_time = Carbon::today()->addWeek();
                    Log::channel('invoices')->info("Invoice #{$this->invoice->id} is weekly, updating next_run to{$next_time}");
                } else {
                    $next_time = Carbon::today()->addDay();
                    Log::channel('invoices')->info("Invoice #{$this->invoice->id} is daily, updating next_run to{$next_time}");
                }

                $this->invoice->update([
                    'status' => 2,
                    'invoice_status_id' => 3,
                    'next_run' => $next_time,
                    'due_date' => $next_time->addDays(7)
                ]);
            } catch (\Exception $e) {
                $this->invoice->update([
                    'status' => 3,
                    'invoice_status_id' => 2,
                    'last_run' => Carbon::now()
                ]);
            }
        }
//    }
}
