<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class ProcessEmployeesLeaveDays implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $employees;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($employees)
    {
        $this->employees = $employees;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        foreach ($this->employees as $employee) {
            // if date is today in months multiple annual leave days and store
            Log::channel('employees')->info("Getting Leave Details for Employee #{$employee->id}");
            Log::channel('employees')->info($employee);
            Log::channel('employees')->info("Employee is {$employee->employment_type}");

            if(!empty($employee->appointment_date)){
                Log::channel('employees')->info("Employee #{$employee->id} has appointment set to {$employee->appointment_date}");
                // get current leave days
                $currentDays = $employee->available_leave_days;
                Log::channel('employees')->info("Employee #{$employee->id} has {$currentDays} days");

                // current leave days > 0  then
                if($currentDays > 0) {
                    Log::channel('employees')->info("Employee #{$employee->id} has now Leave type: {$employee->leaveType->name} with {$employee->leaveType->accrual_date} multiple");
                    $newDays = $currentDays * 1.25;

                    $employee->update([
                        'available_leave_days' => $newDays
                    ]);

                    Log::channel('employees')->info("Employee #{$employee->id} has now accumulated {$employee->available_leave_days} days");
                } else {
                    Log::channel('employees')->info("Employee doesn't have current days but has appointment Date");

                    $newDays = 1 * 1.25;
                    Log::channel('employees')->info("Employee #{$employee->id} has now {$newDays} days on 1.25 multiple");

                    $employee->update([
                        'available_leave_days' => $newDays
                    ]);
                }
            } else {
                Log::channel('employees')->info("Employee #{$employee->id} has no appointment date");
            }
        }
    }
}
