<?php

namespace App\Jobs;

use Illuminate\Support\Facades\DB;
use App\Models\{Estimate, Item};
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Arr;

class AttachItemsToModel implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected Array $items;
    protected $model;
    protected string $type;
    protected $table;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Array $items, $model, string $type)
    {
        $this->items = $items;
        $this->model = $model;
        $this->type = $type;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        \Log::info('In Attach Job');

        \Log::debug($this->model);
        if($this->type == 'estimate'){
            $this->table = 'estimate_item';
            $modelId = 'estimate_id';
        } else {
            $this->table = 'invoice_item';
            $modelId = 'invoice_id';
        }
        $estItems = DB::table($this->table)
            ->where($modelId, $this->model->id)
            ->get()->pluck('item_id')->toArray();

        // Check if the estimate has any items attached
        if($estItems) {
            // We need to Check if items in the estimate match the name
            foreach($this->items as $item){
              // check if items in the estimate are already attached to this estimate
              if(Arr::has($item, 'id') && in_array($item['id'], $estItems)){

                  \Log::info("Gotcha {$item['id']} ");
                  //TODO refine logic here, if the estimate has items and some items are matched, update what needs to be updated

                  $itemObj = Item::find($item['id']);
                  $itemObj->update([
                      'name' => $item['name'],
                      'description' => $item['description'],
                      'rate' => $item['rate']
                  ]);

                  \Log::debug("Quantity: {$item['pivot']['quantity']}");
                  if($this->type == 'estimate'){
                      $itemObj->estimate()->updateExistingPivot($this->model->id, [
                          'discount' => $item['pivot']['discount'],
                          'quantity' => $item['pivot']['quantity']]);
                  } elseif($this->type == 'invoice') {
                      $itemObj->invoice()->updateExistingPivot($this->model->id, [
                          'discount' => $item['pivot']['discount'],
                          'quantity' => $item['pivot']['quantity']]);
                  }
              } else {

                  //TODO refine logic here, if estimate has items linked, but the current item doesn't match create a new item
                  \Log::info("Creating new Item");
                  \Log::debug($item);

                  $newItem = $this->createItem($item);
                  if($this->type == 'estimate'){
                      $newItem->estimate()->attach($this->model->id,[
                          'discount' => $item['pivot']['discount'],
                          'quantity' => $item['pivot']['quantity']]);
                  } elseif($this->type == 'invoice') {
                      \Log::info("Creating item for invoice");
                      $newItem->invoice()->attach($this->model->id,[
                          'discount' => $item['pivot']['discount'],
                          'quantity' => $item['pivot']['quantity']]);
                  }
              }
          }

        } else {
            \Log::debug("There are no Items linked to this Estimate");

            foreach($this->items as $item) {
                $existingItem = Item::where('name', $item['name'])
                        ->where('company_id', $this->model->company_id)->first();

                if($existingItem) {
                    \Log::debug("There is an item match for {$item['name']}  on company items");
                    $existingItem->update([
                        'description' => $item['description'],
                        'rate' => $item['rate']
                    ]);

                    if($this->type == 'estimate'){
                        $existingItem->estimate()->attach($this->model->id,[
                            'discount' => $item['discount'],
                            'quantity' => $item['quantity']]);
                    } elseif ($this->type == 'invoice'){
                        $existingItem->invoice()->attach($this->model->id,[
                            'discount' => $item['discount'],
                            'quantity' => $item['quantity']]);
                    }

                } else {
                    $newItem = $this->createItem($item);

                    if(Arr::get($item, 'pivot')) {
                       if($this->type == 'estimate'){
                           $newItem->estimate()->attach($this->model->id,[
                               'discount' => $item['pivot']['discount'],
                               'quantity' => $item['pivot']['quantity']]);
                       } elseif ($this->type == 'invoice'){
                           $newItem->invoice()->attach($this->model->id,[
                               'discount' => $item['pivot']['discount'],
                               'quantity' => $item['pivot']['quantity']]);
                       }
                    } else {
                        if($this->type == 'estimate'){
                            $newItem->estimate()->attach($this->model->id, [
                                'discount' => $item['discount'],
                                'quantity' => $item['quantity']]);
                        } elseif ($this->type == 'invoice'){
                            $newItem->invoice()->attach($this->model->id, [
                                'discount' => $item['discount'],
                                'quantity' => $item['quantity']]);
                        }

                    }
                }
            }
        }

//        foreach($this->items as $item) {
//            $existingItem = Item::where('name', $item['name'])->first();
//
////            if($existingItem) {
//
//                $existingEstItem = DB::table('estimate_item')
//                    ->where('estimate_id', $this->model_id)
//                    ->where('item_id', $item['id']);
//
//                if($existingEstItem){
//                    \Log::info("Item: {$item['name']} exists and belongs to this Estimate");
//                    $existingItem->estimate()->sync($this->model_id, [
//                        'discount' => $item['pivot']['discount'],
//                        'quantity' => $item['pivot']['quantity']]);
//                } else {
//                    \Log::info("Item: {$item['name']} exists but not linked to this Estimate");
//                    $existingItem->estimate()->attach($this->model_id, [
//                        'discount' => $item['pivot']['discount'],
//                        'quantity' => $item['pivot']['quantity']]);
//                }


//            } else {
//                \Log::info('Item doesn\'t Exist, creating a new one');
//                $newItem = new Item;
//
//                $newItem->name = $item['name'];
//                $newItem->description = $item['description'];
//                $newItem->rate = $item['rate'];
//
//                $newItem->save();
//
//                $newItem->estimate()->attach($this->model_id,[
//                    'discount' => $item['discount'], 'quantity' => $item['quantity']]);
//
//            }
//        }
    }

    public function createItem($item) {
        $newItem = new Item;

        $newItem->name = $item['name'];
        $newItem->description = $item['description'];
        $newItem->rate = $item['rate'];
        $newItem->company_id = 1; //TODO needs to store created by company id

        $newItem->save();

        return $newItem;
    }


}
