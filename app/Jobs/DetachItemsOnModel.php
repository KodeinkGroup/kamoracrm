<?php

namespace App\Jobs;

use App\Models\{Estimate, Item};
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;

class DetachItemsOnModel implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected Array $ids;
    protected $model;
    public string $type;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Array $ids, $model , string $type)
    {
        $this->ids = $ids;
        $this->model = $model;
        $this->type = $type;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        foreach($this->ids as $id){
            $item = Item::find($id);
            if($this->type === 'estimate'){
                $item->estimate()->where('estimate_id', $this->model->id)->detach();
            }
            if ($this->type === 'invoice') {
                $item->invoice()->where('invoice_id', $this->model->id)->detach();
            }

        }

    }
}
