<?php

namespace App\Jobs;

use App\Models\Alert;
use App\Models\Invoice;
use App\Models\Prospect;
use App\Models\User;
use App\Notifications\SendOverdueInvoicesEmail;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Models\Estimate;
use Illuminate\Support\Facades\Log;

class ProcessInvoices implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    private $invoices;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($invoices)
    {
        $this->invoices = $invoices;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Log::channel('invoices')->info("ProcessInvoice::handle");
        foreach ($this->invoices as $invoice) {
            $client = Prospect::where('id', $invoice->prospect_id)->first();
            $user = User::where('id', $invoice->created_by)->first();
            Log::channel('invoices')->debug("Processing invoice  #{$invoice->id}");
            Log::channel('invoices')->debug("Invoice Due date- {$invoice->due_date}");

            if(now() > $invoice->due_date){
                Log::channel('invoices')->debug("Invoice #{$invoice->id} has passed it's Due date- {$invoice->due_date}");
                $invoice->update(['invoice_status_id' => 6]);
                  $user->notify(new SendOverdueInvoicesEmail($invoice));

                  $alert = new Alert;

                  $alert->subject_type = Invoice::class;
                  $alert->subject_id = $invoice->id;
                  $alert->status_id = 1;
                  $alert->created_by = $user->id;
                  $alert->addressed_to = $user->id;

                  $alert->save();

                Log::channel('alerts')->debug("Alert created for Invoice #{$invoice->id}");

            } else {
                Log::channel('invoices')->debug("Invoice #{$invoice->id} is up to date");
            }
        }
        Log::channel('invoices')->info("ProcessInvoice::end handle");
    }
}
