<?php

namespace App\Http\Requests\Prospects;

use Illuminate\Foundation\Http\FormRequest;

class UpdateContactRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'street'    =>  'nullable',
            'surburb'   =>  'nullable',
            'city'      =>  'nullable',
            'province'  =>  'nullable',
            'country'   =>  'nullable',
            'notes'     =>  'nullable',
        ];
    }
}
