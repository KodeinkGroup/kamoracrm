<?php

namespace App\Http\Requests\Prospects;

use Illuminate\Foundation\Http\FormRequest;

class StoreProspectRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'  => 'required',
            'business_name' => 'nullable',
            'email' =>  'required|unique:prospects',
            'mobile_no' => 'nullable',
            'location' =>   'nullable',
            'logo'  =>  'nullable|mimes:png,jpg,jpeg|max:2000',
            'prospect_type_id' => 'required',
            'source_id' => 'required',
            'salesperson' => 'nullable'
        ];
    }
}
