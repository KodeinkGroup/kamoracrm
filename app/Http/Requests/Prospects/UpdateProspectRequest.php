<?php

namespace App\Http\Requests\Prospects;

use Illuminate\Foundation\Http\FormRequest;

class UpdateProspectRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
         return [
            'name'  => 'required|string',
            'business_name' => 'nullable|string',
            'email' =>  'required|unique:prospects,email,'.$this->prospect,
            'mobile_no' => 'nullable|string',
            'location' =>   'nullable|string',
            'logo'  =>  'nullable|mimes:png,jpg,jpeg|max:2000',
            'prospect_type_id' => 'required',
            'source_id' => 'required',
            'salesperson' => 'nullable'
        ];
    }
}
