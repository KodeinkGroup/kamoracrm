<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use App\Models\{Company, Trip, FuelUsage, Vehicle, Employee};

class TripController extends Controller
{
    // Show all trips
    public function index()
    {
        $user = Auth::user();
        $companyId = $user->company_id;

        // Assuming $companyId is the ID of the company you want trips for
        $companyTrips = Company::with(['fleets.vehicles.trips' => function ($query) {
        $query->orderBy('start_time', 'desc');
    }])->findOrFail($companyId);

        Log::channel('trips')->info("Loaded Company Trips");
        Log::channel('trips')->info($companyTrips);

        return view('admin.vehicles.trips.index', compact('companyTrips'));
    }

    // Show form to create a new trip
    public function create()
    {
        $user = Auth::user();
        $company = Company::with('fleets.vehicles', 'fleets.vehicles.department', 'employees')->find($user->company_id);

        return view('admin.vehicles.trips.create', compact('company'));
    }

    // Store a newly created trip
    public function store(Request $request)
    {
        // Validate request data
        $request->validate([
            'vehicle_id' => 'required|exists:vehicles,id',
            'driver_id' => 'required|exists:employees,id',
            'start_location' => 'required|string',
            'end_location' => 'required|string',
            'start_time' => 'required|date',
            'end_time' => 'required|date|after_or_equal:start_time',
        ]);

        // Create new trip
        Trip::create($request->all());

        return redirect()->route('admin.company.vehicles.trips.index')->with('success', 'Trip created successfully!');
    }

    // Show form to edit an existing trip
    public function edit(Trip $trip)
    {
        $user = Auth::user();
        $company = Company::with('fleets.vehicles', 'fleets.vehicles.department', 'employees')->find($user->company_id);

        return view('admin.vehicles.trips.edit', compact('trip', 'company'));
    }

    // Update the trip
    public function update(Request $request, $id)
    {
        $request->validate([
            'vehicle_id' => 'required|exists:vehicles,id',
            'driver_id' => 'required|exists:employees,id',
            'start_location' => 'required|string',
            'end_location' => 'required|string',
            'start_time' => 'required|date',
            'end_time' => 'required|date|after_or_equal:start_time',
        ]);

        $trip = Trip::find($id);
        $trip->update($request->all());

        return redirect()->route('admin.company.vehicles.trips.index')->with('success', 'Trip updated successfully!');
    }

    // Delete the trip
    public function destroy(Trip $trip)
    {
        $trip->delete();
        return redirect()->route('trips.index')->with('success', 'Trip deleted successfully!');
    }
}
