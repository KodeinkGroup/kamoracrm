<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Employee;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
class EmployeesController extends Controller
{
   public function index()
   {
       $user = Auth::user();
       $employees = Employee::with('user', 'user.department')->where('company_id', $user->company_id)->get();

       return view('admin.employees.index', ['employees' => $employees]);
   }

   public function create() {
       return view('admin.employees.create');
   }
   public function store(Request $request) {

       $loggedInUser = Auth::user();

       $password = Str::random(12);

       $user_password = Hash::make($password);

       $user = new User;
       $user->name = $request->full_names . " " . $request->surname;
       $user->email = $request->email;
       $user->role = $request->role;
       $user->password = $user_password;
       $user->company_id = $loggedInUser->company_id;
       $user->department_id = null;

       $user->save();

       $employee = new Employee;

       $employee->create([
            'user_id' => $user->id,
           'company_id' =>$loggedInUser->company_id,
           'employee_number' => $request->employee_number,
           'position' => $request->role,
           'initials' => $request->initials,
           'title' => $request->title,
           'full_names' => $request->full_names,
           'surname' => $request->surname,
           'id_no' => $request->id_no,
           'mobile_number' => $request->mobile_number,
           'appointment_date' => $request->appointment_date,
           'next_of_kin_name' => $request->next_of_kin_name,
           'next_of_kin_relationship' => $request->next_of_kin_relationship,
           'next_of_kin_mobile_number' => $request->next_of_kin_mobile_number,
           'salary' => $request->salary,
           'date_of_birth' => $request->date_of_birth,
           'id_type' => $request->id_type,
           'employment_type' => $request->employment_type,
           'available_leave_days' => $request->available_leave_days,
           'street' => $request->street,
           'suburb' => $request->suburb,
           'city' => $request->city,
           'province' => $request->province,
           'country' => $request->country,
           'zip_code' => $request->zip_code,
       ]);

       return redirect()->route('admin.employees.index')->with('success', 'Successfully added New Employee Details');
   }
   public function edit($id) {
       $employee = Employee::with('user', 'user.department')->find($id);

       return view('admin.employees.edit', ['employee' => $employee]);
   }
   public function update(Request $request, $id) {
       $employee = Employee::find($id);

       Log::channel('employees')->info("Updating Employee #{$employee->id} details");
       Log::channel('employees')->debug($employee);

       $employee->update([
//           'user_id' => $request->user_id,
//           'company_id' => $request->company_id,
           'employee_number' => $request->employee_number,
//           'position' => $request->position,
           'initials' => $request->initials,
           'title' => $request->title,
           'full_names' => $request->full_names,
           'surname' => $request->surname,
           'id_no' => $request->id_no,
           'mobile_number' => $request->mobile_number,
           'appointment_date' => $request->appointment_date,
           'next_of_kin_name' => $request->next_of_kin_name,
           'next_of_kin_relationship' => $request->next_of_kin_relationship,
           'next_of_kin_mobile_number' => $request->next_of_kin_mobile_number,
           'salary' => $request->salary,
           'date_of_birth' => $request->date_of_birth,
           'id_type' => $request->id_type,
           'employment_type' => $request->employment_type,
           'available_leave_days' => $request->available_leave_days,
           'street' => $request->street,
           'suburb' => $request->suburb,
           'city' => $request->city,
           'province' => $request->province,
           'country' => $request->country,
           'zip_code' => $request->zip_code,
       ]);

       return redirect()->route('admin.employees.index')->with('success', 'Successfully updated the Employee Details');
   }
}
