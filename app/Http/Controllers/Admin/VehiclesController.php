<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Company;
use App\Models\Fleet;
use App\Models\Vehicle;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class VehiclesController extends Controller
{
    public function index() {

        $user = Auth::user();
        $company = Company::with('fleets.vehicles', 'fleets.vehicles.department')->find($user->company_id);

        return view('admin.vehicles.index', ['company' => $company]);
    }

    public function create()
    {
        Log::channel('vehicles')->info("Collecting Vehicles");
        $user = Auth::user();
        $company = Company::with('departments', 'fleets')->find($user->company_id);

        return view('admin.vehicles.create', ['company' => $company]);
    }

    public function view($id)
    {
        $vehicle = Vehicle::with(['trips' => function ($query) {
            $query->orderBy('start_time', 'desc');
        }])->find($id);

        return view('admin.vehicles.show', ['vehicle' => $vehicle]);
    }

    public function fleetsIndex() {
        $user = Auth::user();
        $company = Company::with('fleets')->find($user->company_id);

        return view('admin.fleets.index', ['company' => $company]);
    }
    public function createFleet() {
        return view('admin.fleets.create');
    }
    public function storeFleet(Request $request) {
        $user = Auth::user();
        $company = Company::find($user->company_id);

        $fleet = new Fleet;

        $fleet->name =  $request->get('name');
        $fleet->description = $request->get('description');
        $fleet->company_id = $company->id;
        $fleet->status = "active";

        $fleet->save();

        return redirect()->route('admin.company.fleets.index')->with('success', 'Successfully added a new Fleet Type');
    }

    public function store(Request $request)
    {
        $user = Auth::user();

        $company = Company::find($user->company_id);

        $company->load('fleets');

        $vehicle = new Vehicle;
        $vehicle->make = $request->get('make');
        $vehicle->model = $request->get('model');
        $vehicle->license_plate = $request->get('license_plate');
        $vehicle->vin = $request->get('vin', '');
        $vehicle->odometer_reading = $request->get('odometer_reading');
        $vehicle->year = $request->get('year');
        $vehicle->last_service_date = $request->get('last_service_date');
        $vehicle->next_service_date = $request->get('next_service_date');
        $vehicle->fuel_type = $request->get('fuel_type');
        $vehicle->transmission_type = $request->get('transmission_type');
        $vehicle->status = $request->get('status');
        $vehicle->fleet_id = $request->get('fleet_id');
        $vehicle->department_id = $request->get('department_id');
        $vehicle->ownership = $request->get('ownership');

        Log::channel('vehicles')->info("Created Vehicle");

        $vehicle->save();

        return redirect()->route('admin.company.vehicles.index')->with('success', 'Successfully added a new Vehicle');
    }

    public function destroyFleet($id) {
        $fleet = Fleet::find($id);
        $fleet->delete();
        return redirect()->route('admin.company.fleets.index')->with('success', 'Successfully removed Fleet Type');
    }
    public function destroyVehicle($id) {
        $vehicle= Vehicle::find($id);
        $vehicle->delete();
        return redirect()->route('admin.company.vehicles.index')->with('success', 'Successfully removed Vehicle');
    }
}
