<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Resources\ProspectResource;
use App\Models\Company;
use App\Models\Prospect;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use phpDocumentor\Reflection\Types\Resource_;

class AccountsController extends Controller
{
    public function __construct(){
        $this->middleware('auth:api')->except(['index', 'create']);
    }
    public function index() {
        $user = Auth::user();
        $userCompany = Company::with(['prospects' => function ($q) {
            $q->where('prospect_type_id', 3);
        }])->where('id', $user->company_id)->first();
        return view('admin.accounts.index', ['accounts' => $userCompany->prospects]);
    }

    public function customers() {
        $user = Auth::user();
        $userCompany = Company::where('user_id', $user->id)->first();

        $accounts = $userCompany->prospects()
            ->where('prospect_type_id', 3)
            ->get();
            return ProspectResource::collection($accounts);
    }

    public function create() {
        return view('admin.prospects.create');
    }

    public function store() {
        return 0;
    }
}
