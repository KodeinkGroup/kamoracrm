<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Resources\InvoiceResource;
use App\Jobs\AttachItemsToModel;
use App\Jobs\DetachItemsOnModel;
use App\Models\Company;
use App\Models\Estimate;
use App\Models\Invoice;
use App\Models\InvoicePayment;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use PDF;

class InvoicesController extends Controller
{
    public function __construct() {
        $this->middleware('auth:api')->except(['index', 'recurring', 'show', 'create','exportInvoice', 'payments']);
    }

    public function index() {
        return view('admin.invoices.index');
    }

    public function recurring() {
        return view('admin.invoices.recurring');
    }

    public function create() {
//        $data = [
//            'scope' => 'create'
//        ];
        return view('admin.invoices.create');
    }

    public function list()
    {
        $invoices = Invoice::where('created_by', Auth::id())
            ->where('invoice_type', 1)
            ->orderBy('created_at', 'desc')->paginate(15);

        $invoices->load('prospect', 'status');

        return InvoiceResource::collection($invoices);
    }

    public function update(Request $request, $id) {
        $invoice = Invoice::find($id);

        $invoice->update([
            'subject' => Arr::get($request, 'subject'),
            'notes' => Arr::get($request, 'notes'),
            'terms' => Arr::get($request, 'terms'),
            'due_date' => Arr::get($request, 'due_date'),
            'discount' => Arr::get($request, 'discount'),
            'amount' => Arr::get($request, 'amount'),
            'invoice_status_id' => Arr::get($request, 'invoice_status_id'),
        ]);

        $itemsRemoved = Arr::get($request, 'itemsRemoved');
        $items = Arr::get($request, 'items');

        if($items || $itemsRemoved) {
            if($itemsRemoved) {
                $this->dispatch(new DetachItemsOnModel(Arr::get($request, 'itemsRemoved'), $invoice, 'invoice'));
            }

            if($items) {
               // $invoice->update(['invoice_status_id' => 2]);
                $this->dispatch(new AttachItemsToModel(Arr::get($request, 'items'), $invoice, 'invoice'));
            }
        }

        return $invoice;
    }

    public function listRecurring()
    {
        $invoices = Invoice::where('created_by', Auth::id())
            ->where('invoice_type', 2)
            ->orderBy('created_at', 'desc')->paginate(15);

        $invoices->load('prospect');

        return InvoiceResource::collection($invoices);
    }

    public function show(Invoice $invoice)
    {
        $invoice->load('items','prospect', 'user', 'status', 'payments');
        $company = Company::join('company_user', 'company_user.company_id', 'companies.id')
            ->join('invoices', 'invoice.created_by', 'company_user.user_id')
            ->where('invoices.created_by', Auth::id());
        return view('admin.invoices.show', ['estimate' => $invoice, 'company' => $company]);
    }

    public function payments()
    {
        $payments = InvoicePayment::where('captured_by', Auth::id())
            ->get();
        return view('admin.invoices.payments', ['payments' => $payments]);
    }

    public function listPayments()
    {
        $payments = InvoicePayment::with('user','invoice')
            ->where('captured_by', Auth::id())
            ->orderBy('payment_date', 'desc')
            ->get();
        if(!empty($payments)) $payments->load('invoice.status', 'invoice.prospect');

        return $payments;
    }

    public function invoiceDetail($id) {
        $invoice = Invoice::find($id);

        if($invoice) {
            $balance = $invoice->amount - $invoice->payments()->sum('amount');
//        $company = Company::join('company_user', 'company_user.company_id', 'companies.id')
//            ->join('estimates', 'estimates.created_by', 'company_user.user_id')
//            ->where('estimates.created_by', Auth::id());
            $invoice->load('items', 'prospect', 'user', 'status', 'payments');
            $invoice->balance = $balance;
        }

        return $invoice;
    }

    public function recordPayment(Request $request) {

        $data = Arr::get($request, 'data');
        \Log::debug($data);
        $invoice = Invoice::where('id', Arr::get($data, 'invoice_id'))->first();
       $payment = InvoicePayment::create(
           [
               'invoice_id' => $invoice->id,
               'amount' => Arr::get($data, 'payment_amount'),
               'payment_date' => Arr::get($data, 'payment_date'),
               'payment_method' => Arr::get($data, 'payment_method'),
               'captured_by' => Auth::id()
           ]
       );
        \Log::debug($payment);
        $paymentsTotal = $invoice->payments->sum('amount');
        if($paymentsTotal >= $invoice->amount) {
            $invoice->update(
                [
                    'invoice_status_id' => 4
                ]
            );
        } else {
            $invoice->update(
                [
                    'invoice_status_id' => 5
                ]
            );
        }
        \Log::debug($paymentsTotal);

        return $invoice;
    }

    public function updateStatus (Request $request, $id){

        $data = $request->get('data');
        $status = Arr::get($data, 'status');
        $invoice = Invoice::where('id', $id)->first();

        if($status == 8) {
            $invoice->delete();
            $invoice->update(['invoice_status_id' => $status]);
        } else {
            $invoice->update(['invoice_status_id' => $status]);
        }

        return $invoice;
    }

    public function updateType (Request $request, $id){

        \Log::channel("invoices")->info('Updating Invoice Type to Recurring');
        \Log::channel("invoices")->debug($request);
        $data = Arr::get($request, 'data');
        \Log::channel("invoices")->debug("Data Retrieved");
        \Log::channel("invoices")->debug($data);

        $invoice = Invoice::where('id', $id)->first();

        if(!empty($invoice)) {
            $run_from = Arr::get($data, 'run_from');
            $run_until = Arr::get($data, 'run_until');
            $frequency = Arr::get($data, 'frequency');
            $type = Arr::get($data, 'type');

            $invoice->update([
                'invoice_type' => $type,
                'run_from' => $run_from,
                'run_until' => $run_until,
                'next_run' => $run_from,
                'frequency' => $frequency,
                'invoice_status_id' => 2,
                'status' => 0
            ]);

        }

        \Log::channel("invoices")->info('Updating Invoice Type to Recurring finished');

        return $invoice;

    }

    public function exportInvoice($id)
    {
        $invoice = Invoice::findOrFail($id);

        $company = Company::where('user_id', $invoice->created_by)->first();
        $invoice->load('items','prospect', 'user');
        $pdf = PDF::loadView('admin.invoices.pdf.invoice', ['invoice' => $invoice, 'company' => $company]);

        return $pdf->stream('Invoice.pdf');
    }

    public function download($invoice)
    {
        $invoice = Invoice::findOrFail($invoice);
        $company = Company::where('user_id', $invoice->created_by)->first();

        $invoice->load('items','prospect', 'user');
        $pdf = PDF::loadView('admin.invoices.pdf.invoice', ['invoice' => $invoice, 'company' => $company]);
        return $pdf->stream('Invoice.pdf');
    }

    public function deletePayment($id)
    {
        $payment = InvoicePayment::find($id);
        $payment->delete();
    }

}
