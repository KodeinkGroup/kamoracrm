<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Company;
use App\Models\Sprint;
use App\Models\Task;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;

class SprintsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api')->except('current');
    }

    public function active()
    {
        Log::info("Retrieving current sprint");
        $user = Auth::user();
        $userCompany = Company::find($user->company_id);
        $sprint = Sprint::with('tasks', 'tasks.project','tasks.assignee')
            ->where('company_id', $userCompany->id)
            ->where('status', 2)
            ->first(); // status 2 is active/current

        if($sprint) {

            $sprint->remainingDays = $sprint->remaining_days;

            $totalTasks = $sprint->tasks->count();
            $completedTasks = $sprint->tasks->where('status_id', 4)->count();
            $progress = $totalTasks > 0 ? ($completedTasks / $totalTasks) * 100 : 0;

            $sprint->percentage = $progress;
        }

        if(empty($sprint)) {
           $sprint = new Sprint;

           $sprint->name = "New Sprint";
           $sprint->status = 1;
           $sprint->company_id = $userCompany->id;
           $companyId = $userCompany->id;

//            $backlogTasks = Task::whereHas('project', function($query) use ($userCompany->id) {
//                return $query->where('company_id', $userCompany->id);
//            })->orWhere(function($query) use ($userCompany->id) {
//                    $query->whereNull('project_id')
//                        ->whereHas('assignee', function($query) use ($userCompany->id) {
//                            $query->where('company_id', $userCompany->id);
//                        });
//                });
//                ->get();
            $backlogTasks = Task::with(['status', 'priority', 'assignee', 'reporter', 'project', 'sprint'])
                ->whereIn('status_id', [1,2,3])
                ->whereHas('project', function($query) use ($companyId) {
                $query->where('company_id', $companyId);
            })
            ->orWhere(function($query) use ($companyId) {
                    $query->whereNull('project_id')
                        ->whereHas('assignee', function($query) use ($companyId) {
                            $query->where('company_id', $companyId);
                        });
                })
            ->get();
            $sprint->tasks = $backlogTasks;

            $sprint->remainingDays = 0;
            $sprint->percentage = 0;
        }

        return $sprint ;
    }

    public function updateStatus($id, Request $request)
    {
        Log::channel('tasks')->info("Closing Sprint");
        $sprint = Sprint::find($id);
        Log::channel('tasks')->debug((array)$sprint);
        $sprint->update(['status' => $request->get('status')]);
    }

    public function store(Request $request)
    {
        $sprint = new Sprint;

        $sprint->name = $request->get('name');
        $sprint->status = 2;
        $sprint->company_id = $request->get('company_id');
        $sprint->goals = $request->get('goals');
        $sprint->start_date = $request->get('start_date');
        $sprint->end_date = $request->get('end_date');

        $tasks = $request->get('tasks');

        $sprint->save();

        foreach($tasks as $sprintTask) {
            $task = Task::find($sprintTask['id']);
            $task->update(['sprint_id' => $sprint->id]);
        }

        return $sprint;
    }
}
