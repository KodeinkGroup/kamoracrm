<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Alert;
use App\Models\Company;
use App\Models\Task;
use App\Models\TicketType;
use App\Models\TimeLog;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class TasksController extends Controller
{

    public function __construct() {
        $this->middleware('auth:api')->except(['index', 'show', 'create','exportEstimate', 'sprints', 'view']);
    }
    public function index(){

        $tasks = Task::with(['status', 'priority'])->get();
        return view('admin.tasks.index', ['tasks' => $tasks]);
    }

    public function list(Request $request){

        $type = $request->has('type') ? $request->get('type') : '';
        $filter = $request->has('filter') ? $request->get('filter') : null;

        // Step 2: Decode the JSON string into a PHP array
        $filterArray = json_decode($filter, true);

        // Step 3: Extract the keywords
        $query = isset($filterArray['keywords']) ? $filterArray['keywords'] : '';
        $taskStatus = isset($filterArray['status']) ? $filterArray['status'] : '';

        Log::channel('tasks')->info("Request query");
        Log::channel('tasks')->info($query);

        $user = Auth::user();
        $userCompany = Company::find($user->company_id);
        $companyId = $userCompany->id;

//        $tasks = Task::with(['status', 'priority', 'assignee', 'reporter', 'project', 'sprint'])
//            ->whereHas('project', function($query) use ($companyId) {
//                $query->where('company_id', $companyId);
//            })
//            ->orWhere(function($query) use ($companyId) {
//                $query->whereNull('project_id')
//                    ->whereHas('assignee', function($query) use ($companyId) {
//                        $query->where('company_id', $companyId);
//                    });
//            })
//            ->whereIn('status_id', [1,2,3])
//            ->get();

        $tasks = Task::with(['status', 'priority', 'assignee', 'reporter', 'project', 'sprint'])
            ->where(function ($query) use ($companyId) {
                $query->whereHas('project', function ($q) use ($companyId) {
                    $q->where('company_id', $companyId);
                })
                    ->orWhere(function ($q) use ($companyId) {
                        $q->whereNull('project_id')
                            ->whereHas('assignee', function ($qpa) use ($companyId) {
                                $qpa->where('company_id', $companyId);
                            });
                    });
            })
            ->when($query, function ($q) use ($query) {
                return $q->where('title', 'like', "%{$query}%");
            })
            ->when($taskStatus !== "", function ($q) use ($taskStatus) {
                return $q->where('status_id', $taskStatus);
            })
            ->when($type !== "", function ($q) {
                return $q->whereNot('status_id', 4);
            })
            ->orderBy('created_at')
            ->orderBy('priority_id', 'desc')
            ->get();


        Log::channel('tasks')->info("Request");
        Log::channel('tasks')->info($tasks);

        return $tasks;
    }

    public function store(Request $request)
    {
        Log::channel('tasks')->info("Creating Task");
        $user = Auth::user();

        $task = new Task;

        $task->title = Arr::get($request, 'title');
        $task->summary = Arr::get($request, 'summary');
        $task->due_date = Arr::get($request, 'due_date');
        $task->assignee = Arr::get($request, 'assignee');
        $task->time_estimate = Arr::get($request, 'time_estimate');
        $task->priority_id = Arr::has($request, 'priority') ? Arr::get($request, 'priority') : 2;
        $task->status_id = 1;
        $task->created_by = $user->id;
        $task->project_id = Arr::get($request, 'project');
        $task->sprint_id = Arr::get($request, 'sprint_id');

        $task->save();

        Log::channel('tasks')->info("Created Task #{$task->id}");
        $alert = new Alert;

        Log::channel('tasks')->info("Creating Alert");
        Log::channel('alerts')->info("Creating Alert for Task #{$task->id}");
//        $alert->subject_type = Task::class;
//        $alert->subject_id = $task->id;
        $alert->created_by = $task->created_by;
        $alert->addressed_to = $task->assignee;

        $task->alerts()->save($alert);


        Log::channel('alerts')->debug($alert);
        Log::channel('alerts')->info("Created Alert for Task #{$task->id}");
    }

    public function view($id) {
        $task = Task::find($id);

        return view('admin.tasks.view', ['task' => $task]);
    }

    public function show($id)
    {
        Log::channel('tasks')->info("Showing Task #{$id}");
        $task = Task::with(['status', 'priority', 'reporter', 'project', 'assignee', 'sprint', 'materialRequests.item',
            'materialRequests.user', 'materialRequests.approvedBy'])->find($id);

        return $task;
    }

    public function update($id, Request $request)
    {
        Log::channel('tasks')->info("Updating Task #{$id}");
        Log::channel('tasks')->info($request);

        $start = Arr::get($request, 'start_time');
        $end = Arr::get($request, 'end_time');

        $spentHours = $this->calculateHoursDifference($start, $end);
        Log::channel('tasks')->info("Hours spent on Ticket #{$id}");
        Log::channel('tasks')->info($spentHours);

        $task = Task::find($id);

        $task->update([
            'title' => Arr::get($request, 'title'),
            'summary' => Arr::get($request, 'summary'),
            'due_date' => Arr::get($request, 'due_date'),
//            'assignee' => Arr::get($request, 'assignee'),
            'priority_id' => Arr::get($request, 'priority_id', 2),
            'project_id' => Arr::get($request, 'project_id'),
            'time_estimate' => Arr::get($request, 'time_estimate'),
            'time_spent' => $spentHours ?? Arr::get($request, 'time_spent'),
            'sprint_id' => Arr::get($request, 'sprint_id'),
        ]);

        // Create the time log and associate it with the task
        $timeLog = new TimeLog;
        $timeLog->start_time = $start;
        $timeLog->end_time = $end;
        $timeLog->user_id = Auth::id();
        $timeLog->task()->associate($task);
        $timeLog->save();

        return $task;
    }

    public function updateStatus($id, Request $request)
    {
        Log::channel('tasks')->info("Updating Task");
        $status = $request->get('status');

        $task = Task::find($id);
        if($status == 6) {
            $this->destroy($task);
        } else {
            $task->update(['status_id' => $status]);
            Log::channel('tasks')->info("Updating Task #{$task->id} finished");
        }
    }

    public function sprints()
    {
        return view("admin.tasks.sprints");
    }

    public function removeFromSprint($id)
    {
        $task = Task::find($id);

        if (!empty($task)) {
            $task->update(['sprint_id' => null]);
        }
    }

    public function addToSprint($id, Request $request)
    {
        $task = Task::find($id);

        if (!empty($task)) {
            $task->update(['sprint_id' => $request->get('sprint_id')]);
        }
    }

    public function destroy(Task $task) {
        $task->delete();
    }

    public function calculateHoursDifference($start, $end)
    {
        $startTime = Carbon::parse($start);
        $endTime = Carbon::parse($end);

        // Calculate the difference in hours
        $hoursDifference = $startTime->diffInHours($endTime);

        return $hoursDifference;
    }

    public function addTaskType(Request $request) {
        $taskType = new TicketType;

        $taskType->name = $request->get('name');
        $taskType->color = $request->get('color');

        $taskType->save();
    }
}
