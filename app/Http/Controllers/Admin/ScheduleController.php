<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Schedule;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class ScheduleController extends Controller
{
    public function __construct(){
        $this->middleware('auth:api')->except(['index', 'create', 'store']);
    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
       $schedules =  Schedule::get();

       return view('admin.schedules.index', ['schedules' => $schedules]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.schedules.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Log::channel('schedules')->info("Schedule Data");
        Log::channel('schedules')->debug($request);

        $schedule = new Schedule;

        $schedule->team = 'KPE';
        $schedule->jobID = $request['job_id'];
        $schedule->accId = $request['acc_id'];
        $schedule->jobType = $request['job_type'];
        $schedule->area = $request['area'];
        $schedule->rate = $request['rate'];
        $schedule->fibre_used = $request['fibre_used'];
        $schedule->comments = $request['comments'];
        $schedule->date = $request['schedule_date'];


        $schedule->save();

        return redirect()->route('admin.schedules.index')->with('success', 'Successfully added a new schedule entry');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Schedule  $schedule
     * @return \Illuminate\Http\Response
     */
    public function show(Schedule $schedule)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Schedule  $schedule
     * @return \Illuminate\Http\Response
     */
    public function edit(Schedule $schedule)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Schedule  $schedule
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Schedule $schedule)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Schedule  $schedule
     * @return \Illuminate\Http\Response
     */
    public function destroy(Schedule $schedule)
    {
        //
    }
}
