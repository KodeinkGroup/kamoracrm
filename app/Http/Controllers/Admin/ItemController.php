<?php

namespace App\Http\Controllers\Admin;

use App\Http\Resources\ItemResource;
use App\Models\Company;
use App\Models\MaterialRequest;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreItemRequest;
use App\Models\Item;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class ItemController extends Controller
{
    public function __construct() {
        $this->middleware('auth:api')
            ->except(['index', 'show', 'create', 'store', 'edit', 'update', 'destroy', 'materialRequests']);
    }

    /**
     *
     */
    public function index()
    {
        $user = Auth::user();
        $userCompany = Company::find($user->company_id);

        Log::channel('inventory')->info("Retrieved Company");
        Log::channel('inventory')->info($userCompany);
        $items = $userCompany->items;
        return view('admin.items.index', ['items' => $items]);
    }

    public function list()
    {
        $user = Auth::user();
        $userCompany = Company::find($user->company_id);

        $items = $userCompany->items;
//        $items = Item::get();
        return ItemResource::collection($items);
    }

    public function create()
    {
        Log::channel('inventory')->info("Rendering create template");
        return view('admin.items.create');
    }

    public function store(Request $request)
    {
        Log::channel('inventory')->info("Creating New Item");
        Log::channel('inventory')->info($request);

        $user = Auth::user();
        Log::channel('inventory')->info("Created by {$user->id}");
        $userCompany = Company::find($user->company_id);
        Log::channel('inventory')->info("User is company");
        Log::channel('inventory')->info($userCompany);

        $item = Item::create($request->only('name', 'description', 'rate','quantity','category', 'serial_number', 'brand', 'condition'));
        $item->update(['company_id' => $user->company_id]);

        return redirect()->route('admin.items.index')->with('success', 'Successfully added a new Item');
    }

    public function edit($id) {
        Log::channel('inventory')->info("Rendering edit template for {$id}");
        $item = Item::find($id);
        Log::channel('inventory')->info("Rendering edit template for {$id}");

        return view('admin.items.edit', ['item' => $item]);
    }

    public function update(Request $request, $id) {

        Log::channel('inventory')->info("Updating Inventory Item");
        $item = Item::find($id);
        $item->update($request->only('name', 'description', 'rate','quantity','category', 'serial_number', 'brand', 'condition'));

        return redirect()->route('admin.items.index')->with('success', 'Successfully updated the Item');
    }

    public function materialRequests() {

        $user = Auth::user();
        $userCompany = Company::find($user->company_id);
        $companyId = $userCompany->id;
        $materialRequests = MaterialRequest::whereHas('project', function ($q) use ($companyId) {
            $q->where('company_id', $companyId);
        })->with('item', 'project')
            ->when($user->role == 'technician', function ($qq) use ($user) {
                return $qq->where('requested_by', $user->id);
            })->orderBy('created_at', 'desc')
            ->get();

        return view('admin.items.material-requests', ['materialRequests' => $materialRequests]);
    }

    public function destroy($id) {
        $now = Carbon::now();
        $item = Item::find($id);

        Log::channel('inventory')->info("Found Item to delete");
        Log::channel('inventory')->info($item);

        $item->update(['deleted_at' =>$now]);

        return back()->with('success', 'Successfully Removed the Record.');
    }
}
