<?php

namespace App\Http\Controllers\Admin;

use App\Http\Resources\UserResource;
use App\Mail\WelcomeEmail;
use App\Models\Company;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class UsersController extends Controller
{

    public function __construct(){
        $this->middleware('auth:api')->except(['index', 'store', 'edit', 'update']);
    }
    public function index() {

        $user = Auth::user();

        if($user->role == 'master'){
            $companies = Company::with('users')->get();

            Log::channel('users')->info('Companies with Users');
            Log::channel('users')->debug($companies);
        } else {
            $companies = Company::with('users', 'departments')->where('id', $user->company_id)->get();
            Log::info('Companies with Users');
            Log::info($companies);
        }

        return view('admin.users.index', [
           'companies' => $companies, 'method' => 'POST', 'url' => 'admin.users.store'
        ]);
    }

    public function list() {
        $user = Auth::user();
        Log::channel('users')->info("Loading LoggedIn User");
        Log::channel('users')->debug($user);
        $userCompany = Company::where('id', $user->company_id)->first();

        Log::channel('users')->info("Loading LoggedIn User Company");
        Log::channel('users')->debug($userCompany);

        if($user->role == 'master'){
            $users = User::with('department')->get();
        } else {
            $users = User::with('department')->where('company_id', $userCompany->id)->get();
        }

        return UserResource::collection($users);
    }

    public function store(Request $request) {
        $user = Auth::user();
        $userCompany = Company::where('user_id', $user->id)->first();

        $request->validate([
            'name'      =>  'required|min:4',
            'email'     =>  'required|unique:users|email',
            'role'      =>  'required',
            'password'  =>  'required|same:confirm_password',
            'confirm_password'  =>  'required'
        ]);

        $user_password = Hash::make($request->password);

        $user = new User;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->role = $request->role;
        $user->password = $user_password;
        $user->company_id = $userCompany->id;
        $user->department_id = $request->department_id;

        $user->save();

        //if(!empty($createdUser)){
//            Log::channel("companies")->info("Sending Welcome Email");
//            Mail::to($user->email)->send(
//                new WelcomeEmail($user)
//            );
        //}

        return redirect('admin/users')->with('success', 'Successfully added a new User');
    }

    public function edit($id) {
        Log::info("Editing User");

        $user = User::find($id);
        Log::debug($user);

        $company = Company::with('departments')->where('id', $user->company_id)->first();
        Log::info("User Company");
        Log::debug($company);

        return view('admin.users.edit', [
            'company' => $company, 'user' => $user, 'method' => 'PUT', 'url' => 'admin.users.update'
        ]);
    }

    public function update(Request $request, $id) {

        Log::debug("User Request");

        Log::debug($request);

        $user = User::find($id);

        $user->update([
            'name' => $request->name,
            'email' => $request->email,
            'role' => $request->role,
            'department_id' => $request->department_id
        ]);

        return redirect('admin/users')->with('success', 'Successfully edited User');
    }
}
