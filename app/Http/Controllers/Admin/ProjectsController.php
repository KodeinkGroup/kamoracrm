<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Alert;
use App\Models\Company;
use App\Models\Item;
use App\Models\MaterialRequest;
use App\Models\Project;
use App\Models\User;
use App\Notifications\SendMaterialRequest;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class ProjectsController extends Controller
{
    public function __construct() {
        $this->middleware('auth:api')->except(['index', 'show', 'create','exportEstimate']);
    }

    public function index() {
        return view('admin.projects.index');
    }
    public function list() {

        $user = Auth::user();
        $userCompany = Company::where('id', $user->company_id)->first();

        $projects = $userCompany->projects;

        $projects->load('company', 'department', 'client');

        foreach($projects as $key => $project){
            $hours = $project->tasks->sum(function ($task) {
                // Extract hours from the time spent string (e.g., "2h")
                $timeSpent = $task->time_spent;
                $hours = (int) filter_var($timeSpent, FILTER_SANITIZE_NUMBER_INT);
                return $hours;
            });
            $project->hours = $hours;
            $project->completedTasksCount = $project->tasks->where('status_id', 4)->count();

            $project->totalTasksCount = $project->tasks->count();

            $project->percentage = ($project->totalTasksCount > 0) ? ($project->completedTasksCount / $project->totalTasksCount) * 100 : 0;
        }

        return $projects;
    }

    public function store(Request $request)
    {
        Log::channel('inventory')->info("Begin Project Create");
        $user = Auth::user();
        $userCompany = Company::where('user_id', $user->id)->first();

        $project = new Project;

        $project->name = Arr::get($request, 'name');
        $project->summary = Arr::get($request, 'summary');
        $project->color = Arr::get($request, 'color');
        $project->account_id = Arr::get($request, 'account_id', '');
        $project->department_id = Arr::get($request, 'department_id', '');
        $project->company_id = $userCompany->id;
        $project->start_date = Arr::get($request,'start_date', null);
        $project->end_date = Arr::get($request,'end_date', null);

        $project->save();
    }

    public function detail($id)
    {
        $project = Project::with('department')->find($id);
        if(!empty($project)) {
            $project->load('company.departments','client','tasks', 'tasks.status', 'tasks.assignee', 'material.item', 'material.user',
                'material.approvedBy', 'material.assignedTo');
        }

        return $project;
    }

    public function update($id, Request $request) {
        Log::channel('inventory')->info("Begin Project Update");

        $user = Auth::user();
        $userCompany = Company::where('user_id', $user->id)->first();

        $project = Project::find($id);
        $project->update(
            [
                'name' => $request->get( 'name'),
                'summary' => $request->get( 'summary'),
                'color' => $request->get('color'),
                'account_id' => $request->get( 'account_id'),
                'department_id' => $request->get( 'department_id'),
                'company_id' => $userCompany->id,
                'start_date' => $request->get('start_date'),
                'end_date' => $request->get('end_date'),
            ]
        );
    }

    public function requestMaterial(Request $request) {
        Log::channel('inventory')->info("Begin Material Request");
        $project_id = $request->get('project_id', 0);
        $task_id = $request->get('task_id', 0);
        $material = $request->get('material');
        $type = $request->get('type');

        $now = Carbon::now();
        $user = Auth::user();

        $inventoryManager = User::where('company_id', $user->company_id)->where('role', 'inventory_manager')->first();

        foreach($material as $item) {
            if($type !== 'project') {
                Log::channel('inventory')->debug("Logging Material for Task");

                $existingItem = Item::where('name', 'like', '"%' .$item['selectedItem'] . '%"')
                    ->orWhere('description', 'like', '"%' .$item['selectedItem'] .'%"')
                    ->first();

                if($existingItem) {
                    $itemId = $existingItem->id;
                } else {
                    $newItem = new Item;

                    $newItem->name = $item['code'];
                    $newItem->supplier = $item['supplier'];
                    $newItem->description = $item['selectedItem'];

                    $newItem->save();
                    $itemId = $newItem->id;
                }
            }

            $materialRequest = new MaterialRequest;

            $materialRequest->project_id = $project_id;
            $materialRequest->task_id = $task_id;
            $materialRequest->item_id = $item['id'] ?? $itemId;
            $materialRequest->quantity = $item['quantity'];
            $materialRequest->requested_by = $user->id;
            $materialRequest->issued_to = $user->id;
            $materialRequest->request_date = $now;
            $materialRequest->status = 0;

            $materialRequest->save();

            $alert = new Alert;

            $alert->created_by = $user->id;
            $alert->addressed_to = $inventoryManager->id;
            Log::channel('inventory')->info("Stored Material Request");
            Log::channel('inventory')->info($materialRequest);
            $materialRequest->alerts()->save($alert);

//            $materialRequest->load('project', 'user');

//            $inventoryManager->notify(new SendMaterialRequest($materialRequest, $material));
        }
    }

    public function updateRequestMaterialStatus(Request $request, $id) {
        Log::channel("inventory")->info("Updating Request Status {$id}");

        $now = Carbon::now();
        $user = Auth::user();
        $status = $request->get('status');

        $materialRequest = MaterialRequest::find($id);

        $materialRequest->update(['status' => $request->get('status')]);
        if($status === 1){
            $materialRequest->approved_by = $user->id;
            $materialRequest->approved_date = $now;

            $materialRequest->save();
        }

        return $materialRequest;
    }

    public function destroy($id) {
        $project = Project::find($id);
        $project->delete();
    }
}
