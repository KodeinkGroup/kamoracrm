<?php

namespace App\Http\Controllers\Admin\Prospects\Activities;

use App\Models\Company;
use App\Models\Prospect;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Models\ProspectActivity;
use App\Models\ProspectDocument;
use App\Http\Controllers\Controller;

class ProspectActivitiesController extends Controller
{
    public function index(Prospect $prospect) {

        $user = Auth::user();
        $userCompany = Company::where('user_id', $user->id)->first();

        if($userCompany->id === $prospect->company_id) {
            return view('admin.prospects.activities.index ', [
                'activities' => ProspectActivity::prospectId($prospect->id)->latest()->paginate(20),
                'prospect' => $prospect
            ]);
        }

    }

    public function create(Prospect $prospect) {
        return view('admin.prospects.activities.create', compact('prospect'));
    }

    public function store(Request $request, Prospect $prospect) {
        ProspectActivity::create([
           'prospect_id' => $prospect->id,
           'communication_type' => $request->communication_type,
           'communication_channel' => $request->communication_channel,
           'type' => $request->type,
           'notes' => $request->notes
        ]);

        if($request->type == 'prospecting') {
           Prospect::where('id', $prospect->id)
                ->update(['prospect_type_id' => 2]);
        } elseif ($request->type == 'unqualify_prospect') {
            Prospect::where('id', $prospect->id)
                ->update(['prospect_type_id' => 1]);
        }
        elseif ($request->type == 'converted') {
            Prospect::where('id', $prospect->id)
                ->update(['prospect_type_id' => 3]);
        }

        return redirect()->route('admin.prospects.activities.dashboard', $prospect->id)->with('success', 'Successfully added activity');
    }

    public function show (Prospect $prospect, ProspectActivity $activity) {

        return view ('admin.prospects.activities.show', [
             'prospect' =>  $prospect,
             'activity' =>  $activity
            ]);
    }

}

