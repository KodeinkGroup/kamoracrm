<?php

namespace App\Http\Controllers\Admin\Prospects;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Models\ProspectType;

class ProspectTypeController extends Controller
{
    public function get_categories() {
        return ProspectType::all();
    }
}
