<?php

namespace App\Http\Controllers\Admin\Prospects;

use App\Models\Prospect;
use Illuminate\Http\Request;
use App\Models\ProspectContact;
use App\Http\Controllers\Controller;
use App\Http\Requests\Prospects\StoreContactRequest;
use App\Http\Requests\Prospects\UpdateContactRequest;


class ProspectContactsController extends Controller
{
    public function create(Prospect $prospect) {
//        dd('It gets here');
        return view('admin.prospects.contacts.create', compact('prospect'));
    }

    public function store(StoreContactRequest $request, Prospect $prospect) {

        $contact = ProspectContact::updateOrCreate(['prospect_id' => $prospect->id], $request->validated());
        return redirect()->route('admin.prospects.show', $prospect->id)->with('success', 'Successfully added prospect contact details');
    }

    public function update(UpdateContactRequest $request, Prospect $prospect){
        $prospect->contact->update($request->validated());
        return redirect()->route('admin.prospects.edit', $prospect->id)->with('success', 'Edited contacts successfully');
    }
}
