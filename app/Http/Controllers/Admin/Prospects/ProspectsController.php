<?php

namespace App\Http\Controllers\Admin\Prospects;

use App\Http\Resources\ProspectResource;
use App\Models\Company;
use App\Models\Prospect;
use App\Models\LeadSource;
use App\Models\ProspectType;
use App\Http\Requests\Prospects\StoreProspectRequest;
use App\Http\Requests\Prospects\UpdateProspectRequest;
use App\Http\Requests\Prospects\UpdateProspectLogoRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Collection;
use App\Jobs\AddContactActivity;


class ProspectsController extends Controller
{

    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){

        $user = Auth::user();
        $userCompany = Company::with(['prospects' => function ($q) {
            $q->where('prospect_type_id', 2);
        }])->where('id', $user->company_id)->first();

        return view('admin.prospects.index', ['prospects' => $userCompany->prospects]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($prospectType)
    {
        $type = ProspectType::find($prospectType);
        $sources = LeadSource::select('id','name')->get();

       return view('admin.prospects.create',['type' => $type, 'sources' => $sources]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreProspectRequest $request)
    {

        $prospect = new Prospect;

        $prospect->name = Arr::get($request, 'name');
        $prospect->email = Arr::get($request, 'email');
        $prospect->business_name = Arr::get($request, 'business_name');
        $prospect->mobile_no = Arr::get($request, 'mobile_no');
        $prospect->salesperson = Arr::get($request, 'salesperson');
        $prospect->location = Arr::get($request, 'location');
        $prospect->prospect_type_id = Arr::get($request, 'prospect_type_id');
        $prospect->source_id = Arr::get($request, 'source_id');
        $prospect->designation = Arr::get($request, 'designation');

        $user = Auth::user();
        $userCompany = Company::where('user_id', $user->id)->first();

        $prospect->company_id = $user->company_id;
        $prospect->save();

        if($request->hasFile('logo')) {
            $path = $request->logo->store('public/prospects/logos');
            $prospect->update(['logo' => $path]);
        }

        $this->dispatch(new AddContactActivity($prospect));

        return redirect()->route('admin.prospects.contacts.create',
            $prospect->id)->with('success', 'Successfully added a new Prospect');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Prospect $prospect){
        $user = Auth::user();
        $userCompany = Company::where('user_id', $user->id)->first();

        $prospect->load('contact', 'source', 'type');

        if($user->company_id === $prospect->company_id){
            return view('admin.prospects.show', ['prospect' => $prospect]);
        }
        return view('home');

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Prospect $prospect)
    {
        $user = Auth::user();
        $userCompany = Company::where('user_id', $user->id)->first();

        if($userCompany->id === $prospect->company_id){
            return view('admin.prospects.edit', ['prospect' => $prospect]);
        }

        return view('home');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Prospect $prospect)
    {
        $prospect->update([
            'name' => $request->name,
            'business_name' => $request->business_name,
            'email' => $request->email,
            'mobile_no' => $request->mobile_no,
            'location' => $request->location,
            'logo' => $request->logo
        ]);

        return view('admin.prospects.show', ['prospect' => $prospect]);
    }

    public function updateProfileImage (UpdateProspectLogoRequest $request, Prospect $prospect) {
        if ($prospect->logo) {
            Storage::delete($prospect->logo);
        }
        $path = $request->image->store('/public/prospects/logos');

        $prospect->update(['logo' => $path]);
        return back()->with('Profile Image updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($prospect)
    {
        $prospect =  Prospect::find($prospect);
        $prospect->delete();

        return back()->with('success', 'Successfully Removed the Record.');
    }

    public function destroyProfileImage(Prospect $prospect) {
        if($prospect->logo) {
            Storage::delete($prospect->logo);

            $prospect->update(['logo'=> null]);
        }

        return back()->with('success', 'Logo removed successfully.');
    }

}
