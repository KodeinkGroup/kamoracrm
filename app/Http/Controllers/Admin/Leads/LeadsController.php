<?php

namespace App\Http\Controllers\Admin\Leads;

use App\Http\Controllers\Controller;
use App\Http\Resources\LeadResource;
use App\Http\Resources\ProspectResource;
use App\Models\Company;
use App\Models\LeadSource;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use App\Models\Prospect;
use Illuminate\Support\Facades\Auth;

class LeadsController extends Controller
{
    /**
     * Get list off all leads
     */
    public function index () {

        $user = Auth::user();
        $userCompany = Company::with(['prospects' => function ($q) {
            $q->where('prospect_type_id', 1);
        }])->where('id', $user->company_id)->first();
        $leads = [];


        $smLeads = [
            ['facebook' => $this->getSMLeads('Facebook')],
            ['linkedIn' => $this->getSMLeads('LinkedIn')],
            ['referrals' => $this->getSMLeads('Referral')],
            ['website'  =>  $this->getSMLeads('Website')],
            ['whatsapp'  =>  $this->getSMLeads('Whatsapp')],
            ['instagram'  =>  $this->getSMLeads('Instagram')],
            ['twitter'  =>  $this->getSMLeads('Twitter')],
            ['other' => $this->getSMLeads('Other')]
        ];


        return view('admin.leads.index', ['leads' => $userCompany->prospects, 'sources' => $smLeads]);
    }

    public function create() {
        $sources = LeadSource::select('id','name')->get();
        return view('admin.prospects.create', ['sources' => $sources]);
    }

    public function qualifyLead() {

    }


    // Social Media Leads Count
    public function getSMLeads(string $social_media) {

        $user = Auth::user();
        $userCompany = Company::where('id', $user->company_id)->first();
        $count = 0;
        if(!empty($userCompany)) {
            $count = $userCompany->prospects()
                ->join('lead_sources', 'lead_sources.id', 'prospects.source_id')
                ->where('lead_sources.name', $social_media)
                ->where('prospect_type_id', 1)
                ->get()
                ->count();
        }

        return $count;
    }
}
