<?php

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Jobs\AttachItemsToModel;
use App\Jobs\DetachItemsOnModel;
use App\Models\{Company, Estimate, Invoice, ProspectActivity, User, Item};
use App\Http\Resources\EstimateResource;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use PDF;


class EstimateController extends Controller
{

    public function __construct() {
        $this->middleware('auth:api')->except(['index', 'show', 'create','exportEstimate']);
    }

    public function index()
    {
        $estimates = Estimate::get()
        ->load('prospect','status', 'user');
        return view('admin.estimates.index', ['estimates' => $estimates]);
    }

    public function list()
    {
        $estimates = Estimate::where('created_by', Auth::id())
            ->orderBy('created_at', 'desc')->paginate(15);

        $estimates->load('prospect','status');
        return EstimateResource::collection($estimates);
    }

    public function show(Estimate $estimate)
    {
        $estimate->load('items','prospect', 'status', 'user');
        $company = Company::join('company_user', 'company_user.company_id', 'companies.id')
            ->join('estimates', 'estimates.created_by', 'company_user.user_id')
            ->where('estimates.created_by', Auth::id());
        return view('admin.estimates.show', ['estimate' => $estimate, 'company' => $company]);
    }

    public function estimateDetail($id) {
        $estimate = Estimate::find($id);

        $company = Company::join('company_user', 'company_user.company_id', 'companies.id')
            ->join('estimates', 'estimates.created_by', 'company_user.user_id')
            ->where('estimates.created_by', Auth::id());
            $estimate->load('items','prospect', 'status', 'user');
//            $estimate->include($company);

        return $estimate;
    }

    public function create($type)
    {
        $data = [
            'scope' => 'create',
        ];
        return view('admin.estimates.create', ['type' => $type])->with($data);
    }

    public function store(Request $request)
    {
        $type = $request->query('type');

        Log::channel('estimates')->info("Model creation starting");
        if ($request->has('invoice')){
            Log::channel('estimates')->info("Model being created is Invoice");
            $data = $request->get('invoice');
        } else {
            Log::channel('estimates')->info("Model being created is Estimate");
            $data = $request->get('estimate');
        }

        if($type == 'estimate'){
            $model = new Estimate;
        } elseif ($type == 'invoice') {
            $model = new Invoice;
        }

        $company = Auth::user()->companies()->first();

        $model->subject = Arr::get($data, 'subject');
        if($type == 'estimate') {$model->reference_no = Arr::get($data, 'reference_no');}
        $model->discount = Arr::get($data, 'discount');
        if($type == 'estimate'){
            $model->expiry_date = Arr::get($data, 'expiry_date');
        } else {
            $model->due_date = Arr::get($data, 'expiry_date');
        }

        $model->amount = Arr::get($data, 'amount');
        $model->terms = Arr::get($data, 'terms');
        $model->notes = Arr::get($data, 'notes');

        if($type == 'invoice'){
            Log::channel('invoices')->info("Creating Invoice");
            $model->invoice_type = 1;
            if(Arr::has($data, 'estimate_id')) {
                $model->estimate_id = Arr::get($data, 'estimate_id');
            }
        }

        $model->prospect_id = Arr::get($data, 'prospect_id');

        if($type == 'estimate'){
            $model->company_id = $company->id;
        }

        $model->created_by =  Auth::id();

        $items = Arr::get($data, 'items');

        if($items) {

            if($type == 'estimate'){
                $model->estimate_status_id = Arr::get($data, 'estimate_status_id');
            } else {
                $model->invoice_status_id = Arr::get($data, 'estimate_status_id');
            }

            // Save the entry if it has items attached
            $model->reference_no = $this->generateReferenceNumber($type);
            $model->save();

            if(Arr::has($data, 'estimate_id')){
                if($type == 'invoice'){
                    $estimate = Estimate::where('id', $data['estimate_id'])->first();
                    $estimate->update(['estimate_status_id' => 7]);
                }
            }

            $this->dispatch(new AttachItemsToModel(Arr::get($data, 'items'), $model, $type));

            ProspectActivity::create([
                'prospect_id' => $model->prospect_id,
                'communication_type' => 'automated',
                'communication_channel' => 'platform',
                'type' => $type == 'estimate' ? 'Created Estimate' : 'Invoiced',
                'notes' => 'An' . $type == 'estimate' ? 'Estimate' : 'Invoice' . 'was created for the Customer'
            ]);
        } else {
            if($type == 'estimate') {
                $model->estimate_status_id = 1;
            }
            else {
                $model->invoice_status_id = 1;
            }
            $model->save();
        }

        Log::channel('estimates')->info("Done Processing Model {$type}");
        return $model;
    }

    public function update($id, Request $request)
    {
        $estimate = Estimate::find($id);

        $estimate->update([
            'subject' => Arr::get($request, 'subject'),
            'notes' => Arr::get($request, 'notes'),
            'terms' => Arr::get($request, 'terms'),
            'expiry_date' => Arr::get($request, 'expiry_date'),
            'discount' => Arr::get($request, 'discount'),
            'amount' => Arr::get($request, 'amount'),
            'estimate_status_id' => Arr::get($request, 'estimate_status_id'),
        ]);

        $itemsRemoved = Arr::get($request, 'itemsRemoved');
        $items = Arr::get($request, 'items');

        if($items || $itemsRemoved) {
            if($itemsRemoved) {
                $this->dispatch(new DetachItemsOnModel(Arr::get($request, 'itemsRemoved'), $estimate), 'estimate');
            }

            if($items) {
//                $this->setEstimateStatus($estimate, 2);
                $this->dispatch(new AttachItemsToModel(Arr::get($request, 'items'), $estimate, 'estimate'));
            }

        } else {
            $this->setEstimateStatus($estimate, 1);
        }

        return $estimate;
    }

    public function setEstimateStatus(Estimate $estimate, $status) {
        $estimate->update(['estimate_status_id' => $status]);
    }

    public function updateEstimateStatus(Request $request, $id) {
        $estimate = Estimate::find($id);
        $status = $request;

        $estimate->update(['estimate_status_id' => $status->id]);

        if($status->id == 3) {
            ProspectActivity::create([
                'prospect_id' => $estimate->prospect_id,
                'communication_type' => 'automated',
                'communication_channel' => 'platform',
                'type' => 'sent_estimate',
                'notes' => 'The generate Quote was sent to the customer'
            ]);
        }
        if($status->id == 4) {
            ProspectActivity::create([
                'prospect_id' => $estimate->prospect_id,
                'communication_type' => 'automated',
                'communication_channel' => 'platform',
                'type' => 'Customer Accepted Quote',
                'notes' => 'The Customer has accepted the Quote sent'
            ]);
        }

        if($status->id == 5) {
            ProspectActivity::create([
                'prospect_id' => $estimate->prospect_id,
                'communication_type' => 'automated',
                'communication_channel' => 'platform',
                'type' => 'Customer Rejected Quote',
                'notes' => 'The Customer has rejected the Quote sent'
            ]);
        }

        return $estimate;
    }

    public function exportEstimate($estimate)
    {
        $estimate = Estimate::findOrFail($estimate);
        $company = Company::where('user_id', Auth::id())->first();
        $estimate->load('items','prospect', 'status', 'user');

        $pdf = PDF::loadView('admin.estimates.pdf.estimate', ['estimate' => $estimate, 'company' => $company]);
        return $pdf->stream('Estimate.pdf');
    }

    public function destroy($id)
    {
        $quote = Estimate::findOrFail($id);

        $quote->delete();
    }

    public function createInvoice($data)
    {
        Log::channel('estimates')->info("Creating Invoice");
        Log::channel('estimates')->info($data);
    }

    /**
     * Function for generating References for Estimates and Invoices
     * @param $type
     * @return string
     */
    public function generateReferenceNumber($type): string
    {
        if($type == 'invoice') {
            // Get the count of existing invoices
            $modelCount = Invoice::count();

            // Increment the count and format it as a five-digit string with leading zeros
            $referenceNumber = 'INV-' . str_pad($modelCount + 1, 5, '0', STR_PAD_LEFT);

        } else {
             // Get the count of existing invoices
            $modelCount = Estimate::count();

            // Increment the count and format it as a five-digit string with leading zeros
            $referenceNumber = 'EST-' . str_pad($modelCount + 1, 5, '0', STR_PAD_LEFT);
        }
        // Assign the reference number to the new invoice
        return $referenceNumber;
    }
}
