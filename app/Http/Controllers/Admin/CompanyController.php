<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Prospects\UpdateProspectLogoRequest;
use App\Mail\WelcomeEmail;
use App\Models\{Company, Department, Industry, LeaveType, User};
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;

class CompanyController extends Controller
{
    public function __construct() {
        $this->middleware('auth:api')
            ->except('index', 'create', 'show', 'edit', 'update', 'updateLogo', 'store',
                'storeDepartment', 'updateDepartment', 'manageLeaveTypes');
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = Auth::user();

        $userCompany = Company::where('id', $user->company_id)->first();

        $industries = Industry::get();
        return view('admin.company.create', ['industries' => $industries, 'company' => $userCompany]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Log::channel("companies")->info("Creating Company:: {$request->name} and ID::{$request->id}");
        $similarCompany = Company::when($request->name, function($query) use ($request) {
            return $query->where('id', $request->id);
        })->first();

        if($similarCompany){
            Log::channel("companies")->info("Found Relating company");

            $similarCompany->update($request->all());

            if($request->hasFile('logo')) {
                $path = $request->logo->store('public/companies/logos');
                $similarCompany->update(['logo_url' => $path]);
            }

//            return back()->with('error', 'The company already exists');
            return redirect()->route('admin.dashboard')->with('success', 'Successfully added your company');
        } else {
            //TODO work on security of this. Only the initial user must be able to create a company, other users should be created by their admin.

//            $company = Company::create($request->only('name', 'email','registration_number', 'type', 'street','surburb',
//                'city', 'country', 'province', 'zip_code', 'bbbee_level', 'size', 'logo_url', 'year_registered', 'user_id', 'industry_id'));
//            if($request->hasFile('logo')) {
//                $path = $request->logo->store('public/companies/logos');
//                $company->update(['logo_url' => $path]);
//            }
//
//            Log::channel("companies")->info("Created Company");
//            Log::channel("companies")->info($company);
//
//            Log::channel("companies")->info("Sending Welcome Email");
////
////            Mail::to($user->email)->send(
////                 new WelcomeEmail($user)
////             );
//
//            Log::channel("companies")->info("Done sending Welcome Email");

            return redirect()->route('admin.dashboard')->with('success', 'Successfully added your company');
        }

        Log::channel("companies")->info("End Creating company");
    }

    public function storeDepartment(Request $request, $company) {
        Log::info("Creating Departments");
        Log::info($request);

        $userCompany = Company::find($company);
        Log::info("Found Company");
        Log::info($userCompany);
        Log::info("Creating Departments");
        $userCompany->departments()->create(['name' => $request->name, 'manager_id' => $request->manager_id]);

        return redirect()->route('admin.company.show', ['user' => Auth::id()])->with('success', 'Successfully added a new Department');
    }

    public function updateDepartment(Request $request, $company, $id) {
        Log::info("Updating Departments for Company #{$company}");
        Log::info($request);
        $department = Department::find($id);

        Log::info("Department Found");
        Log::info($department);
        $department->update(['manager_id' => $request->department_manager_id]);

        return redirect()->route('admin.company.show', ['user' => Auth::id()])->with('success', 'Successfully added a new Department');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function show($user)
    {
        $company = Company::with('departments', 'departments.manager')->where('user_id', $user)->first();

        return view('admin.company.show', ['company' => $company]);
    }

    public function companyDetails($user = false)
    {
        $activeUser = Auth::user();
        $company = Company::with('departments')->where('id', $activeUser->company_id )->first();

        return $company;
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $company = Company::find($id);
        return view('admin.company.edit', ['company' => $company]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Company $company)
    {

        $company->update([
            'name' => $request->name,
            'size' => $request->size,
            'email' => $request->email,
            'contact_number' => $request->contact_number,
            'BBBEE_level' => $request->bbbee_level,
            'registration_number' => $request->registration_number,
            'year_registered' => $request->year_registered,
            'street' => $request->street,
            'surburb' => $request->surburb,
            'city' => $request->city,
            'province' => $request->province,
            'zip_code' => $request->zip_code,
            'country' => $request->country,
        ]);
        return redirect()->route('admin.company.show', $company->id)->with('success', 'Edited your company successfully');
    }

    public function updateLogo(UpdateProspectLogoRequest $request, Company $company) {

        if ($company->logo_url) {
            Storage::delete($company->logo_url);
        }
        $path = $request->image->store('/public/companies/logos');

        $company->update(['logo_url' => $path]);
        return back()->with('Profile Image updated');
    }

    public function manageLeaveTypes($id)
    {
        $leaveTypes = LeaveType::get();
        return view('admin.company.hr.manage-leave-types', ['leaveTypes' => $leaveTypes]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function destroy(Company $company)
    {
        //
    }
}
