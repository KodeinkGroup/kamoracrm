<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Alert;
use Illuminate\Support\Facades\Log;

class AlertsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api')->except('updateStatus');
        $this->middleware('auth');
    }

    public function updateStatus($id) {
        Log::channel('alerts')->info("Updating Alert #{$id}");
        $alert = Alert::find($id);

        Log::channel('alerts')->debug($alert);
        $alert->status_id = 3;

        $alert->save();
        Log::channel('alerts')->info("Updated To");
        Log::channel('alerts')->debug($alert);

        return response()->json(['success' => true, 'message' => 'Alert status updated successfully']);
    }
}
