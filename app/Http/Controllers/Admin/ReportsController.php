<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Company;
use App\Models\Task;
use App\Models\Sale;
use App\Models\Prospect;
use App\Models\Account;
use App\Models\Invoice;
use App\Models\Project;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use PDF; // Import the PDF facade


class ReportsController extends Controller
{
    public function index()
    {
        return view('admin.reports.index');
    }

    public function generateReport()
    {
        $user = Auth::user();

        Log::channel('reports')->info("Getting Reports");
        Log::channel('reports')->info("Retrieved User::{$user->id}");
        $company = Company::with('projects', 'prospects', 'prospects.type', 'fleets.vehicles')
            ->find($user->company_id);

        Log::channel('reports')->info("Retrieved Company::{$company->id}");

        // Pass the data to the Blade view
        $data = compact('company');

        Log::channel('');
        // Load the Blade view and pass the data to it
        $pdf = PDF::loadView('admin.reports.managerial', $data);

        // Download the generated PDF
        return $pdf->stream('Managerial Report.pdf');

    }

    public function generateInventoryReport()
    {
        $user = Auth::user();

        Log::channel('reports')->info("Getting Inventory Report");
        Log::channel('reports')->info("Retrieved User::{$user->id} for Inventory report");
        // 1 New, 2 Returned 3 Damaged, 4 Out of stock
        $company = Company::withCount([
            'items as new_count' => function ($query) {
                $query->where('condition', 1);
            },
            'items as returned_count' => function ($query) {
                $query->where('condition', 2);
            },
            'items as damaged_count' => function ($query) {
                $query->where('condition', 3);
            },
            'items as out_of_stock_count' => function ($query) {
                $query->where('condition', 4);
            }
        ])->find($user->company_id);

//        Log::channel('reports')->info("Retrieved Company::{$company->id}");
        Log::channel('reports')->info($company);

        // Pass the data to the Blade view
        $data = compact('company');

        Log::channel('');
        // Load the Blade view and pass the data to it
        $pdf = PDF::loadView('admin.reports.inventory', $data);

        // Download the generated PDF
        return $pdf->stream('Inventory Report.pdf');

    }

    public function downloadInventoryReport()
    {
        $user = Auth::user();

        Log::channel('reports')->info("Getting Inventory Report");
        Log::channel('reports')->info("Retrieved User::{$user->id} for Inventory report");
        $company = Company::with('items')
            ->find($user->company_id);

        Log::channel('reports')->info("Retrieved Company::{$company->id}");

        // Pass the data to the Blade view
        $data = compact('company');

        Log::channel('');
        // Load the Blade view and pass the data to it
        $pdf = PDF::loadView('admin.reports.inventory', $data);

        // Download the generated PDF
        return $pdf->download('Inventory Report.pdf');

    }

    public function generateProjectsReport() {
        $user = Auth::user();

        Log::channel('reports')->info("Getting Inventory Report");
        Log::channel('reports')->info("Retrieved User::{$user->id} for Projects report");
        $company = Company::with('projects.client', 'projects.tasks', 'projects.material.item', 'projects.material.user')
            ->find($user->company_id);

        Log::channel('reports')->info("Retrieved Company::{$company->id}");
        Log::channel('reports')->info($company);

        // Pass the data to the Blade view
        $data = compact('company');

        Log::channel('');
        // Load the Blade view and pass the data to it
        $pdf = PDF::loadView('admin.reports.projects', $data);

        // Download the generated PDF
        return $pdf->stream('Projects Report.pdf');

    }
}
