<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Subscription;
use Illuminate\Support\Facades\Log;

class SubscriptionsController extends Controller
{
    public function index(){
        $subscriptions = Subscription::with('company', 'plan')->get();
        Log::channel('subscriptions')->info('Retrieving all Subscriptions');
        Log::channel('subscriptions')->debug($subscriptions);
        return view('admin.subscriptions.index', ['subscriptions' => $subscriptions]);
    }
    public function create(){}
    public function store(){}
}
