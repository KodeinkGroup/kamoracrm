<?php

namespace App\Http\Controllers\Admin;
use App\Mail\WelcomeEmail;
use App\Models\{Company, Estimate, Invoice, LeadSource, Prospect, Task, User};
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Log;
use App\Services\{FacebookService, TwitterService};

class DashboardController extends Controller
{
    protected $facebookService;
    protected $twitter;


    public function __construct(FacebookService $facebookService, TwitterService $twitter){
        $this->middleware('auth:api')->except('index');
        $this->middleware('auth');
        $this->facebookService = $facebookService;
        $this->twitter = $twitter;
    }

    public function index()
    {
        $user = Auth::user();

        $userCompany = Company::where('id', $user->company_id)->first();

        if($user->role === 'technician' || $user->role === 'system_admin' ) {
            return redirect()->route('admin.projects.index');
        }

        if($user->role === 'inventory_manager') {
            return redirect()->route('admin.items.index');
        }

        if ($userCompany->industry_id == 0 && $user->role === 'admin') {
//            Log::channel('dashboard')->info("Company Data:$userCompany->name");
            return redirect()->route('admin.company.create');
        }

        // Sales And Estimates
        $monthsOfTheYear = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

        $sales = DB::table('months_of_the_year')
            ->select('name', DB::raw("(SELECT SUM(ip.amount)
                FROM invoice_payments ip
                LEFT JOIN invoices i ON i.id = ip.invoice_id
                WHERE name = monthName(payment_date)
                AND i.invoice_status_id = 4
                AND year(now()) = year(payment_date)
                AND captured_by = {$user->id}
                ) AS total"))
            ->groupBy('name')
            ->get();

        $unpaid = DB::table('months_of_the_year')
            ->select('name', DB::raw("(SELECT SUM(amount)
                FROM invoices
                WHERE name = MONTHNAME(due_date) AND YEAR(NOW()) = YEAR(due_date)
                AND deleted_at IS NULL
                AND created_by = {$user->id}
                AND invoice_status_id in (2,3,6)) AS total"))
            ->groupBy('name')
            ->get();

        $quotations = DB::table('months_of_the_year')
            ->select('name', DB::raw("(SELECT SUM(amount) FROM estimates
             WHERE name = MONTHNAME(created_at)
            AND YEAR(NOW()) = YEAR(created_at)
            AND deleted_at IS NULL
            AND created_by = {$user->id}
            AND estimate_status_id in (2,3,4)) AS total"))
            ->groupBy('name')
            ->get();

        $payments = [];
        $estimates = [];
        $duePayments = [];

        foreach ($sales as $sale) {
            if($sale->total == null){
                $payments[] = 0;
            } else {
                $payments[] = $sale->total;
            }
        }

        foreach ($unpaid as $duePayment) {
            if($duePayment->total == null){
                $duePayments[] = 0;
            } else {
                $duePayments[] = $duePayment->total;
            }
        }

        foreach ($quotations as $quote) {
            if($quote->total == null){
                $estimates[] = 0;
            } else {
                $estimates[] = $quote->total;
            }
        }

        // Lead Sources
        $leadSources = $this->getLeadSourcesData($userCompany->id);

        // Tasks
        $tasksInProgressCount = Task::with('status')
            ->where('status_id', 2)
            ->whereHas('project', function($query) use ($userCompany) {
                $query->where('company_id', $userCompany->id);
            })
            ->orWhere(function($query) use ($userCompany) {
                $query->whereNull('project_id')
                    ->whereHas('assignee', function($query) use ($userCompany) {
                        $query->where('company_id', $userCompany->id);
                    });
            })
            ->count();
        $tasksDoneCount = Task::with('status')
            ->where('status_id', 4)
            ->whereHas('project', function($query) use ($userCompany) {
                $query->where('company_id', $userCompany->id);
            })
            ->orWhere(function($query) use ($userCompany) {
                $query->whereNull('project_id')
                    ->whereHas('assignee', function($query) use ($userCompany) {
                        $query->where('company_id', $userCompany->id);
                    });
            })
            ->count();
        $tasksOverdueCount = Task::with('status')
            ->where('status_id', 5)
            ->whereHas('project', function($query) use ($userCompany) {
                $query->where('company_id', $userCompany->id);
            })
            ->orWhere(function($query) use ($userCompany) {
                $query->whereNull('project_id')
                    ->whereHas('assignee', function($query) use ($userCompany) {
                        $query->where('company_id', $userCompany->id);
                    });
            })
            ->count();

        Log::channel('dashboard')->info("Task Counts");
        Log::channel('dashboard')->debug($tasksInProgressCount);

        Log::channel('dashboard')->info("Facebook Data");
//        Log::channel('dashboard')->info((array)$this->facebookService);
//        $likes = $this->facebookService->getUser();

//        Log::channel('dashboard')->info($likes);

        Log::channel('dashboard')->info("Twitter Data");
//        $twitterData = $this->showTimeline();

//        Log::channel('dashboard')->info($twitterData);


        $data = [
            'leads' => $userCompany->prospects()->where('prospect_type_id', 1)->get()->count(),
            'prospects' => $userCompany->prospects()->where('prospect_type_id', 2)->get()->count(),
            'accounts' => $userCompany->prospects()->where('prospect_type_id', 3)->get()->count(),
            'deals' => $user->estimates()->get()->count(),
            'monthsOfTheYear' => $monthsOfTheYear,
            'payments' => $payments,
            'duePayments' => $duePayments,
            'estimates' => $estimates,
            'projects' =>  $userCompany->projects,
            'leadSources' => $leadSources['data'],
            'leadSourceLabels' => $leadSources['labels'],
            'tasks' => [
                'inProgress' => $tasksInProgressCount,
                'done' => $tasksDoneCount,
                'overdue' => $tasksOverdueCount,
            ]
        ];

        return view('admin.index', ['data' => $data]);
    }

    public function dashboardData(){
        $data = [
            'leads' => Prospect::join('company_prospect', 'company_prospect.prospect_id', 'prospects.id')
                ->where('prospect_type_id', 1)
                ->where('company_prospect.created_by', Auth::id())
                ->get()
                ->count(),
            'prospects' => Prospect::join('company_prospect', 'company_prospect.prospect_id', 'prospects.id')
                ->where('prospect_type_id', 2)
                ->where('company_prospect.created_by', Auth::id())
                ->get()
                ->count(),
            'accounts' => Prospect::join('company_prospect', 'company_prospect.prospect_id', 'prospects.id')
                ->where('prospect_type_id', 3)
                ->where('company_prospect.created_by', Auth::id())
                ->get()->count(),
            'deals' => Estimate::where('created_by', Auth::id())->get()->count()

        ];

            $leads = Prospect::join('company_prospect', 'company_prospect.prospect_id', 'prospects.id')
            ->where('prospect_type_id', 1)
            ->where('company_prospect.created_by', Auth::id())
            ->get()
            ->count();

        return $data;
    }

    /**
     *
     * Prospect channel count for Dashboard cards
     *
     * @param string $channel
     * @return int
     */
    public function getProspectCount(string $channel) {
        return DB::table('prospects')
            ->join('lead_sources', 'lead_sources.id', 'prospects.source_id')
            ->where('lead_sources.name', $channel)
            ->get()->count();
    }

    public function getLeadSourcesData($companyId)
    {
        $leadSourcesData = Prospect::selectRaw('lead_sources.name, COUNT(prospects.id) as count')
            ->join('lead_sources', 'prospects.source_id', '=', 'lead_sources.id')
            ->where('prospects.company_id', $companyId)
            ->groupBy('lead_sources.name')
            ->pluck('count', 'lead_sources.name');

        // Preparing data for Chart.js
        $labels = $leadSourcesData->keys();
        $counts = $leadSourcesData->values();

        return [
            'labels' => $labels,
            'data' => $counts
        ];
    }

    public function showTimeline()
    {
        Log::channel('dashboard')->info("Twitter Data::Init");

        $tweets = $this->twitter->getUserTimeline('KamoraSoftware', 10);
        if(!empty($tweets)){
            Log::channel('dashboard')->info("Here are Tweets");
            Log::channel('dashboard')->info($tweets);
        } else {
            Log::channel('dashboard')->info("Tweets returned empty");

        }

        Log::channel('dashboard')->info("Twitter Data::End");

//        return view('twitter.timeline', compact('tweets'));
    }
}
