<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Industry;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class IndustriesController extends Controller
{
    public function index() {
        $industries = Industry::withCount('companies')->get();
        Log::info("Industries");
        Log::info($industries);
        return view('admin.industries.index', ['industries' => $industries]);
    }
}
