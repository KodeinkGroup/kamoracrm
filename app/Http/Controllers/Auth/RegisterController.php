<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Alert;
use App\Models\Company;
use App\Models\Industry;
use App\Models\SubscriptionPlan;
use App\Providers\RouteServiceProvider;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'business_name' => ['required', 'string', 'max:255'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Models\User
     */
    protected function create(array $data)
    {

        $now = Carbon::now();
        $company = Company::create([
                'name' => $data['business_name'],
                'industry_id' => 0
        ]);

        $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'company_id' => $company->id
        ]);

        Log::channel('subscriptions')->info('Subscription');
        Log::channel('subscriptions')->info(SubscriptionPlan::find($data['subscription_plan_id']));

        // Create a subscription
        $company->subscription()->create([
            'subscription_plan_id' => $data['subscription_plan_id'],
            'subscription_status_id' => 4,
            'trial_active' => 1,
            'trial_start_date' => now(),
            'trial_end_date' => now()->addDays(30),
            'start_date' => now()->addDays(45), // 15 days grace trial period
            'end_date' => now()->addDays(SubscriptionPlan::find($data['subscription_plan_id'] ?? 366)->duration),
        ]);

//        $alert = new Alert;
//
//        $alert->addressedTo = $user->id;
//        $alert->createdBy = 1;
//        $alert->status = 1;
//
//        $subscription->alerts()->save($alert);

        return $user;
    }
}
