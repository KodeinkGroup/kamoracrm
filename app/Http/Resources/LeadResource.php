<?php

namespace App\Http\Resources;

use App\Http\Resources\SourceResource;
use Illuminate\Http\Resources\Json\JsonResource;


class LeadResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'business_name' => $this->business_name,
            'email' => $this->email,
            'designation' => $this->designation,
            'logo' => $this->logo,
            'location' => $this->location,
            'mobile_no' => $this->mobile_no,
            'source' => LeadSourceResource::make($this->whenLoaded('source')),
        ];
    }
}
