<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class InvoiceResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'subject' => $this->subject,
            'reference_no' => $this->reference_no ?: "INV-".$this->id,
            'discount' => $this->discount,
            'amount' => $this->amount,
            'notes' => $this->notes,
            'terms' => $this->terms,
            'prospect_id' => $this->prospect_id,
            'prospect' => ProspectResource::make($this->whenLoaded('prospect')),
            'user' => new UserResource($this->whenLoaded('user')),
            'items' => ItemResource::collection($this->whenLoaded('items')),
            'invoice_type' => $this->invoice_type,
            'invoice_status_id' => $this->invoice_status_id,
            'status' => InvoiceStatusResource::make($this->whenLoaded('status')),
            'due_date' => $this->due_date,
            'created_at' => $this->created_at,
            'frequency' => $this->frequancy,
            'run_from' => $this->run_from,
            'run_until' => $this->run_until,
            'next_run' => $this->next_run,
            'last_run' => $this->last_run,
        ];
    }
}
