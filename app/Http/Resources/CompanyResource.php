<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CompanyResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'email' => $this->email,
            'contact_number' => $this->contact_numnber,
            'type' => $this->size,
            'size' => $this->bbbee_level,
            'street' => $this->street,
            'complex' => $this->complex,
            'surburb' => $this->surburb,
            'city' => $this->city,
            'province' => $this->province,
            'country' => $this->country,
            'user' => UserResource::make($this->whenLoaded('user'))
        ];
    }
}
