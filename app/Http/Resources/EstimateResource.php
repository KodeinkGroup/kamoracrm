<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class EstimateResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'reference_no' => $this->reference_no,
            'subject' => $this->subject,
            'discount' => $this->discount,
            'amount' => $this->amount,
            'notes' => $this->notes,
            'terms' => $this->terms,
            'prospect_id' => $this->prospect_id,
            'prospect' => ProspectResource::make($this->whenLoaded('prospect')),
            'status' => EstimateStatusResource::make($this->whenLoaded('status')),
            'user' => new UserResource($this->whenLoaded('user')),
            'items' => ItemResource::collection($this->whenLoaded('items')),
            'expiry_date' => $this->expiry_date,
            'created_at' => $this->created_at

        ];
    }
}
