<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;

class Prospect extends Model
{
    use HasFactory, Notifiable, SoftDeletes;

    protected $fillable = ['name', 'email','business_name', 'mobile_no', 'salesperson',
            'location','prospect_type_id', 'source_id', 'designation'];

    protected $guarded = [];

    /**
     * Prospect Contact Details
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function contact()
    {
        return $this->hasOne(ProspectContact::class);
    }

    /**
     * Pretty  formated Created At Date for View
     *
     * @return false|string
     */
    public function getPrettyCreatedAttribute()
    {
        return date('F d, Y', strtotime($this->created_at));
    }

    /**
     * Lead sources relation
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */

    public function source()
    {
        return $this->hasOne(LeadSource::class, 'id', 'source_id' );
    }

    /**
     * Prospect Type
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function type()
    {
        return $this->hasOne(ProspectType::class, 'id', 'prospect_type_id');
    }

    /**
     * Estimates linked with the Prospect
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function estimates()
    {
        return $this->hasMany(Estimate::class);
    }

    /**
     * The company the Prospect belongs to
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function company()
    {
        return $this->belongsTo(Company::class);
    }
}
