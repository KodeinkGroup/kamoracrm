<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Task extends Model
{
    use HasFactory, SoftDeletes;

    public $timestamps = true;
    protected $fillable = ['title', 'summary', 'type','status_id', 'assignee', 'due_date', 'priority_id',
        'time_estimate', 'time_spent', 'project_id', 'created_by', 'sprint_id'];

    protected $casts = [
        'due_date' => 'datetime'
    ];

    public function project()
    {
        return $this->belongsTo(Project::class);
    }

    public function assignee() {
        return $this->belongsTo(User::class, 'assignee');
    }

    public function reporter() {
        return $this->belongsTo(User::class, 'created_by');
    }

    public function status() {
        return $this->belongsTo(TaskStatus::class);
    }

    public function priority() {
        return $this->belongsTo(TaskPriority::class);
    }

    public function sprint()
    {
        return $this->belongsTo(Sprint::class);
    }

    public function alerts()
    {
        return $this->morphMany(Alert::class, 'subject');
    }

    public function timeLog()
    {
        return $this->hasMany(TimeLog::class);
    }

    public function materialRequests()
    {
        return $this->hasMany(MaterialRequest::class);
    }

}
