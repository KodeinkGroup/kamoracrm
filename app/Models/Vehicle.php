<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Vehicle extends Model
{
    use HasFactory;

    protected $fillable = [
        'make', 'model', 'year', 'vin', 'license_plate', 'fleet_number', 'ownership',
        'odometer_reading', 'fuel_type', 'status', 'transmission_type', 'last_service_date', 'next_service_date',
        'department_id'
    ];

        public function department()
    {
        return $this->belongsTo(Department::class);
    }

    public function trips()
    {
        return $this->hasMany(Trip::class);
    }

    public function fuelUsages()
    {
        return $this->hasMany(FuelUsage::class);
    }


//    public function maintenanceRecords()
//    {
//        return $this->hasMany(MaintenanceRecord::class);
//    }


//    public function inspections()
//    {
//        return $this->hasMany(Inspection::class);
//    }

}
