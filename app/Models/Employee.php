<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;

class Employee extends Model
{
    use HasFactory,SoftDeletes, Notifiable;

    public $timestamps = true;

    protected $fillable = ['user_id', 'company_id', 'employee_number','position','initials', 'title', 'full_names','surname',
        'id_no', 'mobile_number','appointment_date','next_of_kin_name', 'next_of_kin_relationship','next_of_kin_mobile_number',
        'salary', 'date_of_birth', 'id_type','employment_type','available_leave_days',
        'street', 'suburb', 'city', 'province', 'country', 'zip_code'];

    protected $casts = [
        'appointment_date' => 'datetime'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function alerts()
    {
        return $this->morphMany(Employee::class, 'subject');
    }

    public function leaveType()
    {
        return $this->belongsTo(LeaveType::class, 'leave_type_id');
    }
}
