<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'type', 'size', 'BBBEE_level', 'email', 'contact_number','registration_number', 'logo_url',
        'street', 'surburb', 'city', 'province','zip_code', 'year_registered', 'country', 'vat_number', 'user_id', 'industry_id'];

    public $timestamps = true;

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function subscription()
    {
        return $this->hasOne(Subscription::class);
    }

    public function users()
    {
        return $this->hasMany(User::class);
    }

    public function prospects()
    {
        return $this->hasMany(Prospect::class );
    }

    public function projects()
    {
        return $this->hasMany(Project::class);
    }

    public function employees() {
       return $this->hasMany(Employee::class);
    }

    public function items()
    {
        return $this->hasMany(Item::class);
    }

    public function departments()
    {
        return $this->hasMany(Department::class);
    }

    public function industry()
    {
        return $this->belongsTo(Industry::class);
    }

    public function fleets()
    {
        return $this->hasMany(Fleet::class);
    }

}
