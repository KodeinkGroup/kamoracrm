<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laravel\Passport\HasApiTokens;

class Estimate extends Model
{
    use HasFactory, SoftDeletes, HasApiTokens;

    public $timestamps = true;

    protected $fillable = ['reference_no', 'subject', 'terms', 'notes', 'expiry_date', 'amount',
        'discount', 'prospect_id', 'estimate_status_id', 'created_by', 'company_id'];

    public function items()
    {
        return $this->belongsToMany(Item::class, 'estimate_item')->withPivot([
            'estimate_id', 'item_id', 'quantity', 'discount'
        ]);
    }

    public function status()
    {
        return $this->hasOne(EstimateStatus::class, 'id', 'estimate_status_id');
    }

    public function prospect()
    {
        return $this->belongsTo(Prospect::class, 'prospect_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'created_by', 'id');
    }

}
