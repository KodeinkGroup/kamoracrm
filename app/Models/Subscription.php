<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;

class Subscription extends Model
{
    use HasFactory, Notifiable;

    protected $fillable = ['user_id', 'subscription_plan_id', 'subscription_status_id', 'trial_active',
        'trial_start_date','trial_end_date', 'start_date', 'end_date'];

    protected $casts = [
        'start_date' => 'datetime',
        'end_date' => 'datetime'
    ];


    // Accessor to calculate days left in trial
    public function getTrialDaysLeftAttribute()
    {
        // Check if the trial is active and the trial_end_date is set
        if ($this->trial_active && $this->trial_end_date) {
            $today = Carbon::now();
            $end = Carbon::parse($this->trial_end_date);

            // Calculate the difference in days
            $daysLeft = $today->diffInDays($end, false);

            // If days left is negative, trial has ended
            return $daysLeft > 0 ? $daysLeft : 0;
        }

        // Return 0 if the trial is not active or end date is not set
        return 0;
    }
    public function plan() {
        return $this->belongsTo(SubscriptionPlan::class, 'subscription_plan_id');
    }

    public function company() {
        return $this->belongsTo(Company::class);
    }
}
