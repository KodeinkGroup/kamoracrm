<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laravel\Passport\HasApiTokens;

class Invoice extends Model
{
    use HasFactory, SoftDeletes, HasApiTokens;

    public $timestamps = true;

    protected $fillable = ['subject', 'terms', 'notes', 'due_date', 'amount','discount', 'prospect_id',
        'created_by', 'invoice_status_id', 'invoice_type', 'frequency', 'run_from', 'run_until', 'next_run','last_run', 'status'];

    protected $casts = [
        'created_at' => 'datetime',
        'run_from' => 'datetime',
        'run_until' => 'datetime',
        'next_run' => 'datetime',
    ];

    public function items()
    {
        return $this->belongsToMany(Item::class, 'invoice_item')->withPivot([
            'invoice_id', 'item_id', 'quantity', 'discount'
        ]);
    }

    public function status()
    {
        return $this->hasOne(InvoiceStatus::class, 'id', 'invoice_status_id');
    }

    public function payments ()
    {
        return $this->hasMany(InvoicePayment::class, 'invoice_id', 'id');
    }

    public function prospect()
    {
        return $this->belongsTo(Prospect::class, 'prospect_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'created_by', 'id');
    }

    public function alerts()
    {
        return $this->morphMany(Alert::class, 'subject');
    }
}
