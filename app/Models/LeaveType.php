<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LeaveType extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'description', 'accrual_rate', 'days_per_year', 'carry_over'];  // whether the leave can be carried over to the next year

    public function leaves()
    {
        return $this->hasMany(Leave::class);
    }

    public function employee()
    {
        return $this->belongsTo(Employee::class);
    }
}
