<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Alert extends Model
{
    use HasFactory;

    protected $fillable = ['subject', 'subject_id',' status_id',  'created_by', 'addressed_to'];

    public function subject()
    {
        return $this->morphTo();
    }

    public function createdBy()
    {
        return $this->belongsTo(User::class, 'created_by');
    }

    public function addressedTo()
    {
        return $this->belongsTo(User::class, 'addressed_by');
    }

    public function task()
    {
        return $this->belongsTo(Task::class, 'subject_id');
    }
}
