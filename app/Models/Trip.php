<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Trip extends Model
{
    use HasFactory;

    protected $fillable = ['start_time', 'end_time', 'start_odometer_reading', 'end_odometer_reading', 'start_location',
        'end_location', 'vehicle_id', 'driver_id', 'distance', 'description', 'authorised_by'
        ];

    protected $casts = [
        'end_time' => 'datetime',
        'start_time' => 'datetime'
    ];

    public function vehicle()
    {
        return $this->belongsTo(Vehicle::class);
    }

    public function driver()
    {
        return $this->belongsTo(Employee::class, 'driver_id');
    }

    public function fuelUsage()
    {
        return $this->hasOne(FuelUsage::class);
    }
}
