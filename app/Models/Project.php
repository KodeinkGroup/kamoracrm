<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Project extends Model
{
    use HasFactory, SoftDeletes;

    public $timestamps = true;

    protected $fillable = ['name', 'summary', 'company_id', 'account_id', 'department_id', 'status', 'color', 'start_date', 'end_date'];

    protected $casts = [
        'start_date' => 'datetime',
        'end_date' => 'datetime'
    ];

    public function tasks() {
        return $this->hasMany(Task::class);
    }

    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    public function material()
    {
        return $this->hasMany(MaterialRequest::class);
    }

    public function client() {
        return $this->belongsTo(Prospect::class, 'account_id');
    }

    public function department()
    {
        return $this->belongsTo(Department::class);
    }

}
