<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laravel\Passport\HasApiTokens;

class Item extends Model
{
    use HasFactory, SoftDeletes, HasApiTokens;

    public $timestamps = true;

    protected $fillable = ['name', 'description', 'rate', 'company_id', 'quantity','category', 'serial_number', 'brand', 'supplier', 'condition', 'deleted_at'];

    public function estimate()
    {
        return $this->belongsToMany(Estimate::class, 'estimate_item')->withPivot(['estimate_id', 'item_id']);
    }
    public function invoice()
    {
        return $this->belongsToMany(Invoice::class, 'invoice_item')->withPivot(['invoice_id', 'item_id']);
    }

    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    public function materialRequest()
    {
        return $this->belongsTo(MaterialRequest::class);
    }

}
