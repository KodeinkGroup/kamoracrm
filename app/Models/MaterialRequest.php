<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;

class MaterialRequest extends Model
{
    use Notifiable, HasFactory, SoftDeletes;

    protected $fillable = ['item_id', 'project_id', 'task_id', 'requested_by', 'request_date', 'issued_to', 'quantity', 'status',
        'approved_by', 'approved_date'];

    public function item()
    {
        return $this->hasOne(Item::class, 'id', 'item_id');
    }

    public function project(){
        return $this->belongsTo(Project::class, 'project_id');
    }

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'requested_by');
    }

    public function approvedBy()
    {
        return $this->hasOne(User::class, 'id', 'approved_by');
    }

    public function assignedTo()
    {
        return $this->hasOne(User::class, 'id', 'assigned_to');
    }

    public function alerts()
    {
        return $this->morphMany(Alert::class, 'subject');
    }

}
