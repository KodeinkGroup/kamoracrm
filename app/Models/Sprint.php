<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class Sprint extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'goals', 'status', 'start_date', 'end_date'];

    protected $dates = [
        'start_date', 'end_date',
    ];

    public function getRemainingDaysAttribute()
    {
        $today = Carbon::today();
        if ($this->end_date && $today->lte($this->end_date)) {
            return $today->diffInDays($this->end_date);
        }

        return 0; // Sprint has ended or no end date set
    }

    public function tasks(){
        return $this->hasMany(Task::class);
    }
}
