# Kamora Software

**Product Version:** 2.20.0

**Description**: Kamora Software is a Software for Startups, Small and Medium Businesses. The product is intended to bring effective and efficient leads, prospecting, quoting, invoicing, reporting,monitoring and management of the business activities.

#### Versioning
Kamora Software will use Semantic versioning when releasing new versions of the platform.

```
Version x.x.x

MajorRelease.MinorRelease.Patch

```
**Major Releases** are releases that affect how the users interact with the platform overall and might have to make neccessary adjustments to continue using the product.

**Minor Releases** are features that extends the functionality that users can access.

**Patch Releases** are small releases and bug fixes of the previous version.

## Setup

### Local Setup
#### Requirements
```
    PHP:8.0 | 8.2
    MySQL
    Composer 2.2.x
    NodeJS 14.8.*
    Vue 3
     
```
Clone the Repository from GitLab
```
$ git clone git@gitlab.com:KodeinkGroup/kamoracrm.git
```

After cloning the repository from git then install dependencies on your local machine.

```
$ composer update  // For updating laravel packages
$ composer install // For installing laravel packages
$ npm install // For installing node packages
```

For Mac Run:
``` $ cp .env.example .env ```

For Windows Run:
``` copy .env.example .env ```

Generate application key
``` php artisan key:generate ```

Storage link
``` php artisan storage:link ```

Run the project on your local machine
```php artisan serve ```

Run the watcher for Vue Changes on a different terminal
```npm run watch```


---
### Development

##### Libraries

- domPdf - https://github.com/barryvdh/laravel-dompdf
---

### Deployment

##### Frontend(VueJs)

After pulling the changes on the master branch run the below command

`npm run production`

##### API
After Pulling the master branch on the environment run the following commands

`php artisan optimize:clear`

## Modules

### Users

#### Master Users
Users that have full control of the System, can create other Master Users, Admin Users, and Basic users.

#### Admin Users
- System Admin - Users that full control of their company system. Can Add,Delete, Edit and View other users on their company.
- Sales Admin - Manage Sales and company Financials, Manage Contacts and Pipelines. Create Estimates, Invoices, and Payment Management
- Inventory Manager - Manage Inventory and approve material requests
- Project Leads - Users that can create projects, and have an overview of projects
- Project Supervisors - Users that manage technicians for a given project and can approve and disapprove status of tasks

#### Managers
- Inventory Managers - Manage Inventory And accept Material Requests
- Project Managers - Manage Projects
- Fleet Managers - Manage Fleet in the company
#### Basic Users
Users that have basic company functions. They can see assigned Tasks if the company has privileges.
- Technicians/Developers - Can view their tasks, can update tasks statuses, can also request material
- Sales Agent - Create permitted sales features

### Contact Management
To Capture Leads, Prospects, Contacts, Accounts and Activities revolving around Contacts on the System and move them across the sales pipeline.

***These features were added***
- Contact Activity: Adding a new contact
- Prospect Activity - Leads activities and converting leads into prospects

##### Leads Management
Potential clients for the business
- Potential customers that have not yet shown any interest in buying or procuring the company's services, they could be meeting certain criteria.
- Leads need to be qualified into prospects according to a certain criteria that qualifies them into prospects
- A lead can also be a person who has responded to a marketing campaign

##### Prospect Management
Potential clients for the business that have shown interest in buying, or procuring services. they might also have active deals. 

#### Accounts
Prospects that have been qualified as Clients and have acquired the business's products and/or services before.
___

### Sales
#### Estimates
A company can create estimates and export them into PDF and download estimates.
An estimate can also be marked as Sent, Accepted, and/or Rejected.

#### Invoices
Invoices modules capture the invoices generate, invoices can be converted from Estimates, or can be created from scratch

#### Recurring Invoices
Recurring Invoices are invoices that go to the same client more than once, have exactly same items, and amount. Like hosting services invoice for an example.

#### Payments
Recording of Payments for related Invoices

* Capturing Invoice Payments and adjusting balance and updating invoice Statuses

### HR Functions
#### Employee Information Management:
- Centralized employee database
- Personal details, employment history, and contact information
- Document management
#### Onboarding and Offboarding:
- Automated workflows for new hires and exits
- Orientation and training scheduling
- Exit interviews and clearance processes 

#### Attendance and Time Tracking:

-Time clock integration
- Absence and leave management
- Overtime tracking
#### Payroll Management:

- Salary and wage calculations
- Tax and compliance management
- Direct deposit and payroll processing

#### Benefits Administration:

- Health insurance, retirement plans, and other benefits management
- Open enrollment and benefits selection
- Compliance with benefits regulations

### Talent Management
#### Recruitment and Applicant Tracking:
- Job posting and candidate sourcing
- Resume screening and applicant tracking
- Interview scheduling and evaluation

#### Performance Management:
- Goal setting and tracking
- Performance reviews and appraisals
- Feedback and recognition systems
#### Learning and Development:

- Training programs and course management
- Skills and competency tracking
- E-learning and development plans

#### Succession Planning:

- Identification of key roles and successors
- Career pathing and talent development
- Leadership development programs

### Employee Engagement
#### Employee Self-Service:

- Access to personal and employment information
- Leave requests and approvals
- Benefits selection and updates

#### Employee Surveys and Feedback:
- Pulse surveys and engagement tracking
- Suggestion boxes and feedback channels
- Sentiment analysis

### Reporting and Analytics

#### HR Reporting and Analytics:

- Workforce analytics and dashboards
- Compliance and audit reports
- Custom report generation
#### HR Metrics and KPIs:

- Turnover rates, time-to-hire, and other key metrics
- Benchmarking and trend analysis
- Data-driven decision making
### Compliance and Security
#### Compliance Management:

- Labor law and regulation adherence
- EEO, OSHA, and other regulatory reporting
- Policy and procedure management

#### Security and Data Privacy:

- Role-based access control
- Data encryption and secure storage
- GDPR and other data protection compliance
### Integration and Customization
#### System Integration:
- Integration with other enterprise systems (ERP, CRM, etc.)
- API and third-party application support
#### Single sign-on (SSO) capabilities
#### Customization and Scalability:
- Configurable workflows and processes  
- Custom fields and forms
- Scalability to grow with the organization
### Communication and Collaboration
#### Internal Communication Tools:
- Messaging and chat systems
- Company announcements and newsletters
- Event and meeting scheduling
#### Team Collaboration:
- Project management and team collaboration tools
- Document sharing and version control
- Social and collaboration networks

### Project Management

#### Tasks
Managing Projects and Tasks
- Filters

#### Sprint
- Sprint Statuses 

Status 1 - In Draft
Status 2 - In Progress
Status 3 - Completed/Closed
Status 4 - Overdue

### Fleet Management

#### Vehicle Statuses
- Available: The vehicle is in good condition and available for use.
- In Use: The vehicle is currently being used or is on a trip.
- Maintenance: The vehicle is undergoing regular maintenance or service.
- Repair: The vehicle is being repaired due to a breakdown or damage.
- Reserved: The vehicle has been reserved for future use.
- Out of Service: The vehicle is temporarily out of service and not available for use.
- Decommissioned: The vehicle has been retired from the fleet and is no longer in use.
- Awaiting Inspection: The vehicle is waiting for an inspection before it can be used again.
- Pending Assignment: The vehicle is ready and waiting to be assigned to a task or user.
- Sold: The vehicle has been sold and is no longer part of the fleet.


### Packages
#### Basic
- Features:
    * Dashboard
    * Leads
      * CRUD
    * Prospects
      * CRUD
    * Accounts
      * CRUD
    * Estimations
      * CRUD
    * Invoices
      * CRUD
    * Users
      * 1 User
      * CRUD
#### Standard
- Features:
    * Dashboard
    * Leads
        * CRUD
    * Prospects
        * CRUD
    * Accounts
        * CRUD
    * Estimations
        * CRUD
    * Invoices
        * CRUD
    * Recurring Invoices
      * CRUD
    * Users
      * 1 User
      * CRUD
    * Activities
#### Premium
- Features:
    * Dashboard
    * Leads
        * CRUD
    * Prospects
        * CRUD
    * Accounts
        * CRUD
    * Sales
      * Estimations
          * CRUD
      * Invoices
          * CRUD
      * Recurring Invoices
          * CRUD
      * Payments
    * Project Management
        * Tasks
    * Reporting
      * Monthly Sales
      * Projects
    * Email Integration
      * Overdue Estimates
      * Overdue Invoices
      * Overdue Tasks
    * Users
      * CRUD
      * Multiple Users
