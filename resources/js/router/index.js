import { createRouter, createWebHistory } from "vue-router";

import CreateEstimate from '../pages/estimates/CreateEstimate.vue';
import Estimates from '../pages/estimates/Estimates.vue';
import EditEstimate from "../pages/estimates/EditEstimate";
import DashboardComponent from '../pages/dashboard/DashboardComponent'
import ShowEstimate from "../pages/estimates/ShowEstimate";
import Invoices from "../pages/invoices/Invoices";
import RecurringInvoices from "../pages/invoices/RecurringInvoices";
import ShowInvoice from "../pages/invoices/ShowInvoice";
import EditInvoice from "../pages/invoices/EditInvoice";
import Tasks from "../pages/tasks/Tasks.vue";
import Projects from "../pages/projects/Projects.vue";
import Payments from "../pages/invoices/Payments.vue";
import Sprints from "../pages/tasks/Sprints.vue";
import Backlog from "../pages/tasks/Backlog.vue";
import ViewTask from "../pages/tasks/ViewTask.vue";
import ViewProjectModal from "../pages/projects/modals/ViewProjectModal.vue";

// import ElementUI from 'element-ui';
// import 'element-ui/lib/theme-chalk/index.css';

// Vue.use(ElementUI);

const routes = [
    {
        path: '/estimates/index',
        name: 'admin.estimates.index',
        component: Estimates
    },
    {
        path: '/estimates/edit/:id',
        name: 'admin.estimates.edit',
        component: EditEstimate,
        props: true
    },
    {
        path: '/estimates/create/:type',
        name: 'admin.estimates.create',
        component: CreateEstimate,
        props: true
    },
    {
        path: '/estimates/:id',
        name: 'admin.estimates.detail',
        component: ShowEstimate
    },
    {
        path: '/admin/dashboard',
        name: 'admin.dashboard',
        component: DashboardComponent
    },
    {
        path: '/invoices/index',
        name: 'admin.invoices.index',
        component: Invoices
    },
    {
        path: '/invoices/invoices-recurring',
        name: 'admin.invoices.listRecurring',
        component: RecurringInvoices
    },
    {
        path: '/invoices/:id',
        name: 'admin.invoices.detail',
        component: ShowInvoice
    },
    {
        path: '/invoices/export/:id',
        name: 'admin.invoices.export',
        component: ShowInvoice
    },
    {
        path: '/invoices/edit/:id',
        name: 'admin.invoices.edit',
        component: EditInvoice,
        props: true
    },
    {
        path: '/invoices/update/:id/status',
        name: 'admin.invoices.update.status',
        component: ShowInvoice
    },
    {
        path: '/invoices/invoice-payments',
        name: 'admin.invoices.listPayments',
        component: Payments
    },
    {
        path: '/tasks/index',
        name: 'admin.tasks.index',
        component: Tasks,
        props: true
    },
    {
        path: '/tasks/create',
        name: 'admin.tasks.create',
        component: Tasks,
        props: true
    },
    {
        path: '/tasks/update/:id/status',
        name: 'admin.tasks.updateStatus',
        component: Tasks,
        props: true
    },
    {
        path: '/tasks/update/:id',
        name: 'admin.tasks.update',
        component: Tasks,
        props: true
    },
    {
        path: '/projects/index',
        name: 'admin.projects.index',
        component: Projects,
        props: true
    },
    {
        path: '/projects',
        name: 'admin.projects.store',
        component: Projects,
        props: true
    },
    {
        path: '/projects/:id',
        name: 'admin.projects.detail',
        component: Projects,
        props: true
    },
    {
        path: '/projects/update/:id',
        name: 'admin.projects.update',
        component: Projects,
        props: true
    },
    {
        path: '/tasks/sprints',
        name: 'admin.tasks.sprints',
        component: Sprints,
        props: true
    },
    {
        path: '/tasks/backlog',
        name: 'admin.tasks.backlog',
        component: Backlog,
        props: true
    },
    {
        path: '/tasks/:id/view',
        name: 'admin.tasks.view',
        component: ViewTask,
        props: true
    },

    {
        path: '/projects/requestMaterial/:id',
        name: 'admin.projects.requestMaterial',
        component: Projects,
        props: true
    },
    {
        path: '/projects/requestMaterial/status/:id',
        name: 'admin.projects.requestMaterial',
        component: Projects,
        props: true
    },
    {
        path: '/tasks/task-types/',
        name: 'admin.tasks.addTaskType',
        component: Tasks,
        props: true
    },
]

export default createRouter({
    history: createWebHistory(),
    routes
});
