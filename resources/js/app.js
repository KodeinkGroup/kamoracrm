import axios from "axios";
import { createApp } from 'vue';
import router from './router';
import CreateEstimate from './pages/estimates/CreateEstimate';
import Estimates from './pages/estimates/Estimates';
import EditEstimate from "./pages/estimates/EditEstimate";
import EditInvoice from "./pages/invoices/EditInvoice";
import DashboardComponent from "./pages/dashboard/DashboardComponent";
import BarChartComponent from "./pages/dashboard/BarChartComponent";
import ShowEstimate from './pages/estimates/ShowEstimate';
import Invoices from "./pages/invoices/Invoices";
import RecurringInvoices from "./pages/invoices/RecurringInvoices";
import ShowInvoice from "./pages/invoices/ShowInvoice";
import ConvertInvoiceTypeModal from "./pages/invoices/modals/ConvertInvoiceTypeModal";
import RecordPaymentModal from "./pages/invoices/modals/RecordPaymentModal";
import TasksComponent from "./pages/tasks/Tasks.vue";
import CreateTaskModal from "./pages/tasks/modals/CreateTaskModal.vue";
import EditTaskModal from "./pages/tasks/modals/EditTaskModal.vue";
import ViewTaskModal from "./pages/tasks/modals/ViewTaskModal.vue";
import Payments from "./pages/invoices/Payments.vue";
import Projects from "./pages/projects/Projects.vue";
import CreateProjectModal from "./pages/projects/modals/CreateProjectModal.vue";
import EditProjectModal from "./pages/projects/modals/EditProjectModal.vue";
import Sprints from "./pages/tasks/Sprints.vue";
import CreateSprintModal from "./pages/tasks/modals/CreateSprintModal.vue";
import Backlog from "./pages/tasks/Backlog.vue";
import ViewProjectModal from "./pages/projects/modals/ViewProjectModal.vue";
import MaterialRequestModal from "./pages/projects/modals/MaterialRequestModal.vue";
import userStore from "./store/src/userStore";
import AddTaskTypesModal from "./pages/tasks/modals/AddTaskTypesModal.vue";
import ViewTask from "./pages/tasks/ViewTask.vue";

// Set up Axios to include credentials and base URL
axios.defaults.withCredentials = true;
axios.defaults.baseURL = process.env.APP_URL;

userStore.setUser();

createApp({
    components: {
        Estimates,
        EditEstimate,
        CreateEstimate,
        DashboardComponent,
        BarChartComponent,
        ShowEstimate,
        Invoices,
        RecurringInvoices,
        ShowInvoice,
        EditInvoice,
        ConvertInvoiceTypeModal,
        RecordPaymentModal,
        TasksComponent,
        CreateTaskModal,
        EditTaskModal,
        Payments,
        Projects,
        CreateProjectModal,
        EditProjectModal,
        Sprints,
        CreateSprintModal,
        Backlog,
        ViewTaskModal,
        ViewProjectModal,
        MaterialRequestModal,
        AddTaskTypesModal,
        ViewTask

    }
}).use(router).mount('#app');


