import { ref } from 'vue';
import axios from 'axios';
import { useRouter } from 'vue-router';

export default function useTasks() {
    const router = useRouter();

    const tasks = ref([]);
    const taskDetail = ref({});
    const task = ref([]);
    const projects = ref([]);
    const users = ref([]);
    const sprint = ref({});
    const sprintTasks = ref([]);

    const getTasks = async (filters = false) => {
        const data = {};
        data.filter = filters ? filters: [];

        let response = await axios.get('/api/tasks', {params:
            data
         });
        tasks.value = response.data;
    }

    const getBacklogTasks = async (type, filters = false) => {
        const data = {};
        data.type = type ? type : 'backlog';
        data.filter = filters ? filters : '';

        let response = await axios.get('/api/tasks', {params:
            data
        });
        tasks.value = response.data;
    }

    const getTask = async (id) => {
        if(id === undefined){
            id = router.currentRoute.value.params.id;
        }

        let response = await axios.get('/api/tasks/'+id);
        taskDetail.value = response.data;
    }

    const getUsers = async () => {
        let response = await axios.get('/api/users');
        users.value = response.data.data;
    }

    const createNewTask = async(data) => {
        let response = await axios.post('/api/tasks/', data);
        task.value = response.data;
    }

    const getProjects = async() => {
        let response = await axios.get('/api/projects');
        projects.value = response.data;
    }

    const updateTaskStatus = async(id, data) => {
        let response = await axios.put('/api/tasks/update/'+id+'/status', data);
        task.value = response.data;
    }

    const updateTask = async (id, data) => {
        let response = await axios.put('/api/tasks/update/'+id, data);
        task.value = response.data;
    }

    const getActiveSprint = async () => {
        let response = await axios.get('/api/sprints/active');
        sprint.value = response.data;

        sprintTasks.value = sprint.value.tasks;
    }

    const createSprint = async (data) => {
        let response = await axios.post('/api/sprints', data);
        // sprint.value = response.data;
        // sprintTasks.value = sprint.value.tasks;
    }

    const updateSprintStatus = async (id, data) => {
        let response = await axios.put('/api/sprints/update/'+id + '/status', data);
        sprint.value = response.data;
        sprintTasks.value = sprint.value.tasks;
    }

    const addTaskToSprint = async (id, data) => {
        let response = await axios.put('/api/tasks/'+id + '/addToSprint', data);
        sprint.value = response.data;
        sprintTasks.value = sprint.value.tasks;
    }

    const removeTaskFromSprint = async (id) => {
        let response = await axios.put('/api/tasks/'+id + '/removeFromSprint');
        sprint.value = response.data;
        sprintTasks.value = sprint.value.tasks;
    }

    const addTaskTypes = async (data) => {
        let response = await axios.post('/api/tasks/task-types', data);
    }

    const submitMaterialRequest = async (data) => {
        let response = await axios.post('/api/projects/requestMaterial/', data);
    }

    const updateMaterialRequestStatus = async (id, data) => {
        let response = await axios.put('/api/projects/requestMaterial/status/' + id, data);
    }

    return {
        getTasks,
        tasks,
        getTask,
        task,
        taskDetail,
        createNewTask,
        projects,
        updateTaskStatus,
        getProjects,
        getUsers,
        users,
        updateTask,
        getActiveSprint,
        sprint,
        sprintTasks,
        updateSprintStatus,
        createSprint,
        addTaskToSprint,
        removeTaskFromSprint,
        getBacklogTasks,
        addTaskTypes,
        submitMaterialRequest,
        updateMaterialRequestStatus
    }
}
