import { ref } from 'vue';
import axios from 'axios';
import { useRouter } from 'vue-router';

export default function useEstimates() {
    const estimates = ref([]);
    const estimate = ref({});
    const router = useRouter();
    const errors = ref('');
    let id = ref('')
    let items = ref([]);
    const customers = ref([]);
    let company = ref({});

    const getEstimates = async () => {
        let response = await axios.get('/api/estimates');
        estimates.value = response.data.data;
    }

    const getEstimate = async (id = null) => {
        if(id == null){
            id = router.currentRoute.value.params.id;
        } else {
            id = id;
        }
        let response = await axios.get('/api/estimates/' + id);
        estimate.value = response.data;

        localStorage.setItem('estimate', JSON.stringify(estimate.value));

       await getUserCompany(estimate.value.user.id);
    }

    const storeEstimate = async (data, model) => {
        errors.value = '';

       try {
          await axios.post('/api/estimates/?type='+model, data);
       } catch (e) {
            if(e.response.status === 500) {
                for(const key in e.response.data.errors) {
                    errors.value += e.response.data.errors[key][0] + ' ';
                }
            }

            if(e.response.status === 422) {
                for(const key in e.response.data.errors) {
                    errors.value += e.response.data.errors[key][0] + ' ';
                }
            }
       }
    }
    const updateEstimate = async (id) => {
        let response = await axios.put('/api/estimates/' + id + '/edit', estimate.value);
        estimate.value = response.data.data;

        await router.push({name: 'admin.estimates.index'});
    }

    const publishEstimate = async (id) => {
        let status = {id: 2};

        let response = await axios.post('/api/estimates/' + id, status);
    }

    const markAsSent = async (id) => {
        let status = {id: 3};

        let response = await axios.post('/api/estimates/' + id, status);
    }

    const acceptEstimate = async (id) => {
        let status = {id: 4};

        let response = await axios.post('/api/estimates/' + id, status);
    }

    const rejectEstimate = async (id) => {
        let status = {id: 6};

        let response = await axios.post('/api/estimates/' + id, status);
    }

    const destroyEstimate = async (id) => {
        await axios.delete('/api/estimates/' + id);
    }

    const convertEstimate = async (id) => {
        let status = {id: 5};

        let response = await axios.post('/api/estimates/' + id, status);
    }

    const getItems = async () => {
        let response = await axios.get('/api/items');
        items.value = response.data.data;
    }

    const getCustomers = async () => {
        let response = await axios.get('/api/customers');
        customers.value = response.data.data;
    }

    const getUserCompany = async (id) => {
       let response = await axios.get('//api/company/' + id);
        company.value = response.data;
    }

    return {
        customers,
        company,
        estimates,
        errors,
        estimate,
        destroyEstimate,
        getEstimates,
        getCustomers,
        getEstimate,
        getItems,
        getUserCompany,
        items,
        storeEstimate,
        updateEstimate,
        markAsSent,
        acceptEstimate,
        rejectEstimate,
        convertEstimate
    }
}
