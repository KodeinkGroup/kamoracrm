import { ref } from 'vue';
import axios from 'axios';
import { useRouter } from 'vue-router';
import da from "element-ui/src/locale/lang/da";

export default function useProjects() {
    const router = useRouter();
    let projects = ref([]);
    let projectDetail = ref({});
    let clients = ref([]);
    let material = ref([]);
    let company = ref({});

    const getProjects = async() => {
        let response = await axios.get('/api/projects');
        projects.value = response.data;
    }

    const getProject = async(id) => {
        let response = await axios.get('/api/projects/'+id);
        projectDetail.value = response.data;
    }

    const getClients = async () => {
        let response = await axios.get('/api/customers');
        clients.value = response.data.data;
    }

    const getCompany = async () => {
        let response = await axios.get('/api/company');
        company.value = response.data;
    }

    const createNewProject = async (data) => {
        let response = await axios.post('/api/projects/', data);
        project.value = response.data;
    }

    const updateProject = async (id, data) => {
        let response = await axios.put('/api/projects/update/' + id, data);
        projectDetail.value = response.data;
    }

    const removeProject = async (id) => {
        let response = await axios.delete('/api/projects/' + id);
    }

    const getMaterial = async (id = null) => {
        let response = await axios.get('/api/items');
        material.value = response.data.data;
    }

    const submitMaterialRequest = async (data) => {
        let response = await axios.post('/api/projects/requestMaterial/', data);
    }

    const updateMaterialRequestStatus = async (id, data) => {
        let response = await axios.put('/api/projects/requestMaterial/status/' + id, data);
    }

    return {
        getProjects,
        projects,
        getClients,
        clients,
        createNewProject,
        getProject,
        updateProject,
        projectDetail,
        getMaterial,
        material,
        submitMaterialRequest,
        updateMaterialRequestStatus,
        getCompany,
        company,
        removeProject
    }
}
