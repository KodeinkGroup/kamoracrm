import { ref } from 'vue';
import axios from 'axios';
import { useRouter } from 'vue-router';

export default function useInvoices() {
    const invoices = ref([]);
    const invoice = ref({});
    const router = useRouter();
    const errors = ref('');
    let id = ref('')
    let items = ref([]);
    const customers = ref([]);
    let company = ref({});
    let payments = ref([]);

    const getInvoices = async () => {
        let type = 'once-off'
        let response = await axios.get('/api/invoices/');
        invoices.value = response.data.data;
    }

    const getRecurringInvoices = async () => {
        console.log("Running Recurring")
        let type = 'recurring'
        let response = await axios.get('/api/invoices-recurring/');
        invoices.value = response.data.data;
    }

    const getInvoice = async () => {
        id = router.currentRoute.value.params.id;
        let response = await axios.get('/api/invoices/' + id);
        invoice.value = response.data;

        localStorage.setItem('invoice', JSON.stringify(invoice.value));
        localStorage.setItem('hideConvertBtn', false);

       await getUserCompany(invoice.value.user.id);
    }

    const getItems = async () => {
        let response = await axios.get('/api/items');
        items.value = response.data.data;
    }

    const updateInvoice = async (id, invoice) => {
        let response = await axios.put('/api/invoices/' + id + '/edit', invoice);
        invoice.value = response.data.data;
        await router.push({name: 'admin.invoices.index'})
    }

    const getCustomers = async () => {
        let response = await axios.get('/api/customers');
        customers.value = response.data.data;
    }

    const getUserCompany = async (id) => {
       let response = await axios.get('/api/company/' + id);
        company.value = response.data;
    }

    const updateStatus = async (id, data) => {
        let response = await axios.put('/api/invoices/update/'+ id + '/status', data);
        if(response.data.invoice_status_id == 8){
            await router.push({name: 'admin.invoices.index'})
        }
        invoice.value = response.data;
    }

    const updateType = async (id, data) => {
        let response = await axios.put('/api/invoices/update/'+ id + '/type', data);
        invoice.value = response.data;
    }

    const processPayment = async (data) => {
        let response = await axios.post('/api/invoices/payments', data);
        invoice.value = response.data;
    }

    const deletePayment = async (id) => {
        let response = await axios.delete('/api/invoices/payments/delete/' + id);
        // await router.push({name: 'admin.payments.index'})
    }

    const downloadInvoice = async (id) => {
        await axios({
            url:'/api/invoices/export/' + id,
            method: 'GET',
            responseType: 'arraybuffer',
        }).then((response) => {
            let blob = new Blob([response.data], {
                type: 'application/pdf'
            })
            let link = document.createElement('a')
            link.href = window.URL.createObjectURL(blob)
            link.download = invoice.pdf
            link.click
        });
    }

    const getPayments = async () => {
        let response = await axios.get('/api/invoice-payments');
        payments.value = response.data;
    }

    return {
        customers,
        company,
        invoices,
        invoice,
        errors,
        getInvoices,
        getRecurringInvoices,
        getInvoice,
        getCustomers,
        getItems,
        getUserCompany,
        items,
        downloadInvoice,
        updateInvoice,
        updateStatus,
        updateType,
        processPayment,
        getPayments,
        payments,
        deletePayment
    }
}
