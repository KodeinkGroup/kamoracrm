import { ref } from 'vue';
import axios from 'axios';
import { useRouter } from 'vue-router';

export default function useDashboard() {
    const cardsData = ref([]);
    const router = useRouter();
    const errors = ref('');

    const getDataCards = async () => {
        let response = await axios.get('/api/dashboard');
        cardsData.value = response.data;
    }

    return {
        cardsData,
        errors,
        router,
        getDataCards
    }
}
