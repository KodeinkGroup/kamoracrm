import {reactive, ref} from "vue";
import axios from "axios";

const state = reactive({user: { id: null, permissions: [] }});

const setUser = async () => {
    try {
        const response = await axios.get('/api/user');
        state.user = response.data;
        console.log("State User");
        console.log(state.user);
    } catch (error) {
        state.user = null;
    }
};

export default {
    state,
    setUser
};
