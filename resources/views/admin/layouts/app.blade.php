    @include('admin.layouts.partials.header')

    <body id="page-top">

        <!-- Page Wrapper -->
        <div id="wrapper">

            <!-- Sidebar -->
            <ul class="navbar-nav sidebar sidebar-light accordion" id="accordionSidebar">

                <!-- Sidebar - Brand -->
                <a class="sidebar-brand d-flex align-items-center justify-content-center" style="background: #fff" href="{{route('admin.dashboard')}}">
                    <div class="sidebar-brand-icon rotate-n-15">
{{--                        <i class="fas fa-laugh-wink"></i>--}}
                    </div>
                    <div class="" style="color: #0D0A0A">Kamora</div>
                </a>


                <!-- Nav Item - Dashboard -->
    @if(\Illuminate\Support\Facades\Auth::user()->role !== 'user' && \Illuminate\Support\Facades\Auth::user()->role !== 'technician'
&& \Illuminate\Support\Facades\Auth::user()->role !== 'inventory_manager' && \Illuminate\Support\Facades\Auth::user()->role !== 'system_admin' &&
 \Illuminate\Support\Facades\Auth::user()->role !== 'project_manager')
                    <!-- Divider -->
                    <hr class="sidebar-divider my-0">

                    <li class="nav-item active">
         <a class="nav-link" href="{{route('admin.dashboard')}}">
             <i class="fas fa-fw fa-tachometer-alt"></i>
             <span>Dashboard</span>
{{--             @if( isset(\Illuminate\Support\Facades\Auth::user()->company->industry) )--}}
{{--             @endif--}}
         </a>
     </li>
     @endif


    @if(\Illuminate\Support\Facades\Auth::user()->role !== 'user' && \Illuminate\Support\Facades\Auth::user()->role !== 'technician' &&
 \Illuminate\Support\Facades\Auth::user()->role !== 'inventory_manager' &&
  \Illuminate\Support\Facades\Auth::user()->role !== 'system_admin' && \Illuminate\Support\Facades\Auth::user()->role !== 'project_manager' )

        <div class="d-none d-sm-block d-md-block">
            <!-- Divider -->
            <hr class="sidebar-divider">
             <!-- Heading -->
             <div class="sidebar-heading">
                 Manage
             </div>

            @if(\Illuminate\Support\Facades\Auth::user()->company->industry->id !== 21)
                 <!-- Nav Item - Pages Collapse Menu -->
                 <li class="nav-item">
                     <a class="nav-link" href="{{route('admin.leads.dashboard')}}" aria-expanded="true">
                         <i class="fas fa-fw fa-users"></i>
                         <span>Leads</span>
                     </a>

                 </li>
                 <li class="nav-item">
                     <a class="nav-link" href="{{route('admin.prospects.dashboard')}}"
                        aria-expanded="true">
                         <i class="fas fa-fw fa-briefcase"></i>
                         <span>Prospects</span>
                     </a>

                 </li>
            @endif
             <li class="nav-item">
                 <a class="nav-link" href="{{route('admin.accounts.index')}}" aria-expanded="true">
                     <i class="fas fa-fw fa-building"></i>
                     <span>Accounts</span>
                 </a>
             </li>
        </div>
    @endif

    @if(\Illuminate\Support\Facades\Auth::user()->role !== 'user' && \Illuminate\Support\Facades\Auth::user()->role !== 'technician'
    && \Illuminate\Support\Facades\Auth::user()->role !== 'system_admin' && \Illuminate\Support\Facades\Auth::user()->role !== 'project_manager')
        <!-- Divider -->
        <hr class="sidebar-divider">

                    <!-- Heading -->
         <div class="sidebar-heading">
             Sales
         </div>
{{--                <!-- Nav Item - Items -->--}}
{{--                <li class="nav-item">--}}
{{--                    <a class="nav-link" href="{{route('admin.items.dashboard')}}">--}}
{{--                        <i class="fas fa-fw fa-table"></i>--}}
{{--                        <span>Items</span></a>--}}
{{--                </li>--}}
       @if(\Illuminate\Support\Facades\Auth::user()->role !== 'inventory_manager')
            @if(\Illuminate\Support\Facades\Auth::user()->company->industry->id !== 21)
                <!-- Nav Item - Estimates -->
                 <li class="nav-item">
                     <a class="nav-link" href="{{route('admin.estimates.index')}}">
                         <i class="fas fa-fw fa-receipt"></i>
                         <span>Estimates</span></a>
                 </li>
            @endif
         <li class="nav-item">
             <a class="nav-link" href="{{route('admin.invoices.index')}}">
                 <i class="fas fa-fw fa-file-invoice"></i>
                 <span>Invoices</span></a>
         </li>
         <li class="nav-item">
             <a class="nav-link" href="{{route('admin.invoices.recurring')}}">
                 <i class="fas fa-fw fa-file-invoice-dollar"></i>
                 <span>Recurring Invoices</span></a>
         </li>
         <li class="nav-item">
             <a class="nav-link" href="{{route('admin.invoices.payments')}}">
                 <i class="fas fa-fw fa-wallet"></i>
                 <span>Payments</span></a>
         </li>
        @endif
         <li class="nav-item">
             <a class="nav-link" href="{{route('admin.items.index')}}">
                 <i class="fas fa-fw fa-boxes"></i>
                 <span>Items</span></a>
         </li>
    @endif
    <!-- Nav Item - Pages Collapse Menu -->
        {{--                <li class="nav-item">--}}
        {{--                      <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages"--}}
        {{--                          aria-expanded="true" aria-controls="collapsePages">--}}
        {{--                          <i class="fas fa-fw fa-folder"></i>--}}
        {{--                          <span>Invoices</span>--}}
        {{--                      </a>--}}
        {{--                      <div id="collapsePages" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">--}}
        {{--                          <div class="bg-white py-2 collapse-inner rounded">--}}
        {{--                              <h6 class="collapse-header">Manage Invoices:</h6>--}}
        {{--                              <a class="collapse-item" href="#">Active</a>--}}
        {{--                              <a class="collapse-item" href="#">Overdue</a>--}}

        {{--                          </div>--}}
        {{--                      </div>--}}
        {{--                </li>--}}


    @if(\Illuminate\Support\Facades\Auth::user()->role !== 'sales_admin')
    <!-- Divider -->
    <hr class="sidebar-divider">
    <!-- Heading -->
                <div class="sidebar-heading">
         Planning
     </div>

{{--                @can('projects.manage')--}}
     <li class="nav-item">
         <a class="nav-link" href="{{route('admin.projects.index')}}">
             <i class="fas fa-fw fa-briefcase"></i>
             <span>Projects</span></a>
     </li>
{{--                    @endcan--}}
     <li class="nav-item">
         <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages"
            aria-expanded="true" aria-controls="collapsePages">
             <i class="fas fa-cubes"></i>
             <span>Tasks</span>
         </a>

         <div id="collapsePages" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
             <div class="bg-white py-2 collapse-inner rounded">
                 <h6 class="collapse-header">Manage Issues:</h6>
                 <a class="collapse-item text-dark" href="{{route('admin.tasks.index')}}"><i class="fas fa-fw fa-tasks task-dark  mr-2"></i><span>Tasks</span>
                 </a>
                 <a class="collapse-item text-dark" href="{{route('admin.tasks.backlog')}}"><i class="fa fa-layer-group  mr-2"></i>Backlog</a>

             </div>
         </div>
     </li>
        @if(\Illuminate\Support\Facades\Auth::user()->role !== 'inventory_manager')
            <li class="nav-item">
                 <a class="nav-link" href="{{route('admin.tasks.sprints')}}">
                     <i class="fas fa-fw fa-running"></i>
                     <span>Sprints</span>
                 </a>
             </li>

            <li class="nav-item">
                <a class="nav-link" href="{{route('admin.schedules.index')}}">
                    <i class="fas fa-fw fa-calendar"></i>
                    <span>Schedules</span>
                </a>
            </li>
        @endif
        @if(\Illuminate\Support\Facades\Auth::user()->role == 'inventory_manager' || \Illuminate\Support\Facades\Auth::user()->role == 'admin' ||
                                 \Illuminate\Support\Facades\Auth::user()->role == 'master' ||  \Illuminate\Support\Facades\Auth::user()->role == 'technician')
            <li class="nav-item">
                <a class="nav-link collapsed" href="{{route('admin.items.listMaterialRequests')}}"
                   aria-expanded="true" >
                    <i class="fas fa-fw fa-gift"></i>
                    <span>Material Requests</span>
                </a>
            </li>
        @endif
    @endif
                <!-- Nav Item - Pages Collapse Menu -->
    @if(\Illuminate\Support\Facades\Auth::user()->role == 'master' || \Illuminate\Support\Facades\Auth::user()->role == 'admin')

                    <hr class="sidebar-divider">

                    <!-- Heading -->
                    <div class="sidebar-heading">
                        Reporting
                    </div>
        <li class="nav-item">
            <a class="nav-link collapsed" href="{{route('admin.reports.managerial')}}"
               aria-expanded="true" target="_blank">
                <i class="fas fa-fw fa-building"></i>
                <span>Management</span>
            </a>
        </li>

        <li class="nav-item">
            <a class="nav-link collapsed" href="{{route('admin.reports.inventory')}}"
               aria-expanded="true" target="_blank">
                <i class="fas fa-fw fa-building"></i>
                <span>Inventory</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link collapsed" href="{{route('admin.reports.projects')}}"
               aria-expanded="true" target="_blank">
                <i class="fas fa-fw fa-building"></i>
                <span>Projects</span>
            </a>
        </li>
    @endif

    @if(\Illuminate\Support\Facades\Auth::user()->role == 'master' || \Illuminate\Support\Facades\Auth::user()->role == 'admin'|| \Illuminate\Support\Facades\Auth::user()->role == 'hr_manager')

                <hr class="sidebar-divider">

    <!-- Heading -->
    <div class="sidebar-heading">
        Human Resources
    </div>

    <li class="nav-item">
        <a class="nav-link collapsed" href="{{route('admin.employees.index')}}"
           aria-expanded="true" >
            <i class="fas fa-fw fa-users"></i>
            <span>Employees</span>
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link collapsed" href="{{route('admin.company.hr.manage.leaveTypes', ['id' => \Illuminate\Support\Facades\Auth::user()->company_id])}}"
           aria-expanded="true" >
            <i class="fas fa-fw fa-users"></i>
            <span>Leave Types</span>
        </a>
    </li>
    @endif

    @if(\Illuminate\Support\Facades\Auth::user()->role == 'master' || \Illuminate\Support\Facades\Auth::user()->role == 'admin')

    <hr class="sidebar-divider">

     <!-- Heading -->
     <div class="sidebar-heading">
         Admin
     </div>
  <!-- Nav Item - Pages Collapse Menu -->
     <li class="nav-item">
           <a class="nav-link collapsed" href="{{route('admin.company.show', ['user' => \Illuminate\Support\Facades\Auth::id()])}}"
               aria-expanded="true" >
               <i class="fas fa-fw fa-building"></i>
               <span>My Company</span>
           </a>
     </li>
    @endif

    @if(\Illuminate\Support\Facades\Auth::user()->role == 'master' || \Illuminate\Support\Facades\Auth::user()->role == 'fleet_manager'
        || \Illuminate\Support\Facades\Auth::user()->role == 'admin' )
    <!-- Heading -->
    <div class="sidebar-heading">
        Fleet
    </div>

        <li class="nav-item">
            <a class="nav-link collapsed" href="{{route('admin.company.fleets.index')}}"
               aria-expanded="true" >
                <i class="fas fa-fw fa-truck-monster"></i>
                <span>Manage Fleets</span>
            </a>
        </li>

        <li class="nav-item">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseVehiclePages"
               aria-expanded="true" aria-controls="collapseVehiclePages">
                <i class="fas fa-car"></i>
                <span>Vehicles</span>
            </a>

            <div id="collapseVehiclePages" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <h6 class="collapse-header">Manage Vehicles:</h6>
                    <a class="collapse-item text-dark" href="{{route('admin.company.vehicles.index')}}"><i class="fas fa-car task-dark  mr-2"></i><span>All Vehicles</span>
                    </a>
                    <a class="collapse-item text-dark" href="{{route('admin.company.vehicles.create')}}"><i class="fa fa-car-side  mr-2"></i>New Vehicle</a>
                </div>
            </div>
        </li>
                    <li class="nav-item">
                        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTripsPages"
                           aria-expanded="true" aria-controls="collapseVehiclePages">
                            <i class="fas fa-car-side"></i>
                            <span>Trips</span>
                        </a>

                        <div id="collapseTripsPages" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
                            <div class="bg-white py-2 collapse-inner rounded">
                                <h6 class="collapse-header">Manage Trips:</h6>
                                <a class="collapse-item text-dark" href="{{route('admin.company.vehicles.trips.index')}}"><i class="fas fa-car task-dark  mr-2"></i><span>All Trips</span>
                                </a>
                                <a class="collapse-item text-dark" href="{{route('admin.company.vehicles.trips.create')}}"><i class="fa fa-car-side  mr-2"></i>New Trip</a>
                            </div>
                        </div>
                    </li>
    @endif
     @if(\Illuminate\Support\Facades\Auth::user()->role == 'master' || \Illuminate\Support\Facades\Auth::user()->role == 'admin' || \Illuminate\Support\Facades\Auth::user()->role === 'system_admin')
         <!-- Divider -->
             <hr class="sidebar-divider">

             <!-- Heading -->
             <div class="sidebar-heading">
                 Back Office
             </div>
             <li class="nav-item">
                 <a class="nav-link collapsed" href="{{route('admin.users.index')}}"
                    aria-expanded="true" >
                     <i class="fas fa-fw fa-users"></i>
                     <span>Users</span>
                 </a>
             </li>
    @if(\Illuminate\Support\Facades\Auth::user()->role == 'master')
            <li class="nav-item">
                <a class="nav-link collapsed" href="{{route('admin.industries.index')}}"
                   aria-expanded="true" >
                    <i class="fas fa-fw fa-users"></i>
                    <span>Industries</span>
                </a>

                <li class="nav-item">
                    <a class="nav-link collapsed" href="{{route('admin.subscriptions.index')}}"
                       aria-expanded="true" >
                        <i class="fas fa-fw fa-gifts"></i>
                        <span>Subscriptions</span>
                    </a>
                </li>
            </li>
    @endif
@endif


{{--                <hr class="sidebar-divider d-none d-md-block">--}}
<!-- Sidebar Toggler (Sidebar) -->
<div class="text-center d-none d-md-inline">
<button class="rounded-circle border-0" id="sidebarToggle"></button>
</div>

{{-- <!-- Sidebar Message -->
<div class="sidebar-card d-none d-lg-flex">
<img class="sidebar-card-illustration mb-2" src="img/undraw_rocket.svg" alt="...">
<p class="text-center mb-2"><strong>Kamora CRM</strong> is packed with premium features, pages, and more!</p>
<a class="btn btn-success btn-sm" href="#">Upgrade to Premium!</a>
</div> --}}

</ul>
<!-- End of Sidebar -->

<!-- Content Wrapper -->
<div id="content-wrapper" class="d-flex flex-column">
<div id="app">
<!-- Main Content -->
<div id="content">

 <!-- Topbar -->
 <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

     <!-- Sidebar Toggle (Topbar) -->
     <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
         <i class="fa fa-bars"></i>
     </button>

     <!-- Topbar Search -->
     <form
         class="d-none d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search">
         <div class="input-group">
             <input type="text" class="form-control bg-light border-0 small" placeholder="Search for..."
                    aria-label="Search" aria-describedby="basic-addon2">
             <div class="input-group-append">
                 <button class="btn btn-primary" type="button">
                     <i class="fas fa-search fa-sm"></i>
                 </button>
             </div>
         </div>
     </form>

     <!-- Topbar Navbar -->
     <ul class="navbar-nav ml-auto">

         <!-- Nav Item - Search Dropdown (Visible Only XS) -->
{{--                                <li class="nav-item dropdown no-arrow d-sm-none">--}}
{{--                                    <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button"--}}
{{--                                       data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">--}}
{{--                                        <i class="fas fa-search fa-fw"></i>--}}
{{--                                    </a>--}}
             <!-- Dropdown - Messages -->
{{--                                    <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in"--}}
{{--                                         aria-labelledby="searchDropdown">--}}
{{--                                        <form class="form-inline mr-auto w-100 navbar-search">--}}
{{--                                            <div class="input-group">--}}
{{--                                                <input type="text" class="form-control bg-light border-0 small"--}}
{{--                                                       placeholder="Search for..." aria-label="Search"--}}
{{--                                                       aria-describedby="basic-addon2">--}}
{{--                                                <div class="input-group-append">--}}
{{--                                                    <button class="btn btn-primary" type="button">--}}
{{--                                                        <i class="fas fa-search fa-sm"></i>--}}
{{--                                                    </button>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                        </form>--}}
{{--                                    </div>--}}
{{--                                </li>--}}

         <!-- Nav Item - Alerts -->
{{--                    @if(\Illuminate\Support\Facades\Auth::user()->role == 'admin')--}}
             <li class="nav-item dropdown no-arrow mx-1">
                 <a class="nav-link dropdown-toggle" href="#" id="alertsDropdown" role="button"
                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                     <i class="fas fa-bell fa-fw"></i>
                     <!-- Counter - Alerts -->
                     <span class="badge badge-danger badge-counter">{{$alerts ? count($alerts) : 0}}</span>
                 </a>
                 <!-- Dropdown - Alerts -->
                 <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in"
                      aria-labelledby="alertsDropdown">
                     <h6 class="dropdown-header">
                         Alerts Center
                     </h6>

                     @if($alerts)
                         @foreach($alerts as $alert)
                             @if($alert->addressed_to === \Illuminate\Support\Facades\Auth::user()->id )
                                 <a class="dropdown-item d-flex align-items-center alert-item"
                                    href="#" data-id="{{ $alert->id }}">
                                     <div class="mr-3">
                                         @if($alert->subject_type == 'App\Models\Task')
                                             <div class="icon-circle bg-primary">
                                                 <i class="fas fa-tasks text-white"></i>
                                             </div>
                                         @elseif($alert->subject_type == 'App\Models\Invoice')
                                             <div class="icon-circle bg-danger">
                                                 <i class="fas fa-file-invoice text-white"></i>
                                             </div>
                                             @elseif($alert->subject_type = 'App\Models\Employee')
                                             <div class="icon-circle bg-pink">
                                                 <i class="fas fa-birthday-cake text-white"></i>
                                             </div>
                                         @else
                                             <div class="icon-circle bg-primary">
                                                 <i class="fas fa-gifts text-white"></i>
                                             </div>
                                         @endif
                                     </div>
                                     <div>
                                         <div class="small text-gray-500">{{$alert->created_at->format('d M Y')}}</div>
                                         @if($alert->subject_type == 'App\Models\Task')
                                             <p> New Task assigned <span class="font-weight-bold"> - {{$alert->subject->title}}.</span>
                                                 <br>
                                                 <small class="text-muted">Due Date - {{$alert->subject->due_date}}</small>
                                             </p>

                                         @elseif($alert->subject_type == 'App\Models\Invoice')
                                             <p> Overdue Invoice <span class="font-weight-bold"> - {{$alert->subject->subject}}.</span>
                                                 <br>
                                                 <small class="text-muted">Ref #{{$alert->subject->reference_no}}</small>
                                             </p>
                                         @elseif($alert->subject_type == 'App\Models\MaterialRequest')
                                             <p> Material Request <span class="font-weight-bold"> - {{$alert->subject->item->name}}.</span>
                                                 <br>
                                                 <small class="text-muted"> for Project <strong> {{$alert->subject->project->name}}</strong> by <strong> {{$alert->subject->user->name}}</strong></small>
                                             </p>
                                         @elseif($alert->subject_type == 'App\Models\Employee')
                                             @if(!empty($alert->subject))
                                                 <p> It's a birthday for <span class="font-weight-bold"> - {{$alert->subject->user->name ?? $alert->subject->full_names}}.</span>
                                                     <br>
                                                     <small class="text-muted"> Send your wishes to <strong> {{$alert->subject->user->email}}</strong> </small>
                                                 </p>
                                             @endif
                                         @else
                                             <p> Notification <span class="font-weight-bold"> - {{$alert->subject->subject}}.</span>
                                                 <br>
{{--                                                 <small class="text-muted">Ref #{{$alert->subject->reference_no}}</small>--}}
                                             </p>
                                         @endif
{{--                                         <button class="btn btn-sm float-right"><i class="fa fa-envelope"></i><small> Mark as Read</small></button>--}}
                                     </div>
                                 </a>

                             @endif
                         @endforeach
                     @endif

                     <a class="dropdown-item text-center small text-gray-500" href="#">Show All Alerts</a>
                 </div>
             </li>

{{--                     @endif--}}
         <!-- Nav Item - Messages -->
         <li class="nav-item dropdown no-arrow mx-1">
             <a class="nav-link dropdown-toggle" href="#" id="messagesDropdown" role="button"
                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                 <i class="fas fa-envelope fa-fw"></i>
                 <!-- Counter - Messages -->
                 <span class="badge badge-danger badge-counter">1</span>
             </a>
             <!-- Dropdown - Messages -->
             <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in"
                  aria-labelledby="messagesDropdown">
                 <h6 class="dropdown-header">
                     Message Center
                 </h6>
                     <a class="dropdown-item d-flex align-items-center" href="#">
                         <div class="dropdown-list-image mr-3">
                             <img class="rounded-circle" src="{{asset('img/undraw_profile_1.svg')}}"
                                  alt="...">
                             <div class="status-indicator bg-success"></div>
                         </div>
                         <div class="font-weight-bold">
                             <div class="text-truncate">Hi {{Auth::user()->name }}, Welcome to Kamora Software!
                             </div>
                             <div class="small text-gray-500">Kamora Team · 19 Jul 2024</div>
                         </div>
                     </a>
{{--                             <a class="dropdown-item d-flex align-items-center" href="#">--}}
{{--                                 <div class="dropdown-list-image mr-3">--}}
{{--                                     <img class="rounded-circle" src="{{asset('img/undraw_profile_2.svg')}}"--}}
{{--                                          alt="...">--}}
{{--                                     <div class="status-indicator"></div>--}}
{{--                                 </div>--}}
{{--                                 <div>--}}
{{--                                     <div class="text-truncate">I have the photos that you ordered last month, how--}}
{{--                                         would you like them sent to you?--}}
{{--                                     </div>--}}
{{--                                     <div class="small text-gray-500">Jae Chun · 1d</div>--}}
{{--                                 </div>--}}
{{--                             </a>--}}
{{--                             <a class="dropdown-item d-flex align-items-center" href="#">--}}
{{--                                 <div class="dropdown-list-image mr-3">--}}
{{--                                     <img class="rounded-circle" src="{{asset('img/undraw_profile_3.svg')}}"--}}
{{--                                          alt="...">--}}
{{--                                     <div class="status-indicator bg-warning"></div>--}}
{{--                                 </div>--}}
{{--                                 <div>--}}
{{--                                     <div class="text-truncate">Last month's report looks great, I am very happy with--}}
{{--                                         the progress so far, keep up the good work!--}}
{{--                                     </div>--}}
{{--                                     <div class="small text-gray-500">Morgan Alvarez · 2d</div>--}}
{{--                                 </div>--}}
{{--                             </a>--}}
{{--                             <a class="dropdown-item d-flex align-items-center" href="#">--}}
{{--                                 <div class="dropdown-list-image mr-3">--}}
{{--                                     <img class="rounded-circle" src="https://source.unsplash.com/Mv9hjnEUHR4/60x60"--}}
{{--                                          alt="...">--}}
{{--                                     <div class="status-indicator bg-success"></div>--}}
{{--                                 </div>--}}
{{--                                 <div>--}}
{{--                                     <div class="text-truncate">Am I a good boy? The reason I ask is because someone--}}
{{--                                         told me that people say this to all dogs, even if they aren't good...--}}
{{--                                     </div>--}}
{{--                                     <div class="small text-gray-500">Chicken the Dog · 2w</div>--}}
{{--                                 </div>--}}
{{--                             </a>--}}
                 <a class="dropdown-item text-center small text-gray-500" href="#">Read More Messages</a>
             </div>
         </li>

         <div class="topbar-divider d-none d-sm-block"></div>

         <!-- Nav Item - User Information -->
         <li class="nav-item dropdown no-arrow">
             <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button"
                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                 @if(Auth::user())
                     <span class="mr-2 d-none d-lg-inline text-gray-600 small">{{Auth::user()->name }}</span>
                 @endif

                 <img class="img-profile rounded-circle" src="{{asset('img/undraw_profile.svg')}}">
             </a>
             <!-- Dropdown - User Information -->
             <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in"
                  aria-labelledby="userDropdown">
{{--                                <a class="dropdown-item" href="#">--}}
{{--                                    <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>--}}
{{--                                    Profile--}}
{{--                                </a>--}}
{{--                                <a class="dropdown-item" href="#">--}}
{{--                                    <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>--}}
{{--                                    Settings--}}
{{--                                </a>--}}
{{--                                <a class="dropdown-item" href="#">--}}
{{--                                    <i class="fas fa-list fa-sm fa-fw mr-2 text-gray-400"></i>--}}
{{--                                    Activity Log--}}
{{--                                </a>--}}
{{--                                <div class="dropdown-divider"></div>--}}
                 <a class="dropdown-item" href="{{ route('logout') }}"
                    onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                     <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                     Logout
                 </a>
                 <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                     @csrf
                 </form>
             </div>
         </li>

     </ul>

 </nav>
 <!-- End of Topbar -->

 <!-- Begin Page Content -->
 <div class="container-fluid">
     @yield('content')
     <router-view/>
 </div>
 <!-- /.container-fluid -->

</div>
<!-- End of Main Content -->
</div>
@include('admin.layouts.partials.footer')
