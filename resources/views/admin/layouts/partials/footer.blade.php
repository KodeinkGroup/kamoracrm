                  <!-- Footer -->
                    <footer class="sticky-footer bg-white" style=" width: 100%; position: fixed; bottom: 0; padding: 0.5rem 0">
                        <div class="container my-auto">
                            <div class="copyright text-center my-auto d-none d-sm-inline-flex d-md-inline-flex d-lg-inline-flex">
                                <span>{{now()->format('Y')}} Kamora Software v2.21.0 | Powered by Kodeink Group (Pty) Ltd  </span>
                            </div>
                            <!-- End of Page Wrapper -->

                            <!-- Scroll to Top Button-->
                            <a class="scroll-to-top rounded" href="#page-top">
                                <i class="fas fa-angle-up"></i>
                            </a>

                            <!-- Logout Modal-->
                            <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                                                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">×</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
                                                <div class="modal-footer">
                                                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                                                    <a class="btn btn-primary" href="login.html">Logout</a>
                                                </div>
                                        </div>
                                </div>
                            </div>
                        </div>
                    </footer>
              </div>

          </div>
                    <!-- Bootstrap core JavaScript-->
                    <script src="{{ asset('vendor/jquery/jquery.min.js')}}"></script>
                    <script src="{{ asset('vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>

                    <!-- Core plugin JavaScript-->
                    <script src="{{ asset('vendor/jquery-easing/jquery.easing.min.js')}}"></script>

                    <!-- Custom scripts for all pages-->
                    <script src="{{ asset('js/sb-admin-2.js')}}"></script>

                    <!-- Page level plugins -->

                     <script src="{{ asset('js/app.js') }}"></script>

                    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
                    <script>

                        var screenWidth = $(window).width();

                        if(screenWidth < 390) {
                            $('.sidebar').addClass('collapse');
                        }

                        let full_url = window.location.href;
                        let url= new URL(full_url);
                        var plan_id = url.searchParams.get("subscription_plan_id");

                        $("#subscription_plan_id").val(plan_id);

                        // Toggle the side navigation
                        $("#sidebarToggle, #sidebarToggleTop").on('click', function(e) {

                            // $("body").toggleClass("sidebar-toggled");
                            if ($(".sidebar").hasClass("collapse")) {
                                $('.sidebar').removeClass('collapse');
                            } else {
                                $('.sidebar').addClass('collapse');
                            }
                        });

                        // CSRF token setup for AJAX requests
                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            }
                        });

                        var deptId;
                       $(".dept-edit-btn").on("click", function (event) {
                            event.preventDefault();
                            deptId = $(this).attr('id');
                           let companyId = $(this).attr('data-company-id');

                           let deptManForm = $("#edit-manager-form-" + deptId);
                           if(deptManForm) {
                               if (deptManForm.length) {
                                   deptManForm.removeClass('hidden');
                               }
                           }

                           let updateUrl = '{{ route('admin.company.update.department', ['company' => '__COMPANY_ID__', 'id' => '__ID__']) }}'
                               .replace('__COMPANY_ID__', companyId)
                               .replace('__ID__', deptId);

                           $('#edit-manager-form-'+deptId).on('submit', function(e) {
                               e.preventDefault();
                               $.ajax({
                                   url: updateUrl, // Your route URL
                                   type: 'POST',
                                   data: $(this).serialize(), // Serialize form data
                                   success: function(response) {
                                       deptManForm.addClass('hidden');
                                       location.reload();
                                   },
                                   error: function(error) {
                                       console.error(error); // Handle the error
                                   }
                               });
                           });
                       });

                        // Update Alerts Statuses
                            $('.alert-item').on('click', function(e) {
                                e.preventDefault();

                                // Get the alert ID from the data attribute
                                var alertId = $(this).data('id');
                                console.log("Updating Alert #"+ alertId);

                                // Send the AJAX request to update the alert status
                                $.ajax({
                                    url: '/alerts/' + alertId + '/update-status',  // Your route URL
                                    type: 'POST',
                                    data: {
                                        _token: '{{ csrf_token() }}',  // Include the CSRF token
                                    },
                                    success: function(response) {
                                        if (response.success) {
                                            console.log('Alert status updated successfully.');

                                            // Optionally, update the UI, e.g., remove the alert item, mark as read, etc.
                                            $(this).find('span').css('color', 'gray'); // Example: change text color to indicate "read"
                                        }
                                    },
                                    error: function(xhr) {
                                        console.error('There was an error updating the alert status.');
                                    }
                                });
                            });

                  let monthsOfTheYear = {!! json_encode($data['monthsOfTheYear'] ?? '') !!};

                        let payments = {!! json_encode($data['payments'] ?? '') !!};
                        let estimates = {!! json_encode($data['estimates'] ?? '') !!};
                        let duePayments = {!! json_encode($data['duePayments'] ??  '')  !!};
                        let leadSources = {!! json_encode($data['leadSources'] ?? []) !!};
                        let leadSourceLabels = {!! json_encode($data['leadSourceLabels'] ?? []) !!};

                        const ctx = document.getElementById('yearlySales');

                        if(ctx){
                            const myChart = new Chart(ctx, {
                                type: 'line',
                                data : {
                                    labels: monthsOfTheYear,
                                    datasets: [{
                                        label: 'Yearly Sales',
                                        data: payments,
                                        order: 1,
                                        backgroundColor: [
                                            'rgba(252, 252, 252)',
                                        ],
                                        borderColor: [
                                            'rgb(72, 245, 66)',

                                        ],
                                        pointBorderWidth: 1,
                                        tension: 0.5
                                    },
                                        {
                                            label: 'Unpaid Invoice',
                                            data: duePayments,
                                            type: 'line',
                                            // this dataset is drawn on top
                                            order: 2,
                                            backgroundColor: ['rgb(252, 252, 252)'],
                                            borderColor: ['rgb(196, 16, 16)'],
                                            pointBorderWidth: 1,
                                            tension: 0.5
                                        }]
                                },
                                options: {
                                    scales: {
                                        y: {
                                            beginAtZero : true
                                        }
                                    }
                                }
                            });

                        }

                        const ctxEstimates =  document.getElementById('yearlyEstimates');
                        if(ctxEstimates) {
                            const myEChart = new Chart(ctxEstimates, {
                                type: 'line',
                                data : {
                                    labels: monthsOfTheYear,
                                    datasets: [{
                                        label: 'Yearly Estimates',
                                        data: estimates,
                                        backgroundColor: [
                                            'rgba(255, 99, 132, 0.2)',
                                            'rgba(255, 159, 64, 0.2)',
                                            'rgba(255, 205, 86, 0.2)',
                                            'rgba(75, 192, 192, 0.2)',
                                            'rgba(54, 162, 235, 0.2)',
                                            'rgba(153, 102, 255, 0.2)',
                                            'rgba(255, 99, 132, 0.2)',
                                            'rgba(255, 159, 64, 0.2)',
                                            'rgba(255, 205, 86, 0.2)',
                                            'rgba(75, 192, 192, 0.2)',
                                            'rgba(54, 162, 235, 0.2)',
                                            'rgba(153, 102, 255, 0.2)',
                                        ],
                                        borderColor: [
                                            'rgb(255, 99, 132)',
                                            'rgb(255, 159, 64)',
                                            'rgb(255, 205, 86)',
                                            'rgb(75, 192, 192)',
                                            'rgb(54, 162, 235)',
                                            'rgb(153, 102, 255)',
                                            'rgb(255, 99, 132)',
                                            'rgb(255, 159, 64)',
                                            'rgb(255, 205, 86)',
                                            'rgb(75, 192, 192)',
                                            'rgb(54, 162, 235)',
                                            'rgb(153, 102, 255)',
                                        ],
                                        pointBorderWidth: 1,
                                        tension: 0.5
                                    }]
                                },
                                options: {
                                    scales: {
                                        y: {
                                            beginAtZero : true
                                        }
                                    }
                                }
                            });
                        }

                        // Lead Sources Chart
                        const ctxLeadSources = document.getElementById('leadSources');

                        if(ctxLeadSources){
                            const myLeadsChart = new Chart(ctxLeadSources, {
                                type: 'doughnut',
                                data: {
                                    labels: leadSourceLabels,
                                    datasets: [{
                                        label: 'Lead Sources',
                                        data: leadSources,
                                        backgroundColor: [
                                            '#FF6384', // Custom color for "Website"
                                            '#36A2EB', // Custom color for "Social Media"
                                            '#FFCE56', // Custom color for "Referral"
                                            '#4BC0C0',  // Custom color for "Email Campaign"
                                            '#FF9E40'  // Custom color for "Email Campaign"
                                        ],
                                        borderColor: [
                                            '#FF6384', // Border color for "Website"
                                            '#36A2EB', // Border color for "Social Media"
                                            '#FFCE56', // Border color for "Referral"
                                            '#4BC0C0',  // Border color for "Email Campaign"
                                            '#FF9E40'  // Border color for "Email Campaign"
                                        ],
                                        borderWidth: 1
                                    }]
                                },
                                options: {
                                    responsive: true,
                                    plugins: {
                                        legend: {
                                            display: true,
                                            position: 'bottom',
                                            labels: {
                                                boxWidth: 10,
                                                padding: 10,
                                                usePointStyle: true,
                                            }
                                        },
                                        tooltip: {
                                            callbacks: {
                                                label: function(tooltipItem) {
                                                    return tooltipItem.label + ': ' + tooltipItem.raw;
                                                }
                                            }
                                        }
                                    }
                                }
                            });
                        }

                        let materialRequestBtn = $('.update-material-request-btn');

                        materialRequestBtn.on('click', function (event) {
                            console.log("Clicked");
                            event.preventDefault(); // Prevent default behavior (e.g., following the link)

                            let id = $(this).data('request-id');
                            let status =  $(this).data('status');
                            updateMaterialRequestStatus(id, status);
                        });
                        function updateMaterialRequestStatus(id, status) {
                            console.log("updateRequest for: " + id);

                            $.ajax({
                                url: '/api/projects/requestMaterial/status/'+ id, // Replace with your API route
                                method: 'PUT', // or 'GET', depending on your route
                                data: {
                                    status: status,
                                },
                                success: function(response) {
                                    // Handle the response from the server
                                    console.log('Success:', response);
                                    location.reload(true);
                                },
                                error: function(xhr, status, error) {
                                    // Handle any errors
                                    console.log('Error:', error);
                                }
                            });
                        }
                    </script>
                     @stack('admin/layouts.scripts.scripts')
                     @stack('footer-scripts')
    </body>

</html>
