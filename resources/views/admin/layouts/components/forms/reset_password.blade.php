<form action="{{route('admin.users.store')}}" method="POST" id="new-user-form">
    @csrf
    <div class="form-group">
        <label for="name">User Name <span class="text-muted">*</span></label>
        <input type="text" name="name" class="form-control {{$errors->has('name') ? 'is-invalid' : ''}}">
        @if ($errors->has('name'))
            <div class="invalid-feedback">
                {{ $errors->first('name') }}
            </div>
        @endif
    </div>
    <div class="form-group">
        <label for="email">Email <span class="text-muted">*</span></label>
        <input type="email" name="email" class="form-control {{$errors->has('email') ? 'is-invalid' : ''}} " placeholder="name@mail.com">
        @if ($errors->has('email'))
            <div class="invalid-feedback">
                {{ $errors->first('email') }}
            </div>
        @endif
    </div>

    <div class="form-group">
        <label for="role">User Role</label>
       <select name="role" id="" class="form-control">
           <option value="user" default>User</option>
           <option value="sales_admin" >Sales Admin</option>
           <option value="admin">System Admin</option>
           <option value="inventory_manager">Inventory Manager</option>
           <option value="technician">Technician</option>

       </select>
    </div>
    <div class="form-group">
        <label for="password">Password <span class="text-muted">*</span></label>
        <input type="password" name="password" class="form-control {{$errors->has('password') ? 'is-invalid' : ''}}" placeholder="*********">
        @if ($errors->has('password'))
            <div class="invalid-feedback">
                {{ $errors->first('password') }}
            </div>
        @endif
    </div>

    <div class="form-group">
        <label for="confirm_password">Confirm Password <span class="text-muted">*</span></label>
        <input type="password" name="confirm_password" class="form-control {{$errors->has('confirm_password') ? 'is-invalid' : ''}} " placeholder="********">

        @if ($errors->has('confirm_password'))
            <div class="invalid-feedback">
                {{ $errors->first('confirm_password') }}
            </div>
        @endif
    </div>
    <button type="submit" class="btn btn-primary float-right">Add User</button>
</form>
