<form action="{{route($route)}}" method="POST" id="new-user-form">
    @csrf
    <div class="form-group">
        <label for="name">User Name <span class="text-muted">*</span></label>
        <input type="text" name="name" class="form-control {{$errors->has('name') ? 'is-invalid' : ''}}">
        @if ($errors->has('name'))
            <div class="invalid-feedback">
                {{ $errors->first('name') }}
            </div>
        @endif
    </div>
    <div class="form-group">
        <label for="email">Email <span class="text-muted">*</span></label>
        <input type="email" name="email" class="form-control {{$errors->has('email') ? 'is-invalid' : ''}} " placeholder="name@mail.com">
        @if ($errors->has('email'))
            <div class="invalid-feedback">
                {{ $errors->first('email') }}
            </div>
        @endif
    </div>

    <div class="form-group">
        <label for="role">Department</label>
        <select name="department_id" id="" class="form-control">
            @if(isset($company->departments))
                <option value="" disabled selected>Select a Department</option>
                @foreach($company->departments as $department)
                    <option value="{{$department->id}}">{{$department->name}}</option>
                @endforeach
            @endif
        </select>
    </div>

    <div class="form-group">
        <label for="role">User Role</label>
       <select name="role" id="" class="form-control">
           <option value="" disabled selected>Select a Role</option>
           <option value="admin">System Admin</option>
           <option value="sales_admin" >Sales Admin</option>
           <option value="system_admin">System Admin</option>
           <option value="inventory_manager">Inventory Manager</option>
           <option value="fleet_manager">Fleet Manager</option>
           <option value="project_manager">Project Manager</option>
           <option value="hr_manager">Human Resources Manager</option>
           <option value="technician">Technician</option>
           <option value="sales_agent">Sales Agent</option>
           <option value="user">User</option>
       </select>
    </div>
    <div class="form-group">
        <label for="password">Password <span class="text-muted">*</span></label>
        <input type="password" name="password" class="form-control {{$errors->has('password') ? 'is-invalid' : ''}}" placeholder="*********">
        @if ($errors->has('password'))
            <div class="invalid-feedback">
                {{ $errors->first('password') }}
            </div>
        @endif
    </div>

    <div class="form-group">
        <label for="confirm_password">Confirm Password <span class="text-muted">*</span></label>
        <input type="password" name="confirm_password" class="form-control {{$errors->has('confirm_password') ? 'is-invalid' : ''}} " placeholder="********">

        @if ($errors->has('confirm_password'))
            <div class="invalid-feedback">
                {{ $errors->first('confirm_password') }}
            </div>
        @endif
    </div>

    <button type="submit" class="btn btn-primary float-right">Add User</button>

</form>
