@extends('admin.layouts.app')

@section('content')
    <!-- Begin Page Content -->
    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <a href="{{route('admin.prospects.create', ['type' => 1])}}" class="d-inline-block d-sm-none d-md-none d-lg-none btn btn-sm btn-primary shadow-sm float-right">
                <i class="fas fa-user-plus fa-sm text-white"></i>
            </a>
            <h1 class="h3 mb-0">Business Leads</h1>
                    <a href="{{route('admin.prospects.create', ['type' => 1])}}" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">
                        <i class="fas fa-download fa-sm text-white-50"></i> New Lead
                    </a>

        </div>

        <div class="row">
            <!-- Earnings (Monthly) Card Example -->
            <div class="col-xl-2 col-md-6 mb-4">
                <div class="card border-left-primary shadow h-100 py-2">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                                <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                    Facebook
                                </div>
                                <div class="h5 mb-0 font-weight-bold text-gray-800">{{$sources[0]['facebook']}}</div>
                            </div>
                            <div class="col-auto">
                                <i class="fas fa-comments fa-2x text-gray-300"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Earnings (Annual) Card Example -->
            <div class="col-xl-2 col-md-6 mb-4">
                <div class="card border-left-success shadow h-100 py-2">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                                <div class="text-xs font-weight-bold text-success text-uppercase mb-1">
                                    Whatsapp
                                </div>
                                <div class="h5 mb-0 font-weight-bold text-gray-800">{{$sources[4]['whatsapp']}}</div>
                            </div>
                            <div class="col-auto">
                                <i class="fas fa-comments fa-2x text-gray-300"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Tasks Card Example -->
            <div class="col-xl-2 col-md-6 mb-4">
                <div class="card border-left-info shadow h-100 py-2">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                                <div class="text-xs font-weight-bold text-info text-uppercase mb-1">Twitter
                                </div>
                                <div class="row no-gutters align-items-center">
                                    <div class="col-auto">
                                        <div
                                            class="h5 mb-0 mr-3 font-weight-bold text-gray-800">{{$sources[6]['twitter']}}</div>
                                    </div>

                                </div>
                            </div>
                            <div class="col-auto">
                                <i class="fas fa-comments fa-2x text-gray-300"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Pending Requests Card Example -->
            <div class="col-xl-2 col-md-6 mb-4">
                <div class="card border-left-warning shadow h-100 py-2">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                                <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">
                                    Instagram
                                </div>
                                <div class="h5 mb-0 font-weight-bold text-gray-800">{{$sources[5]['instagram']}}</div>
                            </div>
                            <div class="col-auto">
                                <i class="fas fa-comments fa-2x text-gray-300"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Pending Requests Card Example -->
            <div class="col-xl-2 col-md-6 mb-4">
                <div class="card border-left-primary shadow h-100 py-2">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                                <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">
                                    LinkedIn
                                </div>
                                <div class="h5 mb-0 font-weight-bold text-gray-800">{{$sources[1]['linkedIn']}}</div>
                            </div>
                            <div class="col-auto">
                                <i class="fas fa-comments fa-2x text-gray-300"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Pending Requests Card Example -->
            <div class="col-xl-2 col-md-6 mb-4">
                <div class="card border-left-default shadow h-100 py-2">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                                <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">
                                    Website
                                </div>
                                <div class="h5 mb-0 font-weight-bold text-gray-800">{{$sources[3]['website']}}</div>
                            </div>
                            <div class="col-auto">
                                <i class="fas fa-comments fa-2x text-gray-300"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <!-- DataTales Example -->
                <div class="card shadow mb-4">
                    <div class="card-header py-3">
                        <h6 class="m-0 font-weight-bold text-primary">All Leads</h6>
                    </div>
                    <div class="card-body">
                        <div class="alert alert-warning alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert">x</button>
                            <i class="fas fa-info-circle fa-2x mr-4"></i>
                            <p class="text-dark"> In our definition, Leads are potential customers that have not yet expressed any
                                interest in your products or services,
                                but you find them fit to be your customers.
                            </p>
                        </div>
                        <div class="table-responsive">
                            @if (!empty($leads))
                                <table class="table" id="dataTable" style="width: 100%" cellspacing="0">
                                    <thead>
                                    <tr>
                                        <th>Avatar</th>
                                        <th>Name</th>
                                        <th>Position</th>
                                        <th>Contacts</th>
                                        <th>Date Added</th>
                                        <th>Source</th>
                                        <th>Actions</th>
                                    </tr>
                                    </thead>
                                    <tfoot>
                                    <tr>
                                        <th>Avatar</th>
                                        <th>Name</th>
                                        <th>Position</th>
                                        <th>Contacts</th>
                                        <th>Date Added</th>
                                        <th>Source</th>
                                        <th>Actions</th>
                                    </tr>
                                    </tfoot>
                                    <tbody>

                                    @foreach ($leads as $lead)
                                        <tr>
                                            <td>
                                                <img class="img-profile rounded-circle" src="{{asset('img/undraw_profile.svg')}}" width="50">
                                            </td>
                                            <td>
                                                <a href="{{route('admin.prospects.show', $lead->id)}}">{{$lead->name}} </a><br>
                                                <small class="text-muted">{{$lead->location}}</small>
                                            </td>
                                            <td>{{$lead->designation ?: 'Not Specified'}} <br>
                                                <small class="text-muted">{{$lead->business_name}}</small>
                                            </td>
                                            <td>{{$lead->email}} <br>
                                                <small class="text-muted">{{$lead->mobile_no}}</small>
                                            </td>
                                            <td>{{$lead->created_at->format('d M, Y')}}</td>
                                            <td>{{$lead->source->name}} <br>
                                                <small class="text-muted">{{$lead->salesperson}}</small>
                                            </td>
                                            <td>
                                                <a href="{{route('admin.prospects.edit', $lead->id)}}"
                                                   class="btn btn-default"> <i class="fas fa-pen text-primary"></i> </a>
                                                <a href="{{route('admin.prospects.activities.create', $lead->id)}}"
                                                   class="btn btn-default"> <i class="fas fa-thumbs-up text-success"></i> </a>
                                                <a href="{{route('admin.prospects.delete', $lead->id)}}"
                                                   class="btn btn-default"> <i class="fas fa-trash text-danger"></i> </a>

                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            @else
                                <div class="alert alert-warning">There are currently no Leads listed on your sales
                                    pipeline
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>
    <!-- /.container-fluid -->
@endsection
