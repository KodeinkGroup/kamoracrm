@extends('admin.layouts.app')
@section('content')
<div class="container">
    <div class="card mt-3">
        <div class="card-body">
            <div class="d-flex">
            <h1>Edit Prospect <small class="text-muted">{{$prospect->name}} | <span> {{$prospect->business_name}}</span></small></h1>
                <div class="ml-auto">
                    <div class="dropdown">
                        <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                               Actions
                        </button>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="{{route('admin.prospects.dashboard')}}">Go to dashboard</a>
                            <a class="dropdown-item" href="{{route('admin.prospects.show', ['prospect' => $prospect])}}">View Prospect</a>
                            <a class="dropdown-item" href="{{route('home')}}">Something else here</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-sm-4">

            <div class="card mt-3">
                <div class="card-body">
                    @if ($prospect->logo)
                        <img src="{{Storage::url($prospect->logo)}}" alt="" width="200">
                    @else
                    <img src="/storage/prospects/logos/store.png" alt="" width="200">
                    @endif
                        <hr>
                        <button class="btn btn-outline-primary btn-sm btn-block" data-toggle="modal" data-target="#updateLogoModal">Upload New Logo</button>
                       @if($prospect->logo)
                       <button class="btn btn-outline-danger btn-sm btn-block" id="delBtn"> <i class="fas fa-trash"></i> Remove Logo</button>
                       @endif
                        <form action="{{route('admin.prospects.delete.logo-delete', $prospect->id)}}" method="POST" id="delete-logo-form">
                            @csrf
                            @method('DELETE')

                        </form>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
        @if (session('success'))
                <div class="alert alert-success">
                    {{session('success')}}
                </div>
            @endif
            <div class="card mt-3">
                <div class="card-body">
                    <h5>Edit Personal details</h5>
                    <hr>

                    @if($errors->count())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $message)
                                            <li>{{$message}}</li>
                                        @endforeach
                                    </ul>

                                </div>
                    @endif

                    <form action="{{route('admin.prospects.update', ['prospect' => $prospect ])}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        <div class="form-group">
                            <label for="">Name</label>
                            <input type="text" class="form-control" name="name" value="{{$prospect->name}}">
                        </div>
                        <div class="form-group">
                            <label for="">Business Name</label>
                            <input type="text" class="form-control" name="business_name" value="{{$prospect->business_name}}">
                        </div>
                        <div class="form-group">
                            <label for="">Email</label>
                            <input type="email" class="form-control" name="email" value="{{$prospect->email}}">
                        </div>
                        <div class="form-group">
                            <label for="">Contact No:</label>
                            <input type="text" class="form-control" name="mobile_no" value="{{$prospect->mobile_no}}">
                        </div>
                        <div class="form-group">
                            <label for="">Location</label>
                            <input type="text" class="form-control" name="location" value="{{$prospect->location}}">
                        </div>

                        <button type="submit" class="btn btn-primary">Update Prospect</button>
                    </form>
                </div>
            </div>
            <div class="card mt-3">

                <div class="card-body">
                    <h5>Edit Contact details</h5>
                    <hr>

                 @if($prospect->contact)
                 <form action="{{route('admin.prospects.contacts.update', $prospect->id)}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        <div class="form-group row">
                            <label for="" class="col-md-3">Street Address</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="street" value="{{$prospect->contact->street}}">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="" class="col-md-3">Surburb</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="surburb" value="{{$prospect->contact->surburb}}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="" class="col-md-3">City</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="city"
                                value="{{$prospect->contact->city}}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="" class="col-md-3">Province</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="province" value="{{$prospect->contact->province}}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="" class="col-md-3">Country</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="country" value="{{$prospect->contact->country}}">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="" class="col-md-3">Additional Notes</label>
                            <div class="col-md-9">
                                <textarea  class="form-control" name="notes" >{{$prospect->contact->notes}}</textarea>
                            </div>

                        </div>
                        <button class="btn btn-primary float-right">Update Contact Details</button>

                    </form>

                @else
                <form action="{{route('admin.prospects.contacts.update', $prospect->id)}}" method="POST" enctype="multipart-form">
                        @csrf
                        @method('PUT')
                        <div class="form-group row">
                            <label for="" class="col-md-3">Street Address</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="street">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="" class="col-md-3">Surburb</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="surburb">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="" class="col-md-3">City</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="city">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="" class="col-md-3">Province</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="province">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="" class="col-md-3">Country</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="country" >
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="" class="col-md-3">Additional Notes</label>
                            <div class="col-md-9">
                                <textarea  class="form-control" name="notes" ></textarea>
                            </div>

                        </div>
                        <button class="btn btn-primary float-right">Update Contact Details</button>

                    </form>
                @endif

                </div>
            </div>

        </div>
    </div>

</div>

<!-- Updating the logo/profile picture -->
<div class="modal fade" id="updateLogoModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Update Logo</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="{{route('admin.prospects.update.logo-update', $prospect->id)}}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PUT')

            <div class="form-group">
                <label for="">Choose Image</label>
                <input type="file" class="form-control-file" name ="image">
            </div>
            <button class="btn btn-primary float-right">Update Logo</button>
        </form>
      </div>

    </div>
  </div>
</div>


@endsection

@push('footer-scripts')
<script>
    $('#delBtn').on('click', function(){
        function deleteLogo() {
            var r = confirm("Are you sure you want to delete the Logo?")

            if(r) {
                document.querySelector('form#delete-logo-form').submit();
            }
        }
    });
<script>
@endpush
