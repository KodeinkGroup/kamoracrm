@extends('admin.layouts.app')

@section('content')
    <div class="container-fluid">
        @if (session('success'))
            <div class="alert alert-success mt-3">
                {{session('success')}}
            </div>
        @endif

        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <a href="{{route('admin.prospects.create', ['type' => 1])}}" class="d-inline-block d-sm-none d-md-none d-lg-none btn btn-sm btn-primary shadow-sm float-right">
                <i class="fas fa-user-plus fa-sm text-white"></i>
            </a>
            <h1 class="h3 mb-0">Business Prospects</h1>
            <a href="{{route('admin.prospects.create', ['type' => 2])}}" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">
                <i class="fas fa-download fa-sm text-white-50"></i> New Prospect
            </a>
        </div>

        {{-- @if ($prospects->count())
            {{$prospects->links()}}
            @foreach ($prospects as $prospect)
                @include('admin.prospects.partials.prospect-card', ['prospect' => $prospect])
            @endforeach
        @endif --}}

        @if (!empty($prospects))
            {{-- {{$prospects->links()}} --}}
                <div class="card shadow mb-4">
                    <div class="card-header py-3">
                        <h6 class="m-0 font-weight-bold text-primary">All Prospects</h6>
                    </div>
                    <div class="card-body">
                        <div class="alert alert-primary alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert">x</button>
                            <i class="fas fa-info-circle fa-2x mr-4"></i>
                            <p> In our definition, Prospects are potential customers that have expressed interest in procuring your
                                products or services. <br> This is where you start creating Quotes / Estimates.
                            </p>
                        </div>
                        <div class="table-responsive">
                            <table class="table" id="dataTable" style="width: 100%" cellspacing="0">
                                <thead>
                                <tr>
                                    <th>Avatar</th>
                                    <th>Prospect</th>
                                    <th>Position</th>
                                    <th>Email</th>
                                    <th>Contacts</th>
                                    <th>Date Added</th>
                                    <th>Source</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($prospects as $prospect)
                                    <tr>
                                        <td>
                                            @if ($prospect->logo)
                                                <img src="{{Storage::url($prospect->logo)}}" alt="" width="100">
                                            @else
                                                <img class="img-profile rounded-circle" src="{{asset('img/undraw_profile.svg')}}"
                                                     width="50">
                                            @endif
                                        </td>
                                        <td>
                                            <a href="{{route('admin.prospects.prospect.dashboard', ['prospect' => $prospect->id])}}">
                                                {{$prospect->name}} <br>
                                                <small class="text-muted">{{$prospect->location}}</small>
                                            </a>
                                        </td>
                                        <td>
                                            {{$prospect->designation ? $prospect->designation : 'Not Specified'}}
                                            <br>
                                            <small class="text-muted">{{$prospect->business_name}}</small>
                                        </td>
                                        <td>{{$prospect->email}}</td>

                                        <td>{{$prospect->mobile_no}}</td>
                                        <td>{{$prospect->created_at->format('d F Y')}}</td>
                                        <td>{{$prospect->source->name}} <br>
                                            <small class="text-muted">{{$prospect->salesperson}}</small>
                                        </td>
                                        <td>
                                            {{--                            <a href="{{route('admin.prospects.activities.create', $prospect->id)}}"--}}
                                            {{--                               class="btn btn-default"> <i class="fas fa-file-invoice"></i></a>--}}
                                            {{--                            <a href="{{route('admin.prospects.prospect.dashboard', ['prospect' => $prospect->id])}}"--}}
                                            {{--                               class="btn btn-default"> <i class="fas fa-chart-line"></i> </a>--}}
                                            <a class="btn btn-default" href="{{route('admin.prospects.edit', ['prospect' => $prospect->id])}}"
                                               title="Edit Prospect">
                                                <i class="fas fa-pen text-primary"></i>
                                            </a>
                                            <a class="btn btn-default" href="{{route('admin.prospects.activities.dashboard', ['prospect' => $prospect->id])}}"
                                               title="View Prospect">
                                                <i class="fas fa-eye text-default"></i>
                                            </a>
                                            <a href="{{route('admin.prospects.activities.create', ['prospect' => $prospect->id])}}"
                                               class="btn btn-default" data-toggle="tooltip" data-placement="top" title="Convert to Customer">
                                                <i class="fas fa-thumbs-up text-success"></i>
                                            </a>
                                            <a class="btn btn-default" href="{{route('admin.prospects.delete', ['prospect' => $prospect->id])}}"
                                               title="Delete Prospect">
                                                <i class="fas fa-trash text-danger"></i>
                                            </a>
                                            {{-- <div class="dropdown d-block">
                                                <button class="btn btn-secondary btn-block btn-sm dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Actions</button>
                                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                                                    {{-- <a class="" href="{{route('admin.prospects.prospect.dashboard', ['prospect' => $prospect->id])}}">Prospect Dashboard</a> --}}
                                            {{-- <a class="dropdown-item" href="{{route('admin.prospects.edit',['prospect' => $prospect->id])}}">Edit Prospect</a>
                                        </div>
                                    </div> --}}
                                        </td>
                                    </tr>

                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
        @else
            <div class="alert alert-warning">
                <p>There are currently no Prospects in your sales pipeline. </p>
            </div>
        @endif


    </div>
@endsection
