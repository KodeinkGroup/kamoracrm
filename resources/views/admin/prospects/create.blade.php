@extends('admin.layouts.app')

@section('content')
    <div class="container-fluid">
        <!-- Page Heading -->
        <h1 class="h3 mb-2 text-gray-800">Add a New {{$type->name}}</h1>
        <div class="row">
            <div class="col-md-8 col-lg-8">
                  {{-- <div class="ml-auto float-right">
            <div class="dropdown">
                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Actions
                </button>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                    <a class="dropdown-item" href="{{route('admin.prospects.dashboard')}}">Go to Dashboard</a>
                    <a class="dropdown-item" href="{{route('home')}}">Another action</a>
                    <a class="dropdown-item" href="{{route('home')}}">Something else here</a>
                </div>
            </div>
        </div> --}}

        <hr>
        @if($errors->count())
             <div class="alert alert-danger">
                 <ul>
                     @foreach ($errors->all() as $message)
                         <li>{{$message}}</li>
                     @endforeach
                 </ul>

             </div>
        @endif
        <div class="card">
            <div class="card-head"></div>
            <div class="card-body">
                <form action="{{route('admin.prospects.store')}}" method="POST" enctype="multipart/form-data">
                    @csrf

                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="">Name</label>
                            <input type="text" class="form-control" name="name">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="">Business Name</label>
                            <input type="text" class="form-control" name="business_name">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="">Designation / Position</label>
                            <input type="text" class="form-control" name="designation">
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="">Email</label>
                            <input type="email" class="form-control" name="email">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="mobile_no">Contact No:</label>
                            <input type="text" class="form-control" name="mobile_no">
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-3">
                            <label for="lead-source">Source </label>
                            <select name="source_id" id="sources" class="form-control">
                                <option value="" selected disabled>Please select source</option>

                                @foreach ($sources as $source)
                                <option value="{{$source->id}}">{{$source->name}}</option>
                                @endforeach

                            </select>
                        </div>
                        <div class="form-group col-md-5">
                            <label for="salesperson">Salesperson </label>
                            <input type="text" name="salesperson" class="form-control" id="salesperson" placeholder="Someone">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="">Location</label>
                            <input type="text" class="form-control" name="location">
                        </div>
                        <div class="form-group col-md-6">
                                <input id="inputState" class="form-control" name="prospect_type_id" hidden value="{{$type->id}}">
                        </div>

                    </div>
                    <div class="form-group">
                        <label for="">Profile Picture</label>
                        <input type="file" class="form-control-file" name="logo">
                    </div>
                    <button type="submit" class="btn btn-primary">Add {{$type->name}}</button>
                </form>
            </div>
        </div>

            </div>
        </div>
            </div>
        </div>

    </div>
@endsection
