@extends('admin.layouts.app')

@section('content')
    <div class="container">
        <div class="card mt-3">
            <div class="card-body">
                <div class="d-flex">
                <h1>Add Contact details | <small class="text-muted">{{$prospect->business_name}}</small> </h1>
                <div class="ml-auto">
                        <div class="dropdown">
                            <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                               Actions
                            </button>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" href="#">View Prospect</a>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Card -->

        <div class="card mt-3">
            <div class="card-body">
                <form action="{{route('admin.prospects.contacts.store', $prospect->id)}}" method="POST" enctype="multipart-form">
                    @csrf
                    <div class="form-group row">
                         <label for="" class="col-md-3">Street Address</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="street">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="" class="col-md-3">Surburb</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="surburb">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="" class="col-md-3">City</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="city" value="{{$prospect->location}}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="" class="col-md-3">Province</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="province">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="" class="col-md-3">Country</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="country">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="" class="col-md-3">Additional Notes</label>
                        <div class="col-md-9">
                            <textarea  class="form-control" name="notes"></textarea>
                        </div>

                    </div>
                    <button class="btn btn-primary float-right">Add Contact Details</button>

                </form>
            </div>
        </div>
    </div>
@endsection
