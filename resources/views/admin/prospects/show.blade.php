@extends('admin.layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-3 mt-4 mb-4 d-flex">
            <div class="card p-4">
                <div class=" image d-flex flex-column justify-content-center align-items-center">
                    <button class="btn btn-secondary">
                        <img src="https://i.imgur.com/wvxPV9S.png" height="100" width="100" />
                    </button> <span class="name mt-3">{{$prospect->name}}</span>
                    <span class="idd">{{$prospect->email}}</span>
                    <div class="d-flex flex-row justify-content-center align-items-center gap-2">
                        <span><i class="fa fa-mobile mr-1"></i></span> <span class="idd1"> {{$prospect->mobile_no}}</span>
                    </div>
{{--                    <div class="d-flex flex-row justify-content-center align-items-center mt-3">--}}
{{--                        <span class="number">1069 <span class="follow">Followers</span></span>--}}
{{--                    </div>--}}
                    <div class=" d-flex mt-2"> <button class="btn1 btn-dark">Edit Profile</button> </div>
                    <div class="text mt-3"> <span> </span>
                    </div>
                    <div class="gap-3 mt-3 icons d-flex flex-row justify-content-center align-items-center"> <span><i class="fa fa-twitter"></i></span>
                        <span><i class="fa fa-facebook"></i></span>
                        <span><i class="fa fa-instagram"></i></span>
                        <span><i class="fa fa-linkedin"></i></span>
                    </div>
{{--                    <div class=" px-2 rounded mt-4 date "> <span class="join">Date Added {{$prospect->created_at}}</span></div>--}}
                </div>
            </div>
        </div>
        <div class="col-md-8 card mt-4">
            <div class="card-body">
                <div class="d-flex">
                    <h1>{{$prospect->name}} </h1>
                    <div class="ml-auto">
                        <p><small>{{$prospect->type->name}}</small></p>
                    </div>
                    <div class="ml-auto">
                        <div class="dropdown d-block">
                            <button class="btn btn-secondary btn-block btn-sm dropdown-toggle" type="button"
                                    id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
                                    aria-expanded="false">Actions
                            </button>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item"
                                   href="{{route('admin.prospects.prospect.dashboard', ['prospect' => $prospect->id])}}">{{$prospect->type->name}}
                                    Dashboard</a>
                                <a class="dropdown-item"
                                   href="{{route('admin.prospects.activities.dashboard', ['prospect' => $prospect->id])}}">View
                                    Activity</a>
                            </div>
                        </div>
                    </div>
                </div>


                <hr>

                <h5><strong>Business Name
                        : </strong> {{$prospect->business_name ? $prospect->business_name : 'Not Specified'}} </h5>
                <h6 class="text-muted">Email : {{$prospect->email}}</h6>
                <h6 class="text-muted">Contact Number : {{$prospect->mobile_no}}</h6>
                <h6 class="text-muted">Source : {{$prospect->source->name}}</h6>
                <hr>
                <h5>Address</h5>
                <p class="text-muted">{{$prospect->contact->street ?? 'No Street name specified'}}
                    <br> {{$prospect->contact->surburb ?? 'No Surburb name specified'}}
                    <br> {{$prospect->contact->city ?? 'No city name specified'}}
                    <br> {{$prospect->contact->province ?? 'No province name specified'}}
                    <br> {{$prospect->contact->country ?? 'No country name specified'}}</p>

            </div>
        </div>

    </div>
@endsection

@push('admin.layouts.scripts.scripts')
    <script src="{{ asset('js/admin/users.js') }}"></script>
@endpush
