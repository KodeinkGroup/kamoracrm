@extends('admin.layouts.app')

@section('title', 'Create an activity for '. $prospect->business_name)

@section('content')
    <div class="container-fluid">
        <h1 class="h3 mb-2 text-gray-800">Create an activity</h1>
        <div class="card mt-3">
            <div class="card-body">
                <div class="d-flex">
                    <h3>Prospect Activity for <small class="text-muted">{{$prospect->business_name}}</small></h3>
                    <div class="ml-auto">
                        <div class="dropdown d-block">
                            <button class="btn btn-outline btn-sm btn-secondary btn-block btn-sm dropdown-toggle" type="button" id="dropdownMenuButton"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Actions
                                    </button>
                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                                        <a class="dropdown-item" href="{{route('admin.prospects.activities.create', $prospect)}}">Dashboard </a>
                                    </div>
                        </div>
                </div>
            </div>
        </div>
        <!-- End header card -->

       <div class="row">

            <div class="col-md-8">
                <div class="card-body">
                    <form action="{{route('admin.prospects.activities.store', $prospect)}}" method="POST" enctype="multipart/form-data">
                        @csrf

                            <div class="form-group row">
                                <label for="" class="col-md-3">Communication Type</label>
                                    <div class="col-md-9">
                                        <select name="communication_type" id="" class="form-control">
                                        <option default value="" disabled>Select Communication Type</option>
                                            <option value="phone_call">Phone Call</option>
                                            <option value="send_email">Email</option>
                                            <option value="texting">Text</option>
                                            <option value="in_person">In person</option>
                                            <option value="video_call">Video call</option>
                                            <option value="none">None</option>
                                        </select>
                                    </div>
                            </div>
                            <div class="form-group row">
                                <label for="" class="col-md-3">Communication Channel</label>
                                <div class="col-md-9">
                                    <select name="communication_channel" id="" class="form-control">
                                        <option default value="" disabled>Select Communication Channel</option>
                                        <option value="instagram">Instagram</option>
                                        <option value="facebook">Facebook</option>
                                        <option value="linkedin">LinkedIn</option>
                                        <option value="messenger">Messenger</option>
                                        <option value="twitter">Twitter</option>
                                        <option value="whatsapp">WhatsApp</option>
                                        <option value="other">Other</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                    <label for="" class="col-md-3">Activity</label>
                                    <div class="col-md-9">
                                        <select name="type" id="" class="form-control">
                                        <option default value="" disabled>Select an Activity</option>
                                            <option value="first_contact">First Contact</option>
                                            <option value="prospecting">Prospecting</option>
                                            <option value="follow_up">Follow up</option>
                                            <option value="unqualify_lead">Unqualify Lead</option>
                                            <option value="unqualify_prospect">Unqualify Prospect</option>
                                            <option value="converted">Convert to Customer</option>
                                            <option value="demo">Demo</option>
                                            <option value="document_signing">Document Signing</option>
                                            <option value="begin_deal">Begin a Deal</option>
                                            <option value="accepted_deal">Accepted Deal</option>
                                            <option value="rejected_deal">Accepted Deal</option>
                                            <option value="kick-off_meeting">Kick-off Meeting</option>
                                            <option value="project_kick-off">Project kick-off</option>
                                        </select>
                                    </div>
                            </div>


                            <div class="form-group row">
                                    <label for="" class="col-md-3">Notes</label>
                                    <div class="col-md-9">
                                        <textarea name="notes" id="" cols="30" rows="10" class="form-control" placeholder="Enter Notes"></textarea>
                                    </div>
                            </div>
                            <button class="btn btn-primary float-right ml-2 mb-4">Add Activity</button>
                            <button class="btn btn-danger float-right mb-4">Cancel</button>

                            </div>
                        </form>
                </div>
            </div>

       </div>


    </div>
@endsection
