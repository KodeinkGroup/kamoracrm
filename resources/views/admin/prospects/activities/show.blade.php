@extends('admin.layouts.app')


@section ('title', $prospect->name . ' Activity')

@section ('content')
    <div class="container">
        <div class="card mt-3">
            <div class="card-body">

                <div class="d-flex">
                    <h1>{{$prospect->business_name}} <small class="text-muted">{{ucwords(str_replace('_', ' ', $activity->type))}} Activity</small></h1>

                    <div class="ml-auto">
                        <div class="dropdown d-block">
                            <button class="btn btn-secondary btn-block btn-sm dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Actions</button>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" href="{{route('admin.prospects.prospect.dashboard', ['prospect' => $prospect->id])}}">Prospect Dashboard</a>
                                <a class="dropdown-item" href="{{route('admin.prospects.activities.dashboard', ['prospect' => $prospect->id])}}">View Activity</a>
                                        
                                       
                            </div>
                        </div>
                    </div>
                </div>

               
                <hr>

                <h5><strong>Communication Type : </strong>  {{ucwords(str_replace('_', ' ', $activity->communication_type))}}</h5>
                <h6 class="text-muted">Logged on: {{date('F d, Y, @ H:i', strtotime($activity->created_at))}}</h6>
                <hr>
                <h5>Notes</h5>
                <p>{{$activity->notes}}</p>

            </div>
        </div>

        <div class="card mt-3">
            <div class="card-body">
                <div class="d-flex">
                    <h3>Prospect Documents</h3>
                    <div class="ml-auto">
                        <div class="dropdown d-block">
                            <button class="btn btn-outline btn-block btn-sm dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Actions</button>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#addProspectDocument">Add a Document</a>
                                
                                        
                                       
                            </div>
                        </div>
                    </div>
                </div>
               
            </div>
        </div>

        {{-- Prospec Document Modal --}}

        <!-- Button trigger modal -->

  <!-- Modal -->
  <div class="modal fade" id="addProspectDocument" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Add document for {{$prospect->business_name}}</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form action="" method="POST">
              <div class="form-group">
                  <label for="">Document Name</label>
                  <input type="text" name="name" class="form-control">
              </div>
              <div class="form-group">
                <label for="">Document Notes</label>
                <textarea cols="30" rows="10" name="name" class="form-control"> </textarea> 
            </div>

            <div class="form-group">
                <label for="">Select File</label>
                <input type="file" name="name" class="form-control-file">
            </div>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary">Save changes</button>
        </div>
      </div>
    </div>
  </div>



    </div>
@endsection