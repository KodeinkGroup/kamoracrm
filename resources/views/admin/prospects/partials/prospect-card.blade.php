<div class="card mt-3">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-3">
                @if ($prospect->logo)
                    <img src="{{Storage::url($prospect->logo)}}" alt="" width="100">
                @endif
            </div>
            <div class="col-sm-6">
             <h5>   {{$prospect->name}} | <strong>{{$prospect->business_name}}</strong> | <span class="text-muted">{{$prospect->location}}</span></h5>
                <ul class="list-style-none">
                    <li>
                        <strong>Email:</strong> {{$prospect->email}}  |
                        <strong>Contact Number:</strong>{{$prospect->mobile_no}}
                    </li>
    
                    <li><strong>Date Added: </strong> {{$prospect->pretty_created}}</li>
                </ul>
            </div>

            <div class="col-sm-3 col-md-2">
                <div class="dropdown d-block">
                    <button class="btn btn-secondary btn-block btn-sm dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Actions</button>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                        <a class="dropdown-item" href="{{route('admin.prospects.prospect.dashboard', ['prospect' => $prospect->id])}}">Prospect Dashboard</a>
                        <a class="dropdown-item" href="{{route('admin.prospects.edit',['prospect' => $prospect->id])}}">Edit Prospect</a>
                        <a class="dropdown-item" href="{{route('admin.prospects.activities.dashboard', ['prospect' => $prospect->id])}}">View Activity</a>
                                
                               
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>