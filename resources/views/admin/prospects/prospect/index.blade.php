@extends('admin.layouts.app')


@section ('title', $prospect->name . ' Dashboard')

@section ('content')
    <div class="container">
        <div class="card">
            <div class="card-body">
                <h1>Prospect Dashboard <small class="text-muted">{{$prospect->business_name}}</small> </h1>
            </div>
        </div>

        <div class="card mt-3">
            <div class="card-body">
                <div class="d-flex">

                    <h3>Recent Activity </h3>

                    <div class="ml-auto">
                        <div class="dropdown d-block">
                            <button class="btn btn-secondary btn-block btn-sm dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Actions</button>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" href="{{route('admin.prospects.activities.create', $prospect->id)}}">Log Activity</a>
                            </div>
                        </div>
                    </div>

                </div>
                <hr>
                <ul class="list-group">
                    @foreach(ProspectActivity::latest()-> where('prospect_id', '$prospect->id')->limit(5)->get() as $activity)

                    <li class="list-group-item">
                        <h5><a href="{{route('admin.prospects.activities.show', ['prospect' => $prospect->id, 'activity' => $activity->id])}}"> {{ucwords(str_replace('_', ' ', $activity->type))}}</a>  <small class="text-muted float-right"> {{date('F m, y, @ H:i')}}</small></h5>
                    </li>

                    @endforeach
                </ul>
            </div>
        </div>
    </div>
@endsection
