@extends('admin.layouts.app')

@section('content')
    <div class="container-fluid" style="width: 80% !important; background: #fff;">
        <!-- Page Heading -->
        <h1 class="h3 mb-2 text-gray-800">New Invoice</h1>

        <div class="row">
            <div class="col-md-8 col-lg-8">
                {{-- <div class="ml-auto float-right">
                  <div class="dropdown">
                      <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                      Actions
                      </button>
                      <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                          <a class="dropdown-item" href="{{route('admin.prospects.dashboard')}}">Go to Dashboard</a>
                          <a class="dropdown-item" href="{{route('home')}}">Another action</a>
                          <a class="dropdown-item" href="{{route('home')}}">Something else here</a>
                      </div>
                  </div>
              </div> --}}

                <hr>
                @if($errors->count())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $message)
                                <li>{{$message}}</li>
                            @endforeach
                        </ul>

                    </div>
                @endif

            </div>
        </div>
    </div>
@endsection
