@extends('admin.layouts.app')

@section('content')
    <div class="container-fluid">
        @if (session('success'))
            <div class="alert alert-success mt-3">
                {{session('success')}}
            </div>
        @endif
            <div class="d-sm-flex align-items-center justify-content-between mb-4">
                <a href="{{route('admin.estimates.create', ['type' => 'invoice'])}}" class="d-inline-block d-sm-none d-md-none d-lg-none btn btn-sm btn-primary shadow-sm float-right">
                    <i class="fas fa-plus-circle fa-sm text-white"></i>
                </a>
                <a class="d-none btn btn-success float-right" href="{{route('admin.estimates.create', ['type' =>'invoice'])}}">New Recurring Invoice</a>

                <h1>Recurring Invoices</h1>
            </div>

    </div>
@endsection
