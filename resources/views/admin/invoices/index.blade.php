@extends('admin.layouts.app')

@section('content')
<div class="container-fluid">
    @if (session('success'))
    <div class="alert alert-success mt-3">
        {{session('success')}}
    </div>
    @endif
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <a href="{{route('admin.estimates.create', ['type' => 'invoice'])}}" class="d-inline-block d-sm-none d-md-none d-lg-none btn btn-sm btn-primary shadow-sm float-right">
            <i class="fas fa-plus-circle fa-sm text-white"></i>
        </a>
        <h1>Invoices</h1>
        <a class="d-none d-sm-inline-block d-md-inline-block btn btn-success btn-sm float-right" href="{{route('admin.estimates.create', ['type' =>'invoice'])}}"><i class="fas fa-plus text-white mr-3"></i>New Invoice</a>

</div>
@endsection
