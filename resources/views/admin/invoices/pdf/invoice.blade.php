<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
       <meta name="author" content="Kodeink Group">
        <title>Invoice - Kamora CRM</title>

        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css" integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous">
    </head>
    <body>
            <div class="card">
                <div class="card-body" style="margin-top: 20px">
                    <div>
                        <!-- Status badges -->

{{--                        @if ($invoice->status->name == 'Draft')--}}
{{--                            <span class="badge badge-pill badge-warning float-right p-2">{{$invoice->status->name }}</span>--}}
{{--                        @endif--}}

{{--                        @if ($invoice->status->name == 'Sent')--}}
{{--                            <span class="badge badge-pill badge-primary float-right p-2">{{$invoice->status->name }}</span>--}}
{{--                        @endif--}}

{{--                        @if ($invoice->status->name == 'Accepted')--}}
{{--                            <span class="badge badge-pill badge-success float-right p-2">{{$invoice->status->name }}</span>--}}
{{--                        @endif--}}
{{--                        @if ($invoice->status->name == 'Rejected')--}}
{{--                            <span class="badge badge-pill badge-danger float-right p-2">{{$invoice->status->name }}</span>--}}
{{--                        @endif--}}

                        <h2 class="float-left">Invoice <span class="mt-2"><strong>#{{$invoice->id}}</strong></span><br>
                            <span style="font-size: 14px"><span class="text-muted">Created on:</span> {{ date('j \\ F Y', strtotime($invoice->created_at))}}</span>
                        </h2>

                        <div class="mt-4" style="margin-left: 75%">
                            <h5 class="mt-4">{{$company->name }}</h5>
                            <p class="text-muted">
                                <small>{{$company->street}} {{$company->complex}}</small><br>
                                <small>{{$company->surburb}}</small><br>
                                <small>{{$company->city}}</small>
                                <small>{{$company->province}}</small><br>
                                <small>{{$company->country}}</small>

                            </p>

                        </div>
                        <div class="" style="">
                            <h6 class="mt-3 text-muted">Bill to:</h6>
{{--                            <p class="float-right"><small>Expiry Date: <strong>{{$invoice->expiry_date}}</strong></small></p>--}}
                            <h5>{{$invoice->prospect->business_name}} </h5>


                            <p class="text-muted">
                                <small><strong>{{$invoice->prospect->name}}</strong><br>{{$invoice->prospect->email}} <br>
                                    {{$invoice->prospect->mobile_no}}
                                </small>
                            </p>
                        </div>

                        <h5 class="mt-2"> {{$invoice->subject}} </h5>

                    </div>
                    <table class="table table-hover p-5">
                        <thead>
                            <tr>
                                <th style="width:250px">Item Name</th>
                                <th>Qty</th>
                                <th>Rate</th>
                                <th>Discount</th>
                                <th>Amount</th>
                            </tr>
                        </thead>
                        <tbody>
                        {{$invoice->items}}
                        @foreach ($invoice->items as $item )

                            <tr>
                                    <td style="width:250px"><a href="">{{$item->name}}</a><br><small class="text-muted">{{$item->description}}</small>
                                    </td>
                                    <td><small class="text-muted"> {{$item->pivot->quantity}} </small> </td>
                                    <td style="width: 60px"><small class="text-muted">R {{$item->rate}}</small> </td>
                                    <td><small class="text-muted">{{$item->pivot->discount}} %</small> </td>
                                    <td>
                                        <small class="text-muted">R {{ number_format(($item->rate * $item->pivot->quantity) - (($item->rate * $item->pivot->quantity) * ($item->pivot->discount/100)))}}</small>
                                    </td>

                            </tr>

                        @endforeach

                     </table>
{{--                     <p style="padding-left: 30rem"> Discount:<strong> R {{$invoice->discount ?? 0}}</strong></p>--}}
                     <p style="padding-left: 30rem; border-top: 2px solid #ccc"> Total:<strong> R {{ $invoice->amount}}</strong></p>
                    <hr>

                    <h5>Notes:</h5>
                    <p class="text-muted"><small>{{$invoice->notes}}</small></p>
                    <h5>Terms and conditions:</h5>
                    <p class="text-muted"> <small>{{$invoice->terms}}</small></p>
                </div>

            </div>


        {{-- Scripts --}}
        <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-fQybjgWLrvvRgtW6bFlB7jaZrFsaBXjsOMm/tB9LTS58ONXgqbR9W8oWht/amnpF" crossorigin="anonymous"></script>
    </body>

</html>
