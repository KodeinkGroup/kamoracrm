@extends('admin.layouts.app')

@section('content')
    <div class="container-fluid">
        <!-- Page Heading -->

        <div class="row">
            <div class="col-md-9 col-lg-9">
                @if($errors->count())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $message)
                                <li>{{$message}}</li>
                            @endforeach
                        </ul>

                    </div>
                @endif
                <div class="card mt-5" style="border-radius: 22px; padding: 20px">
                    <div class="card-head">
                        <h5 class="h3 mb-2 ml-3">Add Schedule Entry</h5>
                    </div>
                    <div class="card-body">
                        <form action="{{route('admin.schedules.store')}}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="form-row">
                                <div class="form-group col-md-4">
                                    <label for="">Schedule Date</label>
                                    <input type="date" class="form-control" name="schedule_date">
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="">Job ID</label>
                                    <input type="text" class="form-control" name="job_id">
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="">Account ID</label>
                                    <input type="text" class="form-control" name="acc_id">
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-4">
                                    <label for="lead-source">Job Type</label>
                                    <select name="job_type" id="job_type" class="form-control" required>
                                        <option value="" selected disabled>Select Job Type</option>
                                        <option value="installs">Installs</option>
                                        <option value="maintenance">Maintenance</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="">Area</label>
                                    <input type="text" class="form-control {{$errors->has('area') ? 'is-valid' : ''}}" name="area" required>
                                    @if ($errors->has('area'))
                                        <div class="invalid-feedback">
                                            {{ $errors->first('area') }}
                                        </div>
                                    @endif
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="">Fibre Used</label>
                                    <input type="text" class="form-control" name="fibre_used" >
{{--                                    @if ($errors->has('fibre_used'))--}}
{{--                                        <div class="invalid-feedback">--}}
{{--                                            {{ $errors->first('fibre_used') }}--}}
{{--                                        </div>--}}
{{--                                    @endif--}}
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="">Rate</label>
                                    <input type="text" class="form-control" name="rate" required>
{{--                                    @if ($errors->has('model'))--}}
{{--                                        <div class="invalid-feedback">--}}
{{--                                            {{ $errors->first('model') }}--}}
{{--                                        </div>--}}
{{--                                    @endif--}}
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="form-group col-md-8">
                                    <label for="">Comments</label>
                                    <textarea cols="5" rows="5" class="form-control" name="vin"> </textarea>
                                </div>
                            </div>

                            <button type="submit" class="btn btn-primary float-right">Add Entry</button>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
