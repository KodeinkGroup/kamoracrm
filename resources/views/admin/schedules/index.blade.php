@extends('admin.layouts.app')
@section('content')
    <div id="app">
        <div class="container-fluid">
            <!-- Page Heading -->
            <div class="d-sm-flex align-items-center justify-content-between mb-4">
                <a href="{{route('admin.schedules.create')}}" class="d-inline-block d-sm-none d-md-none d-lg-none btn btn-sm btn-primary shadow-sm float-right">
                    <i class="fas fa-plus-circle fa-sm text-white"></i>
                </a>

                <h1 class="h3 mb-0">Job Schedules</h1>
                <a href="{{route('admin.schedules.create')}}" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">
                    <i class="fas fa-plus-circle fa-sm text-white-50"></i> New Schedule Entry
                </a>
            </div>
            <div class="row">
                <div class="col-md-12 col-lg-12">
                    <div class="card">
                        <div class="card-header">Company Schedules</div>
                        <div class="card-body">
                            <table class="table align-middle mb-0 bg-white" style="width: 100%">
                                <thead class="bg-light">
                                <tr>
                                    <th style="border: none"></th>
                                    <th style="border: none">Team</th>
                                    <th style="border: none">Date</th>
                                    <th style="border: none">Job ID</th>
                                    <th style="border: none">Acc ID</th>
                                    <th style="border: none">Area</th>
                                    <th style="border: none">Type</th>
                                    <th style="border: none">Rate</th>
                                    <th style="border: none">Fibre(metres)</th>
                                    <th style="border: none">Comments</th>
{{--                                    <th style="border: none"></th>--}}
                                </tr>
                                </thead>
                                <tbody>
                                @if(!empty($schedules))
                                    @foreach($schedules as $schedule)
                                        <tr>
                                            <td>
                                                <i class="fa fa-calendar"></i>
                                            </td>
                                            <td>{{$schedule->team}}</td>
                                            <td>{{$schedule->date}}</td>
                                            <td>{{$schedule->jobId}}</td>
                                            <td>{{$schedule->accId}}</td>
                                            <td>{{$schedule->area}}</td>
                                            <td>{{$schedule->jobType}}</td>
                                            <td>R {{$schedule->rate}}</td>
                                            <td>{{$schedule->fibre_used}} (m)</td>
                                            <td>{{$schedule->comments}}</td>
{{--                                            <td><a href="{{route('admin.company.fleets.delete', ['id' => $schedule->id])}}"><i class="fa fa-trash text-danger"></i> </a></td>--}}
                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
