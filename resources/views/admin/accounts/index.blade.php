@extends('admin.layouts.app')

@section('content')
    <div class="container-fluid">
        @if (session('success'))
            <div class="alert alert-success mt-3">
                {{session('success')}}
            </div>
        @endif

        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <a href="{{route('admin.prospects.create', ['type' => 3])}}" class="d-inline-block d-sm-none d-md-none d-lg-none btn btn-sm btn-primary shadow-sm float-right">
                <i class="fas fa-user-plus fa-sm text-white"></i>
            </a>
            <h1 class="h3 mb-0">Business Customers</h1>
            <a href="{{route('admin.prospects.create', ['type' => 3])}}" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">
                <i class="fas fa-download fa-sm text-white-50"></i> New Account
            </a>
        </div>

        @if (!empty($accounts))
          <div class="card shadow mb-4">
              <div class="card-header py-3">
                  <h6 class="m-0 font-weight-bold text-primary">Customers</h6>
              </div>
              <div class="card-body">
                  <div class="alert alert-success alert-dismissible">
                      <button type="button" class="close" data-dismiss="alert">x</button>
                      <i class="fas fa-info-circle fa-2x mr-4"></i>
                      <p> In our definition, Accounts are customers that have procured your products or services in past. <br>You can create Quotes/Estimates for them also for any new work.
                      </p>
                  </div>
                  <div class="table-responsive">
                      <table class="table" style="width: 100%;">
                          <thead>
                          <tr>
                              <th>Logo</th>
                              <th>Contact Person</th>
                              <th>Company</th>
                              <th>Email</th>
                              <th>Contacts</th>
                              <th>Date Added</th>
                              <th>Source</th>
                              <th>Actions</th>
                          </tr>
                          </thead>

                          <tbody>
                          @foreach ($accounts as $account)

                              <tr>
                                  <td>

                                      @if ($account->logo)
                                          <img src="{{Storage::url($account->logo)}}" alt="" width="100">
                                      @else
                                          <img class="img-profile rounded-circle" src="{{asset('img/undraw_profile.svg')}}"
                                               width="50">
                                      @endif
                                  </td>
                                  <td>
                                      <a href="{{route('admin.prospects.prospect.dashboard', ['prospect' => $account->id])}}">  {{$account->name}} </a><br>
                                      <small class="text-muted">{{$account->location}}</small>

                                  </td>
                                  <td>{{$account->business_name}}<br>
                                      <small class="text-muted">{{$account->designation}}</small>
                                  </td>
                                  <td>{{$account->email}}</td>

                                  <td>{{$account->mobile_no}}</td>
                                  <td>{{$account->created_at->format('d F Y')}}</td>
                                  <td>{{$account->source->name}} <br>
                                      <small class="text-muted">{{$account->salesperson}}</small>
                                  </td>
                                  <td>
                                      <a class="btn btn-default"
                                         href="{{route('admin.prospects.edit', ['prospect' => $account->id])}}">
                                          <i class="fas fa-pen"></i>
                                      </a>
                                      <a class="btn btn-default"
                                         href="{{route('admin.prospects.activities.dashboard', ['prospect' => $account->id])}}">
                                          <i class="fas fa-eye"></i>
                                      </a>
                                      <a class="btn btn-default" href="{{route('admin.prospects.delete', ['prospect' => $account->id])}}"
                                         title="Delete Prospect">
                                          <i class="fas fa-trash text-danger"></i>
                                      </a>
                                      {{-- <div class="dropdown d-block">
                                          <button class="btn btn-secondary btn-block btn-sm dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Actions</button>
                                          <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                                              {{-- <a class="" href="{{route('admin.prospects.prospect.dashboard', ['prospect' => $account->id])}}">Prospect Dashboard</a> --}}
                                      {{-- <a class="dropdown-item" href="{{route('admin.prospects.edit',['prospect' => $account->id])}}">Edit Prospect</a>


                                  </div>
                              </div> --}}
                                  </td>
                              </tr>

                          @endforeach
                          </tbody>
                      </table>
                  </div>
              </div>
          </div>
        @else
            <div class="alert alert-warning">
                <p>There are currently no customers saved. </p>
            </div>
        @endif


    </div>
@endsection
