@extends('admin.layouts.app')

@section('content')
    <div class="container-fluid">

        @if (session('success'))
            <div class="alert alert-success mt-3">
                {{session('success')}}
            </div>
        @endif
        <!-- Page Heading -->
        <h1 class="h3 mb-2 text-gray-800">Subscriptions</h1>
        <div class="row">
            <div class="col-md-8 col-lg-8">
                <div class="card mt-3" style="border-radius: 22px; padding: 20px;">
                    {{--                    <div class="card-header">Company Vehicles</div>--}}
                    <div class="card-body">
                        <table class="table align-middle mb-0 bg-white" style="width: 100%">
                            <thead class="bg-light">
                                <tr>
                                    <th style="border: none"></th>
                                    <th style="border: none">Name</th>
                                    <th style="border: none">Plan</th>
                                    <th style="border: none">Start Date</th>
                                    <th style="border: none">End Date</th>
                                    <th style="border: none">Status</th>
                                    <th style="border: none">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(!empty($subscriptions))
                                    @foreach($subscriptions as $subscription)
                                        <tr>
                                            <td>
                                                <img class="img-profile rounded-circle"
                                                     src="{{asset('assets/img/icons/savings.svg')}}"
                                                     width="25">
                                            </td>
                                            <td>
                                                <span>{{$subscription->company->name}}</span>
                                            </td>
                                            <td>
                                                <span><strong>R {{$subscription->plan->price}} -
                                                    </strong><span class="text-primary">{{$subscription->plan->name}}</span>
                                                </span>
                                            </td>
                                            <td>
                                                <span class="text-success">{{$subscription->start_date->format('d M Y')}}</span>
                                            </td>
                                            <td>
                                                <span class="text-danger">{{$subscription->end_date->format('d M Y')}}</span>
                                            </td>
                                            <td>
                                                @if($subscription->subscription_status_id === 4)
                                                    <span class="badge badge-dot"><i class="bg-primary"></i>Trial Active</span> <br>
                                                    <span class="text-muted"><small><strong>{{$subscription->trial_days_left}}</strong> days left</small></span>
                                                @else
                                                    <span class="badge badge-dot"><i class="bg-danger"></i>Trial Ended</span>
                                                @endif()
                                            </td>

                                        </tr>
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-lg-4">
                <div class="card mt-3" style="border-radius: 22px; padding: 23px;width: 85%;">
                </div>
            </div>
        </div>
    </div>
@endsection
