@extends('admin.layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <i class="fas fa-info-circle fa-2x mr-4"></i>
            Please try to update you company details as much as possible to have a nicer profile.
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        @if($company)
            <div class="card mt-3">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-3">
                            @if ($company->logo_url)
                                <img src="{{Storage::url($company->logo_url)}}" alt="" width="200">
                            @else
                                <img src="{{asset('img/undraw_profile.svg')}}" alt="" width="200">
                            @endif
                            <hr>
                            <button class="btn btn-outline-primary btn-sm" data-toggle="modal"
                                    data-target="#updateLogoModal">Upload New Logo
                            </button>
                            {{--                            @if($company->logo_url)--}}
                            {{--                                <button class="btn btn-outline-danger btn-sm ml-2" id="delBtn"> <i class="fas fa-trash"></i> Remove Logo</button>--}}
                            {{--                            @endif--}}
                            {{--                            <form action="#" method="POST" id="delete-logo-form">--}}
                            {{--                                @csrf--}}
                            {{--                                @method('DELETE')--}}
                            {{--                            </form>--}}

                        </div>
                        <div class="col-md-9">
                            <div class="d-flex">
                                <div class="ml-auto">
                                    <a class="btn btn-secondary"
                                       href="{{route('admin.company.edit', ['id' => $company->id])}}"><i class="fas fa-pen"></i> Edit Company</a>
                                </div>
                            </div>

                            <h1>{{$company->name}}</h1>
                            <p class="text-muted"><small><i class="fas fa-calendar" style="color: #00aced"></i>
                                    Registered
                                    In: <strong>{{$company->year_registered}}</strong> </small></p>
                            <p class="text-muted"><small><i class="fas fa-certificate"
                                                            style="color: red"></i> {{$company->BBBEE_level ?? 'No BBB-EE Level'}}
                                </small></p>
                            <hr>
                        </div>
                    </div>

                    <hr>

                    <p class="text-muted"><i class="fas fa-envelope"> </i> {{$company->email ?? 'name@company.io'}}</p>
                    <p class="text-muted"><i class="fas fa-phone"></i> {{$company->contact_number ?? '+00 00 000 0000'}}
                    </p>

                    <p class="text-muted"><i
                            class="fas fa-barcode"></i> {{$company->registration_number ?? '000000/0000/00'}}</p>
                    <hr>
                    <h5>Address</h5>
                    <p class="text-muted"><i
                            class="fas fa-map-marker"></i> {{$company->street}} {{$company->complex}} {{$company->surburb}}
                        {{$company->city}} {{$company->province}} {{$company->country}}
                    </p>

                </div>
            </div>

            <div class="mt-5">
                <div class="row">
                    <div class="col-md-7">
                        <div class="card">
                            <div class="card-header">Company Departments</div>
                            <div class="card-body">
                                <table class="table">
                                    <thead>
                                        <th>Department</th>
                                        <th>Lead/Manager</th>
                                        <th></th>
                                    </thead>
                                    <tbody>
                                           @if(!empty($company->departments))
                                               @foreach($company->departments as $department)
                                                   <tr>
                                                        <td>{{$department->name}}</td>
                                                        <td>{{$department->manager ? $department->manager->name : ''}}
                                                            <form id="edit-manager-form-{{$department->id}}" class="hidden">
                                                               <select name="department_manager_id" class="form-control">
                                                                   <option disabled selected>Select a Manager</option>
                                                                   @foreach($company->users as $user)
                                                                       <option value="{{$user->id}}">{{$user->name}}</option>
                                                                   @endforeach
                                                               </select>
                                                                <button type="submit" class="btn btn-primary mt-3 float-right">Submit</button>
                                                            </form>
                                                        </td>
                                                       <td>
                                                           <a href="#" class="dept-edit-btn" data-company-id="{{$company->id}}" id="{{$department->id}}">
                                                               <i class="fa fa-pen text-primary"></i>
                                                           </a>
                                                           <a href="#" class="dept-edit-close-btn hidden" id="{{$department->id}}">
                                                               <i class="fas fa-window-close text-primary ml-3"></i>
                                                           </a>
                                                       </td>
                                                   </tr>
                                               @endforeach
                                            @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="card">
                            <div class="card-header">Add New Company Departments</div>
                            <div class="card-body">
                                <form action="{{route('admin.company.store.department', ['company' => $company->id])}}" method="POST" id="new-department-form">
                                    @csrf
                                    <div class="form-group">
                                        <label for="name">Department Name <span class="text-muted">*</span></label>
                                        <input type="text" name="name" class="form-control {{$errors->has('name') ? 'is-invalid' : ''}}">
                                        @if ($errors->has('name'))
                                            <div class="invalid-feedback">
                                                {{ $errors->first('name') }}
                                            </div>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <label for="email">Manager <span class="text-muted">*</span></label>
                                        <select name="manager_id" id="" class="form-control">
                                            <option value="" disabled selected>Select a Manager</option>
                                            @if(isset($company->users))
                                                @foreach($company->users as $user)
                                                    <option value="{{$user->id}}">{{$user->name}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>

{{--                                    <div class="form-group">--}}
{{--                                        <label for="role">User Role</label>--}}
{{--                                        <select name="role" id="" class="form-control">--}}
{{--                                            <option value="user" default>User</option>--}}
{{--                                            <option value="sales_admin" >Sales Admin</option>--}}
{{--                                            <option value="system_admin">System Admin</option>--}}
{{--                                            <option value="inventory_manager">Inventory Manager</option>--}}
{{--                                            <option value="technician">Technician</option>--}}

{{--                                        </select>--}}
{{--                                    </div>--}}
{{--                                    <div class="form-group">--}}
{{--                                        <label for="password">Password <span class="text-muted">*</span></label>--}}
{{--                                        <input type="password" name="password" class="form-control {{$errors->has('password') ? 'is-invalid' : ''}}" placeholder="*********">--}}
{{--                                        @if ($errors->has('password'))--}}
{{--                                            <div class="invalid-feedback">--}}
{{--                                                {{ $errors->first('password') }}--}}
{{--                                            </div>--}}
{{--                                        @endif--}}
{{--                                    </div>--}}

{{--                                    <div class="form-group">--}}
{{--                                        <label for="confirm_password">Confirm Password <span class="text-muted">*</span></label>--}}
{{--                                        <input type="password" name="confirm_password" class="form-control {{$errors->has('confirm_password') ? 'is-invalid' : ''}} " placeholder="********">--}}

{{--                                        @if ($errors->has('confirm_password'))--}}
{{--                                            <div class="invalid-feedback">--}}
{{--                                                {{ $errors->first('confirm_password') }}--}}
{{--                                            </div>--}}
{{--                                        @endif--}}
{{--                                    </div>--}}
                                    <button type="submit" class="btn btn-primary float-right">Add Department</button>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif

    </div>

    <!-- Updating the logo/profile picture -->
    <div class="modal fade" id="updateLogoModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Update Logo</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                @if($company)
                    <div class="modal-body">
                        <form action="{{route('admin.company.update.logo-update', $company->id)}}" method="POST"
                              enctype="multipart/form-data">
                            @csrf
                            @method('PUT')

                            <div class="form-group">
                                <label for="">Choose Image</label>
                                <input type="file" class="form-control-file" name="image">
                            </div>
                            <button class="btn btn-primary float-right">Update Logo</button>
                        </form>
                    </div>
                @endif

            </div>
        </div>
    </div>
@endsection
