    @extends('admin.layouts.app')

    @section('content')
        <div class="container-fluid">
            <!-- Page Heading -->
            <h1 class="h3 mb-2 text-gray-800">Company Details</h1>
            <div class="row">
                <div class="col-md-8 col-lg-8">

                    <hr>
                    @if($errors->count())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $message)
                                    <li>{{$message}}</li>
                                @endforeach
                            </ul>

                        </div>
                    @endif

                    <div class="card">
                        <div class="card-head"></div>
                        <div class="card-body">
                            <form action="{{route('admin.company.update', ['company' => $company])}}" method="POST" enctype="multipart/form-data">
                                @csrf
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for="">Business Name</label>
                                        <input type="text" class="form-control" name="name" value="{{$company->name}}">
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="">Business Size</label>
                                        <select class="form-control" name="size">
                                            <option value="{{$company->size}}">Small Business</option>
                                            <option value="Medium Sized Business">Medium Sized Business</option>
                                            <option value="Large Enterprise">Large Enterprise</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for="">Business Email</label>
                                        <input type="email" class="form-control" name="email" value="{{$company->email}}">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="mobile_no">Contact No:</label>
                                        <input type="text" class="form-control" name="contact_number" value="{{$company->contact_number}}">
                                    </div>
                                </div>

                                <div class="form-row">
                                    {{--                                <div class="form-group col-md-6">--}}
                                    {{--                                    <label for="lead-source">Source </label>--}}
                                    {{--                                    <select name="source_id" id="sources" class="form-control">--}}
                                    {{--                                        <option value="" selected disabled>Please select source</option>--}}

                                    {{--                                        @foreach ($sources as $source)--}}
                                    {{--                                            <option value="{{$source->id}}">{{$source->name}}</option>--}}
                                    {{--                                        @endforeach--}}

                                    {{--                                    </select>--}}
                                    {{--                                </div>--}}
                                    <div class="form-group col-md-4">
                                        <label for="">B-BBEE Level</label>
                                        <select class="form-control" name="bbbee_level">
                                            <option value="BBB-EE Level 1">Level 1</option>
                                            <option value="BBB-EE Level 2">Level 2</option>
                                            <option value="BBB-EELevel 3">Level 3</option>
                                            <option value="BBB-EE Level 4">Level 4</option>
                                            <option value="BBB-EE Level 5">Level 5</option>
                                            <option value="BBB-EE Level 6">Level 6</option>
                                            <option value="BBB-EE Level 7">Level 7</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="">Registration No</label>
                                        <input type="text" class="form-control" name="registration_number" value="{{$company->registration_number}}">
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="">Year Registered</label>
                                        <input type="text" class="form-control" name="year_registered" value="{{$company->year_registered}}">
                                    </div>
                                </div>
                                <hr>
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for="">Street Address</label>
                                        <input type="text" class="form-control" name="street" value="{{$company->street}}">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="">Surburb</label>
                                        <input type="text" class="form-control" name="surburb" value="{{$company->surburb}}">
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for="">City</label>
                                        <input type="text" class="form-control" name="city" value="{{$company->city}}">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="">Province</label>
                                        <input type="text" class="form-control" name="province" value="{{$company->province}}">
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-2">
                                        <label for="">ZIP Code</label>
                                        <input type="text" class="form-control" name="zip_code" value="{{$company->zip_code}}">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="">Country</label>
                                        <input type="text" class="form-control" name="country" value="{{$company->country}}">
                                    </div>
                                </div>

                                <button type="submit" class="btn btn-primary">Save</button>
                            </form>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    @endsection
