@extends('admin.layouts.app')

@section('content')
   <div class="container-fluid">

       <div class="row mt-5">
           <div class="col-md-9 col-lg-9">
               <div class="card" style="border-radius:22px; padding: 20px">
                   <div class="card-head"></div>
                   <div class="card-body">
                       <table class="table align-middle mb-0 bg-white" style="width: 100%">
                           <thead class="bg-light">
                           <tr>
                               <th style="border: none"></th>
                               <th style="border: none">Leave Type</th>
                               <th style="border: none">Accrual Rate</th>
                               {{--                                <th style="border: none">Contacts</th>--}}
                               <th style="border: none">Maximum Leave Days</th>
                               <th style="border: none">Carried Over</th>
                               <th style="border: none">Actions</th>

                           </tr>
                           </thead>
                           <tbody>
                           @if(!empty($leaveTypes))
                               @foreach($leaveTypes as $leaveType)
                                   <tr>
                                       <td></td>
                                       <td><span>{{$leaveType->name}}</span></td>
                                       <td>
                                           <span>{{$leaveType->accrual_rate}}</span>
                                       </td>
                                      <td><span class="text-muted">{{$leaveType->days_per_year}}</span>
                                       </td>
                                       <td>{{$leaveType->carried_over}}</td>
                                       <td>
                                           <a href="" class="text-dark" style="text-decoration: none">Edit
                                           </a>
                                       </td>
                                   </tr>
                               @endforeach
                           @endif
                           </tbody>
                       </table>
                   </div>
               </div>
           </div>
       </div>
   </div>
@endsection
