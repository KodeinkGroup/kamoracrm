@extends('admin.layouts.app')

@section('content')
    <div class="container-fluid">
        @if (session('success'))
            <div class="alert alert-success mt-3">
                {{session('success')}}
            </div>
        @endif

        <!-- Page Heading -->
        <h1 class="h3 mb-2 text-gray-800">Material Requests</h1>
        <div class="row">
            <div class="col-md-9 col-lg-9">
                <div class="card mt-3" style="border-radius: 22px; padding: 23px;width: 85%;">
{{--                    <div class="card-head ml-5"><h5>Recent Material Requests</h5></div>--}}
                    <div class="card-body">
                        <table class="table mt-3" style="width: 100%">
                            <thead>
                                <tr>
                                    <th>Item</th>
                                    <th>Project</th>
                                    <th>Requested By</th>
                                    <th>Status</th>
{{--                                    <th>Quantity</th>--}}
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody class="mt-3">
                               @if(!empty($materialRequests))
                                   @foreach($materialRequests as $materialRequest)
                                       <tr>
                                           <td>
                                               <span>{{$materialRequest->item->name ?? ''}}</span> <br>
                                               <small>Qty: <strong>{{$materialRequest->quantity}}</strong></small>
                                           </td>
                                           <td>
                                               <span class="text-muted">{{$materialRequest->project->name}}</span>
                                           </td>
                                           <td>
                                               <span>{{$materialRequest->user->name}}</span> <br>
                                               <span><small class="text-muted">{{$materialRequest->created_at->format('d M Y')}}</small></span>
                                           </td>
                                           <td>
                                               @if($materialRequest->status == 0)<span class="badge badge-dot"><i class="bg-primary"></i> New </span> @endif
                                               @if($materialRequest->status == 1)<span class="badge badge-dot"><i class="bg-success"></i> Approved</span> @endif
                                               @if($materialRequest->status == 2)<span class="badge badge-dot"><i class="bg-danger"></i> Rejected </span> @endif
                                           </td>
{{--                                           <td>--}}
{{--                                               <span>{{$materialRequest->quantity}}</span>--}}
{{--                                           </td>--}}
                                           <td>
                                               @if(\Illuminate\Support\Facades\Auth::user()->role == 'inventory_manager' || \Illuminate\Support\Facades\Auth::user()->role == 'admin' ||
                             \Illuminate\Support\Facades\Auth::user()->role == 'master')
                                                   @if($materialRequest->status !== 1 && $materialRequest->status !== 2)
                                                       <a class="update-material-request-btn" href="#" data-request-id="{{$materialRequest->id}}"
                                                          data-status="1"><i class="fas fa-check-circle text-success"></i>
                                                       </a>
                                                   @endif
                                                   @if($materialRequest->status !== 2 && $materialRequest->status !== 1)
                                                       <a class="ml-3 update-material-request-btn" href="#" data-request-id="{{$materialRequest->id}}" data-status="2">
                                                           <i class="fas fa-thumbs-down text-danger"></i>
                                                       </a>
                                                   @endif
                                               @endif
                                           </td>
                                       </tr>
                                   @endforeach
                               @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
