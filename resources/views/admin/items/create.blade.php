@extends('admin.layouts.app')

@section('content')
    <div class="container-fluid">
        @if($errors->count())
             <div class="alert alert-danger">
                 <ul>
                     @foreach ($errors->all() as $message)
                         <li>{{$message}}</li>
                     @endforeach
                 </ul>
             </div>
        @endif
{{--            <h3 class="h3 mb-2 mt-3 text-gray">Add New Item</h3>--}}

            <div class="card mt-5" style="border-radius: 22px; padding: 23px;width: 75%;">
            <div class="card-head"><h5 class="ml-3">Adding New Item</h5></div>
            <div class="card-body">
                <form action="{{route('admin.items.store')}}" method="POST" enctype="multipart/form-data">
                    @csrf

                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="">Name</label>
                            <input type="text" class="form-control  {{$errors->has('name') ? 'is-invalid' : ''}}" name="name" placeholder="Web Hosting" required>
                            @if ($errors->has('name'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('name') }}
                                </div>
                            @endif
                        </div>
                        <div class="form-group col-md-6">
                            <label for="">Department</label>
                            <select class="form-control" name="category">
                                <option value="" selected="selected" disabled="disabled">Select Department</option>
                                <option value="Civil">Civil</option>
                                <option value="Installations">Installations</option>
                                <option value="Maintenance">Maintenance</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label for="">Description</label>
                           <textarea name="description" class="form-control" id="" cols="30" rows="5"></textarea>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="">Serial Number</label>
                            <input type="text" class="form-control" name="serial_number" placeholder="0000-000-00">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="">Brand</label>
                            <input type="text" class="form-control" name="brand" placeholder="">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="">Supplier</label>
                            <input type="text" class="form-control" name="supplier" placeholder="">
                        </div>

                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="">Rate</label>
                            <input type="number" class="form-control" name="rate" placeholder="1,000">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="">Quantity</label>
                            <input type="number" class="form-control" name="quantity" placeholder="10">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="">Condition</label>
                            <select class="form-control" name="condition">
                                <option value="1">New</option>
                                <option value="2">Returned</option>
                                <option value="3">Damaged</option>
                                <option value="4">Out of Stock</option>
                            </select>
                        </div>
                    </div>


                    {{-- <div class="form-group">
                        <label for="">Logo</label>
                        <input type="file" class="form-control-file" name="logo">
                    </div> --}}
                    <button type="submit" class="btn btn-primary float-right">Add New Item</button>
                </form>
            </div>
        </div>

    </div>
@endsection
