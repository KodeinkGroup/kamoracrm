@extends('admin.layouts.app')

@section('content')
    <div class="container-fluid">
        <h1 class="h3 mb-2 text-gray-800">Update {{$item->name}}</h1>

        <hr>
        @if($errors->count())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $message)
                        <li>{{$message}}</li>
                    @endforeach
                </ul>

            </div>
        @endif
        <div class="card">
            <div class="card-head"></div>
            <div class="card-body">
                <form action="{{route('admin.items.update', ['id' => $item->id])}}" method="POST" enctype="multipart/form-data">
                    @csrf
                    @method('PUT') <!-- Use PUT or PATCH method here -->

                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="">Name</label>
                            <input type="text" value="{{$item->name}}" class="form-control" name="name" placeholder="Web Hosting">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="">Category</label>
                            <input type="text" class="form-control" name="category" value="{{$item->category}}" placeholder="Web Hosting">
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label for="">Description</label>
                            <textarea name="description" class="form-control" id="" cols="30" rows="10">{{$item->description}}</textarea>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="">Serial Number</label>
                            <input type="text" class="form-control"  value="{{$item->serial_number}}"name="serial_number" placeholder="0000-000-00">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="">Brand</label>
                            <input type="text" class="form-control" value="{{$item->brand}}" name="brand" placeholder="">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="">Supplier</label>
                            <input type="text" class="form-control" name="supplier" value="{{$item->supplier}}" placeholder="">
                        </div>

                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="">Rate</label>
                            <input type="number" class="form-control" name="rate" value="{{$item->rate}}">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="">Quantity</label>
                            <input type="number" class="form-control" name="quantity" value="{{$item->quantity}}">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="">Condition</label>
                            <select class="form-control" name="condition">
                                <option value="1">New</option>
                                <option value="2">Returned</option>
                                <option value="3">Damaged</option>
                                <option value="4">Out of Stock</option>
                            </select>
                        </div>
                    </div>

                    {{-- <div class="form-group">
                        <label for="">Logo</label>
                        <input type="file" class="form-control-file" name="logo">
                    </div> --}}
                    <button type="submit" class="btn btn-primary">Update Item</button>
                </form>
            </div>
        </div>

    </div>
@endsection
