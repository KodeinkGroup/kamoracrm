@extends('admin.layouts.app')

@section('content')
    <div class="container-fluid">
        @if (session('success'))
            <div class="alert alert-success mt-3">
                {{session('success')}}
            </div>
        @endif

        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <a href="{{route('admin.items.create')}}" class="d-inline-block d-sm-none d-md-none d-lg-none btn btn-sm btn-primary shadow-sm float-right">
                <i class="fas fa-plus-circle fa-sm text-white"></i>
            </a>

            <h1 class="h3 mb-0">Sales Inventory</h1>
            <a href="{{route('admin.items.create')}}" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">
                <i class="fas fa-plus-circle fa-sm text-white-50"></i> New Item
            </a>
        </div>

        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Products or Services</h6>
            </div>
            <div class="card-body">
                <div class="alert alert-primary alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert">x</button>
                    <i class="fas fa-info-circle fa-2x mr-4"></i>
                    <p> You can add your Products or Services here with all their rates.
                        Keep this section as updated as possible for accurate Estimates and Invoices and Inventory Management.
                    </p>
                </div>
                <div class="table-responsive">
                    <table class="table table-hover" style="width: 100%">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Description</th>
                            <th>Rate</th>
                            <th>Quantity</th>
                            <th>Serial No</th>
                            <th>Condition</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($items as $item )

                            <tr>

                                <td><a href=""> {{$item->name}} </a> <br>
                                    <small><strong>Department:</strong> {{$item->category ?? 'Not Categorised'}} </small>
                                </td>
                                <td>
                                    <small class="text-muted">{{$item->description}}</small> <br>
                                    <small><strong>Brand:</strong> {{$item->brand}} </small>
                                </td>
                                <td><small class="text-muted">{{$item->rate}}</small></td>
                                <td><small class="text-muted">{{$item->quantity}}</small></td>
                                <td><small class="text-muted">{{$item->serial_number}}</small></td>
                                <td><small class="text-muted">{{$item->condition}}</small></td>
                                <td>
                                    <a href="{{route('admin.items.edit', ['id' => $item->id])}}" class=""><i class="fas fa-pen"></i></a>
                                    <a class="ml-2" href="{{route('admin.items.destroy', ['id' => $item->id])}}"> <i class="fas fa-trash text-danger"></i> </a>
                                    {{-- <div class="dropdown d-block">
                                        <button class="btn btn-secondary btn-block btn-sm dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Actions</button>
                                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                                            {{-- <a class="" href="{{route('admin.prospects.prospect.dashboard', ['prospect' => $prospect->id])}}">Prospect Dashboard</a> --}}
                                    {{-- <a class="dropdown-item" href="{{route('admin.prospects.edit',['prospect' => $prospect->id])}}">Edit Prospect</a>


                                </div>
                            </div> --}}
                                </td>
                            </tr>

                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>
@endsection
