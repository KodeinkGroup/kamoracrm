@extends('admin.layouts.app')

@section('content')
    <div class="container-fluid">
        <!-- Page Heading -->
        <h1 class="h3 mb-2 text-gray-800">Add Fleet Type</h1>
        <div class="row">
            <div class="col-md-9 col-lg-9">
                @if($errors->count())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $message)
                                <li>{{$message}}</li>
                            @endforeach
                        </ul>

                    </div>
                @endif
                <div class="card">
                    <div class="card-head"></div>
                    <div class="card-body">
                        <form action="{{route('admin.company.fleets.store')}}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="form-row">
                                <div class="form-group col-md-12">
                                    <label for="">Name</label>
                                    <input type="text" class="form-control" name="name" required>
                                </div>
                                <div class="form-group col-md-12">
                                    <label for="">Description</label>
                                    <textarea rows="5" cols="30" class="form-control" name="description"></textarea>

                                    <button type="submit" class="btn btn-primary btn-sm float-right mt-4">Add Fleet</button>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
