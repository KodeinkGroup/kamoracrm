@extends('admin.layouts.app')
@section('content')
    <div class="container-fluid">
        <!-- Page Heading -->
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <a href="{{route('admin.company.fleets.create')}}" class="d-inline-block d-sm-none d-md-none d-lg-none btn btn-sm btn-primary shadow-sm float-right">
                <i class="fas fa-plus-circle fa-sm text-white"></i>
            </a>

            <h1 class="h3 mb-0">Fleet Management</h1>
            <a href="{{route('admin.company.fleets.create')}}" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">
                <i class="fas fa-plus-circle fa-sm text-white-50"></i> New Fleet
            </a>
        </div>
        <div class="row">
            <div class="col-md-12 col-lg-12">
                <div class="card">
                    <div class="card-header">Company Vehicles</div>
                    <div class="card-body">
                        <table class="table align-middle mb-0 bg-white" style="width: 100%">
                            <thead class="bg-light">
                            <tr>
                                <th style="border: none"></th>
                                <th style="border: none">Name</th>
                                <th style="border: none">Description</th>
                                <th style="border: none">Status</th>
                                <th style="border: none">Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                                @if(!empty($company->fleets))
                                    @foreach($company->fleets as $fleet)
                                        <tr>
                                            <td>
                                                <img class="img-profile rounded-circle"
                                                     src="{{asset('img/generic-bakkie.png')}}"
                                                     width="75">
                                            </td>
                                            <td>{{$fleet->name}}</td>
                                            <td>{{$fleet->description}}</td>
                                            <td>{{$fleet->status}}</td>
                                            <td><a href="{{route('admin.company.fleets.delete', ['id' => $fleet->id])}}"><i class="fa fa-trash text-danger"></i> </a></td>
                                        </tr>
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
