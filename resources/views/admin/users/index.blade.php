@extends('admin.layouts.app')

@section('content')

    <div class="row">
        {{--        <div class="col-md-2">--}}
        {{--            @component('admin.layouts.menus.sidebar') @endcomponent--}}
        {{--        </div> --}}
        {{-- / End Menu--}}

        <div class="col-md-7">
            <div class="card">
                <div class="card-header">Current users</div>
                <div class="card-body">
                    @if(!empty($companies))
                        @foreach($companies as $company)
                            <table class="table align-middle mb-0 bg-white" style="width: 100%">
                                <h5>{{$company->name}}</h5>
                                <thead class="bg-light">
                                <tr>
                                    <th style="border: none"></th>
                                    <th style="border: none">Name</th>
                                    <th style="border: none">Role</th>
                                    <th style="border: none">Department</th>
{{--                                    <th style="border: none">Created Date</th>--}}
                                    <th style="border: none">Last Login</th>
                                    <th style="border: none"></th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(!empty($company->users))
                                    @foreach($company->users as $user)
                                        <tr>
                                            <td><img class="img-profile rounded-circle"
                                                     src="{{asset('img/undraw_profile.svg')}}"
                                                     width="50"></td>
                                            <td>
                                                {{$user->name}}
                                            </td>
                                            <td>{{$user->role}}</td>
                                            <td>{{$user->department ? $user->department->name : 'Not Assigned'}}</td>
{{--                                            <td>{{$user->created_at->format('d M Y')}}</td>--}}
                                            <td>{{$user->last_login_at}}</td>
                                            <td>
                                                <a href="{{route('admin.users.edit', ['id' => $user->id])}}">
                                                    <i class="fa fa-pen text-primary"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                        @endforeach
                    @endif

                    {{--                        @if(!empty($companies))--}}
                    {{--                           @foreach($companies as $company)--}}
                    {{--                                <h5>{{$company->name}}</h5>--}}
                    {{--                                @if(!empty($company->users))--}}
                    {{--                                    <ul class="list-group list-group-flush">--}}
                    {{--                                        @foreach($company->users as $user)--}}
                    {{--                                            <li class="list-group-item">{{$user->name}} - <span class="text-muted">{{ucfirst(str_replace('_', ' ', $user->role))}}</span> | {{$user->email}} </li>--}}
                    {{--                                        @endforeach--}}
                    {{--                                    </ul>--}}
                    {{--                                @endif--}}
                    {{--                           @endforeach--}}
                    {{--                        @endif--}}
                </div>
            </div>
        </div>

        <div class="col-md-5">
            {{--User Form --}}
            <div class="card">
                <div class="card-body">

                    <h3>New User</h3>

                    @if (session('success'))
                        <div class="alert alert-success">
                            {{session('success')}}
                        </div>
                    @endif
                    @if (session('errors'))

                        <div class="alert alert-danger">
                            Unsuccessful request, Please check the form for errors, and make sure you fill in all
                            required fields!
                        </div>
                    @endif
                    <hr>
                    {{--                    <button class="btn btn-default btn-primary btn-sm" id="show-new-user-form">Add New User</button>--}}

                    @component('admin.layouts.components.forms.add_user',
                        ['company' => $company, 'method' => $method, 'route' => $url, 'user' => $user])
                    @endcomponent

                </div>
            </div>
        </div>
    </div> {{-- / End row--}}
@endsection

@push('admin.layouts.scripts.scripts')
    <script src="{{ asset('js/admin/users.js') }}"></script>
@endpush
