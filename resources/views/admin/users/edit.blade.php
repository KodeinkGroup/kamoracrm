@extends('admin.layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-7">
            <div class="card">
                <div class="card-header">

                </div>
                <div class="card-body">

                    <form action="{{route('admin.users.update', ['id' => $user->id])}}" method="POST" id="edit-user-form">
                        @csrf
                        @method('PUT')

                        <div class="form-group">
                            <label for="name">User Name <span class="text-muted">*</span></label>
                            <input type="text" name="name" class="form-control {{$errors->has('name') ? 'is-invalid' : ''}}" value="{{$user->name}}">
                            @if ($errors->has('name'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('name') }}
                                </div>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="email">Email <span class="text-muted">*</span></label>
                            <input type="email" name="email" class="form-control {{$errors->has('email') ? 'is-invalid' : ''}}" placeholder="name@mail.com" value="{{$user->email}}">
                            @if ($errors->has('email'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('email') }}
                                </div>
                            @endif
                        </div>

                        <div class="form-group">
                            <label for="role">Department</label>
                            <select name="department_id" id="" class="form-control">
                                <option value="" disabled {{ is_null($user->department_id) ? 'selected' : '' }}>Select a Department</option>
                                @if(isset($company->departments))
                                    @foreach($company->departments as $department)
                                        <option value="{{$department->id}}" {{ $user->department_id == $department->id ? 'selected' : '' }}>{{$department->name}}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="role">User Role</label>
                            <select name="role" id="" class="form-control">
                                <option value="admin" {{ $user->role == 'admin' ? 'selected' : '' }}>System Admin</option>
                                <option value="sales_admin" {{ $user->role == 'sales_admin' ? 'selected' : '' }}>Sales Admin</option>
                                <option value="system_admin" {{ $user->role == 'system_admin' ? 'selected' : '' }}>System Admin</option>
                                <option value="inventory_manager" {{ $user->role == 'inventory_manager' ? 'selected' : '' }}>Inventory Manager</option>
                                <option value="fleet_manager" {{ $user->role == 'fleet_manager' ? 'selected' : '' }}>Fleet Manager</option>
                                <option value="project_manager" {{ $user->role == 'project_manager' ? 'selected' : '' }}>Project Manager</option>
                                <option value="technician" {{ $user->role == 'technician' ? 'selected' : '' }}>Technician</option>
                                <option value="sales_agent" {{ $user->role == 'sales_agent' ? 'selected' : '' }}>Sales Agent</option>
                                <option value="user" {{ $user->role == 'user' ? 'selected' : '' }}>User</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="password">Password <span class="text-muted">*</span></label>
                            <input type="password" name="password" class="form-control {{$errors->has('password') ? 'is-invalid' : ''}}" placeholder="*********" disabled="disabled">
                            @if ($errors->has('password'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('password') }}
                                </div>
                            @endif
                        </div>

                        <div class="form-group">
                            <label for="confirm_password">Confirm Password <span class="text-muted">*</span></label>
                            <input type="password" name="confirm_password" class="form-control {{$errors->has('confirm_password') ? 'is-invalid' : ''}} " placeholder="********" disabled="disabled">

                            @if ($errors->has('confirm_password'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('confirm_password') }}
                                </div>
                            @endif
                        </div>

                        <button type="submit" class="btn btn-primary float-right">Edit User</button>

                    </form>
                </div>

            </div>
        </div>
    </div>
@endsection

