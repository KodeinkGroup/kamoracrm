@extends('admin.layouts.app')

@section('content')
    <div class="container-fluid">

        @if (session('success'))
            <div class="alert alert-success mt-3">
                {{session('success')}}
            </div>
        @endif
        <!-- Page Heading -->
        <h1 class="h3 mb-2 text-gray-800">Fleet</h1>
        <div class="row">
            <div class="col-md-12 col-lg-12">
                <div class="card mt-3" style="border-radius: 22px; padding: 23px;width: 85%;">
{{--                    <div class="card-header">Company Vehicles</div>--}}
                    <div class="card-body">
                        <table class="table align-middle mb-0 bg-white" style="width: 100%">
                            <thead class="bg-light">
                            <tr>
                                <th style="border: none"></th>
                                <th style="border: none">Name</th>
                                <th style="border: none">Specs</th>
                                <th style="border: none">VIN Number</th>
                                <th style="border: none">Mileage</th>
                                <th style="border: none">Status</th>
                                <th style="border: none">Last Service</th>
                                <th style="border: none">Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(!empty($company->fleets))
                                @foreach($company->fleets as $fleet)
                                    <h5>{{$fleet->name}}</h5>
                                    @foreach($fleet->vehicles as $vehicle)
                                        <tr>
                                            <td>
                                                <img class="img-profile rounded-circle"
                                                     src="{{asset('img/generic-bakkie.png')}}"
                                                     width="75">
                                            </td>
                                            <td><strong>{{$vehicle->make}}</strong> - {{$vehicle->model}}
                                                <br>
                                                <span class="badge badge-pill badge-primary">{{$vehicle->license_plate}}</span><br>
                                               @if(!empty($vehicle->department)) <small class="text-muted mt-2">{{$vehicle->department->name}}</small>@endif

                                            </td>
                                            <td>
                                              Year: <strong>{{$vehicle->year}}</strong> <span class="badge badge-pill badge-{{$vehicle->fuel_type == 'petrol' ? 'success' : 'primary'}}">{{ucfirst($vehicle->fuel_type . ' Engine')}}</span><br><strong>{{ucfirst($vehicle->transmission_type)}} </strong> <span class="text-muted">Transmission</span>  <br>

                                            </td>
                                            <td> <span class="text-primary">{{$vehicle->vin}}</span></td>
                                            <td>{{$vehicle->odometer_reading . 'kms'}}</td>
                                            <td>
                                                @if($vehicle->status === 'active')<span class="text-success">{{ucfirst($vehicle->status)}}</span>@endif
                                                @if($vehicle->status === 'available')<span class="text-primary">{{ucfirst($vehicle->status)}}</span>@endif
                                                @if($vehicle->status === 'reserved')<span class="text-danger">{{ucfirst($vehicle->status)}}</span>@endif
                                                @if($vehicle->status === 'pending_assignment')<span class="text-warning">{{ucfirst($vehicle->status)}}</span>@endif
                                                @if($vehicle->status === 'awaiting_inspection')<span class="text-warning">{{ucfirst($vehicle->status)}}</span>@endif
                                                @if($vehicle->status === 'maintenance')<span class="text-warning">{{ucfirst($vehicle->status)}}</span>@endif
                                                @if($vehicle->status === 'repair')<span class="text-warning">{{ucfirst($vehicle->status)}}</span>@endif
                                                @if($vehicle->status === 'out_of_service')<span class="text-secondary">{{ucfirst($vehicle->status)}}</span>@endif
                                                @if($vehicle->status === 'decommissioned')<span class="text-danger">{{ucfirst($vehicle->status)}}</span>@endif
                                                @if($vehicle->status === 'sold')<span class="text-secondary">{{ucfirst($vehicle->status)}}</span>@endif

                                                <br><small>{{ucwords(ucfirst($vehicle->ownership))}}</small>
                                            </td>
                                            <td>{{$vehicle->last_service_date}}</td>
                                            <td>
                                                <a href="{{route('admin.company.vehicles.view', ['id' => $vehicle->id])}}"><i class="fa fa-eye text-secondary mr-2"></i> </a>
                                                <a href="{{route('admin.company.vehicles.delete', ['id' => $vehicle->id])}}"><i class="fa fa-trash text-danger"></i> </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
