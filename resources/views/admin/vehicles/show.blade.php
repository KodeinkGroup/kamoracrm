@extends('admin.layouts.app')

@section('content')

    <div class="container mt-5">
            <div class="row">
                <div class="col-md-4">
                    <div class="card vehicle-card" style="border-radius: 22px;">
                        <img src="{{asset('img/generic-bakkie.png')}}" class="card-img-top vehicle-image" alt="Vehicle Image">
                        <div class="card-body vehicle-details">
                            <h5 class="card-title">{{$vehicle->make}} {{$vehicle->model}}</h5>
                            <p class="card-text vehicle-specs"><span>License Plate:</span> {{$vehicle->license_plate}}</p>
                            <p class="card-text vehicle-specs"><span>VIN:</span> {{$vehicle->vin}}</p>
                            <p class="card-text vehicle-specs"><span>Odometer Reading:</span> {{$vehicle->odometer_reading}} <strong>kms</strong></p>
                            <p class="card-text vehicle-specs"><span>Year:</span> {{$vehicle->year}}</p>
                            <p class="card-text vehicle-specs"><span>Last Service Date:</span> 30 Aug 2023</p>
                            <p class="card-text vehicle-specs"><span>Fuel Type:</span> {{ucfirst($vehicle->fuel_type)}}</p>
                            <p class="card-text vehicle-specs"><span>Transmission:</span> {{ucfirst($vehicle->transmission_type)}}</p>
                            <p class="card-text vehicle-specs"><span class="mr-2">Status:</span>
                                @if($vehicle->status === 'active')<span class="text-success">{{ucfirst($vehicle->status)}}</span>@endif
                                @if($vehicle->status === 'available')<span class="text-primary">{{ucfirst($vehicle->status)}}</span>@endif
                                @if($vehicle->status === 'reserved')<span class="text-danger">{{ucfirst($vehicle->status)}}</span>@endif
                                @if($vehicle->status === 'pending_assignment')<span class="text-warning">{{ucfirst($vehicle->status)}}</span>@endif
                                @if($vehicle->status === 'awaiting_inspection')<span class="text-warning">{{ucfirst($vehicle->status)}}</span>@endif
                                @if($vehicle->status === 'maintenance')<span class="text-warning">{{ucfirst($vehicle->status)}}</span>@endif
                                @if($vehicle->status === 'repair')<span class="text-warning">{{ucfirst($vehicle->status)}}</span>@endif
                                @if($vehicle->status === 'out_of_service')<span class="text-secondary">{{ucfirst($vehicle->status)}}</span>@endif
                                @if($vehicle->status === 'decommissioned')<span class="text-danger">{{ucfirst($vehicle->status)}}</span>@endif
                                @if($vehicle->status === 'sold')<span class="text-secondary">{{ucfirst($vehicle->status)}}</span>@endif
                            </p>
{{--                            <a href="#" class="btn btn-primary mt-3">Edit Vehicle Details</a>--}}
                        </div>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="card vehicle-card" style="border-radius: 22px;">
                        <div class="card-header">
                            <h5 class="card-title">Trip Log</h5>
                        </div>
                        <div class="card-body">
                            <table class="table align-middle mb-0 bg-white" style="width: 100%">
                                <thead class="bg-light">
                                <tr>
{{--                                    <th style="border: none"></th>--}}
                                    <th style="border: none">Driver</th>
                                    <th style="border: none">Trip Details</th>
                                    <th style="border: none">Start Odo</th>
                                    <th style="border: none">End Odo</th>
                                    <th style="border: none">Distance</th>
                                    <th style="border: none">Date</th>
                                    <th style="border: none">Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(isset($vehicle->trips))
                                    {{--                                <p>{{$companyTrips->name}}</p>--}}
{{--                                    @foreach($companyTrips->fleets as $fleet)--}}
                                        {{--                                    <p>{{$fleet->id}}</p>--}}
{{--                                        @foreach($fleet->vehicles as $vehicle)--}}
                                            @foreach($vehicle->trips as $trip)
                                                <tr>
{{--                                                    <td>--}}
{{--                                                        <img class="img-profile rounded-circle"--}}
{{--                                                             src="{{asset('img/generic-bakkie.png')}}"--}}
{{--                                                             width="75">--}}
{{--                                                    </td>--}}
                                                    <td>
    {{--                                                    Year: <strong>{{$trip->vehicle->year}}</strong> <span class="badge badge-pill badge-{{$trip->vehicle->fuel_type == 'petrol' ? 'success' : 'primary'}}">--}}
    {{--                                                        {{ucfirst($trip->vehicle->fuel_type . ' Engine')}}</span><br><strong>{{ucfirst($trip->vehicle->transmission_type)}} </strong> <span class="text-muted">Transmission</span>  <br>--}}
                                                                {{$trip->driver->full_names}} {{$trip->driver->surname}}
                                                    </td>
                                                    <td> <span class="text-secondary">{{$trip->start_location}}</span> to <span class="text-primary">{{$trip->end_location}}</span>
                                                        <br>
                                                        <small>{{$trip->description}}</small>
                                                        <br><small class="text-muted">{{$trip->start_time->format('d M Y')}} to {{$trip->end_time->format('d M Y')}}</small>
                                                    </td>
                                                    <td>{{$trip->start_odometer_reading . ' kms'}}</td>
                                                    <td><span class="text-danger">{{$trip->end_odometer_reading . ' kms'}} </span></td>
                                                    <td>
                                                        {{$trip->distance}} <small><strong>kms</strong></small>
                                                    </td>
                                                    <td>{{$trip->created_at->format('d-m-Y')}}</td>
                                                    <td>
                                                        <a href="{{route('admin.company.vehicles.view', ['id' => $vehicle->id])}}"><i class="fa fa-eye text-secondary mr-2"></i> </a>
                                                        <a href="{{route('admin.company.vehicles.delete', ['id' => $vehicle->id])}}"><i class="fa fa-trash text-danger"></i> </a>
                                                    </td>
                                                </tr>
                                            @endforeach
{{--                                        @endforeach--}}

{{--                                    @endforeach--}}
                                @endif
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
@endsection
