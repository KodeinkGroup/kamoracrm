
@extends('admin.layouts.app')

@section('content')
    <div class="container-fluid">
        <!-- Page Heading -->

        <div class="row">
            <div class="col-md-9 col-lg-9">
                @if($errors->count())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $message)
                                <li>{{$message}}</li>
                            @endforeach
                        </ul>

                    </div>
                @endif
                <div class="card mt-5" style="border-radius: 22px; padding: 20px">
                    <div class="card-head">
                        <h5 class="h3 mb-2 ml-3">Create a Trip</h5>
                    </div>
                    <div class="card-body">
                        <form action="{{route('admin.company.vehicles.trips.store')}}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="form-row">
                                <div class="form-group col-md-4">
                                    <label for="lead-source">Driver</label>
                                    <select name="driver_id" id="driver_id" class="form-control" required>
                                        <option value="" selected disabled>Select Driver</option>
                                        @if(isset($company))
                                            @if(!empty($company->employees))
                                                @foreach($company->employees as $driver)
                                                    <option value="{{$driver->id}}">{{$driver->full_names}} {{$driver->surname}}</option>
                                                @endforeach
                                            @endif
                                        @endif
                                    </select>
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="lead-source">Vehicle</label>
                                    <select name="vehicle_id" id="vehicle_id" class="form-control" required>
                                        <option value="" selected disabled>Select a Vehicle</option>
                                        @if(isset($company) && !empty($company->fleets))
                                            @foreach($company->fleets as $fleet)
                                                <optgroup label="{{ $fleet->name }}">
                                                    @foreach($fleet->vehicles as $vehicle)
                                                        <option value="{{ $vehicle->id }}">
                                                            {{ $vehicle->make }} {{ $vehicle->model }} - {{ $vehicle->license_plate }}
                                                        </option>
                                                    @endforeach
                                                </optgroup>
                                            @endforeach
                                        @endif
                                    </select>

                                </div>
                            </div>

                            <div class="form-row">
                                <div class="form-group col-md-4">
                                    <label for="odometer_reading">Start Odometer Reading </label>
                                    <input type="text" name="start_odometer_reading" class="form-control
                                     {{$errors->has('start_odometer_reading') ? 'is-invalid' : ''}}" id="start_odometer_reading" placeholder="250000" required>
                                    @if ($errors->has('odometer_reading'))
                                        <div class="invalid-feedback">
                                            {{ $errors->first('start_odometer_reading') }}
                                        </div>
                                    @endif
                                </div>

                                <div class="form-group col-md-4">
                                    <label for="end_odometer_reading">End Odometer Reading </label>
                                    <input type="text" name="end_odometer_reading" class="form-control
                                    {{$errors->has('end_odometer_reading') ? 'is-invalid' : ''}}" id="end_odometer_reading" placeholder="250000" required>
                                    @if ($errors->has('end_odometer_reading'))
                                        <div class="invalid-feedback">
                                            {{ $errors->first('end_odometer_reading') }}
                                        </div>
                                    @endif
                                </div>

                            </div>

                            <div class="form-row">
                                <div class="form-group col-md-4">
                                    <label for="">Start Location</label>
                                    <input type="text" class="form-control" name="start_location">
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="">End Location</label>
                                    <input type="text" class="form-control" name="end_location">
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="">Distance</label>
                                    <input type="text" class="form-control" name="distance" placeholder="250">
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="form-group col-md-4">
                                    <label for="">Trip Start Date</label>
                                    <input type="date" class="form-control" name="start_time">
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="">Trip End Date</label>
                                    <input type="date" class="form-control" name="end_time">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="">Notes/Summary</label>
                                <textarea rows="5" cols="20" class="form-control-file" name="description"> </textarea>
                            </div>
                            <button type="submit" class="btn btn-primary float-right">Log Trip</button>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
