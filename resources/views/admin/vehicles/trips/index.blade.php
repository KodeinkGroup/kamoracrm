@extends('admin.layouts.app')

@section('content')
    <div class="container-fluid">

        @if (session('success'))
            <div class="alert alert-success mt-3">
                {{session('success')}}
            </div>
        @endif
        <!-- Page Heading -->
        <h1 class="h3 mb-2 text-gray-800">Trip Logs</h1>
        <div class="row">
            <div class="col-md-12 col-lg-12">
                <div class="card mt-3" style="border-radius: 22px; padding: 23px;width: 99%;">
                    {{--                    <div class="card-header">Company Vehicles</div>--}}
                    <div class="card-body">
                        <table class="table align-middle mb-0 bg-white" style="width: 100%">
                            <thead class="bg-light">
                            <tr>
                                <th style="border: none"></th>
                                <th style="border: none">Vehicle Details</th>
                                <th style="border: none">Driver</th>
                                <th style="border: none">Trip Details</th>
                                <th style="border: none">Start Odo</th>
                                <th style="border: none">End Odo</th>
                                <th style="border: none">Distance</th>
                                <th style="border: none">Date Captured</th>
                                <th style="border: none">Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(isset($companyTrips))
{{--                                <p>{{$companyTrips->name}}</p>--}}
                                @foreach($companyTrips->fleets as $fleet)
{{--                                    <p>{{$fleet->id}}</p>--}}
                                    @foreach($fleet->vehicles as $vehicle)
                                        @foreach($vehicle->trips as $trip)
                                            <tr>
                                                <td>
                                                    <img class="img-profile rounded-circle"
                                                         src="{{asset('img/generic-bakkie.png')}}"
                                                         width="75">
                                                </td>
                                                <td><strong>{{$trip->vehicle->make}}</strong> - {{$trip->vehicle->model}}
                                                    <br>
                                                    <span class="badge badge-pill badge-primary">{{$trip->vehicle->license_plate}}</span><br>
                                                    <small class="text-muted">{{$trip->vehicle->mileage}}</small> <br>
                                                    @if(!empty($vehicle->department)) <small class=" badge badge-pill badge-secondary text-white mt-2">{{$vehicle->department->name}}</small>@endif

                                                </td>
                                                <td>
{{--                                                    Year: <strong>{{$trip->vehicle->year}}</strong> <span class="badge badge-pill badge-{{$trip->vehicle->fuel_type == 'petrol' ? 'success' : 'primary'}}">--}}
{{--                                                        {{ucfirst($trip->vehicle->fuel_type . ' Engine')}}</span><br><strong>{{ucfirst($trip->vehicle->transmission_type)}} </strong> <span class="text-muted">Transmission</span>  <br>--}}
                                                        {{$trip->driver->full_names}} {{$trip->driver->surname}}
                                                </td>
                                                <td> <span class="text-secondary">{{$trip->start_location}}</span> to <span class="text-primary">{{$trip->end_location}}</span>
                                                    <br>
                                                    <small>{{$trip->description}}</small>
                                                    <br><small class="text-muted">{{$trip->start_time->format('d M Y')}} to {{$trip->end_time->format('d M Y')}}</small>
                                                </td>
                                                <td>{{$trip->start_odometer_reading . ' kms'}}</td>
                                                <td><span class="text-danger">{{$trip->end_odometer_reading . ' kms'}} </span></td>
                                                <td>
                                                    {{$trip->distance}} <small><strong>kms</strong></small>
                                                </td>
                                                <td>{{$trip->created_at->format('d-m-Y')}}</td>
                                                <td>
                                                    <a href="{{route('admin.company.vehicles.trips.edit', ['trip' => $trip])}}"><i class="fa fa-pen text-secondary mr-2"></i> </a>
{{--                                                    <a href="{{route('admin.company.vehicles.delete', ['id' => $vehicle->id])}}"><i class="fa fa-trash text-danger"></i> </a>--}}
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endforeach

                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
