@extends('admin.layouts.app')

@section('content')
    <div class="container-fluid">
        <!-- Page Heading -->

        <div class="row">
            <div class="col-md-9 col-lg-9">
                @if($errors->count())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $message)
                                <li>{{$message}}</li>
                            @endforeach
                        </ul>

                    </div>
                @endif
                    <div class="card mt-5" style="border-radius: 22px; padding: 20px">
                        <div class="card-head">
                            <h5 class="h3 mb-2 ml-3">Add a New Vehicle</h5>
                        </div>
                        <div class="card-body">
                            <form action="{{route('admin.company.vehicles.store')}}" method="POST" enctype="multipart/form-data">
                                @csrf
                                <div class="form-row">
                                    <div class="form-group col-md-4">
                                        <label for="lead-source">Fleet Type</label>
                                        <select name="fleet_id" id="department_id" class="form-control" required>
                                            <option value="" selected disabled>Select Fleet Type</option>
                                            @if(!empty($company->fleets))
                                                @foreach($company->fleets as $fleet)
                                                    <option value="{{$fleet->id}}">{{$fleet->name}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="lead-source">Ownership</label>
                                        <select name="ownership" id="ownership" class="form-control" required>
                                            <option value="" selected disabled>Select Ownership Type</option>
                                            <option value="company_owned">Available</option>
                                            <option value="rental">Rental</option>
                                            <option value="employee_owned">Employee Owned</option>
                                            <option value="shared">Shared Ownership</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-4">
                                        <label for="">Make</label>
                                        <input type="text" class="form-control {{$errors->has('make') ? 'is-valid' : ''}}" name="make" required>
                                        @if ($errors->has('make'))
                                            <div class="invalid-feedback">
                                                {{ $errors->first('make') }}
                                            </div>
                                        @endif
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="">Model</label>
                                        <input type="text" class="form-control {{$errors->has('model') ? 'is-valid' : ''}}" name="model" required>
                                        @if ($errors->has('model'))
                                            <div class="invalid-feedback">
                                                {{ $errors->first('model') }}
                                            </div>
                                        @endif
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="">License Plate</label>
                                        <input type="text" class="form-control {{$errors->has('license_plate') ? 'is-valid' : ''}}" name="license_plate" required>
                                        @if ($errors->has('license_plate'))
                                            <div class="invalid-feedback">
                                                {{ $errors->first('license_plate') }}
                                            </div>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-row">
                                    <div class="form-group col-md-4">
                                        <label for="">VIN Number</label>
                                        <input type="text" class="form-control" name="vin">
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="odometer_reading">Odometer Reading </label>
                                        <input type="text" name="odometer_reading" class="form-control {{$errors->has('year') ? 'is-invalid' : ''}}" id="odometer_reading" placeholder="" required>
                                        @if ($errors->has('odometer_reading'))
                                            <div class="invalid-feedback">
                                                {{ $errors->first('odometer_reading') }}
                                            </div>
                                        @endif
                                    </div>
                                    <div class="form-group col-md-2">
                                        <label for="year">Year</label>
                                        <input type="text" class="form-control {{$errors->has('year') ? 'is-invalid' : ''}}" name="year" required>
                                        @if ($errors->has('year'))
                                            <div class="invalid-feedback">
                                                {{ $errors->first('year') }}
                                            </div>
                                        @endif
                                    </div>

                                </div>

                                <div class="form-row">
                                    <div class="form-group col-md-4">
                                        <label for="lead-source">Vehicle Department</label>
                                        <select name="department_id" id="department_id" class="form-control" required>
                                            <option value="" selected disabled>Select Department</option>
                                            @if(!empty($company->departments))
                                                @foreach($company->departments as $department)
                                                    <option value="{{$department->id}}">{{$department->name}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>

                                    <div class="form-group col-md-3">
                                        <label for="lead-source">Vehicle Status</label>
                                        <select name="status" id="status" class="form-control" required>
                                            <option value="" selected disabled>Select Status</option>
                                            <option value="available">Available</option>
                                            <option value="active">Active</option>
                                            <option value="maintenance">Maintenance</option>
                                            <option value="repair">In Repair</option>
                                            <option value="reserved">Reserved</option>
                                            <option value="out_of_service">Out of Service</option>
                                            <option value="decommissioned">Decommissioned</option>
                                            <option value="waiting_inspection">Waiting Inspection</option>
                                            <option value="pending_assignment">Pending Assignment</option>
                                            <option value="sold">Sold</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="lead-source">Fuel Type </label>
                                        <select name="fuel_type" id="sources" class="form-control" required>
                                            <option value="" selected disabled>Select Fuel Type</option>
                                                <option value="petrol">Petrol</option>
                                                <option value="diesel">Diesel</option>
                                                <option value="electric">Electric</option>
                                                <option value="hybrid">Hybrid</option>
                                        </select>
                                    </div>

                                    <div class="form-group col-md-3">
                                        <label for="lead-source">Transmission Type </label>
                                        <select name="transmission_type" id="sources" class="form-control" required>
                                            <option value="" selected disabled>Select Transmission Type</option>
                                            <option value="manual">Manual</option>
                                            <option value="automatic">Automatic</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-4">
                                        <label for="">Last Service Date</label>
                                        <input type="date" class="form-control" name="last_service_date">
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="">Next Service Date</label>
                                        <input type="date" class="form-control" name="next_service_date">
                                    </div>
                                </div>
{{--                                <div class="form-group">--}}
{{--                                    <label for="">Picture</label>--}}
{{--                                    <input type="file" class="form-control-file" name="image">--}}
{{--                                </div>--}}
                                <button type="submit" class="btn btn-primary float-right">Add Vehicle</button>
                            </form>
                        </div>
                    </div>

            </div>
        </div>
    </div>
@endsection
