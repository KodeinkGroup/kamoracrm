@extends('admin.layouts.app')

@section('content')
    <div class="container-fluid">
        <!-- Page Heading -->
        <h1 class="h3 mb-2 text-gray-800">Industries</h1>
        <div class="row">
            <div class="col-md-8 col-lg-8">
                <div class="card">
                    <div class="card-header">Listed Industries</div>
                    <div class="card-body">
                        <table class="table align-middle mb-0 bg-white" style="width: 100%">
                            <thead class="bg-light">
                            <tr>
                                <th style="border: none"></th>
                                <th style="border: none">Name</th>
                                <th style="border: none">Config</th>
                                <th style="border: none">Companies</th>
                                <th style="border: none"></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($industries as $industry)
                                <tr>
                                    <td>
                                        <img class="img-profile rounded-circle"
                                             src="{{asset('img/undraw_profile.svg')}}"
                                             width="50">
                                    </td>
                                    <td>{{$industry->name}}</td>
                                    <td></td>
                                    <td>{{$industry->companies_count}}</td>
                                    <td></td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
