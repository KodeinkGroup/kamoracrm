@extends('admin.layouts.app')

@section('content')
    <div class="container-fluid">
        @if (session('success'))
            <div class="alert alert-success mt-3">
                {{session('success')}}
            </div>
        @endif
            <div class="d-sm-flex align-items-center justify-content-between mb-4">
                <a href="{{route('admin.estimates.create', ['type' => 1])}}" class="d-inline-block d-sm-none d-md-none d-lg-none btn btn-sm btn-primary shadow-sm float-right">
                    <i class="fas fa-plus-circle fa-sm text-white"></i>
                </a>

                <h1 class="h3 mb-3">Estimates</h1>
                <a class="d-none d-sm-inline-block d-md-inline-block d-lg-inline-block btn btn-success float-right" href="{{route('admin.estimates.create', ['type' =>'estimate'])}}">
                    <i class="fas fa-plus-circle fa-sm text-white"></i>
                    New Estimate</a>
            </div>
    </div>
@endsection
