@extends('admin.layouts.app')

@section('content')
    <div class="container-fluid" id="invoice-cont" style="width:74%; margin-left: 15.5%; border: 1px solid #ccc; border-radius: 10px;
     padding: 20px; margin-bottom: 10px; height:100px">
        <div class="row">
            <div class="col-md-2">
                <img src="{{asset('assets/icons/add-file.png')}}" class="rounded" width="50"/>
            </div>
            <div class="col-md-7">
                <h3>Download Estimate</h3>
                <p>Download this Quote/Estimate as PDF </p>
            </div>
            <div class="col-md-3">
                <a href="{{route('admin.estimates.export', $estimate->id)}}" target="_blank"
                   class="badge badge-success invoice-download-pdf mt-3 p-2 float-right"> <i class="fas fa-download text-white"></i>
                    Download PDF</a>
            </div>
        </div>
    </div>
@endsection
