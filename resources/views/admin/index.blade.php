@extends('admin.layouts.app')


@section('content')
    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Dashboard</h1>
{{--        <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">--}}
{{--            <i class="fas fa-download fa-sm text-white-50"></i> Generate Report--}}
{{--        </a>--}}
    </div>
            <div class="row">
                <!-- Earnings (Monthly) Card Example -->
                <div class="col-xl-3 col-md-3 mb-4">
                    <div class="card border-left-primary bg-gradient-primary h-100 py-2">
                        <div class="card-body">
                            <div class="row no-gutters align-items-center">
                                <div class="col mr-2">
                                    <div class="text-xs font-weight-bold text-white text-uppercase mb-1">
                                        Leads</div>
                                    <div class="h5 mb-0 font-weight-bold text-white">{{$data['leads'] ?? 0}}</div>
                                </div>
                                <div class="col-auto">
                                    <i class="fas fa-users fa-2x text-gray-300"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Earnings (Monthly) Card Example -->
                <div class="col-xl-3 col-md-3 mb-4">
                    <div class="card border-left-success bg-gradient-success shadow h-100 py-2">
                        <div class="card-body">
                            <div class="row no-gutters align-items-center">
                                <div class="col mr-2">
                                    <div class="text-xs font-weight-bold text-white text-uppercase mb-1">
                                        Prospects</div>
                                    <div class="h5 mb-0 font-weight-bold text-white">{{$data['prospects'] ?? 0}}</div>
                                </div>
                                <div class="col-auto">
                                    <i class="fas fa-briefcase fa-2x text-gray-300"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Earnings (Monthly) Card Example -->
                <div class="col-xl-3 col-md-3 mb-4">
                    <div class="card border-left-info bg-gradient-info shadow h-100 py-2">
                        <div class="card-body">
                            <div class="row no-gutters align-items-center">
                                <div class="col mr-2">
                                    <div class="text-xs font-weight-bold text-white text-uppercase mb-1">Accounts
                                    </div>
                                    <div class="row no-gutters align-items-center">
                                        <div class="col-auto">
                                            <div class="h5 mb-0 mr-3 font-weight-bold text-white">{{$data['accounts'] ?? 0}}</div>

                                        </div>
                                        <!--                                <div class="col">-->
                                        <!--                                    <div class="progress progress-sm mr-2">-->
                                        <!--                                        <div class="progress-bar bg-info" role="progressbar"-->
                                        <!--                                             style="width: 50%" aria-valuenow="50" aria-valuemin="0"-->
                                        <!--                                             aria-valuemax="100">-->
                                        <!--                                            -->
                                        <!--                                        </div>-->
                                        <!--                                    </div>-->
                                        <!--                                </div>-->
                                    </div>
                                </div>
                                <div class="col-auto">
                                    <i class="fas fa-clipboard-list fa-2x text-gray-300"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Pending Requests Card Example -->
                <div class="col-xl-3 col-md-3 mb-4">
                    <div class="card border-left-warning bg-gradient-warning shadow h-100 py-2">
                        <div class="card-body">
                            <div class="row no-gutters align-items-center">
                                <div class="col mr-2">
                                    <div class="text-xs font-weight-bold text-white text-uppercase mb-1">
                                        Deals</div>
                                    <div class="h5 mb-0 font-weight-bold text-white">{{$data['deals'] ?? 0}}</div>
                                </div>
                                <div class="col-auto">
                                    <i class="fas fa-comments fa-2x text-gray-300"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <!--End Content Row Dashboard Cards -->
    <!-- Content Row -->
    <div class="row">
        <div class="col-md-8">
            <div class="card shadow">
                <div class="card-head py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="ml-3 font-weight-bold text-primary">Earnings Overview</h6>
                    <div class="dropdown no-arrow">
                        <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
                            <div class="dropdown-header">Options:</div>
{{--                                <a class="dropdown-item" href="#">Action</a>--}}
                        </div>
                    </div>
                </div>
                <div class="card-body">
                   <div class="chart-area">
                       <canvas id="yearlySales" width="1554" height="640" style="display: block; height: 320px; width: 777px;" class="chartjs-render-monitor"></canvas>
                   </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card shadow">
                <div class="card-head ml-3 py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">Leads by Source</h6>
                    <div class="dropdown no-arrow">
                        <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
                            <div class="dropdown-header">Options:</div>
                            {{--                                <a class="dropdown-item" href="#">Action</a>--}}
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="chart-area">
                        <canvas id="leadSources" width="1554" height="640" style="display: block; height: 320px; width: 777px;" class="chartjs-render-monitor"></canvas>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-8 mt-5">
            <div class="card shadow">
                <div class="card-head py-3 d-flex flex-row align-items-center justify-content-between ml-3">
                    <h6 class="m-0 font-weight-bold text-primary">Possible Income</h6>
                    <div class="dropdown no-arrow">
                        <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
                            <div class="dropdown-header">Dropdown Header:</div>
                            <a class="dropdown-item" href="#">Action</a>
                            <a class="dropdown-item" href="#">Another action</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="#">Something else here</a>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="chart-area">
{{--                        <canvas id="myAreaChart" width="1554" height="640" style="display: block; height: 320px; width: 777px;" class="chartjs-render-monitor"></canvas>--}}
                        <canvas id="yearlyEstimates" width="1554" height="640" style="display: block; height: 320px; width: 777px;" class="chartjs-render-monitor"></canvas>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-4 col-md-3 mb-4 mt-4">
                    <div class="card border-left-success bg-gradient-success shadow h-100 py-2">
                        <div class="card-body">
                            <div class="row no-gutters align-items-center">
                                <div class="col mr-2">
                                    <div class="text-xs font-weight-bold text-white text-uppercase mb-1">
                                        Completed Tasks</div>
                                    <div class="h5 mb-0 font-weight-bold text-white">{{$data['tasks']['done'] ?? 0}}</div>
                                </div>
                                <div class="col-auto">
                                    <i class="fas fa-check fa-2x text-gray-300"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-md-3 mb-4 mt-4">
                    <div class="card border-left-primary bg-gradient-primary shadow h-100 py-2">
                        <div class="card-body">
                            <div class="row no-gutters align-items-center">
                                <div class="col mr-2">
                                    <div class="text-xs font-weight-bold text-white text-uppercase mb-1">
                                        In Progress Tasks</div>
                                    <div class="h5 mb-0 font-weight-bold text-white">{{$data['tasks']['inProgress'] ?? 0}}</div>
                                </div>
                                <div class="col-auto">
                                    <i class="fas fa-user-clock fa-2x text-gray-300"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-md-3 mb-4 mt-4">
                    <div class="card border-left-danger bg-gradient-danger shadow h-100 py-2">
                        <div class="card-body">
                            <div class="row no-gutters align-items-center">
                                <div class="col mr-2">
                                    <div class="text-xs font-weight-bold text-white text-uppercase mb-1">
                                        Overdue Tasks</div>
                                    <div class="h5 mb-0 font-weight-bold text-white">{{$data['tasks']['overdue'] ?? 0}}</div>
                                </div>
                                <div class="col-auto">
                                    <i class="fas fa-calendar-minus fa-2x text-gray-300"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4 mt-5">
            <!-- Project Card Example -->
            <div class="card shadow mb-4">
                <div class="card-head py-3">
                    <h6 class="ml-3 font-weight-bold text-primary">Projects</h6>
                </div>
                <div class="card-body">
                    {{--                    @if(!empty($data->projects))--}}
                    @foreach($data['projects'] as $project)
                        @php
                            $project->completedTasksCount = $project->tasks->where('status_id', 4)->count();
                            $project->totalTasksCount = $project->tasks()->count();
                            $project->percentage = ($project->totalTasksCount > 0) ? round(($project->completedTasksCount / $project->totalTasksCount) * 100, 1) : 0;
                        @endphp
                        <h4 class="small font-weight-bold">{{$project->name}} <span
                                class="float-right">{{$project->percentage}}%</span></h4>
                        <div class="progress mb-4">
                            @if($project->percentage > 24 && $project->percentage < 50)
                                <div class="progress-bar bg-primary" role="progressbar" style="width: {{$project->percentage}}%"
                                     aria-valuenow="{{$project->percentage}}" aria-valuemin="0" aria-valuemax="100"></div>
                            @elseif($project->percentage >= 25 && $project->percentage < 75)
                                <div class="progress-bar bg-primary" role="progressbar" style="width: {{$project->percentage}}%"
                                     aria-valuenow="{{$project->percentage}}" aria-valuemin="0" aria-valuemax="100"></div>
                            @elseif($project->percentage >= 75)
                                <div class="progress-bar bg-success" role="progressbar" style="width: {{$project->percentage}}%"
                                     aria-valuenow="{{$project->percentage}}" aria-valuemin="0" aria-valuemax="100"></div>
                            @else
                                <div class="progress-bar bg-danger" role="progressbar" style="width: {{$project->percentage}}%"
                                     aria-valuenow="{{$project->percentage}}" aria-valuemin="0" aria-valuemax="100"></div>
                            @endif
                        </div>
                    @endforeach
                    {{--                    @endif--}}
                    {{--                    <h4 class="small font-weight-bold">Sales Tracking <span--}}
                    {{--                            class="float-right">40%</span></h4>--}}
                    {{--                    <div class="progress mb-4">--}}
                    {{--                        <div class="progress-bar bg-warning" role="progressbar" style="width: 40%"--}}
                    {{--                            aria-valuenow="40" aria-valuemin="0" aria-valuemax="100"></div>--}}
                    {{--                    </div>--}}
                    {{--                    <h4 class="small font-weight-bold">Customer Database <span--}}
                    {{--                            class="float-right">60%</span></h4>--}}
                    {{--                    <div class="progress mb-4">--}}
                    {{--                        <div class="progress-bar" role="progressbar" style="width: 60%"--}}
                    {{--                            aria-valuenow="60" aria-valuemin="0" aria-valuemax="100"></div>--}}
                    {{--                    </div>--}}
                    {{--                    <h4 class="small font-weight-bold">Payout Details <span--}}
                    {{--                            class="float-right">80%</span></h4>--}}
                    {{--                    <div class="progress mb-4">--}}
                    {{--                        <div class="progress-bar bg-info" role="progressbar" style="width: 80%"--}}
                    {{--                            aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"></div>--}}
                    {{--                    </div>--}}
                    {{--                    <h4 class="small font-weight-bold">Account Setup <span--}}
                    {{--                            class="float-right">Complete!</span></h4>--}}
                    {{--                    <div class="progress">--}}
                    {{--                        <div class="progress-bar bg-success" role="progressbar" style="width: 100%"--}}
                    {{--                            aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>--}}
                    {{--                    </div>--}}
                </div>
            </div>

            {{--            <!-- Color System -->--}}
            {{--            <div class="row">--}}
            {{--                <div class="col-lg-6 mb-4">--}}
            {{--                    <div class="card bg-primary text-white shadow">--}}
            {{--                        <div class="card-body">--}}
            {{--                            Primary--}}
            {{--                            <div class="text-white-50 small">#4e73df</div>--}}
            {{--                        </div>--}}
            {{--                    </div>--}}
            {{--                </div>--}}
            {{--                <div class="col-lg-6 mb-4">--}}
            {{--                    <div class="card bg-success text-white shadow">--}}
            {{--                        <div class="card-body">--}}
            {{--                            Success--}}
            {{--                            <div class="text-white-50 small">#1cc88a</div>--}}
            {{--                        </div>--}}
            {{--                    </div>--}}
            {{--                </div>--}}
            {{--                <div class="col-lg-6 mb-4">--}}
            {{--                    <div class="card bg-info text-white shadow">--}}
            {{--                        <div class="card-body">--}}
            {{--                            Info--}}
            {{--                            <div class="text-white-50 small">#36b9cc</div>--}}
            {{--                        </div>--}}
            {{--                    </div>--}}
            {{--                </div>--}}
            {{--                <div class="col-lg-6 mb-4">--}}
            {{--                    <div class="card bg-warning text-white shadow">--}}
            {{--                        <div class="card-body">--}}
            {{--                            Warning--}}
            {{--                            <div class="text-white-50 small">#f6c23e</div>--}}
            {{--                        </div>--}}
            {{--                    </div>--}}
            {{--                </div>--}}
            {{--                <div class="col-lg-6 mb-4">--}}
            {{--                    <div class="card bg-danger text-white shadow">--}}
            {{--                        <div class="card-body">--}}
            {{--                            Danger--}}
            {{--                            <div class="text-white-50 small">#e74a3b</div>--}}
            {{--                        </div>--}}
            {{--                    </div>--}}
            {{--                </div>--}}
            {{--                <div class="col-lg-6 mb-4">--}}
            {{--                    <div class="card bg-secondary text-white shadow">--}}
            {{--                        <div class="card-body">--}}
            {{--                            Secondary--}}
            {{--                            <div class="text-white-50 small">#858796</div>--}}
            {{--                        </div>--}}
            {{--                    </div>--}}
            {{--                </div>--}}
            {{--                <div class="col-lg-6 mb-4">--}}
            {{--                    <div class="card bg-light text-black shadow">--}}
            {{--                        <div class="card-body">--}}
            {{--                            Light--}}
            {{--                            <div class="text-black-50 small">#f8f9fc</div>--}}
            {{--                        </div>--}}
            {{--                    </div>--}}
            {{--                </div>--}}
            {{--                <div class="col-lg-6 mb-4">--}}
            {{--                    <div class="card bg-dark text-white shadow">--}}
            {{--                        <div class="card-body">--}}
            {{--                            Dark--}}
            {{--                            <div class="text-white-50 small">#5a5c69</div>--}}
            {{--                        </div>--}}
            {{--                    </div>--}}
            {{--                </div>--}}
            {{--            </div>--}}

            {{--        </div>--}}

            {{--        <div class="col-lg-6 mb-4">--}}

            {{--            <!-- Illustrations -->--}}
            {{--            <div class="card shadow mb-4">--}}
            {{--                <div class="card-header py-3">--}}
            {{--                    <h6 class="m-0 font-weight-bold text-primary">Illustrations</h6>--}}
            {{--                </div>--}}
            {{--                <div class="card-body">--}}
            {{--                    <div class="text-center">--}}
            {{--                        <img class="img-fluid px-3 px-sm-4 mt-3 mb-4" style="width: 25rem;"--}}
            {{--                            src="img/undraw_posting_photo.svg" alt="...">--}}
            {{--                    </div>--}}
            {{--                    <p>Add some quality, svg illustrations to your project courtesy of <a--}}
            {{--                            target="_blank" rel="nofollow" href="https://undraw.co/">unDraw</a>, a--}}
            {{--                        constantly updated collection of beautiful svg images that you can use--}}
            {{--                        completely free and without attribution!</p>--}}
            {{--                    <a target="_blank" rel="nofollow" href="https://undraw.co/">Browse Illustrations on--}}
            {{--                        unDraw &rarr;</a>--}}
            {{--                </div>--}}
            {{--            </div>--}}

            {{--            <!-- Approach -->--}}
            {{--            <div class="card shadow mb-4">--}}
            {{--                <div class="card-header py-3">--}}
            {{--                    <h6 class="m-0 font-weight-bold text-primary">Development Approach</h6>--}}
            {{--                </div>--}}
            {{--                <div class="card-body">--}}
            {{--                    <p>SB Admin 2 makes extensive use of Bootstrap 4 utility classes in order to reduce--}}
            {{--                        CSS bloat and poor page performance. Custom CSS classes are used to create--}}
            {{--                        custom pages and custom utility classes.</p>--}}
            {{--                    <p class="mb-0">Before working with this theme, you should become familiar with the--}}
            {{--                        Bootstrap framework, especially the utility classes.</p>--}}
            {{--                </div>--}}
            {{--            </div>--}}

            {{--        </div>--}}
        </div>
    </div>
{{--        <!-- Content Column -->--}}

@endsection
