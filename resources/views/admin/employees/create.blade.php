@extends('admin.layouts.app')

@section('content')
    <div class="container-fluid">
        <h1 class="h3 mb-2 text-gray-800">New Employee</h1>

        @if($errors->count())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $message)
                        <li>{{$message}}</li>
                    @endforeach
                </ul>

            </div>
        @endif

        <div class="row">
            <div class="col-md-9 col-lg-9">
                <div class="card mt-5" style="border-radius: 22px; padding: 20px">
                    <div class="card-head">
                        <h5 class="h3 mb-2 ml-3">Add Employee <small>Details</small></h5>
                    </div>
                    <div class="card-body">
                        <form action="{{route('admin.employees.store')}}" method="POST"
                              enctype="multipart/form-data">
                            @csrf
                            <label for=""><strong>Personal Details</strong></label>
                            <hr>
                            <div class="form-row">
                                <div class="form-group col-md-1">
                                    <label for="">Title</label>
                                    <select class="form-control" name="title" required>
                                        <option selected disabled></option>
                                        <option value="mr">Mr.</option>
                                        <option value="ms">Ms.</option>
                                        <option value="mrs">Mrs.</option>
                                        <option value="dr">Dr</option>
                                        <option value="prof">Prof.</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-1">
                                    <label for="">Initials</label>
                                    <input type="text" value="" class="form-control" name="initials" required>
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="">Full Names</label>
                                    <input type="text" value="" class="form-control" required
                                           name="full_names">
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="">Surname</label>
                                    <input type="text" value="" class="form-control" name="surname" required>
                                </div>

                                <div class="form-group col-md-2">
                                    <label for="">Identification Type</label>
                                    <select class="form-control" name="id_type" required>
                                        <option  disabled selected> Select Type</option>
                                        <option value="id">ID</option>
                                        <option value="passport">Passport</option>
                                    </select>
                                </div>

                                <div class="form-group col-md-3">
                                    <label for="">ID Number</label>
                                    <input type="text" class="form-control" name="id_no" value=""
                                           required>
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="">Date of Birth</label>
                                    <input type="date" class="form-control" name="date_of_birth"
                                           value=""
                                           required>
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="">Email</label>
                                    <input type="email" class="form-control" name="email" value=""
                                           required>
                                </div>
                            </div>
                            <hr>
                            <label for=""><strong>Contact Details | </strong><small> Next of Kin Details</small></label>
                            <hr>
                            <div class="form-row">
                                <div class="form-group col-md-3">
                                    <label for="">Full Name</label>
                                    <input type="text" class="form-control" name="next_of_kin_name"
                                           value=""
                                           required>
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="">Relationship</label>
                                    <input type="text" class="form-control" name="next_of_kin_relationship"
                                           value=""
                                           required>
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="">Mobile</label>
                                    <input type="text" class="form-control" name="next_of_kin_mobile_number"
                                           value=""
                                           required>
                                </div>
                                {{--                                <div class="form-group col-md-3">--}}
                                {{--                                    <label for="">Email Address</label>--}}
                                {{--                                    <input type="text" class="form-control" name="category" value="{{$employee->id_no}}"--}}
                                {{--                                           placeholder="">--}}
                                {{--                                </div>--}}
                            </div>
                            <hr>
                            <label for=""><strong>Address Details</strong></label>
                            <hr>
                            <div class="form-row">
                                <div class="form-group col-md-3">
                                    <label for="">Street Address</label>
                                    <input type="text" class="form-control" name="street"
                                           value=""
                                           placeholder="">
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="">Complex</label>
                                    <input type="text" class="form-control" name="complex" value=""
                                           placeholder="">
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="">Suburb</label>
                                    <input type="text" class="form-control" name="suburb" value=""
                                           placeholder="">
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="">City</label>
                                    <input type="text" class="form-control" name="city" value=""
                                           placeholder="">
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="">Province</label>
                                    <input type="text" class="form-control" name="province" value=""
                                           placeholder="">
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="">ZIP Code</label>
                                    <input type="text" class="form-control" name="zip_code" value=""
                                           placeholder="">
                                </div>
                            </div>
                            <hr>
                            <label for=""><strong>Employment Details</strong></label>
                            <hr>
                            <div class="form-row">
                                <div class="form-group col-md-3">
                                    <label for="">Employment Type</label>
                                    <select class="form-control" name="employment_type" required>
                                        <option  selected disabled> Select Type</option>
                                        <option value="contract">Contract</option>
                                        <option value="permanent">Permanent</option>
                                        <option value="internship">Internship</option>
                                        <option value="learnership">Learnership</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="">Position / Role</label>
                                    <select name="role" id="" class="form-control">
                                        <option value="" disabled selected>Select a Role</option>
                                        <option value="admin">System Admin</option>
                                        <option value="sales_admin" >Sales Admin</option>
                                        <option value="system_admin">System Admin</option>
                                        <option value="inventory_manager">Inventory Manager</option>
                                        <option value="fleet_manager">Fleet Manager</option>
                                        <option value="project_manager">Project Manager</option>
                                        <option value="hr_manager">Human Resources Manager</option>
                                        <option value="technician">Technician</option>
                                        <option value="sales_agent">Sales Agent</option>
                                        <option value="user">User</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="">Appointment Date</label>
                                    <input type="datetime-local" class="form-control" name="appointment_date" value="" required>
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="">Employee Number</label>
                                    <input type="text" class="form-control" value=""
                                           name="employee_number" required>
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="">Rate</label>
                                    <input type="number" class="form-control" value="" name="salary"
                                           placeholder="">
                                </div>

                                <div class="form-group col-md-4">
                                    <label for="">Available Leave Days</label>
                                    <input type="text" class="form-control" name="available_leave_days"
                                           value="">
                                </div>

                            </div>

                            {{-- <div class="form-group">
                                <label for="">Logo</label>
                                <input type="file" class="form-control-file" name="logo">
                            </div> --}}
                            <button type="submit" class="btn btn-primary float-right">Create Employee</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
