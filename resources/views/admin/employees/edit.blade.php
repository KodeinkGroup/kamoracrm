@extends('admin.layouts.app')

@section('content')
    <div class="container-fluid">
        <h1 class="h3 mb-2 text-gray-800">Update {{$employee->user->name}}</h1>

        @if($errors->count())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $message)
                        <li>{{$message}}</li>
                    @endforeach
                </ul>

            </div>
        @endif

        <div class="row">
            <div class="col-md-9 col-lg-9">
                <div class="card mt-5" style="border-radius: 22px; padding: 20px">
                    <div class="card-head">
                        <h5 class="h3 mb-2 ml-3">{{$employee->user->name}} <small>Details</small></h5>
                    </div>
                    <div class="card-body">
                        <form action="{{route('admin.employees.update', ['id' => $employee->id])}}" method="POST"
                              enctype="multipart/form-data">
                            @csrf
                            @method('PUT')
                            <label for=""><strong>Personal Details</strong></label>
                            <hr>
                            <div class="form-row">
                                <div class="form-group col-md-1">
                                    <label for="">Title</label>
                                    <select class="form-control" name="title" required>
                                        <option {{ is_null($employee->title) ? 'selected' : '' }} disabled></option>
                                        <option value="mr" {{ $employee->title == 'mr' ? 'selected' : '' }}>Mr.</option>
                                        <option value="ms"{{ $employee->title == 'ms' ? 'selected' : '' }} >Ms.</option>
                                        <option value="mrs" {{ $employee->title == 'mrs' ? 'selected' : '' }}>Mrs.</option>
                                        <option value="dr" {{ $employee->title == 'dr' ? 'selected' : '' }}>Dr</option>
                                        <option value="prof" {{ $employee->title == 'prof' ? 'selected' : '' }}>Prof.</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-1">
                                    <label for="">Initials</label>
                                    <input type="text" value="{{$employee->initials}}" class="form-control" name="initials" required>
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="">Full Names</label>
                                    <input type="text" value="{{$employee->full_names}}" class="form-control" required
                                           name="full_names">
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="">Surname</label>
                                    <input type="text" value="{{$employee->surname}}" class="form-control" name="surname" required>
                                </div>

                                <div class="form-group col-md-2">
                                    <label for="">Identification Type</label>
                                    <select class="form-control" name="id_type" required>
                                        <option  disabled {{ $employee->id_type == '' ? 'selected' : '' }}> Select Type</option>
                                        <option value="id" {{ $employee->id_type == 'id' ? 'selected' : '' }}>ID</option>
                                        <option value="passport" {{ $employee->id_type == 'passport' ? 'selected' : '' }}>Passport</option>
                                    </select>
                                </div>

                                <div class="form-group col-md-3">
                                    <label for="">ID Number</label>
                                    <input type="text" class="form-control" name="id_no" value="{{$employee->id_no}}"
                                           required>
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="">Date of Birth</label>
                                    <input type="date" class="form-control" name="date_of_birth"
                                           value="{{$employee->date_of_birth}}"
                                           required>
                                </div>
                            </div>
                            <hr>
                            <label for=""><strong>Contact Details | </strong><small> Next of Kin Details</small></label>
                            <hr>
                            <div class="form-row">
                                <div class="form-group col-md-3">
                                    <label for="">Full Name</label>
                                    <input type="text" class="form-control" name="next_of_kin_name"
                                           value="{{$employee->next_of_kin_name}}"
                                           required>
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="">Relationship</label>
                                    <input type="text" class="form-control" name="next_of_kin_relationship"
                                           value="{{$employee->next_of_kin_relationship}}"
                                           required>
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="">Mobile</label>
                                    <input type="text" class="form-control" name="next_of_kin_mobile_number"
                                           value="{{$employee->next_of_kin_mobile_number}}"
                                           required>
                                </div>
{{--                                <div class="form-group col-md-3">--}}
{{--                                    <label for="">Email Address</label>--}}
{{--                                    <input type="text" class="form-control" name="category" value="{{$employee->id_no}}"--}}
{{--                                           placeholder="">--}}
{{--                                </div>--}}
                            </div>
                            <hr>
                            <label for=""><strong>Address Details</strong></label>
                            <hr>
                            <div class="form-row">
                                <div class="form-group col-md-3">
                                    <label for="">Street Address</label>
                                    <input type="text" class="form-control" name="street"
                                           value="{{$employee->street}}"
                                           placeholder="">
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="">Complex</label>
                                    <input type="text" class="form-control" name="complex" value="{{$employee->complex}}"
                                           placeholder="">
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="">Suburb</label>
                                    <input type="text" class="form-control" name="suburb" value="{{$employee->suburb}}"
                                           placeholder="">
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="">City</label>
                                    <input type="text" class="form-control" name="city" value="{{$employee->city}}"
                                           placeholder="">
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="">Province</label>
                                    <input type="text" class="form-control" name="province" value="{{$employee->province}}"
                                           placeholder="">
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="">ZIP Code</label>
                                    <input type="text" class="form-control" name="zip_code" value="{{$employee->zip_code}}"
                                           placeholder="">
                                </div>
                            </div>
                            <hr>
                            <label for=""><strong>Employment Details</strong></label>
                            <hr>
                            <div class="form-row">
                                <div class="form-group col-md-3">
                                    <label for="">Employment Type</label>
                                    <select class="form-control" name="employment_type" required>
                                        <option  {{ $employee->employment_type == '' ? 'selected' : '' }} disabled> Select Type</option>
                                        <option value="contract" {{ $employee->employment_type == 'contract' ? 'selected' : '' }}>Contract</option>
                                        <option value="permanent" {{ $employee->employment_type == 'permanent' ? 'selected' : '' }}>Permanent</option>
                                        <option value="internship" {{ $employee->employment_type == 'internship' ? 'selected' : '' }}>Internship</option>
                                        <option value="learnership" {{ $employee->employment_type == 'learnership' ? 'selected' : '' }}>Learnership</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="">Appointment Date</label>
                                    <input type="datetime-local" class="form-control" name="appointment_date" value="{{$employee->appointment_date}}" required>
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="">Employee Number</label>
                                    <input type="text" class="form-control" value="{{$employee->employee_number}}"
                                           name="employee_number" required>
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="">Rate</label>
                                    <input type="number" class="form-control" value="{{$employee->salary}}" name="salary"
                                           placeholder="">
                                </div>

                                <div class="form-group col-md-4">
                                    <label for="">Available Leave Days</label>
                                    <input type="text" class="form-control" name="available_leave_days"
                                           value="{{$employee->available_leave_days}}">
                                </div>

                            </div>

                            {{-- <div class="form-group">
                                <label for="">Logo</label>
                                <input type="file" class="form-control-file" name="logo">
                            </div> --}}
                            <button type="submit" class="btn btn-primary float-right">Update Employee</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
