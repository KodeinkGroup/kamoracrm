@extends('admin.layouts.app')

@section('content')
    <div class="container-fluid">
        @if (session('success'))
            <div class="alert alert-success mt-3">
                {{session('success')}}
            </div>
        @endif
            <div class="d-sm-flex align-items-center justify-content-between mb-4">
                <a href="{{route('admin.employees.create')}}" class="d-inline-block d-sm-none d-md-none d-lg-none btn btn-sm btn-primary shadow-sm float-right">
                    <i class="fas fa-plus-circle fa-sm text-white"></i>
                </a>

                <h1 class="h3 mb-0">Manage Employees</h1>
                <a href="{{route('admin.employees.create')}}" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">
                    <i class="fas fa-plus-circle fa-sm text-white-50"></i> Add Employee
                </a>
            </div>
        <div class="row mt-5">
            <div class="col-md-9 col-lg-9">
                <div class="card" style="border-radius:22px; padding: 20px">
                    <div class="card-head"></div>
                    <div class="card-body">
                        <table class="table align-middle mb-0 bg-white" style="width: 100%">
                            <thead class="bg-light">
                            <tr>
                                <th style="border: none"></th>
                                <th style="border: none">Name</th>
                                <th style="border: none">Department</th>
{{--                                <th style="border: none">Contacts</th>--}}
                                <th style="border: none">Appointment Date</th>
                                <th style="border: none">Leave Days</th>
                                <th style="border: none">Actions</th>

                            </tr>
                            </thead>
                            <tbody>
                            @if(!empty($employees))
                                @foreach($employees as $employee)
                                    <tr>
                                        <td></td>
                                        <td>{{ucfirst($employee->title)}} {{$employee->user->name}}
                                            <br>
                                            <small><i class="fa fa-briefcase mr-2 text-primary"></i>
                                                <span
                                                    class="text-muted">{{ucwords(str_replace('_', ' ',$employee->position ?? $employee->user->role))}}</span>
                                            </small>
                                            <br><span class="text-muted"><i class="fas fa-birthday-cake fa-sm text-pink mr-2"></i>{{$employee->date_of_birth ?? ''}}</span>

                                        </td>
                                        <td>
                                            <i class="fa fa-building mr-2 text-dark"></i>{{$employee->user->department->name ?? ''}}
                                        </td>
{{--                                        <td>--}}
{{--                                            <i class="fa fa-envelope mr-2 text-primary"></i>{{$employee->user->email}}--}}
{{--                                            <br>--}}
{{--                                            <small> <i class="fa fa-phone mr-2 text-dark"></i><span--}}
{{--                                                    class="text-muted">{{$employee->mobile_number ?? ''}}</span>--}}
{{--                                            </small>--}}
{{--                                            <br>--}}
{{--                                            <small> <i class="fa fa-home mr-2 text-muted"></i>--}}
{{--                                                <span class="text-muted">{{$employee->street ?? ''}}</span>--}}
{{--                                            </small>--}}

{{--                                        </td>--}}
                                        <td><i class="fa fa-calendar text-primary mr-2"></i>{{$employee->appointment_date ? $employee->appointment_date->format('d M Y') : ''}}
                                            <br><span class="text-muted"><i class="fa fa-briefcase mr-2 text-brown"></i>{{ucfirst($employee->employment_type) ?? ''}}</span>
                                       </td>
                                        <td><i class="fa fa-sun text-primary mr-2"></i>{{number_format(round($employee->available_leave_days)) }} days</td>
                                        <td>
                                            <a href="{{route('admin.employees.edit', ['id' => $employee->id])}}" class="text-dark" style="text-decoration: none">
                                                <i class="fas fa-pen fa-sm text-dark"></i> Edit
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
