<!-- resources/views/reports/combined_report.blade.php -->
@php
use Carbon\Carbon;
@endphp
<!DOCTYPE html>
<html>
<head>
    <title>Management Report - {{$company->name}}</title>

    <link rel="stylesheet" href="{{ public_path('css/bootstrap.min.css') }}" type="text/css">

    {{--        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">--}}
    <style>
        html {
            font-family: "Manrope", sans-serif;
            line-height: 1.15;
            -webkit-text-size-adjust: 100%;
            -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
        }

        body {
            margin: 0;
            font-family: "Manrope", sans-serif;
            font-size: 0.9rem;
            font-weight: 400;
            line-height: 1.6;
            color: #212529;
            text-align: left;
            background-color: #f8fafc;
            padding: 20px;
        }

        .table {
            width: 100%;
            margin-bottom: 1rem;
            color: #212529;
            background: #fff;
        }
        .table th,
        .table td {
            padding: 0.75rem;
            vertical-align: top;
            border-top: 1px solid #dee2e6;
            text-align: left;
        }

        .table thead th {
            vertical-align: top;
            /*border-bottom: 1px solid #dee2e6;*/
            background-color: #e6e6e6;
            text-align: left;
        }

        .table tbody + tbody {
            border-top: 2px solid #dee2e6;
            height: 75px;
        }

        small {
            font-family: "Manrope", sans-serif;
            font-size: 12px;
            /*font-weight: 100;*/
            line-height: 1.2;
        }
    </style>
</head>
<body>
<div class="main-content">
    <div class="container mt-7">
        <!-- Table -->
        <h2 class="mb-3"> {{$company->name}} - Management Report </h2>
        <p class="mb-5"> <small>Date: {{now()->format('d F Y')}}</small></p>
        <div class="row">

            <div class="col">
                <div class="card shadow">
                    <div class="card-header border-0">
                        <h3 class="mb-0">Projects</h3>
                        <p style="font-size: 12px">All Company Projects for the current year with their statuses.</p>
                    </div>
                    <div class="table-responsive">
                        @if(!empty($company->projects))

                            @foreach($company->projects as $project)
                            <div>
                                <h3>{{$project->name}}</h3>

                                <p> Client Name: <strong class="mb-0 text-sm">{{$project->client ? $project->client->business_name : 'No Linked Client'}}</strong></p>
                                <p> Period: <strong class="mb-0 text-sm">{{$project->start_date ? $project->start_date->format('d M Y') : 'N/A'}} - {{$project->start_date ? $project->end_date->format('d M Y') : 'N/A'}}</strong></p>
                                @php
                                    // Assuming you have $project with start_date and end_date fields
                                    $start_date = Carbon::parse($project->start_date);
                                    $end_date = Carbon::parse($project->end_date);

                                    // Calculate duration in days
//                                    $duration = $end_date->diffInDays($start_date);

                                    $total_days = $end_date->diffInDays($start_date);

                                    // Average days per month (considering leap years and common years)
                                    $days_in_month = 30.4;

                                    // Calculate months and remaining days
                                    $months = floor($total_days / $days_in_month);
                                    $remaining_days = round($total_days % $days_in_month);
                                @endphp
                                <p> Duration: <strong class="mb-0 text-sm">{{$months}} months and {{$remaining_days}} days</strong></p>
{{--                                <small class="mt-2" style="margin-bottom: 10px; padding-bottom: 10px">{{$project->summary}}</small>--}}
                                <table class="table align-items-center table-flush">
                                    <thead class="thead-light">
                                    <th>Material</th>
                                    <th>Description</th>
                                    <th>Qty</th>
                                    <th>Assigned To</th>

                                    </thead>
                                    <tbody>
                                    @foreach($project->material as $request)
                                    <tr>
                                        <td>{{$request->item ? $request->item->name : 'Not Specified'}}</td>
                                        <td>{{$request->item ? $request->item->description : 'Not Specified'}}</td>
                                        <td>{{$request->quantity ?? 0 }}</td>
                                        <td>{{$request->user ? $request->user->name : 'Not Assigned' }}</td>
                                    </tr>
                                    @endforeach

                                    </tbody>
                                </table>
                               <h3>Project Tasks</h3>
                                <table class="table align-items-center table-flush">
                                    <thead class="thead-light">
                                        <tr>
                                            <th scope="col">Title</th>
                                            <th scope="col">Start</th>
                                            <th scope="col">End</th>
                                            <th scope="col">Duration</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($project->tasks as $task)
                                            <tr>
                                                <td>{{$task->title}}</td>
                                                <td>{{ $task->created_at ? $task->created_at->format('d M Y') : ''}}</td>
                                                <td>{{ $task->due_date ? $task->due_date->format('d M Y') : ''}}</td>
                                                <td>{{$task->time_estimate ?? 0}}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                <br><br/>
                                <hr>
                            </div>
                       @endforeach
                       @endif
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<footer class="footer">
    <div class="row align-items-center justify-content-xl-between">
        <div class="col-xl-6 m-auto text-center">
            <div class="copyright" style="margin-top: 20px; text-align: center; position: fixed; bottom: 20px">
                <small> &copy; <a href="#" target="_blank">{{$company->name}}</a> 2024, Powered by Kamora Software</small>
            </div>
        </div>
    </div>
</footer>
</body>
</html>
