<!-- resources/views/reports/combined_report.blade.php -->
<!DOCTYPE html>
<html>
    <head>
        <title>Management Report - {{$company->name}}</title>

        <link rel="stylesheet" href="{{ public_path('css/bootstrap.min.css') }}" type="text/css">

{{--        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">--}}
        <style>
            html {
                font-family: "Manrope", sans-serif;
                line-height: 1.15;
                -webkit-text-size-adjust: 100%;
                -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
            }

            body {
                margin: 0;
                font-family: "Manrope", sans-serif;
                font-size: 0.9rem;
                font-weight: 400;
                line-height: 1.6;
                color: #212529;
                text-align: left;
                background-color: #f8fafc;
                padding: 20px;
            }

            .table {
                width: 100%;
                margin-bottom: 1rem;
                color: #212529;
                background: #fff;
            }
            .table th,
            .table td {
                padding: 0.75rem;
                vertical-align: top;
                border-top: 1px solid #dee2e6;
                text-align: left;
            }

            .table thead th {
                vertical-align: top;
                /*border-bottom: 1px solid #dee2e6;*/
                background-color: #e6e6e6;
                text-align: left;
            }

            .table tbody + tbody {
                border-top: 2px solid #dee2e6;
                height: 75px;
            }

            small {
                font-family: "Manrope", sans-serif;
                font-size: 12px;
                /*font-weight: 100;*/
                line-height: 1.2;
            }
        </style>
    </head>
    <body>
        <div class="main-content">
            <div class="container mt-7">
                <!-- Table -->
                <h2 class="mb-3"> {{$company->name}} - Management Report </h2>
                <p class="mb-5"> <small>Date: {{now()->format('d F Y')}}</small></p>
                <div class="row">

                    <div class="col">
                        <div class="card shadow">
                            <div class="card-header border-0">
                                <h3 class="mb-0">Projects</h3>
                                <p style="font-size: 12px">All Company Projects for the current year with their statuses.</p>
                            </div>
                            <div class="table-responsive">
                                <table class="table align-items-center table-flush">
                                    <thead class="thead-light">
                                    <tr>
                                        <th scope="col">Project</th>
{{--                                        <th scope="col">Details</th>--}}
                                        <th scope="col">Start Date</th>
                                        <th scope="col">End Date</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        @if(!empty($company->projects))
                                            @foreach($company->projects as $project)
                                                <tr>
                                                    <td scope="row">
{{--                                                        <div class="media align-items-center">--}}
{{--                                                            <p class="media-body">--}}
                                                                <span class="mb-0 text-sm">{{$project->name}}</span>
                                                              <br><small class="mt-2">{{$project->summary}}</small>
{{--                                                            </p>--}}
{{--                                                        </div>--}}
                                                    </td>
{{--                                                    <td>--}}
{{--                                                          <span class="badge badge-dot mr-4">--}}
{{--                                                            <i class="bg-warning"></i> {{$project->status}}--}}
{{--                                                          </span>--}}
{{--                                                        <div class="d-flex align-items-center">--}}

{{--                                                        </div>--}}
{{--                                                    </td>--}}
                                                    <td>
                                                        <div class="d-flex align-items-center">
                                                            @if(!empty($project->start_date)) <span class="mr-2">{{$project->start_date->format('d M Y')}}</span>
                                                            @else
                                                                <span class="mr-2">No Date</span>
                                                            @endif

                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="d-flex align-items-center">
                                                            @if(!empty($project->end_date)) <span class="mr-2">{{$project->end_date->format('d M Y')}}</span>
                                                            @else
                                                                <span class="mr-2">No Date</span>
                                                            @endif
                                                        </div>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                    <div class="col mt-5">
                        <div class="card shadow">
                            <div class="card-header border-0">
                                <h3 class="mb-0">Business Leads</h3>
                                <p style="font-size: 12px">All Company Leads that needs to be nurtured and qualified into business prospects </p>
                            </div>
                            <div class="table-responsive">
                                <table class="table align-items-center table-flush">
                                    <thead class="thead-light">
                                    <tr>
                                        <th scope="col">Business Name</th>
                                        <th scope="col">Contact Person</th>
                                        <th scope="col">Source</th>
{{--                                        <th scope="col">End Date</th>--}}
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(!empty($company->prospects))
                                        @foreach($company->prospects as $lead)
                                           @if($lead->prospect_type_id == 1)
                                               <tr>
                                                   <th scope="row">
                                                       <div class="media align-items-center">
                                                           <div class="media-body">
                                                               <span class="mb-0 text-sm">{{$lead->business_name}}</span>
                                                           </div>
                                                       </div>
                                                   </th>
                                                   <td>
                                                       <span class="mr-2">{{$lead->name}}</span>
                                                       <br><span class="mr-2">{{$lead->mobile_no}}</span>

                                                   </td>
                                                   <td>
                                                       <div class="d-flex align-items-center">
                                                           @if(!empty($lead->source)) <span class="mr-2">{{$lead->source->name}}</span>
                                                           @else
                                                               <span class="mr-2">No Date</span>
                                                           @endif

                                                       </div>
                                                   </td>
                                               </tr>
                                           @endif
                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                    <div class="col mt-5">
                        <div class="card shadow">
                            <div class="card-header border-0">
                                <h3 class="mb-0">Business Prospects</h3>
                                <p style="font-size: 12px">All Company Prospects that needs are qualified as business prospects and can be converted into possible customers </p>
                            </div>
                            <div class="table-responsive">
                                <table class="table align-items-center table-flush">
                                    <thead class="thead-light">
                                    <tr>
                                        <th scope="col">Business Name</th>
                                        <th scope="col">Contact Person</th>
                                        <th scope="col">Source</th>
                                        {{--                                        <th scope="col">End Date</th>--}}
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(!empty($company->prospects))
                                        @foreach($company->prospects as $lead)
                                            @if($lead->prospect_type_id == 2)
                                                <tr>
                                                    <th scope="row">
                                                        <div class="media align-items-center">
                                                            <div class="media-body">
                                                                <span class="mb-0 text-sm">{{$lead->business_name}}</span>
                                                            </div>
                                                        </div>
                                                    </th>
                                                    <td>
                                                        <span class="mr-2">{{$lead->name}}</span>
                                                        <br><span class="mr-2">{{$lead->mobile_no}}</span>

                                                    </td>
                                                    <td>
                                                        <div class="d-flex align-items-center">
                                                            @if(!empty($lead->source)) <span class="mr-2">{{$lead->source->name}}</span>
                                                            @else
                                                                <span class="mr-2">No Date</span>
                                                            @endif

                                                        </div>
                                                    </td>
                                                </tr>
                                            @endif
                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                    <div class="col mt-5">
                        <div class="card shadow">
                            <div class="card-header border-0">
                                <h3 class="mb-0">Business Customers/Accounts</h3>
                                <p style="font-size: 12px">All Customers and Accounts that generates income into the business </p>
                            </div>
                            <div class="table-responsive">
                                <table class="table align-items-center table-flush">
                                    <thead class="thead-light">
                                    <tr>
                                        <th scope="col">Business Name</th>
                                        <th scope="col">Contact Person</th>
                                        <th scope="col">Source</th>
                                        {{--                                        <th scope="col">End Date</th>--}}
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(!empty($company->prospects))
                                        @foreach($company->prospects as $lead)
                                            @if($lead->prospect_type_id == 3)
                                                <tr>
                                                    <th scope="row">
                                                        <div class="media align-items-center">
                                                            <div class="media-body">
                                                                <span class="mb-0 text-sm">{{$lead->business_name}}</span>
                                                            </div>
                                                        </div>
                                                    </th>
                                                    <td>
                                                        <span class="mr-2">{{$lead->name}}</span>
                                                        <br><span class="mr-2">{{$lead->mobile_no}}</span>

                                                    </td>
                                                    <td>
                                                        <div class="d-flex align-items-center">
                                                            @if(!empty($lead->source)) <span class="mr-2">{{$lead->source->name}}</span>
                                                            @else
                                                                <span class="mr-2">No Date</span>
                                                            @endif

                                                        </div>
                                                    </td>
                                                </tr>
                                            @endif
                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>



                    <div class="col mt-5">
                        <div class="card shadow">
                            <div class="card-header border-0">
                                <h3 class="mb-0">Business Fleet</h3>
                                <p style="font-size: 12px">All Company Vehicles, Rented and Owned</p>
                            </div>
                            <div class="table-responsive">
                                <table class="table align-items-center table-flush">
                                    <thead class="thead-light">
                                    <tr>
                                        <th scope="col">Name</th>
                                        <th>
                                            <span class="mr-2">Licence Plate</span>
                                        </th>
                                        <th>Ownership</th>
                                        <th>Last Service</th>
                                        <th>Next Service</th>
                                        {{--                                        <th scope="col">End Date</th>--}}
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(!empty($company->fleets))
                                        @foreach($company->fleets as $fleet)
                                            @if(!empty($fleet->vehicles))
                                                @foreach($fleet->vehicles as $vehicle)
                                                    <tr>
                                                        <td scope="row">
                                                            <div class="media align-items-center">
                                                                <div class="media-body">
                                                                    <span class="mb-0 text-sm">{{$vehicle->make}} - {{$vehicle->model}}</span>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <span class="mr-2">{{$vehicle->license_plate}}</span>
                                                        </td>
                                                        <td>
                                                            <span class="mr-2">{{ucwords(ucfirst($vehicle->ownership))}}</span>
                                                        </td>
                                                        <td>
                                                            @if(!empty($vehicle->last_service_date)) <span class="mr-2">{{$vehicle->last_service_date}}</span>
                                                            @else
                                                                <span class="mr-2">No Date</span>
                                                            @endif
                                                        </td>
                                                        <td>
                                                            @if(!empty($vehicle->next_service_date)) <span class="mr-2">{{$vehicle->next_service_date}}</span>
                                                            @else
                                                                <span class="mr-2">No Date</span>
                                                            @endif
                                                        </td>

                                                    </tr>
                                                @endforeach
                                            @endif
                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <footer class="footer">
            <div class="row align-items-center justify-content-xl-between">
                <div class="col-xl-6 m-auto text-center">
                    <div class="copyright" style="margin-top: 20px; text-align: center">
                        <small><a href="#" target="_blank">{{$company->name}}</a> Powered by Kamora Software</small>
                    </div>
                </div>
            </div>
        </footer>
    </body>
</html>
