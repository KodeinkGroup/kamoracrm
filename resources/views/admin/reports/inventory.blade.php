<!-- resources/views/reports/combined_report.blade.php -->
<!DOCTYPE html>
<html>
<head>
    <title>Inventory Report - {{$company->name}}</title>

    <link rel="stylesheet" href="{{ public_path('css/bootstrap.min.css') }}" type="text/css">

    {{--        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">--}}
    <style>
        html {
            font-family: "Manrope", sans-serif;
            line-height: 1.15;
            -webkit-text-size-adjust: 100%;
            -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
        }

        body {
            margin: 0;
            font-family: "Manrope", sans-serif;
            font-size: 0.9rem;
            font-weight: 400;
            line-height: 1.6;
            color: #212529;
            text-align: left;
            background-color: #f8fafc;
            padding: 20px;
        }

        .table {
            width: 100%;
            margin-bottom: 1rem;
            color: #212529;
            background: #fff;
        }
        .table th,
        .table td {
            padding: 0.75rem;
            vertical-align: top;
            border-top: 1px solid #dee2e6;
            text-align: left;
        }

        .table thead th {
            vertical-align: top;
            /*border-bottom: 1px solid #dee2e6;*/
            background-color: #e6e6e6;
            text-align: left;
        }

        .table tbody + tbody {
            border-top: 2px solid #dee2e6;
            height: 75px;
        }

        small {
            font-family: "Manrope", sans-serif;
            font-size: 12px;
            /*font-weight: 100;*/
            line-height: 1.2;
        }

        .data-blocks {
            width: 100%;
            margin-top: 20px;
            /*text-align: center;*/
        }

        .data-block {
            display: inline-block;
            width: auto;
            margin: 5% 1%;
            padding: 10px;
            border: none;
            background-color: transparent;
            box-sizing: border-box;
            border-radius: 10px;
            /*color: #fff;*/
        }

        .data-block h3 {
            margin: 0;
            font-size: 18px;
            color: #333;
        }

        .data-block p {
            font-size: 12px;
            margin: 10px 10px 10px 10px;
            font-weight: bold;
            text-align: center;
            border-bottom: 1px solid #cccccc;
            padding-bottom: 5px;
            /*background-color: #0a58ca;*/
            /*border-radius: 50%;*/
            /*height: 35px;*/
            /*width: 35px;*/
            /*color: #fff*/
        }

        .badge {
            width: auto;
            height: 50px;
            background-color: #F1F1F1;
            border: 1px solid #F1F1F1;
            border-radius: 10px;
            padding: 10px;
        }
    </style>
</head>
<body>
<div class="main-content">
    <div class="container mt-7">
        <!-- Table -->
        <h2 class="mb-3"> {{$company->name}} - Inventory Report </h2>
        <p class=""> <small>Date: {{now()->format('d F Y')}}</small></p>

        <div class="data-blocks">
            <h3>Overview</h3>
            <div class="data-block">
                <p>{{$company->items->count() ?? 0}}</p>
                <small>Total Inventory</small>
            </div>

            <div class="data-block">
                <p>{{$company->new_count}}</p>
                <small>New Material</small>
            </div>

            <div class="data-block">
                <p>{{$company->damaged_count}}</p>
                <small>Damaged Material</small>
            </div>

            <div class="data-block">
                <p>{{$company->returned_count}}</p>
                <small>Returned Material</small>
            </div>

            <div class="data-block">
                <p>0</p>
                <small>Lost Material</small>
            </div>
        </div>

        <div class="row">
            <div class="col">
                <div class="card shadow">
                    <div class="card-header border-0">
                        <h3 class="mb-0">Inventory</h3>
                        <p style="font-size: 12px">All Company Inventory recorded.</p>
                    </div>
                    <div class="table-responsive">
                        <table class="table align-items-center table-flush">
                            <thead class="thead-light">
                            <tr>
                                <th scope="col">Name</th>
                                <th scope="col">Details</th>
                                <th scope="col">Code</th>
                                <th scope="col">Quantity</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(!empty($company->items))
                                @foreach($company->items as $item)
                                    <tr>
                                        <td scope="row">
                                            {{--                                                        <div class="media align-items-center">--}}
                                            {{--                                                            <p class="media-body">--}}
                                            <span class="mb-0 text-sm">{{$item->name}}</span> <br>
                                            <small class="badge mt-2">{{$item->supplier}}</small>
                                            <br><small class="mt-2">{{$item->brand}}</small>
                                            {{--                                                            </p>--}}
                                            {{--                                                        </div>--}}
                                        </td>
                                        {{--                                                    <td>--}}
                                        {{--                                                          <span class="badge badge-dot mr-4">--}}
                                        {{--                                                            <i class="bg-warning"></i> {{$project->status}}--}}
                                        {{--                                                          </span>--}}
                                        {{--                                                        <div class="d-flex align-items-center">--}}

                                        {{--                                                        </div>--}}
                                        {{--                                                    </td>--}}
                                        <td>
                                            <div class="d-flex align-items-center">
                                                {{ $item->description}}
                                                <br><small class="badge mt-2">{{$item->category}}</small>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="d-flex align-items-center">
                                                {{--                                                @if(!empty($project->start_date)) <span class="mr-2">{{$project->start_date->format('d M Y')}}</span>--}}
                                                {{--                                                @else--}}
                                                {{--                                                    <span class="mr-2">No Date</span>--}}
                                                {{--                                                @endif--}}
                                                {{ $item->serial_number}}
                                            </div>
                                        </td>
                                        <td>
                                            <div class="d-flex align-items-center">
{{--                                                @if(!empty($project->end_date)) <span class="mr-2">{{$project->end_date->format('d M Y')}}</span>--}}
{{--                                                @else--}}
{{--                                                    <span class="mr-2">No Date</span>--}}
{{--                                                @endif--}}
                                                {{ $item->quantity }}
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    <footer class="footer">
        <div class="row align-items-center justify-content-xl-between">
            <div class="col-xl-6 m-auto text-center">
                <div class="copyright" style="margin-top: 20px; text-align: center">
                    <small><a href="#" target="_blank">{{$company->name}}</a> Powered by Kamora Software</small>
                </div>
            </div>
        </div>
    </footer>

    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
    <script>
        console.log("It's Raining");
        alert("It's raining man");
        var ctx = document.getElementById('inventoryChart').getContext('2d');
        var inventoryChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: ['Mouse', 'Keyboards'], // Product names
                datasets: [{
                    label: 'Quantity',
                    data: [10,55], // Product quantities
                    backgroundColor: 'rgba(54, 162, 235, 0.2)',
                    borderColor: 'rgba(54, 162, 235, 1)',
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    y: {
                        beginAtZero: true
                    }
                }
            }
        });
    </script>

{{--    <script>--}}
{{--        function generatePDF() {--}}
{{--            var canvas = document.getElementById('inventoryChart');--}}
{{--            var imgData = canvas.toDataURL('image/png');--}}

{{--            // Add the image into the DOM for PDF rendering--}}
{{--            var img = document.createElement('img');--}}
{{--            img.src = imgData;--}}
{{--            document.body.appendChild(img);--}}

{{--            // You can now proceed to generate the PDF including the chart as an image.--}}
{{--            window.location.href = "{{ route('admin.reports.inventory.download') }}"; // Call the route for generating the PDF--}}
{{--        }--}}
{{--    </script>--}}
</body>
</html>
