<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description"
          content="Kamora Software is a South Africa CRM Software that helps Small Businesses manage
          their sales pipelines, and helps them to understand their business better and continously grow with better.">
    <meta name="keywords"
          content="South African Products, CRM Software in South Africa, CRM Software, CRM System,
          Invoicing Platform,Free Online Quotes, Free Online Invoicing Application, South African Softwares">

    {{--	<link rel="shortcut icon" href="{{ asset('assets/img/favicon.png') }}">--}}
    <title>Kamora Software</title>
    <link rel="stylesheet" href="{{ asset('assets/css/plugins.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/theme/blue.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/font/dm.css') }}">
</head>
<body>
<div class="content-wrapper">
    <header class="wrapper bg-soft-primary">
        <nav class="navbar classic transparent navbar-expand-lg navbar-light">
            <div class="container flex-lg-row flex-nowrap align-items-center">
                <div class="navbar-brand w-100">
                    <a href="/">
                        <img src="{{asset('assets/img/kamora-logo.png')}}"
                             class="mt-5" srcset="{{asset('assets/img/kamora-logo.png')}} 2x"
                             alt="Kamora Software - CRM for Small for business" width="210"/>
                    </a>
                </div>
                <div class="navbar-collapse offcanvas-nav">
                    <div class="offcanvas-header d-lg-none d-xl-none">
                        <a href="/"><img src="{{ asset('assets/img/kamora-logo.png')}}"
                                         srcset="{{ asset('assets/img/kamora-logo.png')}} 2x" alt=""/></a>
                        <button type="button" class="btn-close btn-close-white offcanvas-close offcanvas-nav-close"
                                aria-label="Close"></button>
                    </div>
                    <ul class="navbar-nav">
                        @auth
                            <li class="nav-item"><a class="nav-link" href="{{route('admin.dashboard')}}">Dashboard</a>
                            </li>
                        @endauth
                        <li class="nav-item"><a class="nav-link" href="#product">Product</a></li>
                        <li class="nav-item"><a class="nav-link" href="#pricing">Pricing</a></li>

                    </ul>
                    <!-- /.navbar-nav -->
                </div>
                <!-- /.navbar-collapse -->
                <div class="navbar-other ms-lg-4">

                    <ul class="navbar-nav flex-row align-items-center ms-auto" data-sm-skip="true">
                        <li class="nav-item"><a class="nav-link" data-toggle="offcanvas-info"><i
                                    class="uil uil-info-circle"></i></a></li>
                        @guest
                            @if (Route::has('login'))
                                <li class="nav-item"><a class="nav-link"
                                                        href="{{ route('login') }}">{{ __('Login') }}</a></li>
                            @endif

{{--                            @if (Route::has('register'))--}}
{{--                                <li class="nav-item d-none d-md-block">--}}
{{--                                    <a href="{{ route('register') }}" class="btn btn-sm btn-primary rounded">Sign Up</a>--}}
{{--                                </li>--}}
{{--                            @endif--}}
                        @endguest

                        <li class="nav-item ms-lg-0">
                            <div class="navbar-hamburger d-lg-none d-xl-none ms-auto">
                                <button class="hamburger animate plain" data-toggle="offcanvas-nav"><span></span>
                                </button>
                            </div>
                        </li>
                    </ul>
                    <!-- /.navbar-nav -->
                </div>
                <!-- /.navbar-other -->
            </div>
            <!-- /.container -->
        </nav>
        <!-- /.navbar -->

        <div class="offcanvas-info text-inverse">
            <button type="button" class="btn-close btn-close-white offcanvas-close offcanvas-info-close"
                    aria-label="Close"></button>
            <a href="/"><img src="{{asset('assets/img/kamora-logo-light.png')}}"
                             srcset="{{asset('assets/img/kamora-logo-light.png')}} 2x" alt="" width="150"/></a>
            <div class="mt-4 widget">
                <p>Kamora Software is a South Africa CRM Software that helps <strong>Small Businesses</strong> manage
                    their sales pipelines, and get them to understand their business and how they can improve their
                    sales, customer satisfaction and maintain good customer relations by providing them with relevant
                    information and reporting tools to monitor their progress.</p>
            </div>
            <!-- /.widget -->
            <div class="widget">
                <h4 class="widget-title text-white mb-3">Contact Info</h4>
                <address> Johannesburg, Gauteng South Africa</address>
                <a href="sales@kodeinkgroup.co.za">sales@kodeinkgroup.co.za</a><br/> +27 61 479 5246
            </div>
            <!-- /.widget -->

            <!-- /.widget -->
            <div class="widget">
                <h4 class="widget-title text-white mb-3">Follow Us</h4>
                <nav class="nav social social-white">
                    <!-- <a href="#"><i class="uil uil-twitter"></i></a> -->
                    <a href="https://www.facebook.com/kamorasoftware"><i class="uil uil-facebook-f"></i></a>
                    <!-- <a href="#"><i class="uil uil-dribbble"></i></a>
                    <a href="#"><i class="uil uil-instagram"></i></a>
                    <a href="#"><i class="uil uil-youtube"></i></a> -->
                </nav>
                <!-- /.social -->
            </div>
            <!-- /.widget -->
        </div>
        <!-- /.offcanvas-info -->
    </header>
    <!-- /header -->
    <section class="wrapper bg-soft-primary">
        <div class="container pt-10 pb-12 pt-md-14 pb-md-17">
            <div class="row gx-lg-8 gx-xl-12 gy-10 align-items-center">
                <div
                    class="col-md-10 offset-md-1 offset-lg-0 col-lg-5 mt-lg-n2 text-center text-lg-start order-2 order-lg-0"
                    data-cues="slideInDown" data-group="page-title" data-delay="600">
                    <h1 class="display-1 mb-5 mx-md-10 mx-lg-0">Kamora Software is effortless and powerful with
                        <br/><span class="typer text-primary text-nowrap" data-delay="100" data-delim=":"
                                   data-words="online quotes or estimates:online invoicing:sales pipeline management"></span><span
                            class="cursor text-primary" data-owner="typer"></span></h1>
                    <p class="lead fs-lg mb-7">Understand your business sales pipeline, improve and grow. Better way to
                        send quotes and invoices</p>
                    <div class="d-flex justify-content-center justify-content-lg-start" data-cues="slideInDown"
                         data-group="page-title-buttons" data-delay="900">
                        <span><a href="{{route('register')}}"
                                 class="btn btn-lg btn-primary rounded me-2">Get Started</a></span>
                        <!-- <span><a class="btn btn-lg btn-green rounded">Free Trial</a></span> -->
                    </div>
                </div>

                <div class="col-lg-7">
                    <div class="row">
                        <div class="col-3 offset-1 offset-lg-0 col-lg-4 d-flex flex-column" data-cues="zoomIn"
                             data-group="col-start" data-delay="300">
                            <div class="ms-auto mt-auto"><img class="img-fluid rounded shadow-lg"
                                                              src="{{asset('assets/img/photos/sa20.jpg')}}"
                                                              srcset="{{asset('assets/img/photos/sa20.jpg')}} 2x"
                                                              alt=""/></div>
                            <div class="ms-auto mt-5 mb-10"><img class="img-fluid rounded shadow-lg"
                                                                 src="{{asset('assets/img/photos/sa18.jpg')}}"
                                                                 srcset="{{asset('assets/img/photos/sa20.jpg')}} 2x"
                                                                 alt=""/></div>
                        </div>

                        <div class="col-4 col-lg-5" data-cue="zoomIn">
                            <div><img class="w-100 img-fluid rounded shadow-lg"
                                      src="{{asset('assets/img/photos/sa16.jpg')}}"
                                      srcset="{{asset('assets/img/photos/sa16.jpg')}} 2x" alt=""/></div>
                        </div>

                        <div class="col-3 d-flex flex-column" data-cues="zoomIn" data-group="col-end" data-delay="300">
                            <div class="mt-auto"><img class="img-fluid rounded shadow-lg"
                                                      src="{{asset('assets/img/photos/sa21.jpg')}}"
                                                      srcset="{{asset('assets/img/photos/sa21.jpg')}} 2x" alt=""/></div>
                            <div class="mt-5"><img class="img-fluid rounded shadow-lg"
                                                   src="{{asset('assets/img/photos/sa19.jpg')}}"
                                                   srcset="{{asset('assets/img/photos/sa19.jpg')}} 2x" alt=""/></div>
                            <div class="mt-5 mb-10"><img class="img-fluid rounded shadow-lg"
                                                         src="{{asset('assets/img/photos/sa17.jpg')}}"
                                                         srcset="{{asset('assets/img/photos/sa17.jpg')}} 2x" alt=""/>
                            </div>
                        </div>

                    </div>
                    <!-- /.row -->
                </div>

            </div>
            <!-- /.row -->
        </div>
        <!-- /.container -->
    </section>
    <!-- /section -->

    <section class="wrapper" style="margin-top:8.25rem !important" id="pricing">
        <div class="container pb-1 pb-md-1">
            <div class="pricing-wrapper position-relative mt-n18 mt-md-n21 mb-12 mb-md-15">
                <div class="shape bg-dot primary rellax w-16 h-18" data-rellax-speed="1"
                     style="top: 2rem; right: -2.4rem;"></div>
                <div class="shape rounded-circle bg-line red rellax w-18 h-18 d-none d-lg-block" data-rellax-speed="1"
                     style="bottom: 0.5rem; left: -2.5rem;"></div>
                <div class="pricing-switcher-wrapper switcher">
                    <p class="mb-0 pe-3">Monthly</p>
                    <div class="pricing-switchers">
                        <div class="pricing-switcher pricing-switcher-active"></div>
                        <div class="pricing-switcher"></div>
                        <div class="switcher-button bg-primary"></div>
                    </div>
                    <p class="mb-0 ps-3">Yearly</p>
                </div>
                <div class="row gy-6 mt-3 mt-md-5">
                    <div class="col-md-6 col-lg-4">
                        <div class="pricing card text-center">
                            <div class="card-body">
                                <img src="{{asset('assets/img/icons/shopping-basket.svg')}}"
                                     class="svg-inject icon-svg icon-svg-md text-primary mb-3" alt=""/>
                                <h4 class="card-title">Basic Plan</h4>
                                <div class="prices text-dark">
                                    <div class="price price-show"><span class="price-currency">R</span><span
                                            class="price-value">29</span> <span class="price-duration">pm</span></div>
                                    <div class="price price-hide price-hidden"><span
                                            class="price-currency">R</span><span class="price-value">290</span> <span
                                            class="price-duration">yr</span></div>
                                </div>
                                <!--/.prices -->
                                <ul class="icon-list bullet-bg bullet-soft-primary mt-7 mb-8 text-start">
                                    <li><i class="uil uil-check"></i><span><strong>1</strong> User Account </span></li>
                                    <li><i class="uil uil-check"></i><span><strong>Account, Lead, and Opportunity </strong> Management</span></li>
                                    <li><i class="uil uil-check"></i><span><strong>Sales</strong> Pipeline</span></li>
                                    <li><i class="uil uil-check"></i><span><strong>Estimates/Quotes</strong> Management </span></li>
                                    <li>
                                        <i class="uil uil-check"></i><span> Tasks <strong>Management</strong> </span>
                                    </li>
                                </ul>
                                <a href="{{route('register', ['subscription_plan_id' => 1])}}" class="btn btn-primary rounded-pill">Try it Free</a>
                                <br>
                                <small class="text-muted mt-3">* Free 1 Month trial. No credit card required</small>
                            </div>
                            <!--/.card-body -->
                        </div>
                        <!--/.pricing -->
                    </div>
                    <!--/column -->
                    <div class="col-md-6 col-lg-4 popular">
                        <div class="pricing card text-center">
                            <div class="card-body">
                                <img src="{{asset('/assets/img/icons/home.svg')}}"
                                     class="svg-inject icon-svg icon-svg-md text-primary mb-3" alt=""/>
                                <h4 class="card-title">Standard Plan</h4>
                                <div class="prices text-dark">
                                    <div class="price price-show"><span class="price-currency">R</span><span
                                            class="price-value">99</span> <span class="price-duration">mo</span></div>
                                    <div class="price price-hide price-hidden"><span
                                            class="price-currency">R</span><span class="price-value">999</span> <span
                                            class="price-duration">yr</span></div>
                                </div>
                                <!--/.prices -->
                                <ul class="icon-list bullet-bg bullet-soft-primary mt-7 mb-8 text-start">
                                    <li><i class="uil uil-check"></i><span><strong>5</strong> User Accounts </span></li>
                                    <li><i class="uil uil-check"></i><span><strong>Account, Lead, and Opportunity </strong> Management</span></li>
                                    <li><i class="uil uil-check"></i><span><strong>Sales</strong> Pipeline</span></li>
                                    <li><i class="uil uil-check"></i><span><strong>Estimates/Quotes</strong> Management </span></li>
                                    <li><i class="uil uil-check"></i><span><strong>Invoice</strong> Management </span></li>
                                    <li>
                                        <i class="uil uil-check"></i><span> Tasks <strong>Management</strong> </span>
                                    </li>
                                </ul>
                                <a href="{{route('register', ['subscription_plan_id' => 2])}}"class="btn btn-primary rounded-pill">Try it Free</a>
                                <br>
                                <small class="text-muted mt-3">* Free 1 Month trial. No credit card required</small>
                            </div>
                            <!--/.card-body -->
                        </div>
                        <!--/.pricing -->
                    </div>
                    <!--/column -->
                    <div class="col-md-6 offset-md-3 col-lg-4 offset-lg-0">
                        <div class="pricing card text-center">
                            <div class="card-body">
                                <img src="{{ asset('assets/img/icons/briefcase-2.svg')}}"
                                     class="svg-inject icon-svg icon-svg-md text-primary mb-3" alt=""/>
                                <h4 class="card-title">Premium Plan</h4>
                                <div class="prices text-dark">
                                    <div class="price price-show"><span class="price-currency">R</span><span
                                            class="price-value">499</span> <span class="price-duration">mo</span></div>
                                    <div class="price price-hide price-hidden"><span
                                            class="price-currency">R</span><span class="price-value">4999</span> <span
                                            class="price-duration">yr</span></div>
                                </div>
                                <!--/.prices -->
                                <ul class="icon-list bullet-bg bullet-soft-primary mt-7 mb-8 text-start">
                                    <li><i class="uil uil-check"></i><span><strong>10</strong> User Accounts </span></li>
                                    <li><i class="uil uil-check"></i><span><strong>Standard Plan </strong> +</span></li>
                                    <li><i class="uil uil-check"></i><span><strong>Financial</strong> Management </span></li>
                                    <li><i class="uil uil-check"></i><span> Project <strong>Management</strong> </span></li>
                                    <li><i class="uil uil-check"></i><span> Reporting <strong>Management</strong> </span></li>
                                    <li><i class="uil uil-check"></i><span> Automated  <strong>Email Logging</strong> </span></li>
                                </ul>
                                <a href="{{route('register', ['subscription_plan_id' => 3])}}" class="btn btn-primary rounded-pill">Try it Free</a>
                                <br>
                                <small class="text-muted mt-3">* Free 1 Month trial. No credit card required</small>
                            </div>
                            <!--/.card-body -->
                        </div>
                        <!--/.pricing -->
                    </div>
                    <!--/column -->
                </div>
                <!--/.row -->
            </div>
            <!--/.pricing-wrapper -->
            <div class="row gx-lg-8 gx-xl-12 gy-6 mb-14 mb-md-1">
                <div class="col-lg-4">
                    <div class="d-flex flex-row">
                        <div>
                            <div class="icon btn btn-circle pe-none btn-primary me-4"><i
                                    class="uil uil-phone-volume"></i></div>
                        </div>
                        <div>
                            <h4>Support Services</h4>
                            <p class="mb-2">We dedicate ourselves into serving you and supporting you timely.</p>
                            <a href="#" class="more hover">Learn More</a>
                        </div>
                    </div>
                </div>
                <!--/column -->
                <div class="col-lg-4">
                    <div class="d-flex flex-row">
                        <div>
                            <div class="icon btn btn-circle pe-none btn-primary me-4"><i
                                    class="uil uil-laptop-cloud"></i></div>
                        </div>
                        <div>
                            <h4>Frequent Updates</h4>
                            <p class="mb-2">We update our Software Frequently to add more features and resolve any bugs.</p>
                            <a href="#" class="more hover">Learn More</a>
                        </div>
                    </div>
                </div>
                <!--/column -->
                <div class="col-lg-4">
                    <div class="d-flex flex-row">
                        <div>
                            <div class="icon btn btn-circle pe-none btn-primary me-4"><i class="uil uil-chart-line"></i>
                            </div>
                        </div>
                        <div>
                            <h4>Automated Services</h4>
                            <p class="mb-2">We try to automate as much functionality as we can to for efficients and accuracy.</p>
                            <a href="#" class="more hover">Learn More</a>
                        </div>
                    </div>
                </div>
                <!--/column -->
            </div>
            <!--/.row -->
        </div>
        <!-- /.container -->
    </section>
    <!-- /section -->

    <section class="wrapper bg-light">
        <div class="container py-14 pt-md-17 pb-md-25" id="product">
            <!-- <h2 class="fs-15 text-uppercase text-muted text-center mb-8">Trusted by Over 5000 Clients</h2> -->
            <!-- <div class="px-lg-5 mb-14 mb-md-19">
                <div class="row gx-0 gx-md-8 gx-xl-12 gy-8 align-items-center">
                    <div class="col-4 col-md-2">
                        <figure class="px-5 px-md-0 px-lg-2 px-xl-3 px-xxl-4"><img src="src/img/brands/c1.png" alt="" /></figure>
                    </div>

                    <div class="col-4 col-md-2">
                        <figure class="px-5 px-md-0 px-lg-2 px-xl-3 px-xxl-4"><img src="src/img/brands/c2.png" alt="" /></figure>
                    </div>

                    <div class="col-4 col-md-2">
                        <figure class="px-5 px-md-0 px-lg-2 px-xl-3 px-xxl-4"><img src="src/img/brands/c3.png" alt="" /></figure>
                    </div>

                    <div class="col-4 col-md-2">
                        <figure class="px-5 px-md-0 px-lg-2 px-xl-3 px-xxl-4"><img src="src/img/brands/c4.png" alt="" /></figure>
                    </div>

                    <div class="col-4 col-md-2">
                        <figure class="px-5 px-md-0 px-lg-2 px-xl-3 px-xxl-4"><img src="src/img/brands/c5.png" alt="" /></figure>
                    </div>

                    <div class="col-4 col-md-2">
                        <figure class="px-5 px-md-0 px-lg-2 px-xl-3 px-xxl-4"><img src="src/img/brands/c6.png" alt="" /></figure>
                    </div>

                </div>

            </div> -->
            <!-- /div -->
            <div class="row">
                <div class="col-md-10 offset-md-1 col-lg-8 offset-lg-2 mx-auto text-center">
                    <h2 class="fs-15 text-uppercase text-muted mb-3">Why Choose Kamora?</h2>
                    <h3 class="display-4 mb-10 px-xl-10 px-xxl-15">Here are a few reasons why you should choose
                        Kamora.</h3>
                </div>

            </div>
            <!-- /.row -->
            <ul class="nav nav-tabs nav-tabs-bg nav-tabs-shadow-lg d-flex justify-content-between nav-justified flex-lg-row flex-column">
                <li class="nav-item"><a class="nav-link d-flex flex-row active" data-bs-toggle="tab" href="#tab2-1">
                        <div><img src="{{asset('assets/img/icons/rocket.svg')}}"
                                  class="svg-inject icon-svg icon-svg-md text-yellow me-4" alt=""/></div>
                        <div>
                            <h4 class="mb-1">You grow with us</h4>
                            <p>We continuous improve our platform, and add more features to suit different business
                                needs, industries and size.</p>
                        </div>
                    </a></li>
                <li class="nav-item"><a class="nav-link d-flex flex-row" data-bs-toggle="tab" href="#tab2-2">
                        <div><img src="{{asset('assets/img/icons/savings.svg')}}"
                                  class="svg-inject icon-svg icon-svg-md text-green me-4" alt=""/></div>
                        <div>
                            <h4 class="mb-1">We keep it simple</h4>
                            <p>Our focus is giving you the best experience and support without complex processes, we
                                take care of our business so you can take care of yours.</p>
                        </div>
                    </a></li>
                <li class="nav-item"><a class="nav-link d-flex flex-row" data-bs-toggle="tab" href="#tab2-3">
                        <div><img src="{{asset('assets/img/icons/shield.svg')}}"
                                  class="svg-inject icon-svg icon-svg-md text-red me-4" alt=""/></div>
                        <div>
                            <h4 class="mb-1">Leads source Integration</h4>
                            <p>We can hook you up with leads on your sales pipeline from other platforms integrated with
                                our platform.</p>
                        </div>
                    </a></li>
            </ul>
            <!-- /.nav-tabs -->
            <div class="tab-content mt-6 mt-lg-8 mb-md-11">
                <div class="tab-pane fade show active" id="tab2-1">
                    <div class="row gx-lg-8 gx-xl-12 gy-10 align-items-center">
                        <div class="col-lg-6">
                            <div class="row gx-md-5 gy-5 align-items-center">
                                <div class="col-6">
                                    <img class="img-fluid rounded shadow-lg d-flex ms-auto"
                                         src="{{asset('assets/img/photos/sa13.jpg')}}"
                                         srcset="{{asset('assets/img/photos/sa13@2x.jpg')}} 2x" alt=""/>
                                </div>

                                <div class="col-6">
                                    <img class="img-fluid rounded shadow-lg mb-5"
                                         src="{{asset('assets/img/photos/sa14.jpg')}}"
                                         srcset="{{asset('assets/img/photos/sa14@2x.jpg')}} 2x" alt=""/>
                                    <img class="img-fluid rounded shadow-lg d-flex col-10"
                                         src="{{asset('assets/img/photos/sa15.jpg')}}"
                                         srcset="{{asset('assets/img/photos/sa15@2x.jpg')}} 2x" alt=""/>
                                </div>

                            </div>
                            <!-- /.row -->
                        </div>

                        <div class="col-lg-6">
                            <h2 class="mb-3">CRM Software for every Business</h2>
                            <p>Kamora Software is a proud South African digital product that helps small businesses
                                learn more about their businesses, improve their sales process, generate online
                                estimates and invoices and trace their revenue, and maintain good customer relationships
                                with their customers using Kamora CRM software.</p>
                            <ul class="icon-list bullet-bg bullet-soft-yellow">
                                <li><i class="uil uil-check"></i>Was designed with a small business owner in mind.</li>
                                <li><i class="uil uil-check"></i>Understands and addresses the difficulties small
                                    businesses face in maintaining customer relationships.
                                </li>
                                <li><i class="uil uil-check"></i>Helps business lean and manage their sales pipelines.
                                </li>
                            </ul>
                            <a href="{{route('register')}}" class="btn btn-yellow mt-2">Try It!</a>
                        </div>

                    </div>
                    <!--/.row -->
                </div>
                <!--/.tab-pane -->
                <div class="tab-pane fade" id="tab2-2">
                    <div class="row gx-lg-8 gx-xl-12 gy-10 align-items-center">
                        <div class="col-lg-6 order-lg-2">
                            <div class="row gx-md-5 gy-5">
                                <div class="col-5">
                                    <img class="img-fluid rounded shadow-lg my-5 d-flex ms-auto"
                                         src="{{asset('assets/img/photos/sa9.jpg')}}"
                                         srcset="{{asset('assets/img/photos/sa9@2x.jpg')}} 2x" alt=""/>
                                    <img class="img-fluid rounded shadow-lg d-flex col-10 ms-auto"
                                         src="{{asset('assets/img/photos/sa10.jpg')}}"
                                         srcset="{{asset('assets/img/photos/sa10@2x.jpg')}} 2x" alt=""/>
                                </div>

                                <div class="col-7">
                                    <img class="img-fluid rounded shadow-lg mb-5"
                                         src="{{asset('assets/img/photos/sa11.jpg')}}"
                                         srcset="{{asset('assets/img/photos/sa11@2x.jpg')}} 2x" alt=""/>
                                    <img class="img-fluid rounded shadow-lg d-flex col-11"
                                         src="{{asset('assets/img/photos/sa12.jpg')}}"
                                         srcset="{{asset('assets/img/photos/sa12@2x.jpg')}} 2x" alt=""/>
                                </div>

                            </div>
                            <!-- /.row -->
                        </div>

                        <div class="col-lg-6">
                            <h2 class="mb-3">Fast Transactions</h2>
                            <p>Etiam porta sem malesuada magna mollis euismod. Donec ullamcorper nulla non metus auctor
                                fringilla. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Fusce dapibus,
                                tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit
                                amet risus. Nullam quis risus eget urna.</p>
                            <ul class="icon-list bullet-bg bullet-soft-green">
                                <li><i class="uil uil-check"></i>Aenean eu leo quam. Pellentesque ornare.</li>
                                <li><i class="uil uil-check"></i>Nullam quis risus eget urna mollis ornare.</li>
                                <li><i class="uil uil-check"></i>Donec id elit non mi porta gravida at eget.</li>
                            </ul>
                            <a href="#" class="btn btn-green mt-2">Learn More</a>
                        </div>

                    </div>
                    <!--/.row -->
                </div>
                <!--/.tab-pane -->
                <div class="tab-pane fade" id="tab2-3">
                    <div class="row gx-lg-8 gx-xl-12 gy-10 align-items-center">
                        <div class="col-lg-6">
                            <div class="row gx-md-5 gy-5">
                                <div class="col-6">
                                    <img class="img-fluid rounded shadow-lg mb-5"
                                         src="{{asset('assets/img/photos/sa5.jpg')}}"
                                         srcset="{{asset('assets/img/photos/sa5@2x.jpg')}} 2x" alt=""/>
                                    <img class="img-fluid rounded shadow-lg d-flex col-10 ms-auto"
                                         src="{{asset('assets/img/photos/sa6.jpg')}}"
                                         srcset="{{asset('assets/img/photos/sa6@2x.jpg')}} 2x" alt=""/>
                                </div>

                                <div class="col-6">
                                    <img class="img-fluid rounded shadow-lg my-5"
                                         src="{{asset('assets/img/photos/sa7.jpg')}}"
                                         srcset="{{asset('assets/img/photos/sa7@2x.jpg')}} 2x" alt=""/>
                                    <img class="img-fluid rounded shadow-lg d-flex col-10"
                                         src="{{asset('assets/img/photos/sa8.jpg')}}"
                                         srcset="{{asset('assets/img/photos/sa8@2x.jpg')}} 2x" alt=""/>
                                </div>

                            </div>
                            <!-- /.row -->
                        </div>

                        <div class="col-lg-6">
                            <h2 class="mb-3">Secure Payments</h2>
                            <p>Etiam porta sem malesuada magna mollis euismod. Donec ullamcorper nulla non metus auctor
                                fringilla. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Fusce dapibus,
                                tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit
                                amet risus. Nullam quis risus eget urna.</p>
                            <ul class="icon-list bullet-bg bullet-soft-red">
                                <li><i class="uil uil-check"></i>Aenean eu leo quam. Pellentesque ornare.</li>
                                <li><i class="uil uil-check"></i>Nullam quis risus eget urna mollis ornare.</li>
                                <li><i class="uil uil-check"></i>Donec id elit non mi porta gravida at eget.</li>
                            </ul>
                            <a href="#" class="btn btn-red mt-2">Learn More</a>
                        </div>

                    </div>
                    <!--/.row -->
                </div>
                <!--/.tab-pane -->
            </div>
            <!-- /.tab-content -->
        </div>
        <!-- /.container -->
    </section>
    <!-- /section -->

    <section class="wrapper bg-soft-primary">
        <div class="container py-14 py-md-17">
            <div class="row mt-md-n25">
                <div class="col-md-10 offset-md-1 col-lg-8 offset-lg-2 mx-auto text-center">
                    <h2 class="fs-15 text-uppercase text-muted mb-3">Happy Customers</h2>
                    <h3 class="display-4 mb-10 px-xl-10 px-xxl-15">Don't take our word for it. See what customers are
                        saying about us.</h3>
                </div>

            </div>
            <!-- /.row -->
            <div class="grid">
                <div class="row isotope gy-6">
                    <div class="item col-md-6 col-xl-4">
                        <div class="card shadow-lg">
                            <div class="card-body">
                                <span class="ratings five mb-3"></span>
                                <blockquote class="icon mb-0">
                                    <p>“I am impressed with how easy it is to navigate through the platform, looks clean
                                        and simple.”</p>
                                    <div class="blockquote-details">
                                        {{--                                        <img class="rounded-circle w-12" src="{{asset('assets/img/avatars/te1.jpg')}}"--}}
                                        {{--                                             srcset="{{asset('assets/img/avatars/te1.jpg')}} 2x" alt=""/>--}}
                                        <div class="info">
                                            <h5 class="mb-1">Lehlohonolo Motsoeneng</h5>
                                            <p class="mb-0">Kodeink Group</p>
                                        </div>
                                    </div>
                                </blockquote>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>

                    <div class="item col-md-6 col-xl-4">
                        <div class="card shadow-lg">
                            <div class="card-body">
                                <span class="ratings five mb-3"></span>
                                <blockquote class="icon mb-0">
                                    <p>“I am looking forward to enjoying this platform, I already like how simple it
                                        is.”</p>
                                    <div class="blockquote-details">
                                        {{--                                        <img class="rounded-circle w-12" src="{{asset('assets/img/avatars/te1.jpg')}}"--}}
                                        {{--                                             srcset="{{asset('assets/img/avatars/te1.jpg')}} 2x" alt=""/>--}}
                                        <div class="info">
                                            <h5 class="mb-1">Thembi</h5>
                                            <p class="mb-0">Kodeink Group</p>
                                        </div>
                                    </div>
                                </blockquote>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>

                    <div class="item col-md-6 col-xl-4">
                        <div class="card shadow-lg">
                            <div class="card-body">
                                <span class="ratings five mb-3"></span>
                                <blockquote class="icon mb-0">
                                    <p>“Finally using a South African software to generate quote and invoices. I like
                                        it”</p>
                                    <div class="blockquote-details">
                                        {{--                                        <img class="rounded-circle w-12" src="{{asset('assets/img/avatars/te1.jpg')}}"--}}
                                        {{--                                             srcset="{{asset('assets/img/avatars/te1.jpg')}} 2x" alt=""/>--}}
                                        <div class="info">
                                            <h5 class="mb-1">Gabriel</h5>
                                            <p class="mb-0">The Classy CNF</p>
                                        </div>
                                    </div>
                                </blockquote>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>


                </div>
                <!-- /.row -->
            </div>
            <!-- /.grid-view -->
        </div>
        <!-- /.container -->
    </section>
    <!-- /section -->

    <section class="wrapper bg-soft-primary">
        <div class="container py-14 py-md-17" id="faqs">
            <div class="row">
                <div class="col-lg-11 col-xxl-10 mx-auto text-center">
                    <h2 class="fs-15 text-uppercase text-muted mb-3">FAQ</h2>
                    <h3 class="display-4 mb-10 px-lg-12 px-xl-10 px-xxl-15">If you don't see an answer to your question,
                        you can send us an email directly at <a href="mailto:support@kodeinkgroup.co.za">Support
                            Email</a>.</h3>
                </div>

            </div>
            <!--/.row -->
            <div class="row">
                <div class="col-lg-7 mx-auto">
                    <div id="accordion-3" class="accordion-wrapper">
                        <div class="card shadow-lg">
                            <div class="card-header" id="accordion-heading-3-1">
                                <button class="collapsed" data-bs-toggle="collapse"
                                        data-bs-target="#accordion-collapse-3-1" aria-expanded="false"
                                        aria-controls="accordion-collapse-3-1">What formats can I download of
                                    Quotes/Estimates and Invoices?
                                </button>
                            </div>
                            <!-- /.card-header -->
                            <div id="accordion-collapse-3-1" class="collapse" aria-labelledby="accordion-heading-3-1"
                                 data-bs-target="#accordion-3">
                                <div class="card-body">
                                    <p>You can only download Quotes/Estimates and invoices in PDF format.</p>
                                </div>
                                <!-- /.card-body -->
                            </div>
                            <!-- /.collapse -->
                        </div>
                        <!-- /.card -->
                        <div class="card shadow-lg">
                            <div class="card-header" id="accordion-heading-3-2">
                                <button class="collapsed" data-bs-toggle="collapse"
                                        data-bs-target="#accordion-collapse-3-2" aria-expanded="false"
                                        aria-controls="accordion-collapse-3-2">How can Kamora help my business?
                                </button>
                            </div>
                            <!-- /.card-header -->
                            <div id="accordion-collapse-3-2" class="collapse" aria-labelledby="accordion-heading-3-2"
                                 data-bs-target="#accordion-3">
                                <div class="card-body">
                                    <p>By helping you understand your sales pipeline, how many leads your business
                                        generates, how many of them gets converted into customers and how many of your
                                        deals fail and succeed. </p>
                                </div>
                                <!-- /.card-body -->
                            </div>
                            <!-- /.collapse -->
                        </div>
                        <!-- /.card -->
                        <div class="card shadow-lg">
                            <div class="card-header" id="accordion-heading-3-3">
                                <button class="collapsed" data-bs-toggle="collapse"
                                        data-bs-target="#accordion-collapse-3-3" aria-expanded="false"
                                        aria-controls="accordion-collapse-3-3">What is a lead?
                                </button>
                            </div>
                            <!-- /.card-header -->
                            <div id="accordion-collapse-3-3" class="collapse" aria-labelledby="accordion-heading-3-3"
                                 data-bs-target="#accordion-3">
                                <div class="card-body">
                                    <p>Our definition of lead, is any entity that can potentially become your customer.
                                        It's the customer entry point on our platform in the sales funnel.</p>
                                </div>
                                <!-- /.card-body -->
                            </div>
                            <!-- /.collapse -->
                        </div>
                        <!-- /.card -->
                        <div class="card shadow-lg">
                            <div class="card-header" id="accordion-heading-3-4">
                                <button class="collapsed" data-bs-toggle="collapse"
                                        data-bs-target="#accordion-collapse-3-4" aria-expanded="false"
                                        aria-controls="accordion-collapse-3-4">What is a prospect?
                                </button>
                            </div>
                            <!-- /.card-header -->
                            <div id="accordion-collapse-3-4" class="collapse" aria-labelledby="accordion-heading-3-4"
                                 data-bs-target="#accordion-3">
                                <div class="card-body">
                                    <p>A prospect in our definition is any entity or a lead that has met certain
                                        criteria and have better chances of becoming a customer. A prospect is the next
                                        level in the sales funnel.</p>
                                </div>
                                <!-- /.card-body -->
                            </div>
                            <!-- /.collapse -->
                        </div>
                        <div class="card shadow-lg">
                            <div class="card-header" id="accordion-heading-3-4">
                                <button class="collapsed" data-bs-toggle="collapse"
                                        data-bs-target="#accordion-collapse-3-4" aria-expanded="false"
                                        aria-controls="accordion-collapse-3-4">What is an Account?
                                </button>
                            </div>
                            <!-- /.card-header -->
                            <div id="accordion-collapse-3-4" class="collapse" aria-labelledby="accordion-heading-3-4"
                                 data-bs-target="#accordion-3">
                                <div class="card-body">
                                    <p>An account is a customer that has bought the company's products and services.</p>
                                </div>
                                <!-- /.card-body -->
                            </div>
                            <!-- /.collapse -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.accordion-wrapper -->
                </div>

            </div>
            <!-- /.row -->
        </div>
        <!-- /.container -->
    </section>
    <!-- /section -->
</div>
<!-- /.content-wrapper -->
<footer class="bg-dark text-inverse">
    <div class="container py-13 py-md-15">
        <div class="row gy-6 gy-lg-0">
            <div class="col-md-4 col-lg-3">
                <div class="widget">
                    <img class="mb-4" src="{{asset('assets/img/kamora-logo-light.png')}}"
                         srcset="{{asset('assets/img/kamora-logo-light.png')}} 2x" alt=""/>
                    <p class="mb-4">© 2022 Kamora Software <br class="d-none d-lg-block"/>All rights reserved. <br>Product
                        of <a href="https://kodeinkgroup.co.za">Kodeink</a></p>
                    <nav class="nav social social-white">
                        <a href="https://www.facebook.com/kamorasoftware/"><i class="uil uil-facebook-f"></i></a>
                    </nav>
                    <!-- /.social -->
                </div>
                <!-- /.widget -->
            </div>

            <div class="col-md-4 col-lg-3">
                <div class="widget">
                    <h4 class="widget-title text-white mb-3">Get in Touch</h4>
                    <address class="pe-xl-15 pe-xxl-17">Johannesburg, Gauteng , South Africa</address>
                    <a href="mailto:sales@kodeinkgroup.co.za">sales@kodeinkgroup.co.za</a><br/> +27 61 479 5246
                </div>
                <!-- /.widget -->
            </div>

            <div class="col-md-4 col-lg-3">
                <div class="widget">
                    <h4 class="widget-title text-white mb-3">Quick Links</h4>
                    <ul class="list-unstyled mb-0">
                        <li><a href="#product">Product</a></li>
                        <li><a href="#faqs">FAQs</a></li>
                        <li><a href="#">Terms of Use</a></li>
                        <li><a href="#">Privacy Policy</a></li>
                        <li><a href="emailto:support@kodeinkgroup.co.za">Email Support</a></li>
                    </ul>
                </div>
                <!-- /.widget -->
            </div>

            <div class="col-md-12 col-lg-3">
                <div class="widget">
                    <h4 class="widget-title text-white mb-3">Our Newsletter</h4>
                    <p class="mb-5">We will Soon establish our Newsletter, if you're interested in our upcoming news,
                        you can drop your email below..</p>
                    <div class="newsletter-wrapper">
                        <!-- Begin Mailchimp Signup Form -->
                        <div id="mc_embed_signup2">
                            <form
                                action="#"
                                method="post" id="mc-embedded-subscribe-form2" name="mc-embedded-subscribe-form"
                                class="validate dark-fields" target="_blank" novalidate>
                                <div id="mc_embed_signup_scroll2">
                                    <div class="mc-field-group input-group form-label-group">
                                        <input type="email" value="" name="EMAIL" class="required email form-control"
                                               placeholder="Email Address" id="mce-EMAIL2">
                                        <label for="mce-EMAIL2">Email Address</label>
                                        <input type="submit" value="Join" name="subscribe" id="mc-embedded-subscribe2"
                                               class="btn btn-primary">
                                    </div>
                                    <div id="mce-responses2" class="clear">
                                        <div class="response" id="mce-error-response2" style="display:none"></div>
                                        <div class="response" id="mce-success-response2" style="display:none"></div>
                                    </div>
                                    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                                    <div style="position: absolute; left: -5000px;" aria-hidden="true"><input
                                            type="text" name="b_ddc180777a163e0f9f66ee014_4b1bcfa0bc" tabindex="-1"
                                            value=""></div>
                                    <div class="clear"></div>
                                </div>
                            </form>
                        </div>
                        <!--End mc_embed_signup-->
                    </div>
                    <!-- /.newsletter-wrapper -->
                </div>
                <!-- /.widget -->
            </div>

        </div>
        <!--/.row -->
    </div>
    <!-- /.container -->
</footer>
<div class="progress-wrap">
    <svg class="progress-circle svg-content" width="100%" height="100%" viewBox="-1 -1 102 102">
        <path d="M50,1 a49,49 0 0,1 0,98 a49,49 0 0,1 0,-98"/>
    </svg>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"
        integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ=="
        crossorigin="anonymous"></script>
@stack('footer-scripts')
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4"
        crossorigin="anonymous"></script>

<script src="{{ asset('assets/js/plugins.js') }}"></script>
<script src="{{ asset('assets/js/scripts.js') }}"></script>

</body>
</html>
