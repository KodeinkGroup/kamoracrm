<?php

use Illuminate\Support\Facades\Route;

Route::get('/index', 'App\Http\Controllers\Admin\InvoicesController@index')->name('index');
Route::get('invoices-recurring', 'App\Http\Controllers\Admin\InvoicesController@recurring')->name('recurring');
Route::get('{invoice}', 'App\Http\Controllers\Admin\InvoicesController@show')->where('invoice', '[0-9]+')->name('show');
Route::get('export/{id}', 'App\Http\Controllers\Admin\InvoicesController@download')->name('download');
Route::get('create/{type}', 'App\Http\Controllers\Admin\EstimateController@create');
Route::get('invoice-payments', 'App\Http\Controllers\Admin\InvoicesController@payments')->name('payments');
