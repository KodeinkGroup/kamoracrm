<?php
use Illuminate\Support\Facades\Route;

Route::get('index','App\Http\Controllers\Admin\EmployeesController@index' )
    ->name('index');
//Route::get('employees/{id}','App\Http\Controllers\Admin\EmployeesController@show' )
//    ->name('view');
Route::get('employees/create','App\Http\Controllers\Admin\EmployeesController@create' )
    ->name('create');
Route::post('employees/','App\Http\Controllers\Admin\EmployeesController@store' )
    ->name('store');
Route::get('employees/{id}','App\Http\Controllers\Admin\EmployeesController@edit' )
    ->name('edit');
Route::put('update/{id}','App\Http\Controllers\Admin\EmployeesController@update')
    ->name('update');
