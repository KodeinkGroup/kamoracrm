<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\Prospects\ProspectsController;
use App\Http\Controllers\Admin\Prospects\ProspectContactsController;
use App\Http\Controllers\Admin\Prospects\ProspectDashboardController;

//  Prefix: prospects
//  Name:   admin.prospects


//get requests
Route::get('/', [ProspectsController::class, 'index'])->name('dashboard');
Route::get('create/{type}', [ProspectsController::class, 'create'])->name('create');
Route::get('{prospect}/edit', [ProspectsController::class, 'edit'])->where('prospect', '[0-9]+')->name('edit');
Route::get('{prospect}', [ProspectsController::class, 'show'])->where('prospect', '[0-9]+')->name('show');
Route::get('{prospect}/dashboard', [ProspectDashboardController::class, 'index'])->where('prospect', '[0-9]+')->name('prospect.dashboard');


Route::get('/{prospect}/contact/create', 'App\Http\Controllers\Admin\Prospects\ProspectContactsController@create')->where('prospect', '[0-9]+')->name('contacts.create');

//delete request
Route::get('{prospect}/delete', [ProspectsController::class, 'destroy'])->where('prospect', '[0-9]+')->name('delete');
Route::delete('{prospect}/logo-delete', [ProspectsController::class, 'destroyProfileImage'])->where('prospect', '[0-9]+')->name('delete.logo-delete');

//post request
Route::post('/', [ProspectsController::class, 'store'])->name('store');
Route::post('{prospect}/contact', [ProspectContactsController::class, 'store'])->name('contacts.store');

//put request
Route::put('{prospect}', [ProspectsController::class, 'update'])->where('prospect', '[0-9]+')->name('update');
Route::put('{prospect}/contact', [ProspectContactsController::class, 'update'])->where('prospect', '[0-9]+')->name('contacts.update');
Route::put('{prospect}/logo-update', [ProspectsController::class, 'updateProfileImage'])->where('prospect', '[0-9]+')->name('update.logo-update');


Route::prefix('activities')->name('activities.')->group(base_path('routes/web/prospects/activities.php'));


