
<?php

use Illuminate\Support\Facades\Route;

Route::get('/managerial', 'App\Http\Controllers\Admin\ReportsController@generateReport')->name('managerial');
Route::get('/inventory', 'App\Http\Controllers\Admin\ReportsController@generateInventoryReport')->name('inventory');
Route::get('/inventory/download', 'App\Http\Controllers\Admin\ReportsController@downloadInventoryReport')->name('inventory.download');
Route::get('/projects', 'App\Http\Controllers\Admin\ReportsController@generateProjectsReport')->name('projects');
