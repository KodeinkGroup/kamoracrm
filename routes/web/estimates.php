<?php

use Illuminate\Support\Facades\Route;

Route::get('/index', 'App\Http\Controllers\Admin\EstimateController@index')->name('index');
Route::get('{estimate}', 'App\Http\Controllers\Admin\EstimateController@show')->where('estimate','[0-9]+')->name('show');
Route::get('export/{estimate}', 'App\Http\Controllers\Admin\EstimateController@exportEstimate')->name('export');
Route::get('create/{type}','App\Http\Controllers\Admin\EstimateController@create')->name('create');
Route::post('estimates','App\Http\Controllers\Admin\EstimateController@store')->name('store');
