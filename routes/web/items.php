<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\ItemController;

Route::get('/', 'App\Http\Controllers\Admin\ItemController@index')->name('index');
Route::get('/create', 'App\Http\Controllers\Admin\ItemController@create')->name('create');
Route::post('/store', 'App\Http\Controllers\Admin\ItemController@store')->name('store');
Route::get('/edit/{id}', 'App\Http\Controllers\Admin\ItemController@edit')->name('edit');
Route::put('/update/{id}', 'App\Http\Controllers\Admin\ItemController@update')->name('update');
Route::get('/delete/{id}', 'App\Http\Controllers\Admin\ItemController@destroy')->name('destroy');
Route::get('/material-requests', 'App\Http\Controllers\Admin\ItemController@materialRequests')->name('listMaterialRequests');
