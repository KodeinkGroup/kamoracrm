<?php
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\AccountsController;

Route::get('/', 'App\Http\Controllers\Admin\AccountsController@index')->name('index');
Route::get('accounts', 'App\Http\Controllers\Admin\AccountsController@create')->name('create');
Route::post('accounts', 'App\Http\Controllers\Admin\AccountsController@store')->name('store');
