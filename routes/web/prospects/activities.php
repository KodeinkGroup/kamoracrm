<?php

// Prefix: admin/prospects/activities

// use App\Http\Controllers\Admin\Prospects\ProspectsController;
// use App\Http\Controllers\Admin\Prospects\ProspectContactsController;
use App\Http\Controllers\Admin\Prospects\Activities\ProspectActivitiesController;



Route::get('{prospect}', [ProspectActivitiesController::class, 'index'])->name('dashboard');
Route::get('{prospect}/create', [ProspectActivitiesController::class, 'create'])->name('create');
Route::get('{prospect}/{activity}', [ProspectActivitiesController::class, 'show'])->name('show');

Route::post('{prospect}', [ProspectActivitiesController::class, 'store'])->name('store');

