<?php

use Illuminate\Support\Facades\Route;

Route::get('/index', 'App\Http\Controllers\Admin\TasksController@index')->name('index');
Route::get('/backlog', 'App\Http\Controllers\Admin\TasksController@index')->name('backlog');
Route::get('/sprints', 'App\Http\Controllers\Admin\TasksController@sprints')->name('sprints');
Route::get('/{id}/view', 'App\Http\Controllers\Admin\TasksController@view')->name('view');
