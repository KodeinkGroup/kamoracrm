<?php
use Illuminate\Support\Facades\Route;

Route::get('index', 'App\Http\Controllers\Admin\ScheduleController@index')->name('index');
Route::get('schedule', 'App\Http\Controllers\Admin\ScheduleController@create')->name('create');
Route::post('schedule', 'App\Http\Controllers\Admin\ScheduleController@store')->name('store');
