<?php
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\TripController;
use App\Http\Controllers\Admin\FuelUsageController;


Route::get('/', 'App\Http\Controllers\Admin\CompanyController@index')->name('index');
Route::get('/create', 'App\Http\Controllers\Admin\CompanyController@create')->name('create');
Route::get('/{user}', 'App\Http\Controllers\Admin\CompanyController@show')->name('show');
Route::post('/store', 'App\Http\Controllers\Admin\CompanyController@store')->name('store');
Route::get('/edit/{id}', 'App\Http\Controllers\Admin\CompanyController@edit')->name('edit');
Route::post('/{company}', 'App\Http\Controllers\Admin\CompanyController@update')->name('update');
Route::put('{company}/logo-update', 'App\Http\Controllers\Admin\CompanyController@updateLogo')
    ->where('company', '[0-9]+')->name('update.logo-update');
Route::post('{company}/department','App\Http\Controllers\Admin\CompanyController@storeDepartment' )
    ->name('store.department');
Route::post('{company}/department/{id}','App\Http\Controllers\Admin\CompanyController@updateDepartment' )
    ->name('update.department');

Route::get('vehicles/index', 'App\Http\Controllers\Admin\VehiclesController@index')->name('vehicles.index');
Route::get('vehicle/create', 'App\Http\Controllers\Admin\VehiclesController@create')->name('vehicles.create');
Route::post('vehicles/store', 'App\Http\Controllers\Admin\VehiclesController@store')->name('vehicles.store');
Route::get('vehicles/view/{id}', 'App\Http\Controllers\Admin\VehiclesController@view')->name('vehicles.view');
//Route::post('vehicles/store', 'App\Http\Controllers\Admin\VehiclesController@store')->name('vehicles.store');
Route::get('fleets/index', 'App\Http\Controllers\Admin\VehiclesController@fleetsIndex')->name('fleets.index');
Route::get('fleets/create', 'App\Http\Controllers\Admin\VehiclesController@createFleet')->name('fleets.create');
Route::post('fleets/store', 'App\Http\Controllers\Admin\VehiclesController@storeFleet')->name('fleets.store');
Route::get('fleets/{id}/delete', 'App\Http\Controllers\Admin\VehiclesController@destroyFleet')->name('fleets.delete');
Route::get('vehicles/{id}/delete', 'App\Http\Controllers\Admin\VehiclesController@destroyVehicle')->name('vehicles.delete');
Route::get('vehicles/trips', 'App\Http\Controllers\Admin\TripController@create')->name('vehicles.trips.create');
Route::get('vehicles/trips/index', 'App\Http\Controllers\Admin\TripController@index')->name('vehicles.trips.index');
Route::post('vehicles/trips/create', 'App\Http\Controllers\Admin\TripController@store')->name('vehicles.trips.store');
Route::get('vehicles/trips/edit/{trip}', 'App\Http\Controllers\Admin\TripController@edit')->name('vehicles.trips.edit');
Route::put('vehicles/trips/{id}/update', 'App\Http\Controllers\Admin\TripController@update')->name('vehicles.trips.update');
Route::resource('fuel-usages', FuelUsageController::class);

// Human Resources
Route::get('{id}/human-resources/manage-leave-types', 'App\Http\Controllers\Admin\CompanyController@manageLeaveTypes')->name('hr.manage.leaveTypes');
Route::post('human-resources/leave-types', 'App\Http\Controllers\Admin\CompanyController@update')->name('hr.manage.storeLeaveTypes');


