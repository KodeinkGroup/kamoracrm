<?php

use App\Http\Controllers\Admin\Leads\LeadsController;

Route::get('/', 'App\Http\Controllers\Admin\Leads\LeadsController@index')->name('dashboard');
Route::get('/leads/new', 'App\Http\Controllers\Admin\Leads\LeadsController@create')->name('create');
