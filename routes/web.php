    <?php

    use Illuminate\Support\Facades\Auth;
    use Illuminate\Support\Facades\Route;
    use App\Http\Controllers\Admin\UsersController;

    /*
    |--------------------------------------------------------------------------
    | Web Routes
    |--------------------------------------------------------------------------
    |
    | Here is where you can register web routes for your application. These
    | routes are loaded by the RouteServiceProvider within a group which
    | contains the "web" middleware group. Now create something great!
    |
    */

    Route::get('/', [App\Http\Controllers\HomeController::class, 'index']);

    Auth::routes(['register' => true]);

    Route::get('/admin/dashboard', [App\Http\Controllers\Admin\DashboardController::class, 'index'])
        ->name('admin.user.dashboard');
    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])
        ->name('home');
    Route::post('/alerts/{id}/update-status', [App\Http\Controllers\Admin\AlertsController::class, 'updateStatus'])
        ->name('admin.alerts.updateStatus');
    Route::prefix('prospects')
        ->middleware('auth')
        ->name('admin.prospects.')
        ->group(base_path('routes/web/prospects.php'));

    Route::prefix('leads')
        ->middleware('auth')
        ->name('admin.leads.')
        ->group(base_path('routes/web/leads.php'));

    // Admin users routes
    Route::middleware('auth', 'isMaster')
        ->namespace('admin')
        ->group(function() {
        Route::get('admin/users', [UsersController::class, 'index'])
            ->name('admin.users.index');
        Route::post('admin/users', '\App\Http\Controllers\Admin\UsersController@store')
            ->name('admin.users.store');
        Route::get('admin/users/{id}', [UsersController::class, 'edit'])
                ->name('admin.users.edit');
        Route::put('admin/users/{id}', '\App\Http\Controllers\Admin\UsersController@update')
                ->name('admin.users.update');
        Route::get('admin/industries', '\App\Http\Controllers\Admin\IndustriesController@index')
                ->name('admin.industries.index');
        Route::get('admin/industries/create', '\App\Http\Controllers\Admin\IndustriesController@create')
                ->name('admin.industries.create');
        Route::post('admin/industries', '\App\Http\Controllers\Admin\IndustriesController@store')
                ->name('admin.industries.store');
        Route::get('admin/subscriptions', '\App\Http\Controllers\Admin\SubscriptionsController@index')
            ->name('admin.subscriptions.index');
        Route::get('admin/subscriptions/create', '\App\Http\Controllers\Admin\SubscriptionsController@create')
            ->name('admin.subscriptions.create');
        Route::post('admin/industries', '\App\Http\Controllers\Admin\SubscriptionsController@store')
            ->name('admin.subscriptions.store');
    });

    // Items
    Route::prefix('items')
        ->middleware('auth')
        ->name('admin.items.')
        ->group(base_path('routes/web/items.php'));

    // Estimates
    Route::prefix('estimates')
        ->middleware('auth')
        ->name('admin.estimates.')
        ->group(base_path('routes/web/estimates.php'));

    // Invoices
    Route::prefix('invoices')
        ->middleware('auth')
        ->name('admin.invoices.')
        ->group(base_path('routes/web/invoices.php'));

    // Accounts
    Route::prefix('accounts')
        ->middleware('auth')
        ->name('admin.accounts.')
        ->group(base_path('routes/web/accounts.php'));

    //Company
    Route::prefix('company')
        ->middleware('auth')
        ->name('admin.company.')
        ->group(base_path('routes/web/company.php'));

    //Tasks
    Route::prefix('tasks')
        ->middleware('auth')
        ->name('admin.tasks.')
        ->group(base_path('routes/web/tasks.php'));
    //Projects
    Route::prefix('projects')
        ->middleware('auth')
        ->name('admin.projects.')
        ->group(base_path('routes/web/projects.php'));

    Route::prefix('employees')
        ->middleware('auth')
        ->name('admin.employees.')
        ->group(base_path('routes/web/employees.php'));

     Route::prefix('reports')
            ->middleware('auth')
            ->name('admin.reports.')
            ->group(base_path('routes/web/reports.php'));

    Route::prefix('schedules')
        ->middleware('auth')
        ->name('admin.schedules.')
        ->group(base_path('routes/web/schedules.php'));

    // Vue Routes
    Route::view('/{any}', 'admin.index')
        ->middleware('auth')
        ->where('any', '.*');
