<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {

    $user = $request->user();

    // Load the user's role and associated permissions
    $user->load('roles.permissions');

    // Get the user's role
    $role = $user->roles->first(); // Retrieve the single role assigned to the user

    // Get the permissions for that role
    $permissions = $role ? $role->permissions : collect();

    // Add permissions to the user object
    $user->setAttribute('permissions', $permissions);

    return $user;
    // Return the user along with their permissions
//    return response()->json([
//        'user' => $user->only(['id', 'name', 'email', 'company_id', 'role', 'created_at', 'permissions']),
////        'permissions' => $permissions,
//    ]);
});

Route::get('/company', 'App\Http\Controllers\Admin\CompanyController@companyDetails')->name('admin.company.details');
Route::get('/customers', 'App\Http\Controllers\Admin\AccountsController@customers')->name('admin.customers');
Route::get('/users', 'App\Http\Controllers\Admin\UsersController@list')->name('admin.users');


Route::get('/dashboard', 'App\Http\Controllers\Admin\DashboardController@dashboardData')->name('admin.dashboard');

//Estimates
Route::get('/estimates', 'App\Http\Controllers\Admin\EstimateController@list')->name('admin.estimates.list');
Route::get('/estimates/{id}', 'App\Http\Controllers\Admin\EstimateController@estimateDetail')->name('admin.estimates.detail');
Route::post('/estimates', 'App\Http\Controllers\Admin\EstimateController@store')->name('admin.estimates.new');
Route::put('/estimates/{id}/edit', 'App\Http\Controllers\Admin\EstimateController@update')->name('admin.estimates.edit');
Route::delete('/estimates/{id}', 'App\Http\Controllers\Admin\EstimateController@destroy')->name('admin.estimates.delete');
Route::post('/estimates/{id}', 'App\Http\Controllers\Admin\EstimateController@updateEstimateStatus')->name('admin.estimates.accept');

//Invoices

Route::get('/invoices/', 'App\Http\Controllers\Admin\InvoicesController@list')->name('admin.invoices.list');
Route::get('/invoices-recurring/', 'App\Http\Controllers\Admin\InvoicesController@listRecurring')->name('admin.invoices.listRecurring');
Route::get('/invoices/{id}', 'App\Http\Controllers\Admin\InvoicesController@invoiceDetail')->name('admin.invoices.detail');
Route::post('/invoices', 'App\Http\Controllers\Admin\InvoicesController@store')->name('admin.invoices.store');
Route::put('/invoices/{id}/edit', 'App\Http\Controllers\Admin\InvoicesController@update')->name('admin.invoices.edit');
Route::get('/invoices/export/{id}', 'App\Http\Controllers\Admin\InvoicesController@exportInvoice')->name('admin.invoices.export');
Route::put('/invoices/update/{id}/status', 'App\Http\Controllers\Admin\InvoicesController@updateStatus')->name('admin.invoices.update.status');
Route::put('/invoices/update/{id}/type', 'App\Http\Controllers\Admin\InvoicesController@updateType')->name('admin.invoices.update.type');
Route::get('/invoice-payments/', 'App\Http\Controllers\Admin\InvoicesController@listPayments');
Route::post('/invoices/payments', 'App\Http\Controllers\Admin\InvoicesController@recordPayment')
    ->name('admin.invoices.payment');
Route::delete('/invoices/payments/delete/{id}', 'App\Http\Controllers\Admin\InvoicesController@deletePayment')
    ->name('admin.invoices.deletePayment');

Route::get('/items', 'App\Http\Controllers\Admin\ItemController@list')->name('admin.items.list');

// Tasks

Route::get('/tasks/', 'App\Http\Controllers\Admin\TasksController@list')->name('admin.tasks.list');
Route::get('/tasks/{id}', 'App\Http\Controllers\Admin\TasksController@show')->name('admin.tasks.show');
Route::post('/tasks', 'App\Http\Controllers\Admin\TasksController@store')->name('admin.tasks.create');
Route::put('/tasks/update/{id}/status', 'App\Http\Controllers\Admin\TasksController@updateStatus')->name('admin.tasks.updateStatus');
Route::put('/tasks/update/{id}', 'App\Http\Controllers\Admin\TasksController@update')->name('admin.tasks.update');
Route::put('/tasks/{id}/addToSprint', 'App\Http\Controllers\Admin\TasksController@addToSprint')->name('admin.tasks.addToSprint');
Route::put('/tasks/{id}/removeFromSprint', 'App\Http\Controllers\Admin\TasksController@removeFromSprint')->name('admin.tasks.removeFromSprint');

Route::post('/tasks/task-types', 'App\Http\Controllers\Admin\TasksController@addTaskType')->name('admin.tasks.addTaskType');

// Projects
Route::get('/projects/', 'App\Http\Controllers\Admin\ProjectsController@list')->name('admin.projects.list');
Route::post('/projects', 'App\Http\Controllers\Admin\ProjectsController@store')->name('admin.projects.store');
Route::get('/projects/{id}', 'App\Http\Controllers\Admin\ProjectsController@detail')->name('admin.projects.detail');
Route::delete('/projects/{id}', 'App\Http\Controllers\Admin\ProjectsController@destroy')->name('admin.projects.delete');
Route::put('/projects/update/{id}', 'App\Http\Controllers\Admin\ProjectsController@update')->name('admin.projects.update');
Route::post('/projects/requestMaterial', 'App\Http\Controllers\Admin\ProjectsController@requestMaterial')->name('admin.projects.requestMaterial');
Route::put('/projects/requestMaterial/status/{id}', 'App\Http\Controllers\Admin\ProjectsController@updateRequestMaterialStatus')->name('admin.projects.updateRequestMaterialStatus');

// Sprints

Route::get('/sprints/active', 'App\Http\Controllers\Admin\SprintsController@active')->name('admin.sprints.active');
Route::put('/sprints/update/{id}/status', 'App\Http\Controllers\Admin\SprintsController@updateStatus')->name('admin.sprints.updateStatus');
Route::post('/sprints', 'App\Http\Controllers\Admin\SprintsController@store')->name('admin.sprints.store');
