document.body.onload = () => {
    let showUFormBtn = document.getElementById('show-new-user-form');
    let newUserForm = document.getElementById('new-user-form');

    if (newUserForm) {
        newUserForm.style.display = "none";
    }

    showUFormBtn.onclick = () => {
        console.log('Clicked');

        if(newUserForm.style.display == "none") {
            
            newUserForm.style.display = "block";
            showUFormBtn.style.display = "none";
        }
        else {
            newUserForm.style.display == "none";
            showUFormBtn.style.display = "block";
        } 
    };
    
}