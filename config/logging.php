<?php

use Monolog\Handler\NullHandler;
use Monolog\Handler\StreamHandler;
use Monolog\Handler\SyslogUdpHandler;

return [

    /*
    |--------------------------------------------------------------------------
    | Default Log Channel
    |--------------------------------------------------------------------------
    |
    | This option defines the default log channel that gets used when writing
    | messages to the logs. The name specified in this option should match
    | one of the channels defined in the "channels" configuration array.
    |
    */

    'default' => env('LOG_CHANNEL', 'stack'),

    /*
    |--------------------------------------------------------------------------
    | Log Channels
    |--------------------------------------------------------------------------
    |
    | Here you may configure the log channels for your application. Out of
    | the box, Laravel uses the Monolog PHP logging library. This gives
    | you a variety of powerful log handlers / formatters to utilize.
    |
    | Available Drivers: "single", "daily", "slack", "syslog",
    |                    "errorlog", "monolog",
    |                    "custom", "stack"
    |
    */

    'channels' => [
        'stack' => [
            'driver' => 'stack',
            'channels' => ['single'],
            'ignore_exceptions' => false,
        ],

        'single' => [
            'driver' => 'single',
            'path' => storage_path('logs/laravel.log'),
            'level' => env('LOG_LEVEL', 'debug'),
        ],

        'daily' => [
            'driver' => 'daily',
            'path' => storage_path('logs/laravel.log'),
            'level' => 'debug',
            'days' => 7,
        ],

        'slack' => [
            'driver' => 'slack',
            'url' => env('LOG_SLACK_WEBHOOK_URL'),
            'username' => 'Laravel Log',
            'emoji' => ':boom:',
            'level' => env('LOG_LEVEL', 'critical'),
        ],

        'papertrail' => [
            'driver' => 'monolog',
            'level' => env('LOG_LEVEL', 'debug'),
            'handler' => SyslogUdpHandler::class,
            'handler_with' => [
                'host' => env('PAPERTRAIL_URL'),
                'port' => env('PAPERTRAIL_PORT'),
            ],
        ],

        'stderr' => [
            'driver' => 'monolog',
            'level' => env('LOG_LEVEL', 'debug'),
            'handler' => StreamHandler::class,
            'formatter' => env('LOG_STDERR_FORMATTER'),
            'with' => [
                'stream' => 'php://stderr',
            ],
        ],

        'syslog' => [
            'driver' => 'syslog',
            'level' => env('LOG_LEVEL', 'debug'),
        ],

        'errorlog' => [
            'driver' => 'errorlog',
            'level' => env('LOG_LEVEL', 'debug'),
        ],

        'null' => [
            'driver' => 'monolog',
            'handler' => NullHandler::class,
        ],

        'emergency' => [
            'path' => storage_path('logs/laravel.log'),
        ],

        'alerts' => [
            'driver' => 'daily',
            'path' => storage_path('logs/alerts.log'),
            'level' => 'debug',
            'days' => 7
        ],

        'invoices' => [
            'driver' => 'daily',
            'path' => storage_path('logs/invoices.log'),
            'level' => 'debug',
            'days' => 7
        ],

        'estimates' => [
            'driver' => 'daily',
            'path' => storage_path('logs/estimates.log'),
            'level' => 'debug',
            'days' => 7
        ],

        'tasks' => [
            'driver' => 'daily',
            'path' => storage_path('logs/tasks.log'),
            'level' => 'debug',
            'days' => 7
        ],

        'dashboard' => [
            'driver' => 'daily',
            'path' => storage_path('logs/dashboard.log'),
            'level' => 'debug',
            'days' => 7
        ],

        'inventory' => [
            'driver' => 'daily',
            'path' => storage_path('logs/inventory.log'),
            'level' => 'debug',
            'days' => 7
        ],
        'prospects' => [
            'driver' => 'daily',
            'path' => storage_path('logs/prospects.log'),
            'level' => 'debug',
            'days' => 7
        ],

        'companies' => [
            'driver' => 'daily',
            'path' => storage_path('logs/companies.log'),
            'level' => 'debug',
            'days' => 7
        ],

        'projects' => [
            'driver' => 'daily',
            'path' => storage_path('logs/projects.log'),
            'level' => 'debug',
            'days' => 7
        ],

        'subscriptions' => [
            'driver' => 'daily',
            'path' => storage_path('logs/subscriptions.log'),
            'level' => 'debug',
            'days' => 7
        ],
        'vehicles' => [
            'driver' => 'daily',
            'path' => storage_path('logs/vehicles.log'),
            'level' => 'debug',
            'days' => 7
        ],
        'users' => [
            'driver' => 'daily',
            'path' => storage_path('logs/users.log'),
            'level' => 'debug',
            'days' => 7
        ],
        'reports' => [
            'driver' => 'daily',
            'path' => storage_path('logs/reports.log'),
            'level' => 'debug',
            'days' => 7
        ],
        'employees' => [
            'driver' => 'daily',
            'path' => storage_path('logs/employees.log'),
            'level' => 'debug',
            'days' => 7
        ],
        'trips' => [
            'driver' => 'daily',
            'path' => storage_path('logs/trips.log'),
            'level' => 'debug',
            'days' => 7
        ],
        'schedules' => [
            'driver' => 'daily',
            'path' => storage_path('logs/schedules.log'),
            'level' => 'debug',
            'days' => 7
        ],
    ],

];
